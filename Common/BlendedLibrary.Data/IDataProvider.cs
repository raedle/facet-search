﻿using System;
using BlendedLibrary.Data.Configuration;

namespace BlendedLibrary.Data
{
    public interface IDataProvider : IDisposable
    {
        IDataProviderConfiguration DataProviderConfiguration { get; set; }

        string[] GetFacets(DataRepresentation representation);

        string GetDisplayNameOfFacet(string facet);

        IFacetElement[] GetValuesOfCategoricalFacet(string facetName);

        string GetDisplayNameOfCategoricalValue(string facet, string value);

        DataRepresentation GetDataRepresentation(string facet);

        MetricRange GetMetricRange(string facet);

        string GetCategoricalExpression(string facet, IFacetElement[] elements);

        string GetMetricRangeExpression(string facet, int minValue, int maxValue);

        string GetTextExpression(string[] facets, string value);

        /// <summary>
        /// Update the count property of the receiver, 
        /// if this is done asyncronously make sure to check that IDataReceiver.Query equals the original query, 
        /// before changeing the count.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="receiver"></param>
        void UpdateCount(string query, IDataReceiver receiver);

        int TotalResultCount { get; set; }

        void SaveCache();

        #region Data Visualization

        void VisualizeData(string query, byte targetId);

        void UnvisualizeData(byte targetId);

        #endregion
    }
}
