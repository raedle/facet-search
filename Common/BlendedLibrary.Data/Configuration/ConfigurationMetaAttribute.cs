﻿using System;

namespace BlendedLibrary.Data.Configuration
{
    public class ConfigurationMetaAttribute : Attribute
    {
        public readonly string FriendlyName;
        public readonly Type ControlType;

        public ConfigurationMetaAttribute(string friendlyName, Type controlType)
        {
            FriendlyName = friendlyName;
            ControlType = controlType;
        }
    }
}
