﻿namespace BlendedLibrary.Data.Configuration
{
    public interface IDataProviderConfiguration
    {
        #region Type

        string Type { get; }

        #endregion
    }
}
