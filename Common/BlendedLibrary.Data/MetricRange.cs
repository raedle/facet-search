﻿namespace BlendedLibrary.Data
{
    public class MetricRange
    {
        public MetricRange(double min, double max)
        {
            MinValue = min;
            MaxValue = max;
        }

        public double MinValue { get; set; }
        public double MaxValue { get; set; }
    }
}
