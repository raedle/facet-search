﻿using System.Reflection;
using System.Runtime.InteropServices;
// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
using System.Windows.Markup;

[assembly: AssemblyTitle("BlendedLibrary.Data")]
[assembly: AssemblyDescription("Blended Library Data")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("University of Konstanz")]
[assembly: AssemblyProduct("BlendedLibrary.Data")]
[assembly: AssemblyCopyright("Copyright © University of Konstanz 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: XmlnsDefinition("http://facetsearch.blendedlibrary.org/schemas/2013", "BlendedLibrary.FacetSearch.Data")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("7f44bdaa-2d05-4faf-81a4-6f0de64d765e")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]