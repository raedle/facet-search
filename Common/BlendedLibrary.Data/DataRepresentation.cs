﻿using System;

namespace BlendedLibrary.Data
{
    [Flags]
    public enum DataRepresentation
    {
        Textual = 1,
        Categorical = 2,
        Metric = 4
    }
}
