﻿namespace BlendedLibrary.Data
{
    public interface IFacetElement
    {
        #region properties

        string Name { get; set; }

        string Screen { get; set; }

        int Count { get; set; }

        bool IsMultiValue { get; set; }

        #endregion
    }
}
