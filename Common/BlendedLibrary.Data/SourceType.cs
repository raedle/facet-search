﻿using System;

namespace BlendedLibrary.Data
{
    public enum SourceType
    {
        [DatabaseName((""))]
        None,

        [DatabaseName("")]
        Debug,

        [DatabaseName("rds-dummy")]
        Rds,
    }

    public static class DataTypeExtionsions
    {
        public static string DatabaseName(this SourceType evt)
        {
            // Get the type
            var type = evt.GetType();

            // Get fieldinfo for this type
            var fieldInfo = type.GetField(evt.ToString());

            // Get the stringvalue attributes
            var attributes = fieldInfo.GetCustomAttributes(typeof(DatabaseNameAttribute), false) as DatabaseNameAttribute[];

            // Return the first if there was a match.
            return attributes != null && attributes.Length > 0 ? attributes[0].Name : null;
        }
    }

    internal class DatabaseNameAttribute : Attribute
    {
        public readonly string Name;

        public DatabaseNameAttribute(string name)
        {
            Name = name;
        }
    }
}
