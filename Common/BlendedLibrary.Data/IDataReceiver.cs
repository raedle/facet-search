﻿namespace BlendedLibrary.Data
{
    public interface IDataReceiver
    {
        long Id { get; }

        string Query { get; }
        int QueryCount { set; }
        bool IsFetchingQueryCount { set; }
    }
}
