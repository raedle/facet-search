﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace BlendedLibrary.Data.Domain
{
    /// <summary>
    /// <Medium mv_id="1" bib_id="010935088">
    ///   <Institution>Universita╠êt [Kaiserslautern] / Arbeitsgruppe Technomathematik</Institution>
    ///   <Title>Berichte der Arbeitsgruppe Technomathematik</Title>
    ///   <Subtitle>Forschung, Ausbildung, Weiterbildung</Subtitle>
    ///   <Description>Universita╠êt Kaiserslautern, Fachbereich Mathematik</Description>
    ///   <Town>Kaiserslautern</Town>
    ///   <Publisher>Univ.</Publisher>
    ///   <Detail>Bde teilw. als Computerdatei im Fernzugriff mit dem Gesamttitel: AGTM Report. - Bd. 213 nur mit dem Gesamttitel: Forschung, Ausbildung, Weiterbildung<Detail>
    /// </Medium>
    /// 
    /// <Medium mv_id="462797" bib_id="00385826">
    ///   <Person>Goffman, Erving</Person>
    ///   <Title>Where the action is</Title>
    ///   <Subtitle>3 essays</Subtitle>
    ///   <Description>Erving Goffman</Description>
    ///   <Town>London</Town>
    ///   <Publisher>Penguin Pr.</Publisher>
    ///   <Year>1969</Year>
    ///   <Format>206 S.</Format>
    ///   <Note>720 ff. bvb</Note>
    ///   <Signature>soz 255/g63d</Signature>
    /// </Medium>
    /// </summary>
    [XmlType("medium")]
    public class Medium
    {
        [XmlAttribute("id")]
        public int ID
        {
            get;
            set;
        }

        [XmlAttribute("medium_id")]
        public string LibraryId
        {
            get;
            set;
        }

        [XmlElement("author")]
        public List<string> Authors
        {
            get;
            set;
        }

        [XmlElement("institution")]
        public string Institution
        {
            get;
            set;
        }

        [XmlElement("title")]
        public string Title
        {
            get;
            set;
        }

        [XmlElement("subtitle")]
        public string Subtitle
        {
            get;
            set;
        }

        [XmlElement("mediatype")]
        public string Type
        {
            get;
            set;
        }

        [XmlElement("description")]
        public string Description
        {
            get;
            set;
        }

        [XmlElement("language")]
        public string Language
        {
            get;
            set;
        }

        [XmlElement("category")]
        public string Category
        {
            get;
            set;
        }

        [XmlElement("keywords")]
        public List<string> Keywords
        {
            get;
            set;
        }

        [XmlElement("town")]
        public string Town
        {
            get;
            set;
        }

        [XmlElement("publisher")]
        public string Publisher
        {
            get;
            set;
        }

        [XmlElement("year")]
        public int Year
        {
            get;
            set;
        }

        [XmlElement("format")]
        public string Format
        {
            get;
            set;
        }

        [XmlElement("note")]
        public string Note
        {
            get;
            set;
        }

        [XmlElement("signature")]
        public string Signature
        {
            get;
            set;
        }

        [XmlElement("detail")]
        public string Detail
        {
            get;
            set;
        }

        [XmlElement("parallel")]
        public string ParallelId
        {
            get;
            set;
        }

        [XmlElement("url")]
        public string Url
        {
            get;
            set;
        }

        [XmlElement("isbn")]
        public List<string> ISBNs
        {
            get;
            set;
        }

        [XmlElement("issn")]
        public string ISSN
        {
            get;
            set;
        }

        private string _imageUrl;

        [XmlElement("cover_url")]
        public string ImageUrl
        {
            get
            {
                if (_imageUrl != null)
                    return _imageUrl;

                var id = ISBNs.Count > 0 ? ISBNs.First() : "0";
                return string.Format("http://localhost:8009/ImageService/CoverImage?isbn={0}&type={1}", id, Type);
            }
            set { _imageUrl = value; }
        }

        public override string ToString()
        {
            return "Medium[Id=" + ID + ",LibraryId=" + LibraryId + ",ParallelId=" + ParallelId + ",Title=" + Title + ",Subtitle=" + Subtitle + ",Authors=" + Authors + ",Institution=" + Institution + ",Year=" + Year + ",Format=" + Format + ",Note=" + Note + ",Signature=" + Signature + ",Url=" + Url + ",ISBN=" + ISBNs + ",ISSN=" + ISSN + ",Description=" + Description + ",Town=" + Town + ",Publisher=" + Publisher + ",Category=" + Category + ",Detail=" + Detail + ",Keywords=" + Keywords + ",Language=" + Language + ",ImageUrl=" + ImageUrl + "]";
        }
    }
}
