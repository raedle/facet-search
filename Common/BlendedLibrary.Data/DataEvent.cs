﻿using System;

namespace BlendedLibrary.Data
{
    public class EventHelper
    {
        public const string DummyQuery = "1 to 2";

        public class Event
        {
            public const string ShowResults = "ShowResults";
            public const string RemoveResults = "RemoveResults";
            public const string ControlMessage = "ControlMessage";
        }

        public class Message
        {
            public const string KeepAlive = "KeepAlive";
            public const string HyperGridConnected = "HyperGridConnected";
        }
    }
}
