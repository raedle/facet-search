﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace BlendedLibrary.Common.Util
{
    public class FractionToBooleanConverter : DependencyObject, IValueConverter
    {
        #region Total

        /// <summary>
        /// Total Dependency Property
        /// </summary>
        public static readonly DependencyProperty TotalProperty =
            DependencyProperty.Register("Total", typeof(int), typeof(FractionToBooleanConverter),
                new FrameworkPropertyMetadata((int)0,
                    new PropertyChangedCallback(OnTotalChanged)));

        /// <summary>
        /// Gets or sets the Total property. This dependency property 
        /// indicates ....
        /// </summary>
        public int Total
        {
            get { return (int)GetValue(TotalProperty); }
            set { SetValue(TotalProperty, value); }
        }

        /// <summary>
        /// Handles changes to the Total property.
        /// </summary>
        private static void OnTotalChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FractionToBooleanConverter target = (FractionToBooleanConverter)d;
            int oldTotal = (int)e.OldValue;
            int newTotal = target.Total;
            target.OnTotalChanged(oldTotal, newTotal);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Total property.
        /// </summary>
        protected virtual void OnTotalChanged(int oldTotal, int newTotal)
        {
        }

        #endregion



        #region MinPercentage

        /// <summary>
        /// MinPercentage Dependency Property
        /// </summary>
        public static readonly DependencyProperty MinPercentageProperty =
            DependencyProperty.Register("MinPercentage", typeof(int), typeof(FractionToBooleanConverter),
                new FrameworkPropertyMetadata((int)0));

        /// <summary>
        /// Gets or sets the MinPercentage property. This dependency property 
        /// indicates ....
        /// </summary>
        public int MinPercentage
        {
            get { return (int)GetValue(MinPercentageProperty); }
            set { SetValue(MinPercentageProperty, value); }
        }

        #endregion

        #region MaxPercentage

        /// <summary>
        /// MaxPercentage Dependency Property
        /// </summary>
        public static readonly DependencyProperty MaxPercentageProperty =
            DependencyProperty.Register("MaxPercentage", typeof(int), typeof(FractionToBooleanConverter),
                new FrameworkPropertyMetadata((int)0));

        /// <summary>
        /// Gets or sets the MaxPercentage property. This dependency property 
        /// indicates ....
        /// </summary>
        public int MaxPercentage
        {
            get { return (int)GetValue(MaxPercentageProperty); }
            set { SetValue(MaxPercentageProperty, value); }
        }

        #endregion

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = (int)value;

            if (val == 0)
                return false;

            var lowerVal = MinPercentage/100.0*Total;

            if (val >= lowerVal)
                return true;
            
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
