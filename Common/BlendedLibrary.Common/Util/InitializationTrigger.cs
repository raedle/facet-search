﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlendedLibrary.Common.Util
{
    public class InitializationTrigger
    {

        private readonly List<String> _triggerNames;
        private readonly Action _triggerAction;

        public InitializationTrigger(Action triggerAction, params String[] triggerNames)
        {
            _triggerAction = triggerAction;
            _triggerNames = triggerNames.ToList();
        }

        public void SetTrigger(String triggerName)
        {
            _triggerNames.Remove(triggerName);
            if (_triggerNames.Count == 0)
            {
                _triggerAction();
                HasFired = true;
            }

        }

        public bool HasFired { get; private set; }
    }
}
