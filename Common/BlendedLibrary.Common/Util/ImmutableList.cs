﻿using System;

namespace BlendedLibrary.Common.Util
{
    internal class ImmutableList<T>
    {
        // Fields
        private T[] data;

        // Methods
        public ImmutableList()
        {
            this.data = new T[0];
        }

        private ImmutableList(T[] data)
        {
            this.data = data;
        }

        public ImmutableList<T> Add(T value)
        {
            T[] destinationArray = new T[this.data.Length + 1];
            Array.Copy(this.data, destinationArray, this.data.Length);
            destinationArray[this.data.Length] = value;
            return new ImmutableList<T>(destinationArray);
        }

        public bool Contains(T value)
        {
            return (this.IndexOf(value) >= 0);
        }

        public int IndexOf(T value)
        {
            for (int i = 0; i < this.data.Length; i++)
            {
                if (this.data[i].Equals(value))
                {
                    return i;
                }
            }
            return -1;
        }

        public ImmutableList<T> Remove(T value)
        {
            int index = this.IndexOf(value);
            if (index < 0)
            {
                return (ImmutableList<T>)this;
            }
            T[] destinationArray = new T[this.data.Length - 1];
            Array.Copy(this.data, 0, destinationArray, 0, index);
            Array.Copy(this.data, index + 1, destinationArray, index, (this.data.Length - index) - 1);
            return new ImmutableList<T>(destinationArray);
        }

        // Properties
        public int Count
        {
            get
            {
                return this.data.Length;
            }
        }

        public T[] Data
        {
            get
            {
                return this.data;
            }
        }

        public T this[int index]
        {
            get
            {
                return this.data[index];
            }
        }
    }


}
