﻿namespace BlendedLibrary.Common.Util
{
    public static class BaseXUtils
    {
        public static string EscapeQuery(string query)
        {
            return query.Replace("&", "&amp;");
        }

        public static string EscapeTriggerPayload(string payload)
        {
            return payload.Replace("&", "&amp;").Replace("'", "&apos;");
        }
    }
}
