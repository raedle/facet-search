﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace BlendedLibrary.Common.Util
{
    [ValueConversion(typeof (double), typeof (double))]
    internal class DoubleMultiplierConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            double result = 0;

            foreach (object obj in value)
            {
                if (obj == DependencyProperty.UnsetValue) continue;

                if (obj is String)
                {
                    result *= Double.Parse((string) obj);
                }
                else
                {
                    result *= (double) obj;
                }
            }

            return result;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
