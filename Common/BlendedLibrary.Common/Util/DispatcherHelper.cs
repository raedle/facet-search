﻿using System;
using System.Security;
using System.Security.Permissions;
using System.Windows.Threading;

namespace BlendedLibrary.Common.Util
{
    public static class DispatcherHelper
    {
        /// <summary>
        /// This method allows developer to call a non-blocking wait. When calling WaitForPriority, the developer is certain
        /// that all dispatcher operations with priority higher than the one passed as a parameter will have been executed
        /// by the time the line of code that follows it is reached.
        /// Similar to VB's DoEvents.
        /// Keep in mind that this call does not guarantee that all operations at the priority passed will have been executed,
        /// only operations with priority higher than the one passed. In practice it waits for some (and sometimes all) 
        /// operations at the priority passed, but this is not guaranteed.
        /// </summary>
        /// <param name="priority">Priority below the one we want to wait for before the next line of code is executed.</param>
        public static void WaitForPriority(DispatcherPriority priority)
        {
            DispatcherFrame frame = new DispatcherFrame();
            DispatcherOperation dispatcherOperation = Dispatcher.CurrentDispatcher.BeginInvoke(priority, new DispatcherOperationCallback(ExitFrameOperation), frame);
            Dispatcher.PushFrame(frame);
            if (dispatcherOperation.Status != DispatcherOperationStatus.Completed)
            {
                dispatcherOperation.Abort();
            }
        }

        private static object ExitFrameOperation(object obj)
        {
            ((DispatcherFrame)obj).Continue = false;
            return null;
        }

        public static void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new ExitFrameHandler(frm => frm.Continue = false), frame);
            Dispatcher.PushFrame(frame);
        }

        private delegate void ExitFrameHandler(DispatcherFrame frame);

        private static DispatcherOperationCallback exitFrameCallback = new DispatcherOperationCallback(ExitFrame);

        /// <summary>
        /// Processes all UI messages currently in the message queue.
        /// </summary>
        public static void DoEvents2()
        {
            // Create new nested message pump.
            DispatcherFrame nestedFrame = new DispatcherFrame();

            // Dispatch a callback to the current message queue, when getting called,
            // this callback will end the nested message loop.
            // note that the priority of this callback should be lower than the that of UI event messages.
            DispatcherOperation exitOperation = Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, exitFrameCallback, nestedFrame);

            // pump the nested message loop, the nested message loop will
            // immediately process the messages left inside the message queue.
            Dispatcher.PushFrame(nestedFrame);

            // If the "exitFrame" callback doesn't get finished, Abort it.
            if (exitOperation.Status != DispatcherOperationStatus.Completed)
            {
                exitOperation.Abort();
            }
        }

        private static Object ExitFrame(Object state)
        {
            DispatcherFrame frame = state as DispatcherFrame;
            // Exit the nested message loop.
            frame.Continue = false;
            return null;
        }

        #region Public Methods

        /// <summary>
        /// This method will wait until all pending DispatcherOperations of a
        /// priority higher than the specified priority have been processed.
        /// </summary>
        /// <param name="priority">The priority to wait for before continuing.</param>
        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Assert, Name = "FullTrust")]
        public static void WaitFor(DispatcherPriority priority)
        {
            PermissionSet permissions = new PermissionSet(PermissionState.Unrestricted);
            permissions.Demand();
            WaitFor(TimeSpan.Zero, priority);
        }

        /// <summary>
        /// This method will wait for the specified TimeSpan, allowing pending
        /// DispatcherOperations (such as UI rendering) to continue during that
        /// time. This method should be used with caution. Waiting for time is 
        /// generally discouraged, because it may have an adverse effect on the 
        /// overall run time of a test suite when the test suite has a large 
        /// number of tests.
        /// </summary>
        /// <param name="time">Amount of time to wait.</param>
        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Assert, Name = "FullTrust")]
        public static void WaitFor(TimeSpan time)
        {
            PermissionSet permissions = new PermissionSet(PermissionState.Unrestricted);
            permissions.Demand();
            WaitFor(time, DispatcherPriority.SystemIdle);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// This effectively enables a caller to do all pending UI work before continuing.
        /// The method will block until the desired priority has been reached, emptying the
        /// Dispatcher queue of all items with a higher priority.
        /// </summary>
        /// <param name="time">Amount of time to wait.</param>
        /// <param name="priority">The priority to wait for before continuing.</param>
        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Assert, Name = "FullTrust")]
        private static void WaitFor(TimeSpan time, DispatcherPriority priority)
        {
            PermissionSet permissions = new PermissionSet(PermissionState.Unrestricted);
            permissions.Demand();

            // Create a timer for the minimum wait time.
            // When the time passes, the Tick handler will be called,
            // which allows us to stop the dispatcher frame.
            DispatcherTimer timer = new DispatcherTimer(priority);
            timer.Tick += new EventHandler(OnDispatched);
            timer.Interval = time;

            // Run a dispatcher frame.
            DispatcherFrame dispatcherFrame = new DispatcherFrame(false);
            timer.Tag = dispatcherFrame;
            timer.Start();
            Dispatcher.PushFrame(dispatcherFrame);
        }

        /// <summary>
        /// Dummy SystemIdle dispatcher item.  This discontinues the current
        /// dispatcher frame so control can return to the caller of WaitFor().
        /// </summary>
        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Assert, Name = "FullTrust")]
        private static void OnDispatched(object sender, EventArgs args)
        {
            // Stop the timer now.
            DispatcherTimer timer = (DispatcherTimer)sender;
            timer.Tick -= new EventHandler(OnDispatched);
            timer.Stop();
            DispatcherFrame frame = (DispatcherFrame)timer.Tag;
            frame.Continue = false;
        }

        #endregion

    }
}
