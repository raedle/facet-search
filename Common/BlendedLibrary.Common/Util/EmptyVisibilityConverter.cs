﻿using System;
using System.Windows;
using System.Windows.Data;

namespace BlendedLibrary.Common.Util
{
    public class EmptyVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return Visibility.Collapsed;

            var text = value as string;
            if (text == null)
                text = value.ToString();

            if (text.Equals("") || text.Equals("0"))
                return Visibility.Collapsed;
            
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}