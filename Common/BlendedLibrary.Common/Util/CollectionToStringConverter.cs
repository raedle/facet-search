﻿using System;
using System.Collections;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace BlendedLibrary.Common.Util
{
    [ValueConversion(typeof(ICollection), typeof(Visibility))]
    public class CollectionToStringConverter : ConverterMarkupExtension<CollectionToStringConverter>, IValueConverter
    {
        #region ctor

        // ReSharper disable once EmptyConstructor
        public CollectionToStringConverter()
        {
            
        }

        #endregion

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var collection = value as ICollection;

            if (collection == null || collection.Count == 0) return null;

            var result = new StringBuilder();
            foreach (var obj in collection)
            {
                result.AppendFormat("{0}, ", obj);
            }

            result.Remove(result.Length - 2, 2);

            return result.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
