﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace BlendedLibrary.Common.Util
{
    public class IntegerToBrushConverter : ConverterMarkupExtension<IntegerToBrushConverter>, IValueConverter
    {
        public Brush Brush1 { get; set; }
        public Brush Brush2 { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var number = (int)value;
            return (number > 0) ? Brush1 : Brush2;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
