﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace BlendedLibrary.Common.Util
{
    [ValueConversion(typeof(ICollection), typeof(Visibility))]
    public class EmptyCollectionToVisibilityConverter : ConverterMarkupExtension<EmptyCollectionToVisibilityConverter>, IValueConverter
    {
        #region ctor

        // ReSharper disable once EmptyConstructor
        public EmptyCollectionToVisibilityConverter()
        {
            
        }

        #endregion

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var collection = value as ICollection;

            if (collection == null || collection.Count <= 0)
                return Visibility.Collapsed;

            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
