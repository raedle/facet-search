﻿namespace BlendedLibrary.Common.Util
{
    public static class AngleHelper
    {
        public static double NormalizeAngle(double angle)
        {
            angle = angle % 360.0;

            if (angle < 0)
                angle = 360.0 + angle;

            return angle;
        }

        public static bool IsAngleBetween(double leftAngle, double rightAngle, double angleBetween)
        {
            if (leftAngle < rightAngle)
            {
                return leftAngle < angleBetween && angleBetween < rightAngle;
            }

            if (leftAngle > rightAngle)
            {
                double diff = 360.0 - leftAngle;
                leftAngle = NormalizeAngle(leftAngle + diff);
                rightAngle += diff;
                angleBetween = NormalizeAngle(angleBetween + diff);
                return IsAngleBetween(leftAngle, rightAngle, angleBetween);
            }

            return false;
        }
    }
}
