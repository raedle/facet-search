﻿using System.Windows.Input;
using Microsoft.Surface.Presentation.Input;

namespace BlendedLibrary.Common.Util
{
    public static class ContactExtensions
    {
        //#region keys for contact store

        //private static readonly object _segmentUnderContactKey = new Object();
        //private static readonly object _contactDownTimestampKey = new Object();
        //private static readonly object _initialSegmentKey = new Object();

        //#endregion

        //returns a typed user data object
        public static T GetUserData<T>(this TouchDevice c, object key)
        {
            return (T)TouchExtensions.GetUserData(c, key);
        }

        //public static void SetSegmentUnderContact(this Contact c, CircleSegment segment)
        //{
        //    c.SetUserData(_segmentUnderContactKey, segment);
        //}

        //public static CircleSegment GetSegmentUnderContact(this Contact c)
        //{
        //    return c.GetUserData<CircleSegment>(_segmentUnderContactKey);
        //}

        //public static void SetInitialSegment(this Contact c, CircleSegment segment)
        //{
        //    c.SetUserData(_initialSegmentKey, segment);
        //}

        //public static CircleSegment GetInitialSegment(this Contact c)
        //{
        //    return c.GetUserData<CircleSegment>(_initialSegmentKey);
        //}

        //public static void SetContactDownTimestamp(this Contact c)
        //{
        //    c.SetUserData(_contactDownTimestampKey, c.FrameTimestamp);
        //}

        //public static long GetContactDownTimestamp(this Contact c)
        //{
        //    return c.GetUserData<long>(_contactDownTimestampKey);
        //}
    }
}
