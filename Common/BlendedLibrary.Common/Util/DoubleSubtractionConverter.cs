﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace BlendedLibrary.Common.Util
{
    [ValueConversion(typeof(double), typeof(double))]
    public class DoubleSubtractionConverter : IMultiValueConverter
    {
        /**
         * Takes two double values and subtracts the second from the first.
         * If one of the values is invalid, 0 is returned.
         **/
        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value[0] == DependencyProperty.UnsetValue || value[1] == DependencyProperty.UnsetValue) return 0;

            if (value[0] is String) value[0] = Double.Parse((string)value[0]);
            if (value[1] is String) value[1] = Double.Parse((string)value[1]);

            return (double) value[0] - (double) value[1];
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
