﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace BlendedLibrary.Common.Util
{
    [ValueConversion(typeof(bool), typeof(bool))]
    public class InvertBoolConverter : ConverterMarkupExtension<InvertBoolConverter>, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var b = (bool)value;

            return !b;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
