﻿using System;
using System.Windows;
using System.Windows.Data;

namespace BlendedLibrary.Common.Util
{
    [ValueConversion(typeof(TimeSpan), typeof(Duration))]
    public class TimeSpanToDurationConverter : ConverterMarkupExtension<TimeSpanToDurationConverter>, IValueConverter
    {
        public TimeSpanToDurationConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var timeSpan = (TimeSpan)value;

            var duration = new Duration(timeSpan);

            return duration;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}