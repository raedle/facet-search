﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace BlendedLibrary.Common.Util
{
    public class StringToBooleanConverter : ConverterMarkupExtension<StringToBooleanConverter>, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = (String)value;

            return !String.IsNullOrEmpty(s);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
