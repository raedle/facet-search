﻿using System.Text;

namespace BlendedLibrary.Common.Util
{
    public static class SolrQueryUtility
    {
        public static string Escape(string s)
        {
            if (s == null)
                return null;

            var result = new StringBuilder(s.Length);

            foreach (var ch in s)
            {
                switch (ch)
                {
                    case '+':
                    case '-':
                    case '!':
                    case '(':
                    case ')':
                    case '{':
                    case '}':
                    case '[':
                    case ']':
                    case '^':
                    case '"':
                    case '~':
                    case '*':
                    case '?':
                    case ':':
                    case '\\':
                        result.Append("\\");
                        break;
                }

                result.Append(ch);
            }

            result.Replace("&&", "\\&&");
            result.Replace("||", "\\||");

            return result.ToString();
        }
    }
}
