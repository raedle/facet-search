﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Input;
using Microsoft.Surface;
using Tools.WpfEventBridge.InteractionDevices;
using SurfaceInput = Microsoft.Surface.Presentation.Input;

namespace BlendedLibrary.Common
{
    public static class InputExtensions
    {
        public static bool IsFingerRecognized(this TouchDevice inputDevice)
        {
            if (!SurfaceEnvironment.IsSurfaceEnvironmentAvailable &&
                SurfaceEnvironment.IsSurfaceInputAvailable &&
                !(inputDevice is TagDevice) && 
                !(inputDevice is PenDevice))
                return true;

            return SurfaceInput.TouchExtensions.GetIsFingerRecognized(inputDevice) || inputDevice is FingerDevice;
        }

        public static bool IsTagRecognized(this TouchDevice inputDevice)
        {
            return SurfaceInput.TouchExtensions.GetIsTagRecognized(inputDevice) || inputDevice is TagDevice;
        }

        public static bool GetIsStylusRecognized(this StylusDevice inputDevice)
        {
            return inputDevice.StylusButtons.Count > 1;
        }

        public static bool Capture(this InputDevice inputDevice, IInputElement element, CaptureMode captureMode)
        {
            return SurfaceInput.TouchExtensions.Capture(inputDevice, element, captureMode);
        }

        public static IManipulator AsManipulator(this InputDevice inputDevice)
        {
            return SurfaceInput.TouchExtensions.AsManipulator(inputDevice);
        }

        public static TagData GetTagData(this TouchDevice inputDevice)
        {
            if (inputDevice is TagDevice)
                return new TagData((inputDevice as TagDevice).Value);
            else
            {
                var oldTagData = SurfaceInput.TouchExtensions.GetTagData(inputDevice);
                return new TagData(oldTagData.Schema, oldTagData.Series, oldTagData.ExtendedValue,
                    oldTagData.Value);
            }
        }

        public static Point GetCenterPosition(this TouchDevice inputDevice, IInputElement relativeTo)
        {
            return SurfaceInput.TouchExtensions.GetCenterPosition(inputDevice, relativeTo);
        }

        public static double GetOrientation(this TouchDevice inputDevice, IInputElement relativeTo)
        {
            if (inputDevice is TagDevice)
                return (inputDevice as TagDevice).Angle;

            return SurfaceInput.TouchExtensions.GetOrientation(inputDevice, relativeTo);
        }

        public static object GetUserData(this TouchDevice inputDevice, object key)
        {
            return SurfaceInput.TouchExtensions.GetUserData(inputDevice, key);
        }

        public static void SetUserData(this TouchDevice inputDevice, object key, object value)
        {
            SurfaceInput.TouchExtensions.SetUserData(inputDevice, key, value);
        }

        public static Point GetPosition(this TouchDevice inputDevice, IInputElement relativeTo)
        {
            return SurfaceInput.TouchExtensions.GetPosition(inputDevice, relativeTo);
        }
    }

    /// <summary>
    /// Represents the identifying data that is associated with a tagged object.
    /// </summary>
    public struct TagData
    {
        /// <summary>
        /// An instance of a <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> object that represents no tag.
        /// </summary>
        public static readonly TagData None = new TagData(true);
        private readonly int schema;
        private readonly long series;
        private readonly long extendedValue;
        private readonly long value;
        private readonly bool isNone;

        /// <summary>
        /// Gets the schema identifier of a tag.
        /// </summary>
        /// 
        /// <returns>
        /// Returns <see cref="T:System.Int32"/>.
        /// </returns>
        public int Schema
        {
            get
            {
                return this.schema;
            }
        }

        /// <summary>
        /// Gets the series identifier of a tag.
        /// </summary>
        /// 
        /// <returns>
        /// Returns <see cref="T:System.Int64"/>.
        /// </returns>
        public long Series
        {
            get
            {
                return this.series;
            }
        }

        /// <summary>
        /// Gets the value of a tag.
        /// </summary>
        /// 
        /// <returns>
        /// Returns <see cref="T:System.Int64"/>.
        /// </returns>
        public long Value
        {
            get
            {
                return this.value;
            }
        }

        /// <summary>
        /// Gets the extended value of a tag. This property is reserved for future use.
        /// </summary>
        /// 
        /// <returns>
        /// Returns <see cref="T:System.Int64"/>.
        /// </returns>
        public long ExtendedValue
        {
            get
            {
                return this.extendedValue;
            }
        }

        static TagData()
        {
        }

        private TagData(bool none)
        {
            this.schema = 0;
            this.series = 0L;
            this.extendedValue = 0L;
            this.value = 0L;
            this.isNone = none;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structure using the specified data.
        /// </summary>
        /// <param name="schema">The schema identifier of a tag (<see cref="P:Microsoft.Surface.Presentation.Input.TagData.Schema"/>).</param><param name="extendedValue">The extended value of a tag (<see cref="P:Microsoft.Surface.Presentation.Input.TagData.ExtendedValue"/>). This property is reserved for future use.</param><param name="series">The series identifier of a tag (<see cref="P:Microsoft.Surface.Presentation.Input.TagData.Series"/>).</param><param name="value">The identifying value of a tag (<see cref="P:Microsoft.Surface.Presentation.Input.TagData.Value"/>). This represents the actual identifying value of the tag.</param>
        public TagData(int schema, long series, long extendedValue, long value)
        {
            this.schema = schema;
            this.series = series;
            this.extendedValue = extendedValue;
            this.value = value;
            this.isNone = false;
        }

        internal TagData(long value)
        {
            this = new TagData(0, 0L, 0L, value);
        }

        internal TagData(long series, long value)
        {
            this = new TagData(0, series, 0L, value);
        }

        /// <summary>
        /// Determines whether two specified <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structures are different.
        /// </summary>
        /// 
        /// <returns>
        /// Returns true if the two <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structures are different; otherwise, returns false.
        /// </returns>
        /// <param name="right">The second <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structure to use in the comparison.</param><param name="left">The first <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structure to use in the comparison.</param>
        public static bool operator !=(TagData left, TagData right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Determines whether two specified <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structures are the same.
        /// </summary>
        /// 
        /// <returns>
        /// Returns true if the two <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structures are the same; otherwise, returns false.
        /// </returns>
        /// <param name="right">The second <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structure to use in the comparison.</param><param name="left">The first <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structure to use in the comparison.</param>
        public static bool operator ==(TagData left, TagData right)
        {
            if (left.isNone == right.isNone && left.schema == right.schema && (left.series == right.series && left.extendedValue == right.extendedValue))
                return left.value == right.value;
            else
                return false;
        }

        /// <summary>
        /// Determines if this <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structure is equal to another specified <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structure.
        /// </summary>
        /// 
        /// <returns>
        /// Returns true if the two <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structures are identical; otherwise, returns false.
        /// </returns>
        /// <param name="obj">The <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structure to compare to this object.</param>
        public override bool Equals(object obj)
        {
            if (obj is TagData)
                return (TagData)obj == this;
            else
                return false;
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a 32-bit signed integer hash code for this instance.
        /// </returns>
        public override int GetHashCode()
        {
            if (!this.isNone)
                return this.schema ^ ((int)this.series ^ (int)(this.series >> 32)) ^ ((int)this.extendedValue ^ (int)(this.extendedValue >> 32)) ^ ((int)this.value ^ (int)(this.value >> 32));
            else
                return 1;
        }

        /// <summary>
        /// Converts the <see cref="T:Microsoft.Surface.Presentation.Input.TagData"/> structure to its equivalent string representation.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a string representation of the structure. The string is displayed in the following format:Tag (Schema=0x&lt;value&gt;; Series=0x&lt;value&gt;; ExtendedValue=0x&lt;value&gt;; Value=0x&lt;value&gt;)
        /// </returns>
        public override string ToString()
        {
            if (this == TagData.None)
                return "None";
            return string.Format((IFormatProvider)CultureInfo.InvariantCulture, "Tag (Schema=0x{0:x8}; Series=0x{1:x16}; ExtendedValue=0x{2:x16}; Value=0x{3:x16})", (object)this.schema, (object)this.series, (object)this.extendedValue, (object)this.value);
        }
    }
}
