﻿using System;
using System.Windows.Media;

namespace BlendedLibrary.Common
{
    public static class FrameRenderer
    {
        private const int Fps = 60;
        private const double FpsThreshold = 999.0/Fps;
        private static TimeSpan _lastRenderingTime = TimeSpan.Zero;

        private static event EventHandler<RenderingEventArgs> InternalFrameUpdating;

        public static event EventHandler<RenderingEventArgs> FrameUpdating
        {
            add
            {
                if (InternalFrameUpdating == null)
                    CompositionTarget.Rendering += CompositionTargetRendering;

                InternalFrameUpdating += value;
            }
            remove
            {
                InternalFrameUpdating -= value;

                if (InternalFrameUpdating == null)
                    CompositionTarget.Rendering -= CompositionTargetRendering;
            }
        }

        static void CompositionTargetRendering(object sender, EventArgs e)
        {
            var args = (RenderingEventArgs)e;

            var currentRenderingTime = args.RenderingTime;

            if (currentRenderingTime == _lastRenderingTime) return;

            // Do not call renderer if rendering time is faster than 60 fps.
            if ((currentRenderingTime - _lastRenderingTime).TotalMilliseconds < FpsThreshold) return;

            _lastRenderingTime = args.RenderingTime;

            if (InternalFrameUpdating != null)
                InternalFrameUpdating(sender, args);
        }
    }
}
