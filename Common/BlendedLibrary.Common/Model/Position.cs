﻿using System.ComponentModel;
using System.Windows;

namespace BlendedLibrary.Common.Model
{
    public class Position : INotifyPropertyChanged
    {
        #region public properties

        #region X

        /// <summary>
        /// The <see cref="X" /> property's name.
        /// </summary>
        public const string XPropertyName = "X";

        private double _x = 0.0;

        /// <summary>
        /// Sets and gets the X property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double X
        {
            get
            {
                return _x;
            }

            set
            {
                if (_x == value)
                {
                    return;
                }

                _x = value;
                RaisePropertyChanged(XPropertyName);
            }
        }

        #endregion

        #region Y

        /// <summary>
        /// The <see cref="Y" /> property's name.
        /// </summary>
        public const string YPropertyName = "Y";

        private double _y = 0.0;

        /// <summary>
        /// Sets and gets the Y property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double Y
        {
            get
            {
                return _y;
            }

            set
            {
                if (_y == value)
                {
                    return;
                }

                _y = value;
                RaisePropertyChanged(YPropertyName);
            }
        }

        #endregion

        #endregion

        public Position(double x, double y)
        {
            X = x;
            Y = y;
        }

        public static Position operator -(Position position, Vector vector)
        {
            return new Position(position.X - vector.X, position.Y - vector.Y);
        }

        public static Vector operator -(Position position1, Position position2)
        {
            return new Vector(position2.X - position1.X, position2.Y - position1.Y);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion

    }
}
