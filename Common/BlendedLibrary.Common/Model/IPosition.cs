﻿namespace BlendedLibrary.Common.Model
{
    public interface IPosition
    {
        Position TopLeft { get; }

        Position Center { get; }

        void UpdateTopLeftPosition(double x, double y);

        void UpdateCenterPosition(double x, double y);
    }
}
