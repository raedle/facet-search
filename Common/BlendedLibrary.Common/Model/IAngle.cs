﻿
namespace BlendedLibrary.Common.Model
{
    public interface IAngle
    {
        double Angle { get; set; }
    }
}
