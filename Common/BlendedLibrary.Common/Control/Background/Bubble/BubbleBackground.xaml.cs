﻿using System.Windows.Controls;

namespace BlendedLibrary.Common.Control
{
    /// <summary>
    /// Interaction logic for BlubberBackground.xaml
    /// </summary>
    public partial class BlubberBackground : UserControl
    {
        public BlubberBackground()
        {
            InitializeComponent();

            FrameRenderer.FrameUpdating += FrameRenderer_FrameUpdating;
        }

        static void FrameRenderer_FrameUpdating(object sender, System.Windows.Media.RenderingEventArgs e)
        {

        }
    }
}
