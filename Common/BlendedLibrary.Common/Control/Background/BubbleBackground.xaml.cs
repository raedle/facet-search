﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace BlendedLibrary.Common.Control
{
    /// <summary>
    /// Interaction logic for BubbleBackground.xaml
    /// </summary>
    public partial class BubbleBackground : UserControl, INotifyPropertyChanged
    {
        #region static fields

        private static readonly Random Random = new Random();

        #endregion

        #region properties

        #region Bubbles

        /// <summary>
        /// The <see cref="Bubbles" /> property's name.
        /// </summary>
        public const string BubblesPropertyName = "Bubbles";

        private ObservableCollection<Bubble> _bubbles = new ObservableCollection<Bubble>();

        /// <summary>
        /// Sets and gets the Bubbles property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<Bubble> Bubbles
        {
            get
            {
                return _bubbles;
            }

            set
            {
                if (_bubbles == value)
                {
                    return;
                }

                _bubbles = value;
                RaisePropertyChanged(BubblesPropertyName);
            }
        }

        #endregion

        #endregion

        public BubbleBackground()
        {
            InitializeComponent();

            var bubblesLock = new object();
            BindingOperations.EnableCollectionSynchronization(Bubbles, bubblesLock);

            Loaded += BubbleBackgroundLoaded;
        }

        void BubbleBackgroundLoaded(object sender, RoutedEventArgs e)
        {
            FrameRenderer.FrameUpdating += FrameRenderer_FrameUpdating;

            var width = Application.Current.MainWindow.ActualWidth;
            var height = Application.Current.MainWindow.ActualHeight;

            new Thread(() =>
            {
                for (var i = 0; i < 20; i++)
                {
                    var bubble = GetRandomBubble(width, height);

                    Bubbles.Add(bubble);

                    Thread.Sleep(1000);
                }
            }).Start();
        }

        void FrameRenderer_FrameUpdating(object sender, System.Windows.Media.RenderingEventArgs e)
        {
            var bubblesToRemove = new List<Bubble>();

            foreach (var bubble in Bubbles.ToArray())
            {
                bubble.Tick();
            }

            foreach (var bubble in bubblesToRemove)
            {
                Bubbles.Remove(bubble);

                var width = Application.Current.MainWindow.ActualWidth;
                var height = Application.Current.MainWindow.ActualHeight;
                Bubbles.Add(GetRandomBubble(width, height));
            }
        }

        #region private methods

        private Bubble GetRandomBubble(double width, double height)
        {
            var bubble = new Bubble
            {
                X = Random.NextDouble() * width,
                Y = Random.NextDouble() * height,
                MaxRadius = (Random.NextDouble() + 0.5) * 200.0,
                GrowSpeed = Random.NextDouble()
            };

            bubble.Bursted += OnBubbleBursted;

            return bubble;
        }

        private void OnBubbleBursted(object sender, EventArgs e)
        {
            var bubble = sender as Bubble;

            if (bubble == null) return;

            Bubbles.Remove(bubble);
            bubble.Bursted -= OnBubbleBursted;

            var width = Application.Current.MainWindow.ActualWidth;
            var height = Application.Current.MainWindow.ActualHeight;
            Bubbles.Add(GetRandomBubble(width, height));
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
