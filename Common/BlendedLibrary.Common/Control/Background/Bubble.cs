﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlendedLibrary.Common.Annotations;

namespace BlendedLibrary.Common.Control
{
    public class Bubble : INotifyPropertyChanged
    {
        #region events

        public EventHandler<EventArgs> Bursted;

        #endregion

        #region properties

        #region X

        /// <summary>
        /// The <see cref="X" /> property's name.
        /// </summary>
        public const string XPropertyName = "X";

        private double _x = 0.0;

        /// <summary>
        /// Sets and gets the X property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double X
        {
            get
            {
                return _x;
            }

            set
            {
                if (_x == value)
                {
                    return;
                }

                _x = value;
                RaisePropertyChanged(XPropertyName);
            }
        }

        #endregion

        #region Y

        /// <summary>
        /// The <see cref="Y" /> property's name.
        /// </summary>
        public const string YPropertyName = "Y";

        private double _y = 0.0;

        /// <summary>
        /// Sets and gets the Y property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double Y
        {
            get
            {
                return _y;
            }

            set
            {
                if (_y == value)
                {
                    return;
                }

                _y = value;
                RaisePropertyChanged(YPropertyName);
            }
        }

        #endregion

        #region MaxRadius

        /// <summary>
        /// The <see cref="MaxRadius" /> property's name.
        /// </summary>
        public const string MaxRadiusPropertyName = "MaxRadius";

        private double _maxRadius = 100.0;

        /// <summary>
        /// Sets and gets the MaxRadius property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double MaxRadius
        {
            get
            {
                return _maxRadius;
            }

            set
            {
                if (_maxRadius == value)
                {
                    return;
                }

                _maxRadius = value;
                RaisePropertyChanged(MaxRadiusPropertyName);
            }
        }

        #endregion

        #region Radius

        /// <summary>
        /// The <see cref="Radius" /> property's name.
        /// </summary>
        public const string RadiusPropertyName = "Radius";

        private double _radius = 0.0;

        /// <summary>
        /// Sets and gets the Radius property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double Radius
        {
            get
            {
                return _radius;
            }

            set
            {
                if (_radius == value)
                {
                    return;
                }

                _radius = value;
                RaisePropertyChanged(RadiusPropertyName);
            }
        }

        #endregion

        #region Opacity

        /// <summary>
        /// The <see cref="Opacity" /> property's name.
        /// </summary>
        public const string OpacityPropertyName = "Opacity";

        private double _opacity = 1.0;

        /// <summary>
        /// Sets and gets the Opacity property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double Opacity
        {
            get
            {
                return _opacity;
            }

            set
            {
                if (_opacity == value)
                {
                    return;
                }

                _opacity = value;
                RaisePropertyChanged(OpacityPropertyName);
            }
        }

        #endregion

        #region GrowSpeed

        /// <summary>
        /// The <see cref="GrowSpeed" /> property's name.
        /// </summary>
        public const string GrowSpeedPropertyName = "GrowSpeed";

        private double _growSpeed = 1.0;

        /// <summary>
        /// Sets and gets the GrowSpeed property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double GrowSpeed
        {
            get
            {
                return _growSpeed;
            }

            set
            {
                if (_growSpeed == value)
                {
                    return;
                }

                _growSpeed = value;
                RaisePropertyChanged(GrowSpeedPropertyName);
            }
        }

        #endregion

        #region Direction

        /// <summary>
        /// The <see cref="Direction" /> property's name.
        /// </summary>
        public const string DirectionPropertyName = "Direction";

        private Direction _direction = Direction.Increase;

        /// <summary>
        /// Sets and gets the Direction property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Direction Direction
        {
            get
            {
                return _direction;
            }

            set
            {
                if (_direction == value)
                {
                    return;
                }

                _direction = value;
                RaisePropertyChanged(DirectionPropertyName);
            }
        }

        #endregion

        #endregion

        #region public methods

        public void Tick()
        {
            if (Direction == Direction.Increase)
            {
                Radius += GrowSpeed;
            }
            else
                Radius -= GrowSpeed;

            Opacity = 1 - (Radius / MaxRadius);

            //if (Radius > 100)
            //    Direction = Direction.Decrease;

            //if (Radius <= 0)
            //    Direction = Direction.Increase;

            if (!(Radius >= MaxRadius)) return;

            if (Bursted != null)
                Bursted(this, new EventArgs());
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

    public enum Direction
    {
        Increase,
        Decrease
    }
}
