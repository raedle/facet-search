﻿using System;

namespace BlendedLibrary.Common.Control
{
    public class SettingsDialogItem
    {
        #region properties

        #region FriendlyName

        private string _friendlyName;

        public string FriendlyName
        {
            get
            {
                return _friendlyName ?? Key.ToString();
            }

            set
            {
                _friendlyName = value;
            }
        }

        #endregion

        #region Key

        public object Key { get; set; }

        #endregion

        #region Value

        private bool _bypassComputeValueIfAlreadyInComputation = false;
        private object _value;

        public object Value
        {
            get
            {
                if (ComputeValueFunc != null && !_bypassComputeValueIfAlreadyInComputation)
                {
                    _bypassComputeValueIfAlreadyInComputation = true;
                    var computedValue = ComputeValueFunc(this);
                    _bypassComputeValueIfAlreadyInComputation = false;
                    return computedValue;
                }
                return _value;
            }
            set { _value = value; }
        }

        #endregion

        #region ComputeValueFunc

        public Func<SettingsDialogItem, object> ComputeValueFunc { get; set; }

        #endregion

        #endregion

        #region ctor

        public SettingsDialogItem(object key, object value, Func<SettingsDialogItem, object> computeValueFunc = null)
        {
            Key = key;
            Value = value;
            ComputeValueFunc = computeValueFunc;
        }

        #endregion
    }
}
