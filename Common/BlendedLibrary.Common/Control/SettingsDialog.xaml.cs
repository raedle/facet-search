﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using BlendedLibrary.Common.Annotations;

namespace BlendedLibrary.Common.Control
{
    /// <summary>
    /// Interaction logic for ConfigurationDialog.xaml
    /// </summary>
    public partial class SettingsDialog : INotifyPropertyChanged
    {
        #region properties

        #region IsResetEnabled

        /// <summary>
        /// The <see cref="IsResetEnabled" /> property's name.
        /// </summary>
        public const string IsResetEnabledPropertyName = "IsResetEnabled";

        private bool _isResetEnabled;

        /// <summary>
        /// Sets and gets the IsResetEnabled property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsResetEnabled
        {
            get
            {
                return _isResetEnabled;
            }

            set
            {
                if (_isResetEnabled == value)
                {
                    return;
                }

                _isResetEnabled = value;
                RaisePropertyChanged(IsResetEnabledPropertyName);
            }
        }

        #endregion

        #endregion

        #region dependency properties

        #region Objects

        public ObservableCollection<SettingsDialogItem> Objects
        {
            get { return (ObservableCollection<SettingsDialogItem>)GetValue(ObjectsProperty); }
            set { SetValue(ObjectsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Objects.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ObjectsProperty =
            DependencyProperty.Register("Objects", typeof(ObservableCollection<SettingsDialogItem>), typeof(SettingsDialog), new PropertyMetadata(null));

        #endregion

        #region SelectedObject

        public object SelectedObject
        {
            get { return GetValue(SelectedObjectProperty); }
            set { SetValue(SelectedObjectProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedObject.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedObjectProperty =
            DependencyProperty.Register("SelectedObject", typeof(object), typeof(SettingsDialog), new PropertyMetadata(OnSelectedObjectChanged));

        private static void OnSelectedObjectChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var settings = e.NewValue as ApplicationSettingsBase;
            if (settings != null)
            {
                var dialog = d as SettingsDialog;
                if (dialog != null)
                    dialog.IsResetEnabled = true;
            }
        }

        #endregion

        #region SelectedKey

        public object SelectedKey
        {
            get { return GetValue(SelectedKeyProperty); }
            set { SetValue(SelectedKeyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedKey.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedKeyProperty =
            DependencyProperty.Register("SelectedKey", typeof(object), typeof(SettingsDialog), new PropertyMetadata(null));

        #endregion

        #endregion

        #region properties

        public SettingsDialogResult Result { get; private set; }

        #endregion

        public SettingsDialog()
        {
            InitializeComponent();

            Owner = Application.Current.MainWindow;
        }

        private void OnOkButtonClick(object sender, RoutedEventArgs e)
        {
            Result = SettingsDialogResult.Ok;
            DialogResult = true;

            var settings = SelectedObject as ApplicationSettingsBase;
            if (settings != null)
                settings.Save();
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;

            var settings = SelectedObject as ApplicationSettingsBase;
            if (settings != null)
                settings.Reload();
        }

        private void OnResetButtonClick(object sender, RoutedEventArgs e)
        {
            Result = SettingsDialogResult.Reset;

            var settings = SelectedObject as ApplicationSettingsBase;
            if (settings != null)
            {
                var question = MessageBox.Show(string.Format("Do you want to reset current settings to factory defaults?{0}{0}This action cannot be undone.", Environment.NewLine),
                    "Reset to Factory Defaults", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (question == MessageBoxResult.Yes)
                    settings.Reset();
            }
            else
            {
                DialogResult = true;
            }
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedItem = ObjectProviderComboBox.SelectedItem;
            var settingsDialogItem = selectedItem as SettingsDialogItem;

            SelectedObject = settingsDialogItem != null ? settingsDialogItem.Value : selectedItem;
        }

        #region property changed

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        private void SettingsDialog_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.IsRepeat) return;

            // close dialog on escape
            if (e.Key == System.Windows.Input.Key.Escape)
                DialogResult = false;
        }
    }
}
