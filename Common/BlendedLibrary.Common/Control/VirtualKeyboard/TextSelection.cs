﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlendedLibrary.Common.Annotations;

namespace BlendedLibrary.Common.Control
{
    public class TextSelection : INotifyPropertyChanged
    {
        #region properties

        #region StartPosition

        public int StartPosition { get; set; }

        #endregion

        #region EndPosition

        public int EndPosition { get; set; }

        #endregion

        #region MinIndex

        public int MinIndex
        {
            get { return Math.Min(StartPosition, EndPosition); }
        }

        #endregion

        #region MaxIndex

        public int MaxIndex
        {
            get { return Math.Max(StartPosition, EndPosition); }
        }

        #endregion

        #endregion

        #region ctor

        public TextSelection(int index)
        {
            StartPosition = index;
        }

        #endregion

        #region public methods

        public string[] GetTextParts(string text)
        {
            var min = MinIndex;
            var max = MaxIndex + 1;
            var textLength = text.Length;

            var part1 = text.Substring(0, min);
            var part2 = text.Substring(min, max - min);
            var part3 = text.Substring(max, textLength - max);

            return new[]
                   {
                       part1,
                       part2,
                       part3
                   };
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));

        }

        #endregion
    }
}
