﻿using System.Windows.Input;

namespace BlendedLibrary.Common.Control
{
    public class ActionVirtualKey : VirtualKey
    {
        #region private fields

        private readonly string _key;
        private readonly string _shiftKey;
        private ModifierKeys _modifier;

        #endregion

        public ActionVirtualKey(string key)
            : base(key)
        {
            _key = key;
        }

        public ActionVirtualKey(string key, string shiftKey)
            : base(key)
        {
            _key = key;
            _shiftKey = shiftKey;
        }

        public void Modifiy(ModifierKeys modifiers)
        {
            if ((ModifierKeys.Shift & modifiers) > 0)
                DisplayValue = string.IsNullOrEmpty(_shiftKey) ? _key.ToUpperInvariant() : _shiftKey;
            else
                DisplayValue = _key;

        }

        internal virtual void Press(out string output, string input, Caret caret, out int characterDifference, TextSelection selection)
        {
            if (input == null)
            {
                output = DisplayValue;
                characterDifference = DisplayValue.Length;
                return;
            }

            if (selection == null)
            {
                var part1 = input.Substring(0, caret.Index);
                var part2 = input.Substring(caret.Index, input.Length - caret.Index);

                output = part1 + DisplayValue + part2;
                characterDifference = DisplayValue.Length;
            }
            else
            {
                var parts = selection.GetTextParts(input);
                output = parts[0] + DisplayValue + parts[2];
                characterDifference = -parts[1].Length + DisplayValue.Length;
            }
        }
    }
}
