﻿using System.Windows;
using System.Windows.Media;

namespace BlendedLibrary.Common.Control
{
    internal class DeleteVirtualKey : ActionVirtualKey
    {
        public override Brush DefaultBackground
        {
            get { return new SolidColorBrush(Colors.Gray); }
        }

        public override Brush HighlightBackground
        {
            get { return new SolidColorBrush(Colors.DarkSlateGray); }
        }

        public override FontWeight DefaultFontWeight
        {
            get { return FontWeights.Bold; }
        }

        public override double DefaultFontSize
        {
            get { return 20; }
        }

        #region properties

        #endregion

        public DeleteVirtualKey()
            : base("←")
        {
        }

        internal override void Press(out string output, string input, Caret caret, out int characterDifference, TextSelection selection)
        {
            output = input;
            characterDifference = 0;

            if (string.IsNullOrEmpty(input))
            {
                return;
            }

            if (selection == null)
            {
                var part1 = input.Substring(0, caret.Index);
                var part2 = input.Substring(caret.Index, input.Length - caret.Index);

                if (part1.Length <= 0) return;

                output = part1.Substring(0, part1.Length - 1) + part2;
                characterDifference = -1;
            }
            else
            {
                var parts = selection.GetTextParts(input);
                output = parts[0] + parts[2];
                characterDifference = -parts[1].Length;
            }
        }
    }
}
