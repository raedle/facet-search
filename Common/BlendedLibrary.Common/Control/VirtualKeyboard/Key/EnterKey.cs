﻿using System.Windows;
using System.Windows.Media;

namespace BlendedLibrary.Common.Control
{
    public class EnterVirtualKey : VirtualKey
    {
        public override Brush DefaultBackground
        {
            get { return new SolidColorBrush(Colors.Crimson); }
        }

        public override Brush HighlightBackground
        {
            get { return new SolidColorBrush(Colors.DarkRed); }
        }

        public override FontWeight DefaultFontWeight
        {
            get { return FontWeights.Bold; }
        }

        #region properties

        #endregion

        internal EnterVirtualKey()
            : base("Enter")
        {
        }

        internal EnterVirtualKey(string displayName)
            : base(displayName)
        {
        }
    }
}
