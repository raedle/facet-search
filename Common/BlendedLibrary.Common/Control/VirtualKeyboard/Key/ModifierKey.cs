﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace BlendedLibrary.Common.Control
{
    public class ModifierVirtualKey : VirtualKey
    {
        public override Brush DefaultBackground
        {
            get { return new SolidColorBrush(Colors.Gray); }
        }

        public override Brush HighlightBackground
        {
            get { return new SolidColorBrush(Colors.DarkSlateGray); }
        }

        public override FontWeight DefaultFontWeight
        {
            get { return FontWeights.Bold; }
        }

        public override double DefaultFontSize
        {
            get { return 20; }
        }

        #region properties

        #region Modifier

        /// <summary>
        /// The <see cref="Modifier" /> property's name.
        /// </summary>
        public const string ModifierPropertyName = "Modifier";

        private ModifierKeys _modifier;

        /// <summary>
        /// Sets and gets the Modifier property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ModifierKeys Modifier
        {
            get
            {
                return _modifier;
            }

            set
            {
                if (_modifier == value)
                {
                    return;
                }

                _modifier = value;
                RaisePropertyChanged(ModifierPropertyName);
            }
        }

        #endregion

        #endregion

        internal ModifierVirtualKey(ModifierKeys modifier)
        {
            Modifier = modifier;

            if ((ModifierKeys.Shift & modifier) > 0)
                DisplayValue = "⇧";
            else if ((ModifierKeys.Control & modifier) > 0)
                DisplayValue = "Ctrl";
            else if ((ModifierKeys.Alt & modifier) > 0)
                DisplayValue = "Alt";
            else if ((ModifierKeys.Windows & modifier) > 0)
                DisplayValue = "Windows";
        }
    }
}
