﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;
using BlendedLibrary.Common.Annotations;

namespace BlendedLibrary.Common.Control
{
    public abstract class VirtualKey : INotifyPropertyChanged
    {
        public virtual Brush DefaultBackground
        {
            get { return new SolidColorBrush(Colors.Gainsboro); }
        }

        public virtual Brush HighlightBackground
        {
            get { return new SolidColorBrush(Colors.DimGray); }
        }

        public virtual FontWeight DefaultFontWeight
        {
            get { return FontWeights.Regular; }
        }

        public virtual double DefaultFontSize
        {
            get { return 14; }
        }


        #region properties

        #region DisplayValue

        /// <summary>
        /// The <see cref="DisplayValue" /> property's name.
        /// </summary>
        public const string DisplayValuePropertyName = "DisplayValue";

        private string _displayValue;

        /// <summary>
        /// Sets and gets the DisplayValue property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string DisplayValue
        {
            get
            {
                return _displayValue;
            }

            set
            {
                if (_displayValue == value)
                {
                    return;
                }

                _displayValue = value;
                RaisePropertyChanged(DisplayValuePropertyName);
            }
        }

        #endregion

        #region Width

        /// <summary>
        /// The <see cref="Width" /> property's name.
        /// </summary>
        public const string WidthPropertyName = "Width";

        private double _width = 40;

        /// <summary>
        /// Sets and gets the Width property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double Width
        {
            get
            {
                return _width;
            }

            set
            {
                if (_width == value)
                {
                    return;
                }

                _width = value;
                RaisePropertyChanged(WidthPropertyName);
            }
        }

        #endregion

        #region Background

        /// <summary>
        /// The <see cref="VisualStyleElement.TrayNotify.Background" /> property's name.
        /// </summary>
        public const string BackgroundPropertyName = "Background";

        private Brush _background = null;

        /// <summary>
        /// Sets and gets the Background property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Brush Background
        {
            get
            {
                if (_background == null) _background = this.DefaultBackground;
                return _background;
            }

            set
            {
                if (_background == value)
                {
                    return;
                }

                _background = value;
                RaisePropertyChanged(BackgroundPropertyName);
            }
        }

        #endregion

        #region FontWeight

        /// <summary>
        /// The <see cref="Width" /> property's name.
        /// </summary>
        public const string FontWeightPropertyName = "FontWeight";

        private FontWeight? _fontWeight = null;

        /// <summary>
        /// Sets and gets the Width property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public FontWeight FontWeight
        {
            get
            {
                if (_fontWeight == null) _fontWeight = this.DefaultFontWeight;
                return (FontWeight)_fontWeight;
            }
        }

        #endregion

        #region FontSize

        /// <summary>
        /// The <see cref="Width" /> property's name.
        /// </summary>
        public const string FontSizePropertyName = "FontSize";

        private double? _fontSize = null;

        /// <summary>
        /// Sets and gets the Width property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double FontSize
        {
            get
            {
                if (_fontSize == null) _fontSize = this.DefaultFontSize;
                return (double)_fontSize;
            }
        }

        #endregion

        #endregion

        internal VirtualKey(string displayValue = "N/A")
        {
            DisplayValue = displayValue;
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
