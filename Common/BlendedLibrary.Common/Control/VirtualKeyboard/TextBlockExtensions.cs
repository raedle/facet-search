﻿using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace BlendedLibrary.Common.Control
{
    public static class TextBlockExtensions
    {
        public static void ApplyTextSelection(this TextBlock textBlock, TextSelection selection)
        {
            if (selection != null)
            {
                var text = textBlock.Text;

                var inlines = textBlock.Inlines;
                inlines.Clear();

                var parts = selection.GetTextParts(text);

                inlines.Add(CreateRun(textBlock, parts[0], false));
                inlines.Add(CreateRun(textBlock, parts[1], true));
                inlines.Add(CreateRun(textBlock, parts[2], false));
            }
        }

        private static Run CreateRun(TextBlock textBlock, string text, bool highlight)
        {
            return new Run(text)
            {
                Background = highlight ? Brushes.LightBlue : textBlock.Background
            };
        }
    }
}
