﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using BlendedLibrary.Common.Annotations;
using BlendedLibrary.Common.Model;
using BlendedLibrary.Common.Util;
using Point = System.Windows.Point;
using GalaSoft.MvvmLight.CommandWpf;

namespace BlendedLibrary.Common.Control
{
    /// <summary>
    /// Interaction logic for VirtualKeyboard.xaml
    /// </summary>
    public partial class VirtualKeyboard : INotifyPropertyChanged
    {
        #region event helpers

        private readonly List<IDisposable> _changeNotificationActionSubscriptions = new List<IDisposable>();

        protected void AddChangeNotificationActionSubscription(IDisposable subscription)
        {
            _changeNotificationActionSubscriptions.Add(subscription);
        }

        /// <summary>
        /// !Subscription muss manuell zum Disposen per <see cref="AddChangeNotificationActionSubscription"/> eingetragen werden
        /// </summary>
        protected IObservable<PropertyChangedEventArgs> RegisterChangeNotificationAction(INotifyPropertyChanged source, String sourceProperty)
        {
            return Observable.FromEventPattern<PropertyChangedEventArgs>(source, "PropertyChanged").Where(e => e.EventArgs.PropertyName == sourceProperty).Select(e => e.EventArgs);
        }
        protected IDisposable RegisterChangeNotificationAction(INotifyPropertyChanged source, String sourceProperty, Action<PropertyChangedEventArgs> action)
        {
            var subscription = Observable.FromEventPattern<PropertyChangedEventArgs>(source, "PropertyChanged").Where(e => e.EventArgs.PropertyName == sourceProperty).Subscribe(e => action(e.EventArgs));
            AddChangeNotificationActionSubscription(subscription);
            return subscription;
        }

        #endregion

        #region events

        public event EventHandler<EventArgs> OnEnter;

        #endregion

        #region private fields

        private ModifierKeys _modifierKeys = ModifierKeys.None;

        private readonly List<VirtualKey> _keys = new List<VirtualKey>();

        private readonly Dictionary<long, KeyRepeatedTriggerInfo> _keyTriggerInfos = new Dictionary<long, KeyRepeatedTriggerInfo>();

        private TextSelection _textSelection;

        #endregion

        #region commands

        public RelayCommand<SenderAwareEventArgs> KeyPressedCommand { get; private set; }

        public RelayCommand<SenderAwareEventArgs> KeyReleasedCommand { get; private set; }

        public RelayCommand<SenderAwareEventArgs> MoveCommand { get; private set; }

        public RelayCommand<SenderAwareEventArgs> KeyLeftCommand { get; private set; }

        #endregion

        #region dependency properties

        #region AssociatedObjectPosition

        public IPosition AssociatedObjectPosition
        {
            get { return (IPosition)GetValue(AssociatedObjectPositionProperty); }
            set { SetValue(AssociatedObjectPositionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AssociatedObjectPosition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AssociatedObjectPositionProperty =
            DependencyProperty.Register("AssociatedObjectPosition", typeof(IPosition), typeof(VirtualKeyboard), new PropertyMetadata(default(IPosition)));

        #endregion

        #endregion

        #region properties

        #region Caret

        /// <summary>
        /// The <see cref="Caret" /> property's name.
        /// </summary>
        public const string CaretPropertyName = "Caret";

        private Caret _caret;

        /// <summary>
        /// Sets and gets the Caret property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Caret Caret
        {
            get
            {
                return _caret;
            }

            set
            {
                if (_caret == value)
                {
                    return;
                }

                _caret = value;
                RaisePropertyChanged(CaretPropertyName);
            }
        }

        #endregion

        #region ZeroRowKeys

        /// <summary>
        /// The <see cref="ZeroRowKeys" /> property's name.
        /// </summary>
        public const string ZeroRowKeysPropertyName = "ZeroRowKeys";

        private ObservableCollection<VirtualKey> _zeroRowKeys = new ObservableCollection<VirtualKey>();

        /// <summary>
        /// Sets and gets the ZeroRowKeys property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<VirtualKey> ZeroRowKeys
        {
            get
            {
                return _zeroRowKeys;
            }

            set
            {
                if (_zeroRowKeys == value)
                {
                    return;
                }

                _zeroRowKeys = value;
                RaisePropertyChanged(ZeroRowKeysPropertyName);
            }
        }

        #endregion

        #region FirstRowKeys

        /// <summary>
        /// The <see cref="FirstRowKeys" /> property's name.
        /// </summary>
        public const string FirstRowKeysPropertyName = "FirstRowKeys";

        private ObservableCollection<VirtualKey> _firstRowKeys = new ObservableCollection<VirtualKey>();

        /// <summary>
        /// Sets and gets the FirstRowKeys property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<VirtualKey> FirstRowKeys
        {
            get
            {
                return _firstRowKeys;
            }

            set
            {
                if (_firstRowKeys == value)
                {
                    return;
                }

                _firstRowKeys = value;
                RaisePropertyChanged(FirstRowKeysPropertyName);
            }
        }

        #endregion

        #region SecondRowKeys

        /// <summary>
        /// The <see cref="SecondRowKeys" /> property's name.
        /// </summary>
        public const string SecondRowKeysPropertyName = "SecondRowKeys";

        private ObservableCollection<VirtualKey> _secondRowKeys = new ObservableCollection<VirtualKey>();

        /// <summary>
        /// Sets and gets the SecondRowKeys property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<VirtualKey> SecondRowKeys
        {
            get
            {
                return _secondRowKeys;
            }

            set
            {
                if (_secondRowKeys == value)
                {
                    return;
                }

                _secondRowKeys = value;
                RaisePropertyChanged(SecondRowKeysPropertyName);
            }
        }

        #endregion

        #region ThirdRowKeys

        /// <summary>
        /// The <see cref="ThirdRowKeys" /> property's name.
        /// </summary>
        public const string ThirdRowKeysPropertyName = "ThirdRowKeys";

        private ObservableCollection<VirtualKey> _thirdRowKeys = new ObservableCollection<VirtualKey>();

        /// <summary>
        /// Sets and gets the ThirdRowKeys property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<VirtualKey> ThirdRowKeys
        {
            get
            {
                return _thirdRowKeys;
            }

            set
            {
                if (_thirdRowKeys == value)
                {
                    return;
                }

                _thirdRowKeys = value;
                RaisePropertyChanged(ThirdRowKeysPropertyName);
            }
        }

        #endregion

        #region ForthRowKeys

        /// <summary>
        /// The <see cref="ForthRowKeys" /> property's name.
        /// </summary>
        public const string ForthRowKeysPropertyName = "ForthRowKeys";

        private ObservableCollection<VirtualKey> _forthRowKeys = new ObservableCollection<VirtualKey>();

        /// <summary>
        /// Sets and gets the ForthRowKeys property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<VirtualKey> ForthRowKeys
        {
            get
            {
                return _forthRowKeys;
            }

            set
            {
                if (_forthRowKeys == value)
                {
                    return;
                }

                _forthRowKeys = value;
                RaisePropertyChanged(ForthRowKeysPropertyName);
            }
        }

        #endregion

        #region FifthRowKeys

        /// <summary>
        /// The <see cref="FifthRowKeys" /> property's name.
        /// </summary>
        public const string FifthRowKeysPropertyName = "FifthRowKeys";

        private ObservableCollection<VirtualKey> _fifthRowKeys = new ObservableCollection<VirtualKey>();

        /// <summary>
        /// Sets and gets the FifthRowKeys property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<VirtualKey> FifthRowKeys
        {
            get
            {
                return _fifthRowKeys;
            }

            set
            {
                if (_fifthRowKeys == value)
                {
                    return;
                }

                _fifthRowKeys = value;
                RaisePropertyChanged(FifthRowKeysPropertyName);
            }
        }

        #endregion

        #region X

        private double _x = 0.0;

        public double X
        {
            get { return _x; }
            set
            {
                if (_x == value) return;

                _x = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Y

        private double _y = 0.0;

        public double Y
        {
            get { return _y; }
            set
            {
                if (_y == value) return;

                _y = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region OffsetX

        /// <summary>
        /// The <see cref="OffsetX" /> property's name.
        /// </summary>
        public const string OffsetXPropertyName = "OffsetX";

        private double _offsetX = 0.0;

        /// <summary>
        /// Sets and gets the OffsetX property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double OffsetX
        {
            get
            {
                return _offsetX;
            }

            set
            {
                if (_offsetX == value)
                {
                    return;
                }

                _offsetX = value;
                RaisePropertyChanged(OffsetXPropertyName);
            }
        }

        #endregion

        #region OffsetY

        /// <summary>
        /// The <see cref="OffsetY" /> property's name.
        /// </summary>
        public const string OffsetYPropertyName = "OffsetY";

        private double _offsetY = 0.0;

        /// <summary>
        /// Sets and gets the OffsetY property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double OffsetY
        {
            get
            {
                return _offsetY;
            }

            set
            {
                if (_offsetY == value)
                {
                    return;
                }

                _offsetY = value;
                RaisePropertyChanged(OffsetYPropertyName);
            }
        }

        #endregion

        #region Angle

        private double _angle = 0.0;

        public double Angle
        {
            get { return _angle; }
            set
            {
                if (_angle == value) return;

                _angle = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Text

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(VirtualKeyboard), new PropertyMetadata(string.Empty, OnTextChanged));

        private static void OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var keyboard = d as VirtualKeyboard;

            if (keyboard == null) return;

            var text = e.NewValue as string;

            keyboard.IsClearButtonVisible = !string.IsNullOrEmpty(text);
        }

        #endregion

        #region IsDragEnabled

        private bool _isDragEnabled = true;

        public bool IsDragEnabled
        {
            get { return _isDragEnabled; }
            set
            {
                if (_isDragEnabled == value) return;

                _isDragEnabled = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region IsClearButtonVisible

        /// <summary>
        /// The <see cref="IsClearButtonVisible" /> property's name.
        /// </summary>
        public const string IsClearButtonVisiblePropertyName = "IsClearButtonVisible";

        private bool _isClearButtonVisible = false;

        /// <summary>
        /// Sets and gets the IsClearButtonVisible property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsClearButtonVisible
        {
            get
            {
                return _isClearButtonVisible;
            }

            set
            {
                if (_isClearButtonVisible == value)
                {
                    return;
                }

                _isClearButtonVisible = value;
                RaisePropertyChanged(IsClearButtonVisiblePropertyName);
            }
        }

        #endregion

        #endregion

        public VirtualKeyboard()
        {
            InitializeComponent();

            Loaded += (s, e) =>
            {
                // helper construct to display a caret
                Caret = new Caret(TextBlockControl);
            };

            KeyPressedCommand = new RelayCommand<SenderAwareEventArgs>(OnKeyPressed);
            KeyReleasedCommand = new RelayCommand<SenderAwareEventArgs>(OnKeyReleased);
            KeyLeftCommand = new RelayCommand<SenderAwareEventArgs>(OnKeyLeft);
            MoveCommand = new RelayCommand<SenderAwareEventArgs>(OnMove);

            ZeroRowKeys.Add(new EnterVirtualKey(Common.Properties.Resources.Search) { Width = 60 });

            FirstRowKeys.Add(new ActionVirtualKey("1", "!"));
            FirstRowKeys.Add(new ActionVirtualKey("2", "\""));
            FirstRowKeys.Add(new ActionVirtualKey("3", "§"));
            FirstRowKeys.Add(new ActionVirtualKey("4", "$"));
            FirstRowKeys.Add(new ActionVirtualKey("5", "%"));
            FirstRowKeys.Add(new ActionVirtualKey("6", "&"));
            FirstRowKeys.Add(new ActionVirtualKey("7", "/"));
            FirstRowKeys.Add(new ActionVirtualKey("8", "("));
            FirstRowKeys.Add(new ActionVirtualKey("9", ")"));
            FirstRowKeys.Add(new ActionVirtualKey("0", "="));
            FirstRowKeys.Add(new ActionVirtualKey("ß", "?"));
            FirstRowKeys.Add(new DeleteVirtualKey { Width = 60 });

            SecondRowKeys.Add(new ActionVirtualKey("q"));
            SecondRowKeys.Add(new ActionVirtualKey("w"));
            SecondRowKeys.Add(new ActionVirtualKey("e"));
            SecondRowKeys.Add(new ActionVirtualKey("r"));
            SecondRowKeys.Add(new ActionVirtualKey("t"));
            SecondRowKeys.Add(new ActionVirtualKey("z"));
            SecondRowKeys.Add(new ActionVirtualKey("u"));
            SecondRowKeys.Add(new ActionVirtualKey("i"));
            SecondRowKeys.Add(new ActionVirtualKey("o"));
            SecondRowKeys.Add(new ActionVirtualKey("p"));
            SecondRowKeys.Add(new ActionVirtualKey("ü"));
            SecondRowKeys.Add(new ActionVirtualKey("+", "*"));
            //SecondRowKeys.Add(new EnterKey("Suchen") { Width = 60 });

            ThirdRowKeys.Add(new ActionVirtualKey("a"));
            ThirdRowKeys.Add(new ActionVirtualKey("s"));
            ThirdRowKeys.Add(new ActionVirtualKey("d"));
            ThirdRowKeys.Add(new ActionVirtualKey("f"));
            ThirdRowKeys.Add(new ActionVirtualKey("g"));
            ThirdRowKeys.Add(new ActionVirtualKey("h"));
            ThirdRowKeys.Add(new ActionVirtualKey("j"));
            ThirdRowKeys.Add(new ActionVirtualKey("k"));
            ThirdRowKeys.Add(new ActionVirtualKey("l"));
            ThirdRowKeys.Add(new ActionVirtualKey("ö"));
            ThirdRowKeys.Add(new ActionVirtualKey("ä"));
            ThirdRowKeys.Add(new ActionVirtualKey("#", "'"));

            ForthRowKeys.Add(new ModifierVirtualKey(ModifierKeys.Shift) { Width = 60 }); //NEW
            //ForthRowKeys.Add(new ActionKey("<", ">"));
            ForthRowKeys.Add(new ActionVirtualKey("y"));
            ForthRowKeys.Add(new ActionVirtualKey("x"));
            ForthRowKeys.Add(new ActionVirtualKey("c"));
            ForthRowKeys.Add(new ActionVirtualKey("v"));
            ForthRowKeys.Add(new ActionVirtualKey("b"));
            ForthRowKeys.Add(new ActionVirtualKey("n"));
            ForthRowKeys.Add(new ActionVirtualKey("m"));
            ForthRowKeys.Add(new ActionVirtualKey(",", ";"));
            ForthRowKeys.Add(new ActionVirtualKey(".", ":"));
            ForthRowKeys.Add(new ActionVirtualKey("-", "_"));
            ForthRowKeys.Add(new ModifierVirtualKey(ModifierKeys.Shift) { Width = 60 });

            FifthRowKeys.Add(new ActionVirtualKey("<", ">")); //NEW
            FifthRowKeys.Add(new ActionVirtualKey(" ") { Width = 400 }); //NEW
            //FifthRowKeys.Add(new ActionKey(" ") { Width = 400 });
            //FifthRowKeys.Add(new ActionKey("."));
            FifthRowKeys.Add(new ActionVirtualKey("?"));
            FifthRowKeys.Add(new ActionVirtualKey("*"));

            _keys.AddRange(FirstRowKeys);
            _keys.AddRange(SecondRowKeys);
            _keys.AddRange(ThirdRowKeys);
            _keys.AddRange(ForthRowKeys);
            _keys.AddRange(FifthRowKeys);
        }

        private void OnKeyPressed(SenderAwareEventArgs args)
        {
            var sender = args.Sender as FrameworkElement;
            if (sender == null) return;

            var e = args.OriginalEventArgs as TouchEventArgs;
            if (e == null) return;

            if (!e.TouchDevice.IsFingerRecognized()) return;

            var touchInfo = new KeyTouchInfo(args);

            if (!_keyTriggerInfos.ContainsKey(touchInfo.Id))
                _keyTriggerInfos.Add(touchInfo.Id, new KeyRepeatedTriggerInfo(touchInfo));

            touchInfo.VirtualKey.Background = touchInfo.VirtualKey.HighlightBackground;

            //Activate modifiers on press
            if (touchInfo.VirtualKey is ModifierVirtualKey)
            {
                _modifierKeys |= (touchInfo.VirtualKey as ModifierVirtualKey).Modifier;

                foreach (var actionKey in _keys.OfType<ActionVirtualKey>())
                    actionKey.Modifiy(_modifierKeys);
            }

            IsDragEnabled = false;
        }

        private void OnKeyReleased(SenderAwareEventArgs args)
        {
            var sender = args.Sender as FrameworkElement;
            //Don't check for sender == null here, this might be valid due to the finger being released outside of the keyboard

            var e = args.OriginalEventArgs as TouchEventArgs;
            if (e == null) return;

            if (!e.TouchDevice.IsFingerRecognized()) return;

            var touchInfo = new KeyTouchInfo(args);

            if (!_keyTriggerInfos.ContainsKey(touchInfo.Id)) return;

            KeyRepeatedTriggerInfo keyTriggerInfo;
            _keyTriggerInfos.TryGetValue(touchInfo.Id, out keyTriggerInfo);
            _keyTriggerInfos.Remove(touchInfo.Id);
            var startTouchInfo = keyTriggerInfo.StartTouchInfo;

            //If the user moved the finger from the key he pressed down we take that as a cancel and do NOT trigger the key
            if (IsDragEnabled == false && touchInfo.VirtualKey == startTouchInfo.VirtualKey)
            {
                TriggerKey(startTouchInfo.VirtualKey);
            }

            startTouchInfo.VirtualKey.Background = startTouchInfo.VirtualKey.DefaultBackground;

            if (_keyTriggerInfos.Count == 0)
                IsDragEnabled = true;
        }

        private void OnKeyLeft(SenderAwareEventArgs args)
        {
            //It can happen that we only receive a left event for a key but no release (for example if the finger is moved off the keyboard)
            //We take such an action as a cancel of the keypress, but still need to cleanup and release modifier keys

            var sender = args.Sender as FrameworkElement;

            var e = args.OriginalEventArgs as TouchEventArgs;
            if (e == null) return;

            if (!e.TouchDevice.IsFingerRecognized()) return;

            var touchInfo = new KeyTouchInfo(args);

            if (!_keyTriggerInfos.ContainsKey(touchInfo.Id)) return;

            KeyRepeatedTriggerInfo keyTriggerInfo;
            _keyTriggerInfos.TryGetValue(touchInfo.Id, out keyTriggerInfo);
            _keyTriggerInfos.Remove(touchInfo.Id);
            var startTouchInfo = keyTriggerInfo.StartTouchInfo;

            if (startTouchInfo.VirtualKey is ModifierVirtualKey)
            {
                _modifierKeys ^= (startTouchInfo.VirtualKey as ModifierVirtualKey).Modifier;

                foreach (var actionKey in _keys.OfType<ActionVirtualKey>())
                    actionKey.Modifiy(_modifierKeys);
            }

            startTouchInfo.VirtualKey.Background = startTouchInfo.VirtualKey.DefaultBackground;

            if (_keyTriggerInfos.Count == 0)
                IsDragEnabled = true;
        }

        private void OnMove(SenderAwareEventArgs args)
        {
            var sender = args.Sender as FrameworkElement;
            if (sender == null) return;

            var e = args.OriginalEventArgs as TouchEventArgs;
            if (e == null) return;

            if (!e.TouchDevice.IsFingerRecognized()) return;

            var touchInfo = new KeyTouchInfo(args);

            if (!_keyTriggerInfos.ContainsKey(touchInfo.Id)) return;

            KeyRepeatedTriggerInfo keyTriggerInfo;
            _keyTriggerInfos.TryGetValue(touchInfo.Id, out keyTriggerInfo);
            var startTouchInfo = keyTriggerInfo.StartTouchInfo;

            var movement = (startTouchInfo.Position - touchInfo.Position).Length;

            //Console.WriteLine("Movement {0}", movement);

            if (!IsDragEnabled && movement > 50)
            {
                IsDragEnabled = true;
            }
            else if (movement < 10 && touchInfo.VirtualKey == startTouchInfo.VirtualKey)
            {
                //If the finger is kept down still on an ActionKey it is repeatedly triggered
                var timeSinceLastTrigger = touchInfo.Timestamp - keyTriggerInfo.LastTriggerTimestamp;
                Console.WriteLine("Time since last trigger is {0}", timeSinceLastTrigger);


                if (touchInfo.VirtualKey is ActionVirtualKey && touchInfo.VirtualKey is DeleteVirtualKey == false)
                {
                    var timeNeeded = int.MaxValue;
                    if (keyTriggerInfo.NumberOfTriggers == 0)
                    {
                        //Initially we make the user wait a little for the repeated trigger to activate
                        timeNeeded = 1300;
                    }
                    else if (keyTriggerInfo.NumberOfTriggers < 5)
                    {
                        timeNeeded = 700;
                    }
                    else
                    {
                        timeNeeded = 100;
                    }

                    if (timeSinceLastTrigger > timeNeeded)
                    {
                        TriggerKey(startTouchInfo.VirtualKey);
                        keyTriggerInfo.LastTriggerTimestamp = touchInfo.Timestamp;
                    }
                }

                if (touchInfo.VirtualKey is DeleteVirtualKey)
                {
                    if (keyTriggerInfo.NumberOfTriggers == 0)
                    {
                        //Initially we make the user wait a little for the repeated trigger to activate
                        if (timeSinceLastTrigger > 1300)
                        {
                            TriggerKey(startTouchInfo.VirtualKey);
                            keyTriggerInfo.LastTriggerTimestamp = touchInfo.Timestamp;
                        }
                    }
                    else if (keyTriggerInfo.NumberOfTriggers < 5)
                    {
                        //For the first we deletes, we trigger the "normal" delete an remove a single character
                        if (timeSinceLastTrigger > 700)
                        {
                            TriggerKey(startTouchInfo.VirtualKey);
                            keyTriggerInfo.LastTriggerTimestamp = touchInfo.Timestamp;
                        }
                    }
                    else
                    {
                        //If the user keeps the finger on the delete button for a while, we start deleting entire words
                        /*if (timeSinceLastTrigger > 900)
                        {
                            var lastSpaceIndex = Text.LastIndexOf(' ');
                            Text = (lastSpaceIndex == -1) ? "" : Text.Substring(0, lastSpaceIndex);
                            keyTriggerInfo.LastTriggerTimestamp = touchInfo.Timestamp;
                        }*/

                        if (timeSinceLastTrigger > 100)
                        {
                            TriggerKey(startTouchInfo.VirtualKey);
                            keyTriggerInfo.LastTriggerTimestamp = touchInfo.Timestamp;
                        }
                    }
                }
            }
        }

        private void TriggerKey(VirtualKey virtualKey)
        {
            if (virtualKey is EnterVirtualKey)
            {
                if (OnEnter != null)
                    OnEnter(this, new EventArgs());
            }

            else if (virtualKey is ModifierVirtualKey)
            {
                _modifierKeys ^= (virtualKey as ModifierVirtualKey).Modifier;

                foreach (var actionKey in _keys.OfType<ActionVirtualKey>())
                    actionKey.Modifiy(_modifierKeys);
            }

            else if (virtualKey is ActionVirtualKey)
            {
                string output;
                int characterDifference;

                (virtualKey as ActionVirtualKey).Press(out output, Text, Caret, out characterDifference, _textSelection);
                //_textSelection = null;
                //TextBlockControl.Inlines.Clear();
                //TextBlockControl.Text = output;

                Text = output;
                Caret.Index += characterDifference;
            }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        private class KeyRepeatedTriggerInfo
        {
            private int _lastTriggerTimestamp;

            public KeyRepeatedTriggerInfo(KeyTouchInfo startTouchInfo)
            {
                StartTouchInfo = startTouchInfo;
                LastTriggerTimestamp = startTouchInfo.Timestamp;
                NumberOfTriggers = 0;
            }

            public KeyTouchInfo StartTouchInfo { get; private set; }

            public int LastTriggerTimestamp
            {
                get { return _lastTriggerTimestamp; }
                set
                {
                    _lastTriggerTimestamp = value;
                    NumberOfTriggers++;
                }
            }

            public int NumberOfTriggers { get; private set; }
        }

        private class KeyTouchInfo
        {
            public KeyTouchInfo(SenderAwareEventArgs args)
            {
                var e = (args.OriginalEventArgs as TouchEventArgs);
                var sender = (args.Sender as FrameworkElement);

                if (e == null) return;

                Id = e.TouchDevice.Id;
                VirtualKey = (sender == null) ? null : sender.DataContext as VirtualKey;
                Position = e.GetTouchPoint(null).Position;
                Timestamp = e.Timestamp;
            }

            public long Id { get; private set; }

            public VirtualKey VirtualKey { get; private set; }

            public Point Position { get; private set; }

            public int Timestamp { get; private set; }
        }

        private void UIElement_OnTouchDown(object sender, TouchEventArgs e)
        {
            var index = GetCharacterIndexFromPoint(e);
            if (index < 0) return;

            _caret.Index = index;

            //_textSelection = new TextSelection(index);

            if (e.TouchDevice.Capture(TextControlGrid, CaptureMode.SubTree))
            {
                e.Handled = true;
            }
        }

        private void UIElement_OnTouchMove(object sender, TouchEventArgs e)
        {
            var index = GetCharacterIndexFromPoint(e);
            if (index < 0) return;

            _caret.Index = index;

            //_textSelection.EndPosition = index;
            //TextBlockControl.ApplyTextSelection(_textSelection);

            e.Handled = true;
        }

        private void UIElement_OnTouchUp(object sender, TouchEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null) return;

            var index = GetCharacterIndexFromPoint(e);
            if (index < 0) return;

            _caret.Index = index;

            //_caret.TextBeforeCaret = Text.Substring(0, index);

            if (e.TouchDevice.Capture(null))
            {
                e.Handled = true;
            }
        }

        private int GetCharacterIndexFromPoint(TouchEventArgs e)
        {
            var p = e.GetTouchPoint(DummyTextBox);

            if (Text != null && p.Position.X > DummyTextBox.ActualWidth)
                return Text.Length;

            return DummyTextBox.GetCharacterIndexFromPoint(p.Position, true);
        }

        private void OnClearText(object sender, TouchEventArgs e)
        {
            Text = string.Empty;
        }
    }
}
