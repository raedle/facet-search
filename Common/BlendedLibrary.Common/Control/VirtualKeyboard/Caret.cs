﻿using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using BlendedLibrary.Common.Annotations;

namespace BlendedLibrary.Common.Control
{
    public sealed class Caret : INotifyPropertyChanged
    {
        #region private fields

        private TextBlock _textBlock;

        #endregion

        #region properties

        #region Index

        /// <summary>
        /// The <see cref="Index" /> property's name.
        /// </summary>
        public const string IndexPropertyName = "Index";

        private int _index;

        /// <summary>
        /// Sets and gets the Index property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public int Index
        {
            get
            {
                return _index;
            }

            set
            {
                if (_index == value)
                {
                    return;
                }

                _index = value;
                RaisePropertyChanged(IndexPropertyName);
                RaisePropertyChanged(TextBeforeCaretPropertyName);
            }
        }

        #endregion

        #region TextBeforeCaret

        /// <summary>
        /// The <see cref="TextBeforeCaret" /> property's name.
        /// </summary>
        public const string TextBeforeCaretPropertyName = "TextBeforeCaret";

        /// <summary>
        /// Sets and gets the TextBeforeCaret property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string TextBeforeCaret
        {
            get
            {
                return _textBlock.Text.Substring(0, Index);
            }
        }

        #endregion

        #endregion

        #region ctor

        public Caret(TextBlock textBlock)
        {
            Debug.Assert(textBlock != null, "textBlock != null");

            _textBlock = textBlock;
            Index = _textBlock.Text.Length;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
