﻿using System.Xml.Serialization;

namespace BlendedLibrary.Service
{
    [XmlType]
    public class BaseXChildProcessStartInfo
    {
        #region properties

        #region ParentProcessId

        [XmlElement]
        public int ParentProcessId { get; set; }

        #endregion

        #region WorkingDirectory

        [XmlElement]
        public string WorkingDirectory { get; set; }

        #endregion

        #region Port

        [XmlElement]
        public int Port { get; set; }

        #endregion

        #region EventPort

        [XmlElement]
        public int EventPort { get; set; }

        #endregion

        #region Interactive

        [XmlElement]
        public bool Interactive { get; set; }

        #endregion

        #endregion
    }
}
