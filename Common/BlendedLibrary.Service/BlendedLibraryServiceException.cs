﻿using System;
using System.Runtime.Serialization;

namespace BlendedLibrary.Service
{
    [Serializable]
    public class BlendedLibraryServiceException : Exception
    {
        #region properties

        public string Code { get; private set; }

        #endregion

        public BlendedLibraryServiceException()
        {

        }

        public BlendedLibraryServiceException(string code, string message)
            : base(message)
        {
            Code = code;
        }

        public BlendedLibraryServiceException(string message)
            : base(message)
        {

        }

        public BlendedLibraryServiceException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        protected BlendedLibraryServiceException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }
    }
}
