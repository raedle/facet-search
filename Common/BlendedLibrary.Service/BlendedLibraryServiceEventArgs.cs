﻿using System;

namespace BlendedLibrary.Service
{
    public class BlendedLibraryServiceEventArgs : EventArgs
    {
        #region Message

        public string Message { get; set; }

        #endregion
    }
}
