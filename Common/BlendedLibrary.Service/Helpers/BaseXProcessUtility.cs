﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace BlendedLibrary.Service.Helpers
{
    public class BaseXProcessUtility
    {
        /// <summary>
        /// Serializes the start info for the child process to temp file.
        /// </summary>
        /// <param name="baseXChildProcessStartInfo"></param>
        /// <returns></returns>
        public static string SerializeChildProcessStartInfo(BaseXChildProcessStartInfo baseXChildProcessStartInfo)
        {
            var filename = Path.GetTempFileName();

            Console.WriteLine("FILENAME1: {0}", filename);

            var serializer = new XmlSerializer(typeof(BaseXChildProcessStartInfo));
            using (var stream = new FileStream(filename, FileMode.Create))
            {
                serializer.Serialize(stream, baseXChildProcessStartInfo);
            }

            Console.WriteLine("FILENAME2: {0}", filename);

            return filename;
        }

        /// <summary>
        /// Deserializes the start info from temp file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static BaseXChildProcessStartInfo DeserializeChildProcessStartInfo(string filename)
        {
            var serializer = new XmlSerializer(typeof(BaseXChildProcessStartInfo));
            using (var stream = new FileStream(filename, FileMode.Open))
            {
                return serializer.Deserialize(stream) as BaseXChildProcessStartInfo;
            }
        }
    }
}
