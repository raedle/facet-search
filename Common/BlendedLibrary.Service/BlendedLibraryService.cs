﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using BlendedLibrary.Service.Helpers;
using NLog;
using Tools.CommonUtilities;
using Tools.CommonUtilities.Process;

namespace BlendedLibrary.Service
{
    public class BlendedLibraryService
    {
        #region Logger

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region extern

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true), CLSCompliant(false)]
        private static extern uint GetCurrentProcessId();

        #endregion

        #region service codes

        // ReSharper disable once InconsistentNaming
        public const string BSX01 = "BSX01";
        // ReSharper disable once InconsistentNaming
        public const string BEX01 = "BEX01";
        // ReSharper disable once InconsistentNaming
        public const string BEX07 = "BEX07";
        // ReSharper disable once InconsistentNaming
        public const string BEX08 = "BEX08";

        #endregion

        #region private fields

        private static Exception _exception;

        #endregion

        #region events

        public static event EventHandler<BlendedLibraryServiceEventArgs> MessageReceived;

        #endregion

        #region start/stop methods

        #region private

        private static readonly AutoResetEvent WaitEvent = new AutoResetEvent(false);

        private static readonly Dictionary<int, Process> MediatorProcesses = new Dictionary<int, Process>();

        #endregion

        public static void StartAndWait(int port = 1984, int eventPort = 1985, string workingDirectory = ".")
        {
            // Just a check if Java Home exists -> otherwise an exception is thrown
            var unused = GetJavaHome();

            var assembly = System.Reflection.Assembly.GetAssembly(typeof(BlendedLibraryService));

            var pid = GetCurrentProcessId();

            var childProcessStartInfo = new BaseXChildProcessStartInfo
            {
                ParentProcessId = (int)pid,
                WorkingDirectory = workingDirectory,
                Port = port,
                EventPort = eventPort,
                Interactive = false
            };
            var filename = BaseXProcessUtility.SerializeChildProcessStartInfo(childProcessStartInfo);

            var startInfo = new ProcessStartInfo
            {
                FileName = assembly.Location,
                Arguments = string.Format("\"{0}\"", filename),
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            try
            {
                var process = Process.Start(startInfo);

                if (process != null)
                    new Thread(() => ReadProcessOutput(process)) { IsBackground = true }.Start();

                MediatorProcesses.Add(port, process);
            }
            catch (Exception e)
            {
                throw new BlendedLibraryServiceException(string.Format("Error starting {0}", assembly.GetName()), e);
            }

            WaitEvent.WaitOne();

            if (_exception != null)
                throw _exception;
        }

        /// <summary>
        /// Reads process output.
        /// </summary>
        /// <param name="process"></param>
        private static void ReadProcessOutput(Process process)
        {
            var outputStream = process.StandardOutput;

            string line;
            while ((line = outputStream.ReadLine()) != null)
            {
                if (MessageReceived != null)
                    MessageReceived(process, new BlendedLibraryServiceEventArgs { Message = line });

                var code = GetMessageCode(line.Trim());

                // continue if message code is null
                if (code == null) continue;

                switch (code[0])
                {
                    case BSX01:
                        // initialization successful, return to callee
                        WaitEvent.Set();
                        break;
                    case BEX01:
                    case BEX07:
                    case BEX08:
                        _exception = new BlendedLibraryServiceException(code[0], code[1]);
                        WaitEvent.Set();
                        break;
                }
            }
        }

        /// <summary>
        /// Extracts and returns the message code, null otherwise.
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private static string[] GetMessageCode(string line)
        {
            if (!line.StartsWith("["))
                return null;

            var codeEndIdx = line.IndexOf("]", StringComparison.InvariantCulture);

            var code = line.Substring(1, codeEndIdx - 1);
            var message = line.Substring(codeEndIdx + 2, line.Length - codeEndIdx - 2).Trim();

            return new[] { code, message };
        }

        public static void Start(int port = 1984, int eventPort = 1985, bool interactive = false, string workingDirectory = ".")
        {
            // Just a check if Java Home exists -> otherwise an exception is thrown
            var unused = GetJavaHome();

            var assembly = System.Reflection.Assembly.GetAssembly(typeof(BlendedLibraryService));

            var pid = GetCurrentProcessId();

            var childProcessStartInfo = new BaseXChildProcessStartInfo
            {
                ParentProcessId = (int)pid,
                WorkingDirectory = workingDirectory,
                Port = port,
                EventPort = eventPort,
                Interactive = interactive
            };
            var filename = BaseXProcessUtility.SerializeChildProcessStartInfo(childProcessStartInfo);

            var startInfo = new ProcessStartInfo
            {
                FileName = assembly.Location,
                Arguments = string.Format("\"{0}\"", filename),
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            try
            {
                var process = Process.Start(startInfo);

                if (process != null)
                {
                    new Thread(() =>
                    {
                        var outputStream = process.StandardOutput;

                        string line;
                        while ((line = outputStream.ReadLine()) != null)
                        {
                            if (MessageReceived != null)
                                MessageReceived(process, new BlendedLibraryServiceEventArgs { Message = line });

                            Console.WriteLine(line);
                        }

                    }) { IsBackground = true }.Start();

                    MediatorProcesses.Add(port, process);
                }
            }
            catch (Exception e)
            {
                throw new BlendedLibraryServiceException(string.Format("Error starting {0}", assembly.GetName()), e);
            }
        }

        public static void Stop(int port = 1984)
        {
            if (MediatorProcesses.ContainsKey(port) && MediatorProcesses[port] != null)
            {
                if (!MediatorProcesses[port].HasExited)
                {
                    var inputStream = MediatorProcesses[port].StandardInput;
                    
                    // the exit command exited the process in previous versions.
                    inputStream.WriteLine("EXIT");

                    // \x3 is ctrl+c, however it does not exit the process.
                    inputStream.WriteLine("\x3");
                    inputStream.Flush();
                    inputStream.Close();

                    MediatorProcesses[port].StandardOutput.Close();
                    MediatorProcesses[port].StandardError.Close();
                    
                    // not very solid idea to kill the process but it works.
                    MediatorProcesses[port].Kill();
                }

                MediatorProcesses.Remove(port);
            }
        }

        public static bool IsRunning(int port)
        {
            if (MediatorProcesses.ContainsKey(port) && MediatorProcesses[port] != null)
            {
                var process = MediatorProcesses[port];
                return !process.HasExited;
            }
            return false;
        }

        #endregion

        #region private

        private static AutoResetEvent _autoResetEvent;

        #endregion

        private static void Main(string[] args)
        {
            var tempFilename = args[0];

            if (Logger.IsInfoEnabled)
                Logger.Info("Deserializing temp file containing child process start info: {0}", tempFilename);

            var childProcessStartInfo = BaseXProcessUtility.DeserializeChildProcessStartInfo(tempFilename);

            _autoResetEvent = new AutoResetEvent(false);

            // Starts the RDS server
            var process = StartServer(
                childProcessStartInfo.Port,
                childProcessStartInfo.EventPort,
                childProcessStartInfo.Interactive,
                childProcessStartInfo.WorkingDirectory);

            // Read input from mediator process
            new Thread(() =>
            {
                var outputStream = process.MediatorProcess.StandardOutput;

                string line;
                while ((line = outputStream.ReadLine()) != null)
                {
                    Logger.Info("LineA: " + line);
                    Console.WriteLine(line);
                }

            }) { IsBackground = true }.Start();

            // Write input to mediator process
            new Thread(() =>
            {
                Logger.Debug("Start Blended Library service read thread");

                string line;
                while ((line = Console.ReadLine()) != null)
                {
                    Logger.Debug("Read line: " + line);

                    var inputStream = process.MediatorProcess.StandardInput;
                    inputStream.WriteLine(line);
                    inputStream.Flush();
                }
            }) { IsBackground = true }.Start();

            WaitCallback callbackParentProcess = processId =>
            {
                try
                {
                    CheckProcess((int)processId);
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                }
            };
            ThreadPool.QueueUserWorkItem(callbackParentProcess, childProcessStartInfo.ParentProcessId);

            _autoResetEvent.WaitOne();
        }

        private static void CheckProcess(int processId)
        {
            try
            {
                var process = Process.GetProcessById(processId);
                process.WaitForExit();
            }
            catch (ArgumentException e)
            {
                throw new BlendedLibraryServiceException("Could not wait for Blended Library service process to exit.", e);
            }

            _autoResetEvent.Set();
        }

        private static ParentalTerminationProcess StartServer(int port, int eventPort, bool interactive, string workingDirectory)
        {
            var javaHome = GetJavaHome();

            if (Logger.IsInfoEnabled)
                Logger.Info("Found Java home at {0}", javaHome);

            Logger.Info("Working DIR: " + workingDirectory);

            var arguments = string.Format("-jar basexFS.jar");

            var startInfo = new ProcessStartInfo
            {
                WorkingDirectory = workingDirectory,
                FileName = Path.Combine(javaHome, "bin", "java.exe"),
                Arguments = arguments,
                CreateNoWindow = !interactive,
                UseShellExecute = interactive,
                RedirectStandardError = !interactive,
                RedirectStandardInput = !interactive,
                RedirectStandardOutput = !interactive
            };

            try
            {
                return ParentalTerminationProcess.Start(startInfo);
            }
            catch (Exception e)
            {
                if (Logger.IsErrorEnabled)
                    Logger.ErrorException("Could not start Blended Library service process", e);

                throw new BlendedLibraryServiceException("Error while starting Blended Library service process", e);
            }
        }

        private static string GetJavaHome()
        {
            const string jrePath = "SOFTWARE\\JavaSoft\\Java Runtime Environment";
            const string jre32Path = "SOFTWARE\\Wow6432Node\\JavaSoft\\Java Runtime Environment";

            var javaKey = RegistryHelper.OpenLocalMachineRegistryKey(jrePath) ??
                          RegistryHelper.OpenLocalMachineRegistryKey(jre32Path);

            if (javaKey == null)
            {
                if (Logger.IsErrorEnabled)
                    Logger.Error("Could not open Java registry key on local machine. JRE_KEY={0} or JRE_32_KEY={1}", jrePath, jre32Path);

                throw new BlendedLibraryServiceException("Could not find Java installed on the local machine. Please install Java Runtime Environment.");
            }

            var javaVersion = javaKey.GetValue("CurrentVersion").ToString();

            var jreKey = javaKey.OpenSubKey(javaVersion);

            if (jreKey == null)
                throw new BlendedLibraryServiceException("Could not find Java installed on the local machine. Please install Java Runtime Environment.");

            try
            {
                return jreKey.GetValue("JavaHome").ToString();
            }
            catch (Exception e)
            {
                throw new BlendedLibraryServiceException("Could not find Java installed on the local machine. Please install Java Runtime Environment.", e);
            }
        }
    }
}
