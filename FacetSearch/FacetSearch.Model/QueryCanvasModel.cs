﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using BlendedLibrary.Common.Model;

namespace BlendedLibrary.FacetSearch.Model
{
    public class QueryCanvasModel
    {
        private readonly double _canvasWidth;
        private double _canvasHeight;

        #region properties

        #region NodeCount

        public int NodeCount
        {
            get { return _Nodes.Count; }
        }

        #endregion

        #endregion

        public QueryCanvasModel(double canvasWidth, double canvasHeight)
        {
            _canvasWidth = canvasWidth;
            _canvasHeight = canvasHeight;
            Links = new ReadOnlyCollection<QueryCanvasLink>(_Links);
            Nodes = new ReadOnlyCollection<QueryCanvasNode>(_Nodes);
        }

        #region private fields

        private readonly List<QueryCanvasLink> _Links = new List<QueryCanvasLink>();
        private readonly List<QueryCanvasNode> _Nodes = new List<QueryCanvasNode>();

        private readonly List<QueryCanvasLink> _addedLinks = new List<QueryCanvasLink>();
        private readonly List<QueryCanvasNode> _addedNodes = new List<QueryCanvasNode>();
        private readonly List<QueryCanvasLink> _removedLinks = new List<QueryCanvasLink>();
        private readonly List<QueryCanvasNode> _removedNodes = new List<QueryCanvasNode>();

        private readonly object _modelLock = new object();

        #endregion

        #region internal fields

        internal readonly ReadOnlyCollection<QueryCanvasLink> Links;
        internal readonly ReadOnlyCollection<QueryCanvasNode> Nodes;

        #endregion

        #region public methods

        #region Scenario I (Adding a single node to the canvas)

        public void AddNode(QueryCanvasNode node)
        {
            IEnumerable<QueryCanvasLink> removedLinks;
            IEnumerable<QueryCanvasNode> removedNodes;
            IEnumerable<QueryCanvasLink> addedLinks;
            IEnumerable<QueryCanvasNode> addedNodes;

            lock (_modelLock)
            {
                AddNodeInternal(node);

                removedLinks = _removedLinks.ToArray();
                removedNodes = _removedNodes.ToArray();
                addedLinks = _addedLinks.ToArray();
                addedNodes = _addedNodes.ToArray();

                ResetLists();
            }

            RaiseEvents(removedLinks, removedNodes, addedLinks, addedNodes);
        }

        #endregion

        #region Scenario II (Create an ExitLink)

        public void AddExitLink(QueryCanvasNode node, Position exitLinkDestinationPosition)
        {
            if (node == null)
                throw new ArgumentNullException("node");

            IEnumerable<QueryCanvasLink> removedLinks;
            IEnumerable<QueryCanvasNode> removedNodes;
            IEnumerable<QueryCanvasLink> addedLinks;
            IEnumerable<QueryCanvasNode> addedNodes;

            lock (_modelLock)
            {
                AddExitLinkInternal(node, exitLinkDestinationPosition);

                removedLinks = _removedLinks.ToArray();
                removedNodes = _removedNodes.ToArray();
                addedLinks = _addedLinks.ToArray();
                addedNodes = _addedNodes.ToArray();

                ResetLists();
            }

            RaiseEvents(removedLinks, removedNodes, addedLinks, addedNodes);
        }

        #endregion

        #region Scenario III (Connect an Exit Link to another item)

        public void TurnExitLinkIntoLink(QueryCanvasExitLink exitLink, QueryCanvasNode target, Position connectionPoint)
        {
            if (exitLink == null)
                throw new ArgumentNullException("exitLink");

            if (target == null)
                throw new ArgumentNullException("target");


            IEnumerable<QueryCanvasLink> removedLinks;
            IEnumerable<QueryCanvasNode> removedNodes;
            IEnumerable<QueryCanvasLink> addedLinks;
            IEnumerable<QueryCanvasNode> addedNodes;

            lock (_modelLock)
            {
                var source = exitLink.Source;

                ConnectToNode(source, target as QueryCanvasNode);

                removedLinks = _removedLinks.ToArray();
                removedNodes = _removedNodes.ToArray();
                addedLinks = _addedLinks.ToArray();
                addedNodes = _addedNodes.ToArray();

                ResetLists();
            }

            RaiseEvents(removedLinks, removedNodes, addedLinks, addedNodes);
        }

        #endregion

        #region Scenario IV (Place a node onto a link)

        public void PlaceNodeOntoLink(QueryCanvasNode node, QueryCanvasLink target)
        {
            if (node == null)
                throw new ArgumentNullException("node");

            if (target == null)
                throw new ArgumentNullException("target");

            IEnumerable<QueryCanvasLink> removedLinks;
            IEnumerable<QueryCanvasNode> removedNodes;
            IEnumerable<QueryCanvasLink> addedLinks;
            IEnumerable<QueryCanvasNode> addedNodes;

            lock (_modelLock)
            {
                if (node is QueryCanvasResultNode)
                {
                    if (target is QueryCanvasExitLink)
                        PlaceResultNodeOntoExitLink(node as QueryCanvasResultNode, target as QueryCanvasExitLink);
                    else
                        PlaceResultNodeOntoNormalLink(node as QueryCanvasResultNode, target);
                }
                else
                {
                    if (target is QueryCanvasExitLink)
                        PlaceNodeOntoExitLink(node, target as QueryCanvasExitLink);
                    else
                        PlaceNodeOntoNormalLink(node, target);
                }
                removedLinks = _removedLinks.ToArray();
                removedNodes = _removedNodes.ToArray();
                addedLinks = _addedLinks.ToArray();
                addedNodes = _addedNodes.ToArray();

                ResetLists();
            }

            RaiseEvents(removedLinks, removedNodes, addedLinks, addedNodes);
        }

        #endregion

        #region Scenario V (Delete a link)

        public void DeleteLink(QueryCanvasLink link)
        {
            if (link == null)
                throw new ArgumentNullException("link");

            IEnumerable<QueryCanvasLink> removedLinks;
            IEnumerable<QueryCanvasNode> removedNodes;
            IEnumerable<QueryCanvasLink> addedLinks;
            IEnumerable<QueryCanvasNode> addedNodes;

            lock (_modelLock)
            {
                if (link is QueryCanvasExitLink)
                    DeleteExitLink(link as QueryCanvasExitLink);
                else
                    DeleteNormalLink(link);

                removedLinks = _removedLinks.ToArray();
                removedNodes = _removedNodes.ToArray();
                addedLinks = _addedLinks.ToArray();
                addedNodes = _addedNodes.ToArray();

                ResetLists();
            }

            RaiseEvents(removedLinks, removedNodes, addedLinks, addedNodes);
        }

        #endregion

        #region Scenario VI (Release a link from its target)

        public void TurnLinkIntoExitLink(QueryCanvasLink link, Position exitLinkDestinationPosition)
        {
            if (link == null)
                throw new ArgumentNullException("link");

            IEnumerable<QueryCanvasLink> removedLinks;
            IEnumerable<QueryCanvasNode> removedNodes;
            IEnumerable<QueryCanvasLink> addedLinks;
            IEnumerable<QueryCanvasNode> addedNodes;

            lock (_modelLock)
            {
                TurnNormalLinkIntoExitLink(link, exitLinkDestinationPosition);

                removedLinks = _removedLinks.ToArray();
                removedNodes = _removedNodes.ToArray();
                addedLinks = _addedLinks.ToArray();
                addedNodes = _addedNodes.ToArray();

                ResetLists();
            }

            RaiseEvents(removedLinks, removedNodes, addedLinks, addedNodes);
        }

        #endregion

        #region Scenario VII (Remove a node from the canvas)

        public void RemoveNode(QueryCanvasNode node)
        {
            if (node == null)
                throw new ArgumentNullException("node");

            IEnumerable<QueryCanvasLink> removedLinks;
            IEnumerable<QueryCanvasNode> removedNodes;
            IEnumerable<QueryCanvasLink> addedLinks;
            IEnumerable<QueryCanvasNode> addedNodes;

            lock (_modelLock)
            {
                RemoveNodeInternal(node);

                //if we have no ingoing links
                if (node.IngoingLinks.Count == 0)
                {
                    foreach (var outgoingLink in node.OutgoingLinks.ToArray())
                    {
                        if (outgoingLink is QueryCanvasExitLink)
                            RemoveExitLinkInternal(node);
                        else
                        {
                            RemoveLinkInternal(outgoingLink);
                        }
                    }
                }
                //if we have ingoing links
                else
                {

                    //if we have no outgoing links
                    if (node.OutgoingLinks.Count == 0)
                    {
                        //we give the source node of every ingoing link an exit link and delete the ingoing link
                        foreach (var ingoingLink in node.IngoingLinks.ToArray())
                        {
                            RemoveLinkInternal(ingoingLink);
                            AddExitLinkInternal(ingoingLink.Source, GetOutOfScreenPosition(ingoingLink.Source.Position, node.Position));
                        }
                    }
                    //if the only outgoing link we have, is an exit link
                    else if (node.OutgoingLinks.Count == 1 && node.OutgoingLinks.Single() == node.ExitLink)
                    {
                        //we delete the exit link of the node
                        RemoveExitLinkInternal(node);

                        //we give the source node of every ingoing link an exit link and delete the ingoing link
                        foreach (var ingoingLink in node.IngoingLinks.ToArray())
                        {
                            RemoveLinkInternal(ingoingLink);
                            AddExitLinkInternal(ingoingLink.Source, GetOutOfScreenPosition(ingoingLink.Source.Position, node.Position));
                        }
                    }
                    else
                    {
                        //if we have an exit link, we delete it (as leaving it in would cause to much visual clutter)
                        if (node.ExitLink != null)
                            RemoveExitLinkInternal(node);

                        //we have to reconnect the source node of all ingoing links to the destination node of all outgoing links
                        //we have to take care that the proper links are created (normal vs. or links)

                        var ingoingLinks = node.IngoingLinks.ToArray();
                        var outgoingLinks = node.OutgoingLinks.ToArray();

                        foreach (var ingoingLink in ingoingLinks)
                        {
                            var source = ingoingLink.Source;

                            //RemoveLinkInternal(ingoingLink);

                            foreach (var outgoingLink in outgoingLinks)
                            {
                                var destination = outgoingLink.Destination;

                                //RemoveLinkInternal(outgoingLink);

                                AddLinkInternal(new QueryCanvasLink(source, destination));
                            }
                        }

                        foreach (var link in ingoingLinks.Union(outgoingLinks))
                            RemoveLinkInternal(link);
                    }
                }

                removedLinks = _removedLinks.ToArray();
                removedNodes = _removedNodes.ToArray();
                addedLinks = _addedLinks.ToArray();
                addedNodes = _addedNodes.ToArray();

                ResetLists();
            }

            RaiseEvents(removedLinks, removedNodes, addedLinks, addedNodes);
        }

        #endregion

        #region Cycle Test

        public bool WouldResultInCycle(QueryCanvasExitLink exitLink, QueryCanvasItem target)
        {
            lock (_modelLock)
            {
                if (target is QueryCanvasNode)
                    return IsNodeInOutgoingLinks(target as QueryCanvasNode, exitLink.Source);

                if (target is QueryCanvasLink)
                    return IsNodeInOutgoingLinks((target as QueryCanvasLink).Destination, exitLink.Source);

                return false;
            }
        }

        private static bool IsNodeInOutgoingLinks(QueryCanvasNode startNode, QueryCanvasNode node)
        {
            return startNode.OutgoingLinks.Any(outgoingLink => outgoingLink.Destination == node) || startNode.OutgoingLinks.Any(outgoingLink => IsNodeInOutgoingLinks(outgoingLink.Destination, node));
        }

        #endregion

        #region Other

        public bool ContainsNode(long tokenId)
        {
            return _Nodes.OfType<QueryCanvasNode>().Any(node => node.TokenId == tokenId);
        }

        public QueryCanvasNode GetNode(long value)
        {
            return _Nodes.Single(node => node.TokenId == value);
        }

        public bool HasNode(long id)
        {
            return _Nodes.Any(node => node.TokenId == id);
        }

        #endregion

        #endregion

        #region private methods

        #region Basic Node Management

        private void AddNodeInternal(QueryCanvasNode node)
        {
            if (_Nodes.Contains(node))
                throw new InvalidOperationException("Can not add a node that is already part of the model");
            if (node == null)
                throw new ArgumentNullException("node");
            //add out of screen nodes, but not nodes with the same id as a node that is already present
            if (!(node is QueryCanvasOutOfScreenNode))
            {
                if (_Nodes.Exists(n => n.TokenId == node.TokenId))
                    throw new InvalidOperationException("Node with this ID is already part of the model");
            }
            node.CanvasModel = this;
            _Nodes.Add(node);

            _addedNodes.Add(node);
        }

        private void RemoveNodeInternal(QueryCanvasNode node)
        {
            if (!_Nodes.Contains(node))
                throw new InvalidOperationException("Can not remove a node that is not part of the model");
            if (node == null)
                throw new ArgumentNullException("node");
            _removedNodes.Add(node);

            _Nodes.Remove(node);
        }

        #endregion

        #region Basic Link Management

        private void AddExitLinkInternal(QueryCanvasNode node, Position exitLinkDestinationPosition)
        {
            //ignore adding of more than one exitLink
            if (node.ExitLink != null)
                return;
            node.ExitLink = new QueryCanvasExitLink(node, exitLinkDestinationPosition);

            AddNodeInternal(node.ExitLink.Destination);
            AddLinkInternal(node.ExitLink);
        }

        private void AddLinkInternal(QueryCanvasLink link)
        {
            if (_Links.Contains(link))
                throw new InvalidOperationException("link can not added as it is already part of the model");
            if (link.Destination.IngoingLinks.ToList().Exists(incomingLink => incomingLink.Source.Equals(link.Source)))
                return; // ignore, because the nodes are already linked
            link.CanvasModel = this;
            _Links.Add(link);
            link.Source.OutgoingLinks.Add(link);
            link.Destination.IngoingLinks.Add(link);
            link.GlobalExpression = link.Source.GlobalExpression;
            link.Destination.GlobalExpressionChanged(link.GlobalExpression, link);

            _addedLinks.Add(link);
        }

        private void RemoveLinkInternal(QueryCanvasLink link)
        {
            //error if the link was already removed
            if (!_Links.Contains(link))
                return; // ignore, since the link is not part of the model, thos must already have been removed.
            _removedLinks.Add(link);

            _Links.Remove(link);
            link.Source.OutgoingLinks.Remove(link);
            link.Destination.IngoingLinks.Remove(link);
            link.Destination.GlobalExpressionChanged("", link);
        }

        private void RemoveExitLinkInternal(QueryCanvasNode node)
        {
            if (!_Links.Contains(node.ExitLink))
#if DEBUG
                // TODO find out when this happens and remove the unnecessary removal
                throw new InvalidOperationException("Exitlink can not be removed as it is not part of the model");
#else
                return;
#endif

            RemoveNodeInternal(node.ExitLink.Destination);
            RemoveLinkInternal(node.ExitLink);
            node.ExitLink = null;
        }

        #endregion

        #region Scenario III (Connect an Exit Link to another item)

        private void ConnectToNode(QueryCanvasNode source, QueryCanvasNode target)
        {
            RemoveExitLinkInternal(source);

            var newLink = new QueryCanvasLink(source, target);
            AddLinkInternal(newLink);
        }

        #endregion

        #region Scenario IV (Place a node onto a link)

        private void PlaceNodeOntoNormalLink(QueryCanvasNode node, QueryCanvasLink target)
        {
            //1. delete the old link
            RemoveLinkInternal(target);

            //2. add the new node
            AddNodeInternal(node);

            //3. add a link between the link's source node and the new node
            var newLink1 = new QueryCanvasLink(target.Source, node);
            AddLinkInternal(newLink1);

            //4. add a link between the new node and the link's target node
            var newLink2 = new QueryCanvasLink(node, target.Destination);
            AddLinkInternal(newLink2);
        }


        private void PlaceNodeOntoExitLink(QueryCanvasNode node, QueryCanvasExitLink target)
        {
            var source = target.Source;

            //1. delete the old exit link
            RemoveExitLinkInternal(source);

            //2. add the new node
            AddNodeInternal(node);

            //3. connect the source node with the new node
            var sourceToNodeLink = new QueryCanvasLink(source, node);
            AddLinkInternal(sourceToNodeLink);

            //4. create an exit link for the new node
            AddExitLinkInternal(node, new Position(target.Destination.Position.X, target.Destination.Position.Y));
        }

        private void PlaceResultNodeOntoNormalLink(QueryCanvasResultNode node, QueryCanvasLink target)
        {
            //1. delete the old link
            RemoveLinkInternal(target);

            //2. add the new node
            AddNodeInternal(node);

            //3. add a link between the link's source node and the new node
            var newLink1 = new QueryCanvasLink(target.Source, node);
            AddLinkInternal(newLink1);
        }


        private void PlaceResultNodeOntoExitLink(QueryCanvasResultNode node, QueryCanvasExitLink target)
        {
            var source = target.Source;

            //1. delete the old exit link
            RemoveExitLinkInternal(source);

            //2. add the new node
            AddNodeInternal(node);

            //3. connect the source node with the new node
            var sourceToNodeLink = new QueryCanvasLink(source, node);
            AddLinkInternal(sourceToNodeLink);
        }

        #endregion

        #region Scenario V (Delete a link)

        private void DeleteNormalLink(QueryCanvasLink link)
        {

            RemoveLinkInternal(link);

        }

        private void DeleteExitLink(QueryCanvasExitLink link)
        {
            RemoveExitLinkInternal(link.Source);
        }

        #endregion

        #region Scenario VI (Release a link from its target)

        private void TurnNormalLinkIntoExitLink(QueryCanvasLink link, Position exitLinkDestinationPosition)
        {
            RemoveLinkInternal(link);

            //if we already have an exit link, we have to reposition its destination, otherwise, we have to create a new one

            if (link.Source.ExitLink != null)
            {
                link.Source.ExitLink.Destination.Position.X = exitLinkDestinationPosition.X;
                link.Source.ExitLink.Destination.Position.Y = exitLinkDestinationPosition.Y;
            }
            else
                AddExitLinkInternal(link.Source, exitLinkDestinationPosition);
        }

        #endregion

        private void RaiseEvents(IEnumerable<QueryCanvasLink> removedLinks, IEnumerable<QueryCanvasNode> removedNodes, IEnumerable<QueryCanvasLink> addedLinks, IEnumerable<QueryCanvasNode> addedNodes)
        {
            foreach (var oldNode in removedNodes)
                RaiseNodeRemovedEvent(oldNode);

            foreach (var oldLink in removedLinks)
                RaiseLinkRemovedEvent(oldLink);

            foreach (var newNode in addedNodes)
                RaiseNodeAddedEvent(newNode);

            foreach (var newLink in addedLinks)
                RaiseLinkAddedEvent(newLink);
        }

        private void ResetLists()
        {
            _addedNodes.Clear();
            _addedLinks.Clear();
            _removedNodes.Clear();
            _removedLinks.Clear();
        }

        private Position GetOutOfScreenPosition(Position sourcePosition, Position destinationPosition)
        {
            var diff = destinationPosition - sourcePosition;
            var angle = Math.Atan2(diff.Y, diff.X) + Math.PI;
            return new Position(sourcePosition.X + Math.Cos(angle) * _canvasWidth, sourcePosition.Y + Math.Sin(angle) * _canvasWidth);
        }

        #endregion

        #region public events

        public event EventHandler<QueryCanvasItemEventArgs<QueryCanvasLink>> LinkAdded;
        public event EventHandler<QueryCanvasItemEventArgs<QueryCanvasLink>> LinkRemoved;
        public event EventHandler<QueryCanvasItemEventArgs<QueryCanvasNode>> NodeAdded;
        public event EventHandler<QueryCanvasItemEventArgs<QueryCanvasNode>> NodeRemoved;

        private void RaiseLinkAddedEvent(QueryCanvasLink link)
        {
            if (LinkAdded != null)
                LinkAdded(this, new QueryCanvasItemEventArgs<QueryCanvasLink>(link));
        }

        private void RaiseLinkRemovedEvent(QueryCanvasLink link)
        {
            if (LinkRemoved != null)
                LinkRemoved(this, new QueryCanvasItemEventArgs<QueryCanvasLink>(link));
        }

        private void RaiseNodeAddedEvent(QueryCanvasNode node)
        {
            if (NodeAdded != null)
                NodeAdded(this, new QueryCanvasItemEventArgs<QueryCanvasNode>(node));
        }

        private void RaiseNodeRemovedEvent(QueryCanvasNode node)
        {
            if (NodeRemoved != null)
                NodeRemoved(this, new QueryCanvasItemEventArgs<QueryCanvasNode>(node));
        }

        #endregion

        #region public methods

        public void Clear()
        {
            foreach (var link in _Links)
                link.Dispose();

            _Links.Clear();

            foreach (var node in _Nodes)
                node.Dispose();

            _Nodes.Clear();
        }

        #endregion

        #region Model Validity Implementation

        public bool IsValid()
        {
            if (_Links.Any(link => !link.IsValid()))
                return false;

            if (_Nodes.Any(node => !node.IsValid()))
                return false;

            return true;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Clear();
            }
        }

        #endregion
    }
}
