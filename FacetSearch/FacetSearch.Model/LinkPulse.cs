﻿using GalaSoft.MvvmLight;

namespace BlendedLibrary.FacetSearch.Model
{
    public class LinkPulse : ObservableObject
    {
        #region public properties

        #region Speed

        /// <summary>
        /// The <see cref="Speed" /> property's name.
        /// </summary>
        public const string SpeedPropertyName = "Speed";

        private double _speed = 1.0;

        /// <summary>
        /// Sets and gets the Speed property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double Speed
        {
            get
            {
                return _speed;
            }

            set
            {
                if (_speed == value)
                {
                    return;
                }

                _speed = value;
                RaisePropertyChanged(SpeedPropertyName);
            }
        }

        #endregion

        #region Radius

        /// <summary>
        /// The <see cref="Radius" /> property's name.
        /// </summary>
        public const string RadiusPropertyName = "Radius";

        private double _radius = 0.0;

        /// <summary>
        /// Sets and gets the Radius property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double Radius
        {
            get
            {
                return _radius;
            }

            set
            {
                if (_radius == value)
                {
                    return;
                }

                _radius = value;
                RaisePropertyChanged(RadiusPropertyName);
            }
        }

        #endregion

        #endregion
    }
}
