﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BlendedLibrary.Common.Model;

namespace BlendedLibrary.FacetSearch.Model
{
    public class QueryCanvasNode : QueryCanvasItem
    {
        #region ctor

        public QueryCanvasNode(Position position)
            : this(position, 0.0, -1)
        {
        }

        public QueryCanvasNode(Position position, double angle, long tokenId)
        {
            Position = position;
            TokenId = tokenId;
            Angle = angle;

            Position.PropertyChanged += (sender, e) =>
                                            {
                                                if ("X".Equals(e.PropertyName) || "Y".Equals(e.PropertyName))
                                                    RaisePropertyChanged("Position");
                                            };

            OutgoingLinks = new List<QueryCanvasLink>();
            IngoingLinks = new List<QueryCanvasLink>();
        }

        #endregion

        #region public properties

        public long TokenId { get; private set; }

        public Position Position { get; private set; }

        public double Angle { get; set; }

        public QueryCanvasExitLink ExitLink { get; internal set; }

        public virtual ICollection<QueryCanvasLink> OutgoingLinks { get; private set; }

        public ICollection<QueryCanvasLink> IngoingLinks { get; private set; }

        #region WillBeDeletedSoon

        private bool _willBeDeletedSoon;

        public bool WillBeDeletedSoon
        {
            get { return _willBeDeletedSoon; }
            set
            {
                _willBeDeletedSoon = value;
                RaisePropertyChanged("WillBeDeletedSoon");
            }
        }

        #endregion

        #region LocalExpression

        private String _localExpression;

        /// <summary>
        /// Gets or sets the LocalExpression property. This observable property 
        /// indicates
        /// </summary>
        public String LocalExpression
        {
            get { return _localExpression; }
            set
            {
                if (_localExpression != value)
                {
                    String old = _localExpression;
                    _localExpression = value;
                    OnLocalExpressionChanged(old, value);
                    RaisePropertyChanged("LocalExpression");
                }
            }
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the LocalExpression property.
        /// </summary>
        protected virtual void OnLocalExpressionChanged(String oldLocalExpression, String newLocalExpression)
        {
            SignalExpressionChanged();
        }

        #endregion

        #endregion

        #region public methods

        public void AddExitLink(Position exitLinkDestinationPosition)
        {
            if (CanvasModel == null)
                return;

            CanvasModel.AddExitLink(this, exitLinkDestinationPosition);
        }

        #endregion

        internal override void GlobalExpressionChanged(string globalExpression, QueryCanvasItem sender)
        {
            SignalExpressionChanged();
        }

        protected virtual void SignalExpressionChanged()
        {
            if (CanvasModel == null)
                return;

            // Ingoing links will be null when application shuts down.
            if (IngoingLinks != null)
            {
                var ingoingExpressions = IngoingLinks.Select(item => item.GlobalExpression);

                GlobalExpression = BuildQuery(ingoingExpressions.ToArray(), LocalExpression, "or");

                foreach (var link in OutgoingLinks)
                    link.GlobalExpressionChanged(GlobalExpression, this);
            }
        }

        internal virtual string BuildQuery(string[] subQueries, string separator)
        {
            if (subQueries.Length == 0)
                return null;

            var sb = new StringBuilder();

            var lastSubQuery = subQueries.Last();
            foreach (var subQuery in subQueries.Take(subQueries.Length))
            {
                if (!string.IsNullOrWhiteSpace(subQuery))
                {
                    sb.Append(subQuery);
                    sb.Append(" ");
                    if (subQuery != lastSubQuery)
                    {
                        sb.Append(separator);
                        sb.Append(" ");
                    }
                }
            }
            return sb.ToString().Trim();
        }

        internal virtual string BuildQuery(string[] subQueries, string localQuery, string separator)
        {
            if (separator == "or")
                subQueries = subQueries.Where(q => !string.IsNullOrWhiteSpace(q)).ToArray();

            if (localQuery == null)
                return null;

            var sb = new StringBuilder();

            if (subQueries.Any())
            {
                var lastSubQuery = subQueries.Last();
                foreach (var subQuery in subQueries.Take(subQueries.Length))
                {
                    sb.Append("(");
                    if (!string.IsNullOrWhiteSpace(subQuery))
                    {
                        sb.Append(subQuery);

                    }
                    //concatenate every query with the local query
                    if (!string.IsNullOrWhiteSpace(subQuery) && !string.IsNullOrWhiteSpace(localQuery))
                    {
                        sb.Append(" and ");
                    }
                    if (!string.IsNullOrWhiteSpace(localQuery))
                    {
                        sb.Append(localQuery);
                    }
                    sb.Append(") ");
                    //prepare for next one, if i was not the last
                    if (subQuery != lastSubQuery)
                    {
                        sb.Append(separator);
                        sb.Append(" ");
                    }
                }
            }
            else
            {
                sb.Append(string.IsNullOrWhiteSpace(localQuery) ? "" : localQuery);
            }

            return sb.ToString().Trim();
        }

        #region IDisposable implementation

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Position = null;
                ExitLink = null;
                OutgoingLinks = null;
                IngoingLinks = null;
            }
        }

        #endregion

        #region Model Validity Implementation

        internal override bool IsValid()
        {
            return OutgoingLinks.All(link => link != null);
        }

        internal override bool IsValidAfterDisposing()
        {
            return (Position == null) && (ExitLink == null) && (OutgoingLinks == null) && (IngoingLinks == null);
        }

        #endregion
    }
}
