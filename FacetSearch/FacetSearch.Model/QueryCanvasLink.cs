﻿using System;
using BlendedLibrary.Common.Model;

namespace BlendedLibrary.FacetSearch.Model
{
    public class QueryCanvasLink : QueryCanvasItem
    {
        public QueryCanvasLink(QueryCanvasNode source, QueryCanvasNode destination)
        {
            Source = source;
            Destination = destination;
        }

        #region Source

        private QueryCanvasNode _source;

        /// <summary>
        /// Gets or sets the Source property. This observable property 
        /// indicates
        /// </summary>
        public QueryCanvasNode Source
        {
            get { return _source; }
            set
            {
                if (_source != value)
                {
                    QueryCanvasNode old = _source;
                    _source = value;
                    OnSourceChanged(old, value);
                    RaisePropertyChanged("Source");
                }
            }
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Source property.
        /// </summary>
        protected virtual void OnSourceChanged(QueryCanvasNode oldSource, QueryCanvasNode newSource)
        {

        }

        #endregion

        #region Destination

        private QueryCanvasNode _destination;

        /// <summary>
        /// Gets or sets the Destination property. This observable property 
        /// indicates
        /// </summary>
        public QueryCanvasNode Destination
        {
            get { return _destination; }
            set
            {
                if (_destination != value)
                {
                    QueryCanvasNode old = _destination;
                    _destination = value;
                    OnDestinationChanged(old, value);
                    RaisePropertyChanged("Destination");
                }
            }
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Destination property.
        /// </summary>
        protected virtual void OnDestinationChanged(QueryCanvasNode oldDestination, QueryCanvasNode newDestination)
        {
        }

        #endregion

        public void TurnIntoExitLink(Position exitLinkDestinationPosition)
        {
            if (CanvasModel != null)
                CanvasModel.TurnLinkIntoExitLink(this, exitLinkDestinationPosition);
        }

        internal override void GlobalExpressionChanged(String globalExpression, QueryCanvasItem sender)
        {
            if (sender != Source)
                throw new InvalidOperationException("sender must be source of link");

            GlobalExpression = globalExpression;

            Destination.GlobalExpressionChanged(globalExpression, sender);
        }

        #region IDisposable implementation

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Source = null;
                Destination = null;
            }
        }

        #endregion

        public virtual bool IsValidConnectCandidate(QueryCanvasItem item)
        {
            if (item is QueryCanvasNode)
                return true;
            return false;
        }

        #region Model Validity Implementation

        internal override bool IsValid()
        {
            return (Source != null) && (Destination != null);
        }

        internal override bool IsValidAfterDisposing()
        {
            return (Source == null) && (Destination == null);
        }

        #endregion
    }
}
