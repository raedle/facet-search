﻿using System.Linq;
using BlendedLibrary.Common.Model;

namespace BlendedLibrary.FacetSearch.Model
{
    public class QueryCanvasExitLink : QueryCanvasLink
    {
        public QueryCanvasExitLink(QueryCanvasNode source, Position exitLinkDestinationPosition)
            : base(source, new QueryCanvasOutOfScreenNode(exitLinkDestinationPosition))
        {

        }

        internal override void GlobalExpressionChanged(string globalExpression, QueryCanvasItem sender)
        {
            GlobalExpression = globalExpression;
        }

        public void EstablishConnection(QueryCanvasNode item, Position connectionPoint)
        {
            if (CanvasModel == null)
                return;

            CanvasModel.TurnExitLinkIntoLink(this, item, connectionPoint);
        }

        public override bool IsValidConnectCandidate(QueryCanvasItem item)
        {
            if (!base.IsValidConnectCandidate(item))
                return false;

            // we do not allow cycles TODO: EXCEPTION HANDLING
            if (CanvasModel == null)
                return false;

            return !CanvasModel.WouldResultInCycle(this, item);
        }
    }
}
