﻿using System;
using System.Linq;
using System.Text;
using BlendedLibrary.Common.Model;

namespace BlendedLibrary.FacetSearch.Model
{
    public class QueryCanvasResultNode : QueryCanvasNode
    {
        #region properties

        #region ResultExpression

        private String _resultExpression;

        /// <summary>
        /// Gets or sets the ResultExpression property. This observable property 
        /// indicates
        /// </summary>
        public String ResultExpression
        {
            get
            {
                return _resultExpression;
            }
            set
            {
                if (_resultExpression != value)
                {
                    String old = _resultExpression;
                    _resultExpression = value;
                    OnResultExpressionChanged(old, value);
                    RaisePropertyChanged("ResultExpression");
                }
            }
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the ResultExpression property.
        /// </summary>
        protected virtual void OnResultExpressionChanged(String oldResultExpression, String newResultExpression)
        {
        }

        #endregion

        #endregion

        public QueryCanvasResultNode(Position position, double angle, long tokenId)
            : base(position, angle, tokenId)
        {
        }

        protected override void SignalExpressionChanged()
        {
            var ingoingExpressions = IngoingLinks.Select(item => item.GlobalExpression);
            ResultExpression = BuildQuery(ingoingExpressions.ToArray(), "or");

            //base.SignalExpressionChanged();
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the GlobalExpression property.
        /// </summary>
        protected override void OnGlobalExpressionChanged(string oldGlobalExpression, string newGlobalExpression)
        {
            // ignore
        }
    }
}
