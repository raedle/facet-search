﻿using System;
using System.ComponentModel;

namespace BlendedLibrary.FacetSearch.Model
{
    public class QueryCanvasItem : INotifyPropertyChanged, IDisposable
    {

        internal virtual void GlobalExpressionChanged(String globalExpression, QueryCanvasItem sender)
        {
            throw new NotImplementedException();
        }

        internal QueryCanvasModel CanvasModel { get; set; }

        #region GlobalExpression

        private String _globalExpression;

        /// <summary>
        /// Gets or sets the GlobalExpression property. This observable property 
        /// indicates
        /// </summary>
        public String GlobalExpression
        {
            get
            {
                return _globalExpression;
            }
            set
            {
                if (_globalExpression != value)
                {
                    String old = _globalExpression;
                    _globalExpression = value;
                    OnGlobalExpressionChanged(old, value);
                    RaisePropertyChanged("GlobalExpression");
                }
            }
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the GlobalExpression property.
        /// </summary>
        protected virtual void OnGlobalExpressionChanged(String oldGlobalExpression, String newGlobalExpression)
        {
        }

        #endregion

        #region Model Validity Implementation

        internal virtual bool IsValid()
        {
            throw new NotImplementedException();
        }

        internal virtual bool IsValidAfterDisposing()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {

        }

        #endregion
    }
}
