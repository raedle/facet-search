﻿using System;

namespace BlendedLibrary.FacetSearch.Model
{
    public class QueryCanvasItemEventArgs<T> : EventArgs where T:QueryCanvasItem
    {
        public QueryCanvasItemEventArgs(T item)
        {
            Item = item;
        }

        public T Item
        {
            get;
            private set;
        }
    }
}
