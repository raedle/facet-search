﻿using BlendedLibrary.Common.Model;

namespace BlendedLibrary.FacetSearch.Model
{
    public class QueryCanvasFacetNode : QueryCanvasNode
    {
        public QueryCanvasFacetNode(Position position, double angle, long tokenId)
            : base(position, angle, tokenId)
        {
        }
    }
}
