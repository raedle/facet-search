﻿using BlendedLibrary.Common.Model;

namespace BlendedLibrary.FacetSearch.Model
{
    public class QueryCanvasOutOfScreenNode : QueryCanvasNode
    {
        public QueryCanvasOutOfScreenNode(Position position)
            : base(position)
        {
        }

        internal override void GlobalExpressionChanged(string globalExpression, QueryCanvasItem sender)
        {
            //Just do nothing
        }

        protected override void OnLocalExpressionChanged(string oldLocalExpression, string newLocalExpression)
        {
            //Just do nothing
        }

        protected override void SignalExpressionChanged()
        {
            //Just do nothing
        }
    }
}
