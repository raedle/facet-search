﻿using System.Linq;
using System.Text;
using BlendedLibrary.Common.Model;

namespace BlendedLibrary.FacetSearch.Model
{
    public class QueryCanvasTextNode : QueryCanvasNode
    {
        #region properties

        #region Text

        /// <summary>
        /// The <see cref="Text" /> property's name.
        /// </summary>
        public const string TextPropertyName = "Text";

        private string _text = string.Empty;

        /// <summary>
        /// Sets and gets the Text property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string Text
        {
            get
            {
                return _text;
            }

            set
            {
                if (_text == value)
                {
                    return;
                }

                _text = value;
                RaisePropertyChanged(TextPropertyName);
            }
        }

        #endregion

        #endregion

        public QueryCanvasTextNode(Position position, double angle, long tokenId)
            : base(position, angle, tokenId)
        {
        }

        internal override string BuildQuery(string[] subQueries, string separator)
        {
            if (separator == "and")
                subQueries = subQueries.Where(q => !string.IsNullOrEmpty(q)).ToArray();

            if (subQueries.Length == 0)
                return null;

            var sb = new StringBuilder();

            sb.Append("(");

            foreach (var subQuery in subQueries.Take(subQueries.Length - 1))
            {
                if (string.IsNullOrWhiteSpace(subQuery))
                    continue;

                sb.Append(subQuery);
                sb.Append(" ");
                sb.Append(separator);
                sb.Append(" ");
            }

            var lastSubQuery = subQueries.Last();
            if (!string.IsNullOrWhiteSpace(lastSubQuery))
                sb.Append(lastSubQuery);

            sb.Append(")");

            return "()".Equals(sb.ToString()) ? null : sb.ToString();
        }

        internal override string BuildQuery(string[] subQueries, string localQuery, string separator)
        {
            if (string.IsNullOrWhiteSpace(localQuery) && subQueries.Length == 0)
                return null;

            if (separator == "or")
                subQueries = subQueries.Where(q => !string.IsNullOrEmpty(q)).ToArray();

            if (localQuery == null)
                return null;

            var sb = new StringBuilder();

            sb.Append("(");

            if (subQueries.Length > 0)
            {
                foreach (var subQuery in subQueries)
                {
                    if (string.IsNullOrWhiteSpace(subQuery))
                        continue;

                    sb.Append("((");
                    sb.Append(subQuery);
                    sb.Append(") and (");
                    sb.Append(localQuery);
                    sb.Append(")) ");
                    sb.Append(separator);
                    sb.Append(" ");
                }

                if (sb.Length > 0)
                {
                    var length = (separator.Length + 2);
                    sb.Remove(sb.Length - length, length);
                }
            }
            else
            {
                sb.Append(localQuery);
            }

            sb.Append(")");

            return sb.ToString();
        }
    }
}
