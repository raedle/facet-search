﻿using System;
using System.Linq;
using BlendedLibrary.Common.Model;
using BlendedLibrary.FacetSearch.Model;
using NUnit.Framework;

namespace BlendedLibrary.FacetStreams.Model.Tests
{
    [TestFixture]
    public class SimpleModelTest : AbstractCanvasModelTest
    {
        [Test]
        public void DisposingModelWorks()
        {
            var links = Model.Links.ToArray();
            var nodes = Model.Nodes.ToArray();

            Model.Dispose();

            Assert.IsEmpty(Model.Links);
            Assert.IsEmpty(Model.Nodes);

            foreach (var link in links)
                Assert.IsTrue(link.IsValidAfterDisposing());

            foreach (var node in nodes)
                Assert.IsTrue(node.IsValidAfterDisposing());
        }

        [Test]
        public void AddingNodeWorks()
        {
            Model.AddNode(new QueryCanvasNode(new Position(0, 0)));

            Assert.AreEqual(Model.Nodes.Count, 1);
        }

        [Test]
        public void ContainsNodeTest()
        {
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);

            //Test

            Model.AddNode(node1);
            Model.AddNode(node2);
            Model.RemoveNode(node2);

            //Assertion

            Assert.That(Model.ContainsNode(1));
            Assert.That(!Model.ContainsNode(2));

        }

        [Test]
        public void GetNodeTest()
        {
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);

            Model.AddNode(node1);
            Model.AddNode(node2);
            Model.RemoveNode(node2);

            //Test

            Assert.That(Model.GetNode(1) == node1);
            Assert.Throws<InvalidOperationException>(() => Model.GetNode(2));

        }

    }
}
