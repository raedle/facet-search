﻿using System;
using System.Linq;
using BlendedLibrary.Common.Model;
using BlendedLibrary.FacetSearch.Model;
using NUnit.Framework;

namespace BlendedLibrary.FacetStreams.Model.Tests
{
    [TestFixture]
    public class AddSingleNodeTest : AbstractCanvasModelTest
    {
        [Test]
        public void AddSingleNode()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 0);

            //Test

            Model.AddNode(node1);

            //Assertion
            Assert.IsTrue(Model.Links.Count == 0);
            Assert.IsTrue(Model.Nodes.Count == 1);

            Assert.IsTrue(Model.Nodes.OfType<QueryCanvasNode>().Count() == 1);

            Assert.IsTrue(node1.IngoingLinks.Count == 0);
            Assert.IsTrue(node1.OutgoingLinks.Count == 0);

            Assert.IsNull(node1.ExitLink);

            Assert.IsTrue(Model.IsValid());
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void AddASingleNodeTwiceTest()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 0);

            Model.AddNode(node1);

            //Test

            Model.AddNode(node1);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void AddANodeIDTwiceTest()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 0);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 0);

            Model.AddNode(node1);

            //Test

            Model.AddNode(node2);
        }
    }
}
