﻿using System.Linq;
using BlendedLibrary.Common.Model;
using BlendedLibrary.FacetSearch.Model;
using NUnit.Framework;

namespace BlendedLibrary.FacetStreams.Model.Tests
{
    [TestFixture]
    public class ConnectionTest : AbstractCanvasModelTest
    {

        [Test]
        public void ConnectNodeWithExitLinkWorks()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);

            Model.AddNode(node1);
            Model.AddNode(node2);

            node1.AddExitLink(new Position(0,0));
            node2.AddExitLink(new Position(0,0));

            //Test
            Model.TurnExitLinkIntoLink(node1.ExitLink, node2, new Position(0, 0));

            //Assertion

            Assert.AreEqual(2, Model.Links.Count);
            Assert.AreEqual(3, Model.Nodes.Count);

            Assert.IsTrue(node1.OutgoingLinks.Count == 1);

            var link1 = node1.OutgoingLinks.First();

            Assert.IsTrue(link1.Source == node1);
            Assert.IsTrue(link1.Destination == node2);

            Assert.AreEqual(1, node2.OutgoingLinks.Count);

            var link2 = node2.OutgoingLinks.First();

            Assert.IsTrue(link2.Source == node2);
            Assert.IsTrue(link2 is QueryCanvasExitLink);

            Assert.IsTrue(node2.ExitLink is QueryCanvasExitLink);
            Assert.IsTrue(node2.ExitLink.Source == node2);

            Assert.IsTrue(node2.ExitLink.Destination is QueryCanvasOutOfScreenNode);

            Assert.IsTrue(Model.IsValid());
        }

        [Test]
        public void ConnectNodeWithNode()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);

            Model.AddNode(node1);
            Model.AddNode(node2);

            node1.AddExitLink(new Position(0,0));

            //Test

            Model.TurnExitLinkIntoLink(node1.ExitLink, node2, new Position(0, 0));

            //Assertion

            Assert.IsTrue(Model.Links.Count == 1);
            Assert.IsTrue(Model.Nodes.Count == 2);

            Assert.IsTrue(Model.Nodes.OfType<QueryCanvasNode>().Count() == 2);

            Assert.IsTrue(node1.IngoingLinks.Count == 0);
            Assert.IsTrue(node1.OutgoingLinks.Count == 1);

            Assert.IsNull(node1.ExitLink);

            Assert.IsTrue(node1.OutgoingLinks.First().GetType() != typeof(QueryCanvasExitLink));

            Assert.IsTrue(node1.OutgoingLinks.First().Destination == node2);

            Assert.IsTrue(node2.IngoingLinks.Count == 1);
            Assert.IsTrue(node2.OutgoingLinks.Count == 0);

            Assert.IsTrue(node2.IngoingLinks.First().GetType() != typeof(QueryCanvasExitLink));

            Assert.IsTrue(node2.IngoingLinks.Count == 1);

            Assert.IsTrue(node2.IngoingLinks.First().Source == node1);

            Assert.IsNull(node2.ExitLink);

            Assert.IsTrue(Model.IsValid());
        }

        [Test]
        public void ConnectNodeWithNormalLink()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);
            var node3 = new QueryCanvasNode(new Position(0, 0), 0, 3);

            Model.AddNode(node1); 
            Model.AddNode(node2);
            Model.AddNode(node3);

            node1.AddExitLink(new Position(0,0));

            node2.AddExitLink(new Position(0,0));
            Model.TurnExitLinkIntoLink(node2.ExitLink, node3, new Position(0, 0));

            //Test

            Model.TurnExitLinkIntoLink(node1.ExitLink, node3, new Position(0, 0));

            //Assertion

            Assert.AreEqual(2, Model.Links.Count);
            Assert.AreEqual(3, Model.Nodes.Count);

            Assert.AreEqual(3, Model.Nodes.OfType<QueryCanvasNode>().Count());

            Assert.IsTrue(node1.IngoingLinks.Count == 0);
            Assert.IsTrue(node1.OutgoingLinks.Count == 1);

            Assert.IsNull(node1.ExitLink);

            Assert.IsTrue(node1.OutgoingLinks.First().GetType() == typeof(QueryCanvasLink));


            Assert.IsTrue(node2.IngoingLinks.Count == 0);
            Assert.IsTrue(node2.OutgoingLinks.Count == 1);

            Assert.IsNull(node2.ExitLink);

            Assert.IsTrue(node2.OutgoingLinks.First().GetType() == typeof(QueryCanvasLink));


            Assert.IsTrue(node3.IngoingLinks.Count == 2);
            Assert.IsTrue(node3.OutgoingLinks.Count == 0);

            Assert.IsNull(node3.ExitLink);

            Assert.IsTrue(node3.IngoingLinks.First().GetType() == typeof(QueryCanvasLink));

            Assert.IsTrue(Model.IsValid());
        }

        [Test]
        public void ConnectionThatWouldResultInACycleMustBeRejectedTest()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);
            var node3 = new QueryCanvasNode(new Position(0, 0), 0, 3);

            Model.AddNode(node1);
            Model.AddNode(node2);
            Model.AddNode(node3);

            node1.AddExitLink(new Position(0, 0));

            node2.AddExitLink(new Position(0, 0));

            node3.AddExitLink(new Position(0, 0));

            Model.TurnExitLinkIntoLink(node1.ExitLink, node2, new Position(0, 0));
            Model.TurnExitLinkIntoLink(node2.ExitLink, node3, new Position(0, 0));

            //Test
            if(node3.ExitLink.IsValidConnectCandidate(node1))
                Model.TurnExitLinkIntoLink(node3.ExitLink, node1, new Position(0, 0));

            //Assertions

            Assert.AreNotEqual(node3.OutgoingLinks.First().Destination, node1);
            Assert.That(node3.OutgoingLinks.First() is QueryCanvasExitLink);
            Assert.That(node3.ExitLink is QueryCanvasExitLink);
            Assert.AreEqual(4, Model.Nodes.Count);
            Assert.AreEqual(3, Model.Links.Count);
                
        }
    }
}
