﻿using System.Linq;
using BlendedLibrary.Common.Model;
using BlendedLibrary.FacetSearch.Model;
using NUnit.Framework;

namespace BlendedLibrary.FacetStreams.Model.Tests
{
    [TestFixture]
    public class DeleteLinkTest : AbstractCanvasModelTest
    {
      
      

        [Test]
        public void DeleteNormalLink()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);

            Model.AddNode(node1);
            Model.AddNode(node2);

            node1.AddExitLink(new Position(0,0));
            Model.TurnExitLinkIntoLink(node1.ExitLink, node2, new Position(0, 0));

            //Test

            Model.DeleteLink(node1.OutgoingLinks.First());

            //Assertion

            Assert.IsTrue(Model.Links.Count == 0);
            Assert.IsTrue(Model.Nodes.Count == 2);

            Assert.IsTrue(Model.Nodes.OfType<QueryCanvasNode>().Count() == 2);

            Assert.IsTrue(node1.IngoingLinks.Count == 0);
            Assert.IsTrue(node1.OutgoingLinks.Count == 0);

            Assert.IsNull(node1.ExitLink);

            Assert.IsTrue(node2.IngoingLinks.Count == 0);
            Assert.IsTrue(node2.OutgoingLinks.Count == 0);

            Assert.IsNull(node2.ExitLink);

            Assert.IsTrue(Model.IsValid());
        }

        [Test]
        public void DeleteExitLink()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 0);

            Model.AddNode(node1);

            node1.AddExitLink(new Position(0,0));

            //Test

            Model.DeleteLink(node1.ExitLink);

            //Assertion

            Assert.IsTrue(Model.Links.Count == 0);
            Assert.IsTrue(Model.Nodes.Count == 1);

            Assert.IsTrue(Model.Nodes.OfType<QueryCanvasNode>().Count() == 1);

            Assert.IsTrue(node1.IngoingLinks.Count == 0);
            Assert.IsTrue(node1.OutgoingLinks.Count == 0);

            Assert.IsNull(node1.ExitLink);

            Assert.IsTrue(Model.IsValid());
        }
    }
}
