﻿using System.Linq;
using BlendedLibrary.Common.Model;
using BlendedLibrary.FacetSearch.Model;
using NUnit.Framework;

namespace BlendedLibrary.FacetStreams.Model.Tests
{
    [TestFixture]
    public class CreateExitLinkTest : AbstractCanvasModelTest
    {
        [Test]
        public void CreateExitLink()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 0);

            Model.AddNode(node1);

            //Test

            node1.AddExitLink(new Position(0, 0));

            //Assertion
            Assert.IsTrue(Model.Links.Count == 1);
            Assert.IsTrue(Model.Nodes.Count == 2);

            Assert.AreEqual(2, Model.Nodes.OfType<QueryCanvasNode>().Count());
            Assert.AreEqual(1, Model.Nodes.OfType<QueryCanvasOutOfScreenNode>().Count());
            Assert.AreEqual(1, Model.Links.OfType<QueryCanvasExitLink>().Count());
            Assert.AreEqual(1, Model.Links.Count());

            Assert.IsTrue(node1.IngoingLinks.Count == 0);
            Assert.IsTrue(node1.OutgoingLinks.Count == 1);

            Assert.IsTrue(node1.OutgoingLinks.OfType<QueryCanvasExitLink>().Count() == 1);

            Assert.IsNotNull(node1.ExitLink);
            Assert.True(node1.ExitLink.Destination is QueryCanvasOutOfScreenNode);

            Assert.IsTrue(Model.IsValid());
        }

        [Test]
        public void CreateExitLinkAgain()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 0);

            Model.AddNode(node1);
            node1.AddExitLink(new Position(0, 0));

            //Test

           node1.AddExitLink(new Position(0, 0));

            //Assertion
            Assert.IsTrue(Model.Links.Count == 1);
            Assert.IsTrue(Model.Nodes.Count == 2);

            Assert.AreEqual(2, Model.Nodes.OfType<QueryCanvasNode>().Count());
            Assert.AreEqual(1, Model.Nodes.OfType<QueryCanvasOutOfScreenNode>().Count());
            Assert.AreEqual(1, Model.Links.OfType<QueryCanvasExitLink>().Count());
            Assert.AreEqual(1, Model.Links.Count());

            Assert.IsTrue(node1.IngoingLinks.Count == 0);
            Assert.IsTrue(node1.OutgoingLinks.Count == 1);

            Assert.IsTrue(node1.OutgoingLinks.OfType<QueryCanvasExitLink>().Count() == 1);

            Assert.IsNotNull(node1.ExitLink);
            Assert.True(node1.ExitLink.Destination is QueryCanvasOutOfScreenNode);

            Assert.IsTrue(Model.IsValid());
        }
    }
}
