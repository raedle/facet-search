﻿using System.Linq;
using BlendedLibrary.Common.Model;
using BlendedLibrary.Common.Util;
using BlendedLibrary.FacetSearch.Model;
using NUnit.Framework;

namespace BlendedLibrary.FacetStreams.Model.Tests
{
    [TestFixture]
    public class QueryTest : AbstractCanvasModelTest
    {
        [Test]
        public void SimpleQueryTest()
        {
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var resultNode = new QueryCanvasResultNode(new Position(0, 0), 0, 99);

            Model.AddNode(node1);
            Model.AddNode(resultNode);

            node1.AddExitLink(new Position(0, 0));
            Model.TurnExitLinkIntoLink(node1.ExitLink, resultNode, new Position(0, 0));

            var queryPart = "First";
            var finishedQuery = queryPart;
            var resultQuery = queryPart;

            //Test

            node1.LocalExpression = queryPart;

            //Assertions

            Assert.AreEqual(finishedQuery, node1.GlobalExpression);
            Assert.AreEqual(resultQuery, resultNode.ResultExpression);

        }

        [Test]
        public void AndQueryTest()
        {
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);
            var resultNode = new QueryCanvasResultNode(new Position(0, 0), 0, 99);

            Model.AddNode(node1);
            Model.AddNode(resultNode);

            node1.AddExitLink(new Position(0, 0));
            Model.TurnExitLinkIntoLink(node1.ExitLink, resultNode, new Position(0, 0));
            Model.PlaceNodeOntoLink(node2, node1.OutgoingLinks.First());

            var queryPart1 = "First";
            var queryPart2 = "Second";
            var finishedQuery1 = queryPart1;
            var finishedQuery2 = "(" + finishedQuery1 + " and " + queryPart2 + ")";
            var resultQuery = finishedQuery2;

            //Test

            node1.LocalExpression = queryPart1;
            node2.LocalExpression = queryPart2;

            //Assertions

            Assert.AreEqual(finishedQuery1, node1.GlobalExpression);
            Assert.AreEqual(finishedQuery2, node2.GlobalExpression);
            Assert.AreEqual(resultQuery, resultNode.ResultExpression);

        }

        [Test]
        public void OrQueryTest()
        {
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);
            var resultNode = new QueryCanvasResultNode(new Position(0, 0), 0, 99);

            Model.AddNode(node1);
            Model.AddNode(node2);
            Model.AddNode(resultNode);

            node1.AddExitLink(new Position(0, 0));
            Model.TurnExitLinkIntoLink(node1.ExitLink, resultNode, new Position(0, 0));
            node2.AddExitLink(new Position(0, 0));
            Model.TurnExitLinkIntoLink(node2.ExitLink, resultNode, new Position(0, 0));

            var queryPart1 = "First";
            var queryPart2 = "Second";
            var finishedQuery1 = queryPart1;
            var finishedQuery2 = queryPart2;
            var resultQuery = finishedQuery1 + " or " + finishedQuery2;

            //Test

            node1.LocalExpression = queryPart1;
            node2.LocalExpression = queryPart2;

            //Assertions

            Assert.AreEqual(finishedQuery1, node1.GlobalExpression);
            Assert.AreEqual(finishedQuery2, node2.GlobalExpression);
            Assert.AreEqual(resultQuery, resultNode.ResultExpression);
        }

        [Test]
        public void OrQueryWithTextTest()
        {
            var node1 = new QueryCanvasTextNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasTextNode(new Position(0, 0), 0, 2);
            var resultNode = new QueryCanvasResultNode(new Position(0, 0), 0, 99);

            Model.AddNode(node1);
            Model.AddNode(node2);
            Model.AddNode(resultNode);

            node1.AddExitLink(new Position(0, 0));
            Model.TurnExitLinkIntoLink(node1.ExitLink, resultNode, new Position(0, 0));
            node2.AddExitLink(new Position(0, 0));
            Model.TurnExitLinkIntoLink(node2.ExitLink, resultNode, new Position(0, 0));

            var queryPart1 = SolrQueryUtility.Escape("Hello + World");
            var queryPart2 = SolrQueryUtility.Escape("Chip && Chap");
            var finishedQuery1 = "(" + queryPart1 + ")";
            var finishedQuery2 = "(" + queryPart2 + ")";
            var resultQuery = finishedQuery1 + " or " + finishedQuery2;

            //Test
            node1.LocalExpression = queryPart1;
            node2.LocalExpression = queryPart2;

            //Assertions

            Assert.AreEqual(finishedQuery1, node1.GlobalExpression);
            Assert.AreEqual(finishedQuery2, node2.GlobalExpression);
            Assert.AreEqual(resultQuery, resultNode.ResultExpression);
        }

        [Test]
        public void OrQueryTestWithoutResultNode()
        {
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);
            var node3 = new QueryCanvasNode(new Position(0, 0), 0, 99);

            Model.AddNode(node1);
            Model.AddNode(node2);
            Model.AddNode(node3);

            node1.AddExitLink(new Position(0, 0));
            Model.TurnExitLinkIntoLink(node1.ExitLink, node3, new Position(0, 0));
            node2.AddExitLink(new Position(0, 0));
            Model.TurnExitLinkIntoLink(node2.ExitLink, node3, new Position(0, 0));

            var queryPart1 = "First";
            var queryPart2 = "Second";
            var queryPart3 = "Third";
            var finishedQuery1 = queryPart1;
            var finishedQuery2 = queryPart2;
            var resultQuery = "(" + finishedQuery1 + " and " + queryPart3 + ") or (" + finishedQuery2 + " and " + queryPart3 + ")";

            //Test

            node1.LocalExpression = queryPart1;
            node2.LocalExpression = queryPart2;
            node3.LocalExpression = queryPart3;

            //Assertions

            Assert.AreEqual(finishedQuery1, node1.GlobalExpression);
            Assert.AreEqual(finishedQuery2, node2.GlobalExpression);
            Assert.AreEqual(resultQuery, node3.GlobalExpression);

        }
    }
}
