﻿using System.Linq;
using BlendedLibrary.Common.Model;
using BlendedLibrary.FacetSearch.Model;
using NUnit.Framework;

namespace BlendedLibrary.FacetStreams.Model.Tests
{
    [TestFixture]
    public class ReplaceLinkTest : AbstractCanvasModelTest
    {
        [Test]
        public void ReplaceNormalLinkWithExitLink()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);

            Model.AddNode(node1);
            Model.AddNode(node2);

            node1.AddExitLink(new Position(0, 0));

            Model.TurnExitLinkIntoLink(node1.ExitLink, node2, new Position(0, 0));

            //Test

            Model.TurnLinkIntoExitLink(node1.OutgoingLinks.First(), new Position(0, 0));

            //Assertion

            Assert.IsTrue(Model.Links.Count == 1);
            Assert.IsTrue(Model.Nodes.Count == 3);

            Assert.AreEqual(3, Model.Nodes.OfType<QueryCanvasNode>().Count());
            Assert.AreEqual(1, Model.Nodes.OfType<QueryCanvasOutOfScreenNode>().Count());

            Assert.AreEqual(0, node1.IngoingLinks.Count);
            Assert.AreEqual(1, node1.OutgoingLinks.Count);

            Assert.AreEqual(1, node1.OutgoingLinks.OfType<QueryCanvasExitLink>().Count());

            Assert.IsNotNull(node1.ExitLink);
            Assert.IsTrue(node1.ExitLink.Destination is QueryCanvasOutOfScreenNode);

            Assert.AreEqual(0, node2.IngoingLinks.Count);
            Assert.AreEqual(0, node2.OutgoingLinks.Count);

            Assert.IsTrue(Model.IsValid());
        }

        [Test]
        public void ReplaceNormalLinkWithExitLinkWithExitLinkAlreadyPresent()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);

            Model.AddNode(node1);
            Model.AddNode(node2);

            node1.AddExitLink(new Position(0, 0));

            Model.TurnExitLinkIntoLink(node1.ExitLink, node2, new Position(0, 0));

            node1.AddExitLink(new Position(0, 0));

            //Test

            Model.TurnLinkIntoExitLink(node1.OutgoingLinks.First(), new Position(0, 0));

            //Assertion

            Assert.IsTrue(Model.Links.Count == 1);
            Assert.IsTrue(Model.Nodes.Count == 3);

            Assert.AreEqual(3, Model.Nodes.OfType<QueryCanvasNode>().Count());
            Assert.AreEqual(1, Model.Nodes.OfType<QueryCanvasOutOfScreenNode>().Count());

            Assert.AreEqual(0, node1.IngoingLinks.Count);
            Assert.AreEqual(1, node1.OutgoingLinks.Count);

            Assert.AreEqual(1, node1.OutgoingLinks.OfType<QueryCanvasExitLink>().Count());

            Assert.IsNotNull(node1.ExitLink);
            Assert.IsTrue(node1.ExitLink.Destination is QueryCanvasOutOfScreenNode);

            Assert.AreEqual(0, node2.IngoingLinks.Count);
            Assert.AreEqual(0, node2.OutgoingLinks.Count);

            Assert.IsTrue(Model.IsValid());
        }
    }
}
