﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using BlendedLibrary.Common.Model;
using BlendedLibrary.FacetSearch.Model;
using NUnit.Framework;

namespace BlendedLibrary.FacetStreams.Model.Tests
{
    [TestFixture]
    public class RemoveNodeTest : AbstractCanvasModelTest
    {
        [Test]
        public void RemoveNodeWithNoIngoingLinksButExitLink()
        {
            //Initializing

            var nodeA = new QueryCanvasNode(new Position(0, 0), 0, 0);

            Model.AddNode(nodeA);

            nodeA.AddExitLink(new Position(0, 0));

            //Test

            Model.RemoveNode(nodeA);

            //Assertion

            Assert.IsTrue(Model.Nodes.Count == 0);
            Assert.IsTrue(Model.Links.Count == 0);

            Assert.IsTrue(Model.IsValid());
        }

        [Test]
        public void RemoveNodeWithNoIngoingLinksButOutgoingLinksToAndNode()
        {
            //Initializing

            var nodeA = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 2);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 3);

            Model.AddNode(nodeA);
            Model.AddNode(node1);
            Model.AddNode(node2);

            nodeA.AddExitLink(new Position(0, 0));
            Model.TurnExitLinkIntoLink(nodeA.ExitLink, node1, new Position(0, 0));

            nodeA.AddExitLink(new Position(0, 0));
            Model.TurnExitLinkIntoLink(nodeA.ExitLink, node2, new Position(0, 0));

            //Test

            Model.RemoveNode(nodeA);

            //Assertion

            Assert.IsTrue(Model.Nodes.Count == 2);
            Assert.IsTrue(Model.Nodes.OfType<QueryCanvasNode>().Count() == 2);
            Assert.IsTrue(Model.Links.Count == 0);

            foreach (QueryCanvasNode andNode in Model.Nodes.OfType<QueryCanvasNode>())
            {
                Assert.IsTrue(andNode.IngoingLinks.Count == 0);
                Assert.IsTrue(andNode.OutgoingLinks.Count == 0);
                Assert.IsNull(andNode.ExitLink);
            }

            Assert.IsTrue(Model.IsValid());
        }


        [Test]
        public void RemoveNodeWithNoIngoingLinksAndNoOutgoingLinks()
        {
            //Initializing

            var nodeA = new QueryCanvasNode(new Position(0, 0), 0, 0);

            Model.AddNode(nodeA);

            //Test

            Model.RemoveNode(nodeA);

            //Assertion

            Assert.IsTrue(Model.Nodes.Count == 0);
            Assert.IsTrue(Model.Links.Count == 0);

            Assert.IsTrue(Model.IsValid());
        }



        [Test]
        public void RemoveNodeWithIngoingLinkButNoOutgoingLink()
        {
            //Initializing

            var nodeA = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 2);

            Model.AddNode(nodeA);
            Model.AddNode(node1);

            node1.AddExitLink(new Position(0, 0));



            Model.TurnExitLinkIntoLink(node1.ExitLink, nodeA, new Position(0, 0));

            //Test

            Model.RemoveNode(nodeA);

            //Assertion

            Assert.IsTrue(Model.Nodes.Count == 2);
            Assert.IsTrue(Model.Links.Count == 1);

            Assert.IsTrue(Model.Nodes.OfType<QueryCanvasOutOfScreenNode>().Count() == 1);

            Assert.IsTrue(Model.Links.OfType<QueryCanvasExitLink>().Count() == 1);

            Assert.IsTrue(node1.IngoingLinks.Count == 0);
            Assert.IsTrue(node1.OutgoingLinks.Count == 1);

            Assert.IsNotNull(node1.ExitLink);

            Assert.IsTrue(node1.OutgoingLinks.First().GetType() == typeof(QueryCanvasExitLink));

            Assert.IsTrue(node1.OutgoingLinks.First().Destination is QueryCanvasOutOfScreenNode);

            Assert.IsTrue(Model.IsValid());
        }

        [Test]
        public void RemoveNodeWithIngoingLinkButNoOutgoingLinkAndOnlyOneExitLink()
        {
            //Initializing

            var nodeA = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 2);

            Model.AddNode(nodeA);
            Model.AddNode(node1);

            node1.AddExitLink(new Position(0, 0));
            nodeA.AddExitLink(new Position(0, 0));



            Model.TurnExitLinkIntoLink(node1.ExitLink, nodeA, new Position(0, 0));

            //Test

            Model.RemoveNode(nodeA);

            //Assertion

            Assert.IsTrue(Model.Nodes.Count == 2);
            Assert.IsTrue(Model.Links.Count == 1);

            Assert.IsTrue(Model.Nodes.OfType<QueryCanvasOutOfScreenNode>().Count() == 1);

            Assert.IsTrue(Model.Links.OfType<QueryCanvasExitLink>().Count() == 1);

            Assert.IsTrue(node1.IngoingLinks.Count == 0);
            Assert.IsTrue(node1.OutgoingLinks.Count == 1);

            Assert.IsNotNull(node1.ExitLink);

            Assert.IsTrue(node1.OutgoingLinks.First().GetType() == typeof(QueryCanvasExitLink));

            Assert.IsTrue(node1.OutgoingLinks.First().Destination is QueryCanvasOutOfScreenNode);

            Assert.IsTrue(Model.IsValid());
        }


        [Test]
        public void RemoveAlreadyRemovedNode()
        {
            //Initializing

            var nodeA = new QueryCanvasNode(new Position(0, 0), 0, 0);
         

            Model.AddNode(nodeA);
            Model.RemoveNode(nodeA);
          
            //Test

            Assert.Throws<InvalidOperationException>(() => Model.RemoveNode(nodeA));

            //Assertion

            Assert.IsTrue(Model.Nodes.Count == 0);
            Assert.IsTrue(Model.Links.Count == 0);

            Assert.IsTrue(Model.Nodes.OfType<QueryCanvasOutOfScreenNode>().Count() == 0);

            Assert.IsTrue(Model.Links.OfType<QueryCanvasExitLink>().Count() == 0);

            Assert.IsTrue(Model.IsValid());
        }

        [Test]
        public void RemoveNodeFromMassiveNetworkTest()
        {
            //Initializing

            QueryCanvasNode node;
           
            var nodeList = new List<QueryCanvasNode>();
            for (int i = 1; i <= 10 ; i++)
            {
                node = new QueryCanvasNode(new Position(0, 0), 0, i);
                Model.AddNode(node);
                foreach (var node2 in nodeList)
                {
                    node.AddExitLink(new Position(0, 0));
                    Model.TurnExitLinkIntoLink(node.ExitLink, node2, new Position(0, 0));
                }
                nodeList.Add(node);
            }

            Assert.AreEqual(10, Model.Nodes.Count);
            Assert.AreEqual(45, Model.Links.Count);
            Assert.AreEqual(0, Model.Nodes.OfType<QueryCanvasOutOfScreenNode>().Count());
            Assert.AreEqual(0, Model.Links.OfType<QueryCanvasExitLink>().Count());

            //Test

            Model.RemoveNode(nodeList.First());

            //Assertion

            Assert.AreEqual(18, Model.Nodes.Count);
            Assert.AreEqual(45, Model.Links.Count);
            Assert.AreEqual(9, Model.Nodes.OfType<QueryCanvasOutOfScreenNode>().Count());
            Assert.AreEqual(9, Model.Links.OfType<QueryCanvasExitLink>().Count());

            //Test

            Model.RemoveNode(nodeList.Last());

            //Assertion

            Assert.AreEqual(16, Model.Nodes.Count);
            Assert.AreEqual(36, Model.Links.Count);
            Assert.AreEqual(8, Model.Nodes.OfType<QueryCanvasOutOfScreenNode>().Count());
            Assert.AreEqual(8, Model.Links.OfType<QueryCanvasExitLink>().Count());
        }

        [Test]

        public void MultithreadedNodeAddAndRemove()
        {
            //TODO Finish
            Thread thread1;
            Thread thread2;
            thread1 = new Thread(addNodes);
            thread2 = new Thread(removeNodes);
            // Test

            thread1.Start();
            //thread2.Start();
            thread1.Join();
            //thread2.Join();
            //Assert
            Assert.AreEqual(2, Model.Nodes.Count);
            Assert.AreEqual(1, Model.Links.Count);
            Assert.AreEqual(0, Model.Nodes.OfType<QueryCanvasOutOfScreenNode>().Count());
            Assert.AreEqual(0, Model.Links.OfType<QueryCanvasExitLink>().Count());
            Assert.AreEqual(Model.GetNode(1), Model.GetNode(2).IngoingLinks.First().Source);

        }

        private void addNodes()
        {
            for (int i = 0; i < 100; i++)
            {
                Debug.WriteLine("Adding 2 connected Nodes");
                var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
                var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);

                //throws exception, but is not handeled, since it happens in a thread
                Model.AddNode(node1);
                Model.AddNode(node2);
                Model.AddExitLink(node1, new Position(0, 0));
                Model.TurnExitLinkIntoLink(node1.ExitLink, node2, new Position(0, 0));
                Thread.Sleep(10);
            }
        }

        private void removeNodes()
        {
            for (int i = 0; i < 100; i++)
            {
                Debug.WriteLine("removing node with token id 1");
                var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
                Model.RemoveNode(node1);
                Thread.Sleep(10);
            }
        }
    }
}
