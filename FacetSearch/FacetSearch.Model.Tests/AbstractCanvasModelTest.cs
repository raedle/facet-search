﻿using BlendedLibrary.FacetSearch.Model;
using NUnit.Framework;

namespace BlendedLibrary.FacetStreams.Model.Tests
{
    [TestFixture]
    public abstract class AbstractCanvasModelTest
    {

        protected QueryCanvasModel Model;

        [SetUp]
        public void SetUpTest()
        {
            Model = new QueryCanvasModel(250, 250);
        }

        [TearDown]
        public void TearDownTest()
        {
            Model = null;
        }

    }
}
