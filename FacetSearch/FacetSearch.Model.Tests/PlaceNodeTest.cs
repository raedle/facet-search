﻿using System.Linq;
using BlendedLibrary.Common.Model;
using BlendedLibrary.FacetSearch.Model;
using NUnit.Framework;

namespace BlendedLibrary.FacetStreams.Model.Tests
{
    [TestFixture]
    public class PlaceNodeTest : AbstractCanvasModelTest
    {
        [Test]
        public void PlaceNodeOntoExitLink()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);

            Model.AddNode(node1);

            node1.AddExitLink(new Position(0,0));

            //Test

            Model.PlaceNodeOntoLink(node2, node1.ExitLink);

            //Assertion

            Assert.IsTrue(Model.Links.Count == 2);
            Assert.IsTrue(Model.Nodes.Count == 3);

            Assert.IsTrue(Model.Nodes.OfType<QueryCanvasOutOfScreenNode>().Count() == 1);

            Assert.IsTrue(node1.IngoingLinks.Count == 0);
            Assert.IsNotNull(node1.OutgoingLinks.First());
            Assert.IsTrue(node1.OutgoingLinks.Count == 1);

            Assert.IsNull(node1.ExitLink);

            Assert.IsTrue(node1.OutgoingLinks.First().GetType() != typeof(QueryCanvasExitLink));

            Assert.IsTrue(node1.OutgoingLinks.First().Destination == node2);

            Assert.IsTrue(node2.IngoingLinks.Count == 1);
            Assert.IsTrue(node2.OutgoingLinks.Count == 1);

            Assert.IsTrue(node2.IngoingLinks.First().GetType() != typeof(QueryCanvasExitLink));

            Assert.IsTrue(node2.IngoingLinks.Count == 1);

            Assert.IsTrue(node2.IngoingLinks.First().Source == node1);

            Assert.IsNotNull(node2.ExitLink);

            Assert.IsTrue(node2.ExitLink.Destination is QueryCanvasOutOfScreenNode);

            Assert.IsTrue(Model.IsValid());
        }

        [Test]
        public void PlaceNodeOntoNormalLink()
        {
            //Initializing
            var node1 = new QueryCanvasNode(new Position(0, 0), 0, 1);
            var node2 = new QueryCanvasNode(new Position(0, 0), 0, 2);
            var node3 = new QueryCanvasNode(new Position(0, 0), 0, 3);

            Model.AddNode(node1);
            Model.AddNode(node2);

            node1.AddExitLink(new Position(0,0));
            Model.TurnExitLinkIntoLink(node1.ExitLink, node2, new Position(0, 0));
            
            //Test

            Model.PlaceNodeOntoLink(node3, node1.OutgoingLinks.First());

            //Assertion

            Assert.AreEqual(Model.Links.Count, 2);
            Assert.AreEqual(Model.Nodes.Count, 3);

            Assert.IsTrue(Model.Nodes.OfType<QueryCanvasNode>().Count() == 3);

            Assert.IsTrue(node1.IngoingLinks.Count == 0);
            Assert.IsTrue(node1.OutgoingLinks.Count == 1);

            Assert.IsNull(node1.ExitLink);

            Assert.IsTrue(node1.OutgoingLinks.First().GetType() != typeof(QueryCanvasExitLink));

            Assert.IsTrue(node1.OutgoingLinks.First().Destination == node3);

            Assert.IsTrue(node3.IngoingLinks.Count == 1);
            Assert.IsTrue(node3.OutgoingLinks.Count == 1);

            Assert.IsNull(node3.ExitLink);

            Assert.IsTrue(node3.IngoingLinks.First().GetType() != typeof(QueryCanvasExitLink));

            Assert.IsTrue(node3.IngoingLinks.First().Source == node1);

            Assert.IsTrue(node3.OutgoingLinks.First().GetType() != typeof(QueryCanvasExitLink));

            Assert.IsTrue(node3.OutgoingLinks.First().Destination == node2);

            Assert.IsTrue(node2.OutgoingLinks.Count == 0);
            Assert.IsTrue(node2.IngoingLinks.Count == 1);

            Assert.IsNull(node2.ExitLink);

            Assert.IsTrue(node2.IngoingLinks.First().GetType() != typeof(QueryCanvasExitLink));

            Assert.IsTrue(node2.IngoingLinks.First().Source == node3);

            Assert.IsTrue(Model.IsValid());
        }
    }
}
