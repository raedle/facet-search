﻿using System;
using BlendedLibrary.Common.Model;
using BlendedLibrary.FacetSearch.Model;
using NUnit.Framework;

namespace BlendedLibrary.FacetStreams.Model.Tests
{
    [TestFixture]
    public class SimpleNodeTest : AbstractCanvasModelTest
    {

        [Test]
        public void SingleNodeIsValid()
        {
            Model.AddNode(new QueryCanvasNode(new Position(0, 0)));

            Assert.IsTrue(Model.IsValid());
        }

        [Test]
        public void TwoNodesAreValid()
        {
            Model.AddNode(new QueryCanvasNode(new Position(0, 0), 0, 1));
            Model.AddNode(new QueryCanvasNode(new Position(0, 0), 0, 2));

            Assert.IsTrue(Model.IsValid());
        }

        [Test]
        public void TwoUnequalQueryCanvasOutOfScreenNodeAreUnequal()
        {
            var exitNode1 = new QueryCanvasOutOfScreenNode(new Position(0, 0));
            var exitNode2 = new QueryCanvasOutOfScreenNode(new Position(0, 0));

            Assert.IsFalse(exitNode1.Equals(exitNode2));
        }

        [Test]
        public void AddingExistingNodeThrows()
        {
            var node = new QueryCanvasNode(new Position(0, 0));

            Assert.Throws<InvalidOperationException>(() =>
            {

                Model.AddNode(node);
                Model.AddNode(node);

            });
        }

        [Test]
        public void DisposingNodeWorks()
        {
            var node = new QueryCanvasNode(new Position(0, 0));

            node.Dispose();
            
            Assert.IsNull(node.OutgoingLinks);
            Assert.IsNull(node.Position);
            Assert.IsNull(node.ExitLink);

            Assert.IsTrue(node.IsValidAfterDisposing());
        }

    }
}
