﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FacetStreams.Data.Xml
{
    class FacetEntry
    {
        private String name;
        private String query;

        public FacetEntry(string n, string q)
        {
            name = n;
            query = q;
        }
    }
}
