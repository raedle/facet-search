﻿using BlendedLibrary.Data;

namespace BlendedLibrary.FacetStreams.Data.Xml
{
    class TextFacet : Facet
    {
        public TextFacet(string name, string screen, int count, DataRepresentation rep)
            : base(name, screen, count, rep)
        {
            
        }
    }
}
