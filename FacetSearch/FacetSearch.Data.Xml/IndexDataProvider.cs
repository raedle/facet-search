﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using BlendedLibrary.Common.Util;
using BlendedLibrary.Data;
using BlendedLibrary.FacetStreams.Data.Xml.Configuration;
using BlendedLibrary.Service;
using Tools.BaseX;

namespace BlendedLibrary.FacetStreams.Data.Xml
{
    public class IndexDataProvider : XmlDataProviderBase
    {
        #region private fields

        private const string XmlBaseElement = "medium";

        private readonly ThreadSafeDictionary<long, IBaseXQueryTask<int>> _baseXQueryTasks = new ThreadSafeDictionary<long, IBaseXQueryTask<int>>();
        private readonly ThreadSafeDictionary<long, long> _currentBaseXQueryId = new ThreadSafeDictionary<long, long>();

        #endregion

        #region ctor

        public IndexDataProvider(BaseXDataProviderConfiguration dataProviderConfiguration, string queryCachePath)
            : base(dataProviderConfiguration, queryCachePath)
        {
        }

        #endregion

        #region private methods

        protected override void InitializeFacets()
        {
            var result = Client.InitialIndex();

            var reader = new XmlTextReader(new StringReader(result));
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if (reader.AttributeCount > 2)
                        {
                            var name = reader.GetAttribute("name");
                            var screen = reader.GetAttribute("screen");
                            if (name == null)
                                continue;

                            if (XmlBaseElement.Equals(name))
                            {
                                TotalResultCount = Convert.ToInt32(reader.GetAttribute("count"));
                                continue;
                            }
                            var type = reader.GetAttribute("type");
                            var count = Convert.ToInt32(reader.GetAttribute("count"));
                            switch (type)
                            {
                                case "text":
                                    Facets.Add(new TextFacet(name, screen, count, DataRepresentation.Textual));
                                    break;
                                case "category":
                                    {
                                        var cf = new CategoryFacet(name, screen, count, DataRepresentation.Categorical);
                                        AddCategories(reader, cf);
                                        Facets.Add(cf);
                                    }
                                    break;
                            }
                        }
                        continue;
                }
            }
        }

        private T GetFacet<T>(string facet) where T : Facet
        {
            String test = facet;
            if (test.StartsWith("(")) test = facet.Substring(1);
            return Facets.OfType<T>().Single(f => f.Name == test);
        }

        #endregion

        #region public methods

        /// <summary>
        /// Connect to server. It also starts a new server instance if not running.
        /// </summary>
        public override void Connect()
        {
            if (!BlendedLibraryService.IsRunning(BaseXDataProviderConfiguration.Port))
            {
                StartLocalServer();
            }
        }

        /// <summary>
        /// Starts a local RDS server instance.
        /// </summary>
        private void StartLocalServer()
        {
            var workingDirectory = BaseXDataProviderConfiguration.WorkingDirectory;
            if (!Directory.Exists(workingDirectory))
                Directory.CreateDirectory(workingDirectory);

            // Copy BaseX binaries and stylesheets to working directory.
            const string baseXBinary = "basexFS.jar";
            File.Copy(Path.Combine("Resources", baseXBinary), Path.Combine(workingDirectory, baseXBinary), true);
            foreach (var xsltFile in Directory.GetFiles("Resources", "*.xslt", SearchOption.AllDirectories))
            {
                var filename = Path.GetFileName(xsltFile);
                if (filename != null)
                    File.Copy(xsltFile, Path.Combine(workingDirectory, filename), true);
            }

            BlendedLibraryService.MessageReceived += (s, e) =>
                                         {
                                             Console.WriteLine(e.Message);
                                         };

            var port = BaseXDataProviderConfiguration.Port;
            var eventPort = BaseXDataProviderConfiguration.EventPort;

            BlendedLibraryService.StartAndWait(port, eventPort, workingDirectory);
            ConnectToServer();
        }

        /// <summary>
        /// Connects to server.
        /// </summary>
        private void ConnectToServer()
        {
            const string databaseName = "rds-dummy";

            // index data provider requires a dummy database to send events from facet search to visualization apps.
            var checkDatabaseClient = new BaseXClient(BaseXDataProviderConfiguration.Host, BaseXDataProviderConfiguration.Port, BaseXDataProviderConfiguration.Username, BaseXDataProviderConfiguration.Password);

            if (!checkDatabaseClient.DatabaseExists(databaseName))
            {
                checkDatabaseClient.CreateDatabase(databaseName, "");
            }

            Client = new BaseXClient(BaseXDataProviderConfiguration.Host, BaseXDataProviderConfiguration.Port, BaseXDataProviderConfiguration.Username, BaseXDataProviderConfiguration.Password, databaseName)
                    .UseForAdminLogin(BaseXDataProviderConfiguration.Username, BaseXDataProviderConfiguration.Password);

            Initialize();

            // Load cache from file system.
            QueryCache = LoadCache(QueryCachePath);
        }

        /// <summary>
        /// Initialized the Facets by calling index:facets('<see cref="DatabaseName"/>', 'flat')
        /// Fetches the <see cref="TotalResultCount"/> and adds all matching facets specified in <see cref="AllowedFacets"/>
        /// </summary>
        protected override void AddCategories(XmlReader reader, CategoryFacet cf)
        {
            var sub = false;
            while (!sub && reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if (reader.Name == "value" || reader.Name == "entry")
                        {
                            var name = reader.GetAttribute("name");
                            var count = 0; // This attribute is obsolete! Convert.ToInt32(reader.GetAttribute("count"));
                            var screen = reader.GetAttribute("screen");
                            var isMultiValue = bool.Parse(reader.GetAttribute("multi-value") ?? "false");

                            cf.Values.Add(new FacetElement(name, screen, count, isMultiValue));
                        }
                        break;
                    case XmlNodeType.EndElement:
                        sub = "element".Equals(reader.Name);
                        break;
                }
            }
        }

        public override string GetDisplayNameOfFacet(string facet)
        {
            return Facets.Single(f => f.Name == facet).Screen;
        }

        public override IFacetElement[] GetValuesOfCategoricalFacet(string facetName)
        {
            //return Facets.OfType<CategoryFacet>().Single(f => f.Name == facet).Entries.Keys.ToArray();

            var facet = Facets.OfType<CategoryFacet>().Single(f => f.Name == facetName);
            var values = facet.Values.Reverse().ToArray();

            return values;
        }

        public override string GetDisplayNameOfCategoricalValue(string facet, string value)
        {
            return Facets.OfType<CategoryFacet>().Single(f => f.Name == facet).Values.Single(x => x.Name == value).Screen;
        }

        public override DataRepresentation GetDataRepresentation(string facet)
        {
            return Facets.Single(f => f.Name == facet).Representation;
        }

        public override MetricRange GetMetricRange(string facet)
        {
            var mf = Facets.OfType<MetricFacet>().Single(f => f.Name == facet);
            return new MetricRange(mf.Min, mf.Max);
        }

        public override void VisualizeData(string query, byte targetId)
        {
            LastQuery = query;
            LastTargetId = targetId;

            if (query == null) return;

            var xquery = PrepareXQuery(query);

            var task = Task.Factory.StartNew(() =>
                                  {
                                      var result = Client.CQueryIndex(xquery);
                                      return int.Parse(result);
                                  });

            task.ContinueWith(t =>
                              {
                                  query = BaseXUtils.EscapeTriggerPayload(query);

                                  var xQuery = string.Format("//*:{0}[{1}]", XmlBaseElement, query);

                                  Debug.WriteLine("Sending XQuery {0} to target {1}", xQuery, targetId);

                                  Client.TriggerEvent(EventHelper.Event.ShowResults, string.Format("'{0}:{1}'", targetId, t.Result), xQuery);
                              });
        }

        public new void UnvisualizeData(byte targetId)
        {
            LastQuery = null;
            LastTargetId = targetId;

            Client.TriggerEvent(EventHelper.Event.RemoveResults, string.Format("'{0}:Library'", targetId), EventHelper.DummyQuery);
        }

        public override string GetCategoricalExpression(string facet, IFacetElement[] values)
        {
            if (values.Length == 0)
                return "";

            var sb = new StringBuilder();

            sb.Append("(");
            sb.Append(facet);
            sb.Append("/text() = (");

            foreach (var value in values)
            {
                var name = value.Name;

                if (value.IsMultiValue)
                {
                    foreach (var multiName in ParseMultiFacetValue(name))
                    {
                        sb.Append("\"");
                        sb.Append(EscapeFacetValue(multiName));
                        sb.Append("\",");
                    }
                }
                else
                {
                    sb.Append("\"");
                    sb.Append(EscapeFacetValue(name));
                    sb.Append("\",");
                }
            }

            sb.Remove(sb.Length - 1, 1);

            sb.Append("))");

            return sb.ToString();
        }

        public override string GetMetricRangeExpression(string facet, int minValue, int maxValue)
        {
            var sb = new StringBuilder();
            sb.Append("(");
            sb.Append(facet);
            sb.Append(" >= \"");
            sb.Append(minValue);
            sb.Append("\" and ");
            sb.Append(facet);
            sb.Append(" <= \"");
            sb.Append(maxValue);
            sb.Append("\")");

            return sb.ToString();
        }

        public override string GetTextExpression(string[] facets, string value)
        {
            if (facets.Length == 0 || string.IsNullOrWhiteSpace(value)) return "";

            //value aufbereiten und special characters escapen
            value = EscapeSpecialXmlCharacters(value);

            var sb = new StringBuilder();
            sb.Append("(");

            var i = 0;
            foreach (var facet in facets)
            {
                if (i++ > 0)
                {
                    sb.Append(" or ");
                }
                if (value.Contains("*"))
                {
                    sb.Append(facet + "/text() contains text " + value);
                }
                else
                {
                    sb.Append(facet + "/text() contains text \"" + value + "\"");
                }
            }

            sb.Append(")");
            return sb.ToString();
        }

        private const String CategoryPattern = "\"([^\"]*)\"";

        private readonly Regex _categoryRegex = new Regex(CategoryPattern,
                                                          RegexOptions.CultureInvariant | RegexOptions.IgnoreCase |
                                                          RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);

        public override int GetCount(string query)
        {
            var cachedCount = GetCachedCount(query);

            if (cachedCount.HasValue)
            {
                return cachedCount.Value;
            }

            var xquery = PrepareXQuery(query);

            var result = Int32.Parse(Client.CQueryIndex(xquery));

            QueryCache[query] = result;

            return result;
        }

        private string PrepareXQuery(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return null;
            return string.Format("count(//{0}[{1}])", XmlBaseElement, query);
        }

        private int CountWords(string text, string word)
        {
            int count = (text.Length - text.Replace(word, "").Length) / word.Length;
            return count;
        }

        protected override int? GetCachedCount(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return TotalResultCount;

            if (CachingEnabled)
            {
                if (QueryCache.ContainsKey(query))
                    return QueryCache[query];

                if (CountWords(query, "/text()") == 1)
                {
                    var facetname = query.Substring(1, query.IndexOf("/") - 1);

                    // TODO Hack to remove leading '('
                    facetname = facetname.Replace("(", "");

                    var tmpfacet = GetFacet<Facet>(facetname);
                    if (tmpfacet.Representation.Equals(DataRepresentation.Textual)) return null;

                    var matches = _categoryRegex.Matches(query);

                    var values = matches.OfType<Match>().Select(match => match.Groups[1].Value);

                    var facet = GetFacet<CategoryFacet>(facetname);

                    var count = values.Sum(value => facet.Values.Single(v => v.Name.Equals(value)).Count);

                    QueryCache[query] = count;

                    return count;
                }
            }
            return null;
        }

        public override void UpdateCount(string query, IDataReceiver receiver)
        {
            var cachedCount = GetCachedCount(query);
            if (cachedCount.HasValue)
            {
                receiver.QueryCount = cachedCount.Value;
                receiver.IsFetchingQueryCount = false;
                return;
            }

            try
            {
                if (!_currentBaseXQueryId.ContainsKey(receiver.Id))
                    _currentBaseXQueryId.Add(receiver.Id, 0);
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine(e.Message);
#endif
            }

            receiver.IsFetchingQueryCount = true;

            var xquery = PrepareXQuery(query);

            _currentBaseXQueryId[receiver.Id]++;
            var currentQueryId = _currentBaseXQueryId[receiver.Id];

            var task = Task.Factory.StartNew<int>(() =>
            {
                var result = Client.CQueryIndex(xquery);
                return int.Parse(result);
            });
            task.ContinueWith(t =>
            {
                var count = t.Result;
                QueryCache[query] = count;
                //if the query does not match the actual query we must not change the QueryCount,
                //since our request is not valid any more,
                //also we do not know if it is still fetching the query count, 
                //thus must not change the IsFetchingQueryCount property
                var match = query.Equals(receiver.Query);
#if DEBUG
                Debug.WriteLine((match ? "matched " : "no match ") + count + " reciever: " + receiver.Id + " requested: " + query + " actual: " + receiver.Query);
#endif
                if (match)
                {
                    receiver.QueryCount = count;
                    receiver.IsFetchingQueryCount = false;
                }
            });
        }

        #endregion

        #region facet value parsing

        private static IEnumerable<string> ParseMultiFacetValue(string multiFacetValue)
        {
            return multiFacetValue.Split('|'); ;
        }

        private static string EscapeFacetValue(string value)
        {
            return BaseXUtils.EscapeQuery(value);
        }

        #endregion
    }
}