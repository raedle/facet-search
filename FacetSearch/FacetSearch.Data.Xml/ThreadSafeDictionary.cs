﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace BlendedLibrary.FacetStreams.Data.Xml
{
    public class ThreadSafeDictionary<TKey, TValue>
    {
        //This is the internal dictionary that we are wrapping
        readonly IDictionary<TKey, TValue> _dict = new Dictionary<TKey, TValue>();

        #region IDictionary members

        [NonSerialized]
        readonly ReaderWriterLockSlim _dictionaryLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        public virtual bool Remove(TKey key)
        {
            _dictionaryLock.EnterWriteLock();
            try
            {
                return _dict.Remove(key);
            }
            finally
            {
                _dictionaryLock.ExitWriteLock();
            }
        }

        public virtual bool ContainsKey(TKey key)
        {
            _dictionaryLock.EnterReadLock();
            try
            {
                return _dict.ContainsKey(key);
            }
            finally
            {
                _dictionaryLock.ExitReadLock();
            }
        }

        public virtual bool TryGetValue(TKey key, out TValue value)
        {
            _dictionaryLock.EnterReadLock();
            try
            {
                return _dict.TryGetValue(key, out value);
            }
            finally
            {
                _dictionaryLock.ExitReadLock();
            }
        }

        public virtual TValue this[TKey key]
        {
            get
            {
                _dictionaryLock.EnterReadLock();
                try
                {
                    return _dict[key];
                }
                finally
                {
                    _dictionaryLock.ExitReadLock();
                }
            }
            set
            {
                _dictionaryLock.EnterWriteLock();
                try
                {
                    _dict[key] = value;
                }
                finally
                {
                    _dictionaryLock.ExitWriteLock();
                }
            }
        }

        public virtual ICollection Keys
        {
            get
            {
                _dictionaryLock.EnterReadLock();
                try
                {
                    return new List<TKey>(_dict.Keys);
                }
                finally
                {
                    _dictionaryLock.ExitReadLock();
                }
            }
        }

        public virtual ICollection Values
        {
            get
            {
                _dictionaryLock.EnterReadLock();
                try
                {
                    return new List<TValue>(_dict.Values);
                }
                finally
                {
                    _dictionaryLock.ExitReadLock();
                }
            }
        }


        public virtual void Clear()
        {
            _dictionaryLock.EnterWriteLock();
            try
            {
                _dict.Clear();
            }
            finally
            {
                _dictionaryLock.ExitWriteLock();
            }
        }

        public virtual int Count
        {
            get
            {
                _dictionaryLock.EnterReadLock();
                try
                {
                    return _dict.Count;
                }
                finally
                {
                    _dictionaryLock.ExitReadLock();
                }
            }
        }

        public virtual bool Contains(KeyValuePair<TKey, TValue> item)
        {
            _dictionaryLock.EnterReadLock();
            try
            {
                return _dict.Contains(item);
            }
            finally
            {
                _dictionaryLock.ExitReadLock();
            }

        }

        public virtual void Add(KeyValuePair<TKey, TValue> item)
        {
            _dictionaryLock.EnterWriteLock();
            try
            {
                _dict.Add(item);
            }
            finally
            {
                _dictionaryLock.ExitWriteLock();
            }
        }

        public virtual void Add(TKey key, TValue value)
        {
            _dictionaryLock.EnterWriteLock();
            try
            {
                _dict.Add(key, value);
            }
            finally
            {
                _dictionaryLock.ExitWriteLock();
            }
        }

        public virtual bool Remove(KeyValuePair<TKey, TValue> item)
        {
            _dictionaryLock.EnterWriteLock();
            try
            {
                return _dict.Remove(item);
            }
            finally
            {
                _dictionaryLock.ExitWriteLock();
            }
        }

        public virtual void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            _dictionaryLock.EnterReadLock();
            try
            {
                _dict.CopyTo(array, arrayIndex);
            }
            finally
            {
                _dictionaryLock.ExitReadLock();
            }
        }

        public virtual bool IsReadOnly
        {
            get
            {
                return _dict.IsReadOnly;
            }
        }

        public virtual IEnumerator<TValue> GetEnumerator()
        {
            throw new NotSupportedException("Cannot enumerate a threadsafe dictionary. Instead, enumerate the keys or values collection");
        }
        #endregion
    }
}
