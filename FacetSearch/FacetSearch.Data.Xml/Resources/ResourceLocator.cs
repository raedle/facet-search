﻿using System;
using System.Diagnostics;
using System.Resources;

namespace BlendedLibrary.FacetStreams.Data.Xml.Resources
{
    public class ResourceLocator
    {
        private static readonly ResourceManager Manager = new ResourceManager(string.Format("{0}.Labels", typeof(ResourceLocator).Namespace), typeof(ResourceLocator).Assembly);

        public static string GetString(string name, string defaultValue)
        {
            try
            {
                var value = Manager.GetString(name);
                return value ?? defaultValue;
            }
            catch (Exception e)
            {
                // ignore

#if DEBUG
                Debug.WriteLine("Could not find value with name '{0}'. {1}", name, e.Message);
#endif
            }
            return defaultValue;
        }

        public static string GetString(string name)
        {
            return GetString(name, null);
        }
    }
}
