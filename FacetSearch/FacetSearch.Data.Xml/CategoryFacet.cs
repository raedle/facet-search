﻿using System.Collections.Generic;
using BlendedLibrary.Data;

namespace BlendedLibrary.FacetStreams.Data.Xml
{
    public class CategoryFacet : Facet
    {
        #region properties

        // list with entries
        public IList<IFacetElement> Values { get; set; }

        #endregion

        #region ctor

        public CategoryFacet(string name, string screen, int count, DataRepresentation rep)
            : base(name, screen, count, rep)
        {
            Values = new List<IFacetElement>();
        }

        #endregion
    }
}
