﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using BlendedLibrary.Data;
using BlendedLibrary.Data.Configuration;
using BlendedLibrary.FacetStreams.Data.Xml.Configuration;
using Tools.BaseX;

namespace BlendedLibrary.FacetStreams.Data.Xml
{
    /// <summary>
    /// A general Provider for BaseX based providers.
    /// A custom provider can either be a derived class or an instance of this base class
    ///     An Example of an derived class is <see cref="MovieDataProvider"/>. The usage is;
    ///         <example>
    ///             var xmlDataProvider = new MovieDataProvider(host, port, user, password, "movies")
    ///                                                     {QueryCachePath = "MovieDataProviderCache.xml"}
    ///                                         .LoadCache()
    ///                                         .UseForAdminLogin(user, password);
    ///             Application.Current.Exit += (_, __) => xmlDataProvider.Dispose(); 
    ///         </example>
    ///     The corresponding usage by instanciating this class is:
    ///         <example>
    ///             <![CDATA[var xmlDataProvider = new XmlDataProviderBase(s.DatabaseHost, s.DatabasePort, s.DatabaseUsername,
    ///                                          s.DatabasePassword, "movies")
    ///                      {
    ///                          XmlTagName = "movie",
    ///                          DatabaseTypeName = "IMDB",
    ///                          AllowedFacets = new List<string> {"genre", "title", "rating", "numratings"},
    ///                          DisplayNamesOfFacets = new Dictionary<string, string>
    ///                                                     {
    ///                                                         {"genre", "Genre"},
    ///                                                         {"rating", "Bewertung"},
    ///                                                         {"numratings", "# Bewertungen"},
    ///                                                         {"title", "Titel"},
    ///                                                     },
    ///                          DisplayNamesOfCatecoricalValues = new Dictionary<Tuple<string, string>, string>
    ///                                                                {
    ///                                                                    {"none".AsValue(), "Keine"},
    ///                                                                    {"more than 50000".AsValue(), "> 50000"},
    ///                                                                },
    ///                          QueryCachePath = "MovieDataProviderCache.xml"
    ///                      }
    ///                      .Initialize();
    ///             Application.Current.Exit += (_, __) => xmlDataProvider.Dispose();]]>
    ///         </example>
    /// </summary>
    public abstract class XmlDataProviderBase : IDataProvider, IDisposable
    {
        //TODO: InitializeRangeFacet fehlt noch

        #region private fields

        private const string DateTimeFormat = "yyyy-MM-dd-HH-mm-ss";

        private bool _keepAliveThreadRunning;

        #endregion

        #region protected Properties/Fields

        /// <summary>
        /// Client for the BaseX xqueries
        /// </summary>
        protected BaseXClient Client;

        /// <summary>
        /// All cached queries
        /// </summary>
        protected ConcurrentDictionary<string, int> QueryCache { get; set; }

        protected readonly List<Facet> Facets;

        protected bool CachingEnabled = false;

        protected string DatabaseName = null;

        protected string LastQuery;

        protected byte LastTargetId;

        #endregion

        #region public Properties

        public IDataProviderConfiguration DataProviderConfiguration { get; set; }

        /// <summary>
        /// Xml tag for queries: specifies where the objects to count are (e.g. Medium -> in XQuery: //medium[...])
        /// Used in <see cref="GetXQueryToCount"/>
        /// 
        /// Must be specified!
        /// </summary>
        public string XmlTagName { get; set; }

        /// <summary>
        /// The Name of the <see cref="FacetStreams.DataVisualization.DataViewer.DataType"/>.
        /// This name will be sent with events and indicates the type of objects and therefore the visualization
        /// 
        /// ! Must be specified!
        /// </summary>
        public string DatabaseTypeName { get; set; }

        /// <summary>
        /// Number of total results -> no filters
        /// </summary>
        public int TotalResultCount { get; set; }

        /// <summary>
        /// Specifies the length (characters) the (default) display names of facets (<see cref="GetDisplayNameOfFacet"/>) should be shorten to.
        /// If Value is <code>null</code> (default) nothing will not be shortened!
        /// 
        /// Custom display names will never be shortened! (added via <see cref="RegisterDisplayNameOfFacet"/>)
        /// 
        /// Default value is <code>null</code>
        /// </summary>
        public uint? ShortenDisplayNamesOfFacetsTo { get; set; }

        /// <summary>
        /// Specifies the length (characters) the (default) display names of categorical values (<see cref="GetDisplayNameOfCategoricalValue"/>) should be shorten to.
        /// If Value is <code>null</code> (default) nothing will be shortened!
        /// 
        /// Custom display names will never be shortened! (added via <see cref="RegisterDisplayNameOfDisplayNamesOfCatecoricalValues(string,string,string)"/>)
        /// 
        /// Default value is 12
        /// </summary>
        public uint? ShortenDisplayNamesOfCategoricalValuesTo { get; set; }

        /// <summary>
        /// The maximum number of objects to visualize.
        /// If the number of Objects is greater this number, no query/objects will be sent to the visualizer: <see cref="VisualizeData"/> will return immediately
        /// 
        /// If value is null: no limit
        /// 
        /// Default value is 500
        /// </summary>
        public uint? MaximumNumberOfObjectsToVisualize { get; set; }

        #region Initialize Facets

        /// <summary>
        /// The facets that should be taken for facetstreams
        /// If <see cref="AllowedFacets"/> is <code>null</code> all facets will be taken
        /// 
        /// Default value is <code>null</code>: all facets
        /// </summary>
        public IList<string> AllowedFacets { get; set; }
        #endregion

        #endregion

        #region protected properties

        /// <summary>
        /// Path to the xml file where the cached queries are stored or should be stored
        /// If value is null the cache will not be saved
        /// 
        /// Default value is <code>null</code>
        /// </summary>
        protected string QueryCachePath { get; set; }

        protected DateTime DatabaseTimestamp { get; private set; }

        protected BaseXDataProviderConfiguration BaseXDataProviderConfiguration
        {
            get
            {
                return DataProviderConfiguration as BaseXDataProviderConfiguration;
            }
        }

        #endregion

        #region ctor

        protected XmlDataProviderBase(BaseXDataProviderConfiguration dataProviderConfiguration) :
            this(dataProviderConfiguration, "default-cache.qc")
        {

        }

        protected XmlDataProviderBase(BaseXDataProviderConfiguration dataProviderConfiguration, string queryCachePath)
        {
            DataProviderConfiguration = dataProviderConfiguration;
            DatabaseName = dataProviderConfiguration.DatabaseName;
            CachingEnabled = dataProviderConfiguration.CachingEnabled;
            QueryCachePath = queryCachePath;

            Facets = new List<Facet>();
            DisplayNamesOfCatecoricalValues = new Dictionary<Tuple<string, string>, string>();
            DisplayNamesOfFacets = new Dictionary<string, string>();

            MaximumNumberOfObjectsToVisualize = 500;
            ShortenDisplayNamesOfCategoricalValuesTo = 12;
            ShortenDisplayNamesOfFacetsTo = null;
        }

        public virtual void Connect()
        {
            Client = new BaseXClient(BaseXDataProviderConfiguration.Host, BaseXDataProviderConfiguration.Port, BaseXDataProviderConfiguration.Username, BaseXDataProviderConfiguration.Password, BaseXDataProviderConfiguration.DatabaseName)
                    .UseForAdminLogin(BaseXDataProviderConfiguration.Username, BaseXDataProviderConfiguration.Password);

            // e.g., 2013-12-02-22-18-02
            var dateString = Client.QuerySingle(string.Format("db:info('{0}')//databaseproperties/timestamp/text()",
                BaseXDataProviderConfiguration.DatabaseName));

            DatabaseTimestamp = DateTime.ParseExact(dateString, DateTimeFormat, CultureInfo.InvariantCulture);

            Initialize();

            // Load cache from file system.
            QueryCache = LoadCache(QueryCachePath);
        }

        /// <summary>
        /// Initializes the facets
        /// Calls <see cref="RegisterEvents"/> and <see cref="InitializeFacets"/>
        /// 
        /// This is not done in the c'tor, because both methods are virtual and may be own implementations. 
        /// But it would be called BEFORE the own c'tor would be called and therefore not all fields/properties are initialized.
        /// See explanation at <see cref="http://confluence.jetbrains.net/display/ReSharper/Virtual+method+call+in+constructor"/>
        /// 
        /// You can call this function in your own c'tor iff the the class is sealed.
        /// 
        /// This function can be called only once. Further calls will not change or initialize anything (and throws no error)
        /// Note: The methods <see cref="RegisterEvents"/> and <see cref="InitializeFacets"/> should be called only once too (by invoking this method),
        ///     but these methods have no check to prevent this!
        /// </summary>
        public void Initialize()
        {
            lock (this)
            {
                if (_initialized)
                {
                    return;
                }

                RegisterEvents();
                InitializeFacets();
                _initialized = true;
            }
        }

        protected abstract void InitializeFacets();

        private bool _initialized = false;

        #endregion

        #region Init

        /// <summary>
        /// Create the DatabaseName-Event if necessary
        /// </summary>
        protected virtual void RegisterEvents()
        {
            var observableControlMessage = Observable.FromEventPattern<BaseXEventArgs>(Client, "EventTriggered");
            observableControlMessage.Where(evt => evt.EventArgs.EventName.Equals(EventHelper.Event.ControlMessage))
                      .Select(evt => evt.EventArgs)
                      .Subscribe(OnControlMessage);

            CreateIfEventNotExist(Client, EventHelper.Event.ShowResults);
            CreateIfEventNotExist(Client, EventHelper.Event.RemoveResults);
            CreateIfEventNotExist(Client, EventHelper.Event.ControlMessage, true, DatabaseName);

            // send alive message to connected clients -> oh... and I HAT* BaseX (based on my experience, I advise everyone NO* to use BaseX!!!)
            _keepAliveThreadRunning = true;
            var keepAliveThread = new Thread(() =>
                       {
                           while (_keepAliveThreadRunning)
                           {
                               Client.TriggerEvent(EventHelper.Event.ControlMessage, string.Format("\"{0}\"", EventHelper.Message.KeepAlive), EventHelper.DummyQuery);
                               Thread.Sleep(10000);
                           }
                       })
                                  {
                                      IsBackground = true
                                  };
            keepAliveThread.Start();
        }

        private void OnControlMessage(BaseXEventArgs e)
        {
            switch (e.EventName)
            {
                case EventHelper.Event.ControlMessage:
                    switch (e.Results)
                    {
                        case EventHelper.Message.HyperGridConnected:
                            VisualizeData(LastQuery, LastTargetId);
                            break;
                    }
                    break;
            }
        }

        /// <summary>
        /// Create data service events if not already existing.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="eventName"></param>
        /// <param name="watch"></param>
        /// <param name="databaseName"></param>
        private static void CreateIfEventNotExist(BaseXClient client, string eventName, bool watch = false, string databaseName = null)
        {
            var result = client.ShowEvents();

            if (!result.Contains(eventName))
                client.CreateEvent(eventName);

            if (watch)
                client.WatchEvent(eventName, databaseName);
        }

        /// <summary>
        /// Initialized the Facets by calling index:facets('<see cref="DatabaseName"/>', 'flat')
        /// Fetches the <see cref="TotalResultCount"/> and adds all matching facets specified in <see cref="AllowedFacets"/>
        /// </summary>
        protected virtual void AddCategories(XmlReader reader, CategoryFacet cf)
        {
            var sub = false;
            while (!sub && reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if (reader.Name == "value" || reader.Name == "entry")
                        {
                            var name = reader.GetAttribute("name");
                            var count = 0; // This attribute is obsolete! Convert.ToInt32(reader.GetAttribute("count"));
                            var screen = reader.GetAttribute("screen");
                            var isMultiValue = bool.Parse(reader.GetAttribute("multi-value") ?? "false");

                            cf.Values.Add(new FacetElement(name, screen, count, isMultiValue));
                        }
                        break;
                    case XmlNodeType.EndElement:
                        sub = "element".Equals(reader.Name);
                        break;
                }
            }
        }

        #endregion

        #region Cache

        /// <summary>
        /// Saves the current <see cref="QueryCache"/> in a xml file at <see cref="QueryCachePath"/> (must be specified before!)
        /// 
        /// Will be called in <see cref="Dispose()"/> if <see cref="QueryCachePath"/> is specified
        /// </summary>
        public void SaveCache()
        {
            if (!string.IsNullOrWhiteSpace(QueryCachePath))
                SaveCache(QueryCache, QueryCachePath);
        }

        /// <summary>
        /// Saves the cached queries in <paramref name="dictionary"/> in a xml
        /// </summary>
        protected void SaveCache(ConcurrentDictionary<string, int> dictionary, string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                throw new ArgumentException("No path specified!", "path");

            var rootElement = new XElement("CachedQueries",
                from e in dictionary
                select new XElement("CachedQuery",
                    new XAttribute("Query", e.Key),
                    new XAttribute("Count", e.Value))
                );
            rootElement.Add(new XAttribute("DateTime", DatabaseTimestamp.ToString(DateTimeFormat)));

            var xml = new XDocument(rootElement);
            xml.Save(path);

            Debug.WriteLine("Query cache saved: {0} queries", dictionary.Count);
        }

        /// <summary>
        /// Loads cached queries from the given file <paramref name="path"/>
        /// 
        /// If there's no file at <paramref name="path"/> a empty dictionary will be returned
        /// </summary>
        /// <returns>Concurrent dictionary with all cached queries and their counts</returns>
        protected ConcurrentDictionary<string, int> LoadCache(string path)
        {
            if (!File.Exists(path))
                return new ConcurrentDictionary<string, int>();

            var xml = XDocument.Load(path);

            if (xml.Root == null) return new ConcurrentDictionary<string, int>();

            var xmlDateString = xml.Root.Attribute("DateTime").Value;
            var xmlTimestamp = DateTime.ParseExact(xmlDateString, DateTimeFormat, CultureInfo.InvariantCulture);

            if (!Equals(xmlTimestamp, DatabaseTimestamp))
            {
                Console.WriteLine();
                return new ConcurrentDictionary<string, int>();
            }

            return new ConcurrentDictionary<string, int>(xml.Descendants("CachedQuery").ToDictionary(e => e.Attribute("Query").Value,
                                                               e => Convert.ToInt32(e.Attribute("Count").Value)));
        }

        #endregion

        public abstract void UpdateCount(string query, IDataReceiver receiver);

        #region Implementation of IDisposable

        public void Dispose()
        {
            _keepAliveThreadRunning = false;

            SaveCache();
            try
            {
                Client.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        #endregion

        #region Implementation of IDataProvider

        public IDataProviderConfiguration GetConfiguration()
        {
            return DataProviderConfiguration;
        }

        public virtual string[] GetFacets()
        {
            return Facets.Select(f => f.Name).ToArray();
        }

        public virtual string[] GetFacets(DataRepresentation representation)
        {
            var facets = new List<string>();

            if (representation.HasFlag(DataRepresentation.Categorical))
            {
                facets.AddRange(Facets.OfType<CategoryFacet>().Select(f => f.Name));
            }

            if (representation.HasFlag(DataRepresentation.Metric))
            {
                facets.AddRange(Facets.OfType<MetricFacet>().Select(f => f.Name));
            }

            if (representation.HasFlag(DataRepresentation.Textual))
            {
                facets.AddRange(Facets.OfType<TextFacet>().Select(f => f.Name));
            }

            return facets.ToArray();
        }

        #region Display Names of Facets

        /// <summary>
        /// Dictionary with facets and their display names
        /// Public, therefore you can use collection initializer, but don't set to <code>null</code>!
        /// 
        /// Used in <see cref="GetDisplayNameOfFacet"/>
        /// </summary>
        public Dictionary<string, string> DisplayNamesOfFacets { get; set; }

        /// <summary>
        /// Registers the specified <paramref name="displayName"/> for the passed <paramref name="facet"/>
        /// 
        /// Will be returned in <see cref="GetDisplayNameOfFacet"/>
        /// </summary>
        public XmlDataProviderBase RegisterDisplayNameOfFacet(string facet, string displayName)
        {
            DisplayNamesOfFacets[facet] = displayName;

            return this;
        }

        public virtual string GetDisplayNameOfFacet(string facet)
        {
            //erst im Dictionary nachschauen ob mapping
            if (DisplayNamesOfFacets.ContainsKey(facet))
            {
                return DisplayNamesOfFacets[facet];
            }

            //sonst einfach nur Facet-Name zurückgeben
            return Facets.Where(f => f.Name == facet).Select(f => f.Name).FirstOrDefault() ?? string.Empty;
        }
        #endregion

        public virtual IFacetElement[] GetValuesOfCategoricalFacet(string facet)
        {
            return Facets.OfType<CategoryFacet>().Single(f => f.Name == facet).Values.ToArray();
        }

        #region Display Names of Catogrical Values

        /// <summary>
        /// Dictionary with facets, values und display names of the values
        /// Public, therefore you can use collection initializer, but don't set to <code>null</code>!
        /// 
        /// Key is a tuple containing the facet name (Item1) and value name (Item2)
        ///     If facet name is <code>null</code> it applies to all facets
        ///     A value with a specified facet has priority of a value without a facet (<code>null</code>)
        /// 
        /// Best use <see cref="GetDisplayNameOfCategoricalValueFromDictionary"/> to access the Dictionary
        /// 
        /// Used in <see cref="GetDisplayNameOfCategoricalValue"/>
        /// </summary>
        public Dictionary<Tuple<string, string>, string> DisplayNamesOfCatecoricalValues { get; set; }

        /// <summary>
        /// Returns the display name for the passed <paramref name="facet"/> and <paramref name="value"/>
        /// It searches in <see cref="DisplayNamesOfCatecoricalValues"/>.
        /// 
        /// First it tries to find a entry with (<paramref name="facet"/>, <paramref name="value"/>), next (<code>null</code>, <paramref name="value"/>)
        /// 
        /// If there's no matching display name <code>null</code> will be returned
        /// </summary>
        protected string GetDisplayNameOfCategoricalValueFromDictionary(string facet, string value)
        {
            //wenn facet angegeben: schaun ob 
            if (facet != null)
            {
                var t = Tuple.Create(facet, value);
                if (DisplayNamesOfCatecoricalValues.ContainsKey(t))
                {
                    return DisplayNamesOfCatecoricalValues[t];
                }
            }
            //sonst mit null versuchen
            {
                var t = Tuple.Create((string)null, value);
                if (DisplayNamesOfCatecoricalValues.ContainsKey(t))
                {
                    return DisplayNamesOfCatecoricalValues[t];
                }
            }
            //wenn immer noch nichts -> null zurückgeben
            return null;
        }

        /// <summary>
        /// Registers the specified <paramref name="displayName"/> for the passed <paramref name="facet"/> and <paramref name="value"/>
        /// 
        /// If <paramref name="facet"/> is <code>null</code> it applies to all possible facets, but specified facets have priority over <code>null</code>-facets
        /// 
        /// Used in <see cref="GetDisplayNameOfCategoricalValue"/>
        /// </summary>
        public XmlDataProviderBase RegisterDisplayNameOfDisplayNamesOfCatecoricalValues(string facet, string value, string displayName)
        {
            DisplayNamesOfCatecoricalValues[Tuple.Create(facet, value)] = displayName;

            return this;
        }
        /// <summary>
        /// Registers the specified <paramref name="displayName"/> for the passed <paramref name="value"/> and all facets (like calling <see cref="RegisterDisplayNameOfDisplayNamesOfCatecoricalValues"/> with <code>facet = null</code>)
        /// 
        /// Used in <see cref="GetDisplayNameOfCategoricalValue"/>
        /// </summary>
        public XmlDataProviderBase RegisterDisplayNameOfDisplayNamesOfCatecoricalValues(string value, string displayName)
        {
            return RegisterDisplayNameOfDisplayNamesOfCatecoricalValues(null, value, displayName);
        }

        public virtual string GetDisplayNameOfCategoricalValue(string facet, string value)
        {
            //nur kürzen wenn zu lang
            if (value.Length <= 10)
            {
                return value;
            }
            return value.Substring(0, 8) + ".";
        }

        #endregion

        public virtual DataRepresentation GetDataRepresentation(string facet)
        {
            return Facets.Single(f => f.Name == facet).Representation;
        }

        public virtual MetricRange GetMetricRange(string facet)
        {
            var mf = Facets.OfType<MetricFacet>().Single(f => f.Name == facet);
            return new MetricRange(mf.Min, mf.Max);
        }

        public virtual void VisualizeData(string query, byte targetId)
        {
            LastQuery = query;
            LastTargetId = targetId;

            if (query == null) return;

            //query per event verschicken und so objekte an visualisierung geben
            var xquery = GetXQueryToVisualize(query);
            Client.TriggerEvent(EventHelper.Event.ShowResults, string.Format("'{0}:{1}'", targetId, DatabaseTypeName), xquery);
        }

        public virtual void UnvisualizeData(byte targetId)
        {
            LastQuery = null;
            LastTargetId = targetId;

            Client.TriggerEvent(EventHelper.Event.RemoveResults, string.Format("'{0}:{1}'", targetId, DatabaseTypeName), "");
        }

        public virtual string GetCategoricalExpression(string facet, IFacetElement[] values)
        {
            if (values.Length == 0)
                return "";

            var sb = new StringBuilder();

            sb.Append("(");
            sb.Append(facet);
            sb.Append("/text() = (");

            foreach (var value in values.Take(values.Length - 1))
            {
                if (value.IsMultiValue)
                {
                    var names = value.Name.Split('|');
                    foreach (var name in names)
                    {
                        sb.Append("\"");
                        sb.Append(name);
                        sb.Append("\",");
                    }
                }
                else
                {
                    sb.Append("\"");
                    sb.Append(value.Name);
                    sb.Append("\",");
                }
            }

            if (values.Last().IsMultiValue)
            {
                var names = values.Last().Name.Split('|');
                foreach (var name in names.Take(names.Length - 1))
                {
                    sb.Append("\"");
                    sb.Append(name);
                    sb.Append("\",");
                }

                sb.Append("\"");
                sb.Append(names.Last());
                sb.Append("\"");
            }
            else
            {
                sb.Append("\"");
                sb.Append(values.Last().Name);
                sb.Append("\"");
            }
            sb.Append("))");

            return sb.ToString();
        }

        public virtual string GetMetricRangeExpression(string facet, int minValue, int maxValue)
        {
            //string.format: Stringbuilder erst ab großer anzahl string operationen schneller
            return string.Format("({0}/text() >= {1} and {0}/text() <= {2})",
                                 facet, minValue, maxValue);
        }

        public virtual string GetTextExpression(string[] facets, string value)
        {
            if (facets.Length == 0 || string.IsNullOrWhiteSpace(value)) return "";

            //value aufbereiten und special characters escapen
            value = EscapeSpecialXmlCharacters(value);

            var sb = new StringBuilder();
            sb.Append("(");

            var i = 0;
            foreach (var facet in facets)
            {
                if (i++ > 0)
                {
                    sb.Append(" or ");
                }

                //value noch wegen sonderzeichen escapen -> wirft sonst fehler
                sb.Append(facet + "/text() contains text \"" + EscapeSpecialXmlCharacters(value) + "\"");
            }

            sb.Append(")");
            return sb.ToString();
        }

        public virtual int GetCount(string query)
        {
            Debug.WriteLine("GetCount: " + query);

            //erst im cache schauen ob schon vorhanden
            var cachedCount = GetCachedCount(query);
            if (cachedCount.HasValue)
            {
                return cachedCount.Value;
            }

            //sonst xquery erstellen
            var xquery = GetXQueryToCount(query);
            //und ausführen
            var result = Client.QuerySingle<int>(xquery, BaseXDataProviderConfiguration.DatabaseName);
            QueryCache[query] = result;
            return result;
        }

        public virtual Task<int> GetCancelableCountAsync(string query, CancellationToken token)
        {
            Console.WriteLine("Query: {0}", query);

            //async -> in eigenem task ausführen und task zurückgeben
            var t = Task.Factory.StartNew(() =>
            {
                //wenn schon abgebrochen: -> rausgehen
                token.ThrowIfCancellationRequested();

                //dann schauen ob gecachet
                var cachedCount = GetCachedCount(query);
                if (cachedCount.HasValue)
                {
                    return cachedCount.Value;
                }

                //nochmal überprüfen: bereits beendet?
                token.ThrowIfCancellationRequested();

                //query erstellen
                var xquery = GetXQueryToCount(query);

                //jetzt abgebrochen?
                token.ThrowIfCancellationRequested();

                //ein neuen CancellationToken erstellen und mit vorhandenem verknüpfen
                //token für diesen task einfach weiterreichen geht nicht, 
                //  wahrscheinlich da Exceptions dann geteilst werden
                var cts = new CancellationTokenSource();
                token.Register(cts.Cancel);
                //dann query an BaseX schicken und async ausführen
                //!Nochmal ein Task, damit Query in anderem Task ausgeführt werden kann
                //  und in diesem Task dieser bei bedarf abgebrochen werden kann
                var qt = Client.AbortableQuerySingleAsync<int>(xquery, BaseXDataProviderConfiguration.DatabaseName, cts.Token);
                //dann auf ergebnis warten
                try
                {
                    qt.Wait();
                }
                catch (AggregateException)
                {
                    //kommt hier rein wenn Fehler beim ausführen des Queries
                    //Beobachtung: manchmal aber auch wenn query abgebrochen
                    //  -> noch mal auf cancel überprüfen
                    token.ThrowIfCancellationRequested();
                    throw;
                }
                catch (OperationCanceledException)
                {
                    //kommt hier rein wenn abgebrochen -> cancel weiter geben
                    token.ThrowIfCancellationRequested();
                }

                //ergebnis ist da -> auslesen
                var count = qt.Result;
                //ergebnis im cache speichern
                QueryCache[query] = count;
                //und ergebnis zurückgeben
                return count;
            }, token);

            return t;
        }

        #endregion

        #region Facet
        private T GetFacet<T>(string facet) where T : Facet
        {
            return Facets.OfType<T>().Single(f => f.Name == facet);
        }
        #endregion

        #region Query
        private const String MinMaxPattern = @".+\s(?<min>[0-9]+).+ and .+ \s (?<max>[0-9]+).+";
        private readonly Regex _minMaxRegex = new Regex(MinMaxPattern, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);
        private const String CategoryPattern = "\"([0-9a-zA-Zäüö\\-\\s]+)\"";
        private readonly Regex _categoryRegex = new Regex(CategoryPattern, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);

        /// <summary>
        /// Returns the count of the passed <paramref name="query"/> if already cached (in <see cref="QueryCache"/> and caching is enabled, otherwise null
        /// Returns the TotalResultCount if the Query is null or whitespace
        /// </summary>
        protected virtual int? GetCachedCount(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return TotalResultCount;
            if (CachingEnabled)
            {
                if (QueryCache.ContainsKey(query))
                    return QueryCache[query];

                //Die Sachen da unten sind eigentlich BibData-Sachen, aber schaden ja nicht: solche Fassetten sollten oft vorkommen
                if (IsSimpleYearQuery(query))
                {
                    if (_minMaxRegex.IsMatch(query))
                    {
                        var match = _minMaxRegex.Match(query);
                        int minValue = Int32.Parse(match.Groups[1].Value);
                        int maxValue = Int32.Parse(match.Groups[2].Value);

                        var count = GetFacet<MetricFacet>("Year").GetCount(minValue, maxValue);

                        QueryCache[query] = count;
                        return count;
                    }
                }
                else if (IsSimpleLanguageQuery(query))
                {
                    var matches = _categoryRegex.Matches(query);

                    var values = matches.OfType<Match>().Select(match => match.Groups[1].Value);

                    var facet = GetFacet<CategoryFacet>("Language");

                    int count = values.Sum(value => facet.Values.Single(v => v.Name.Equals(value)).Count);

                    QueryCache[query] = count;
                    return count;
                }
                else if (IsSimpleTypeQuery(query))
                {
                    var matches = _categoryRegex.Matches(query);

                    var values = matches.OfType<Match>().Select(match => match.Groups[1].Value);

                    var facet = GetFacet<CategoryFacet>("Type");

                    int count = values.Sum(value => facet.Values.Single(v => v.Name.Equals(value)).Count);

                    QueryCache[query] = count;
                    return count;
                }
                else if (IsSimpleCategoryQuery(query))
                {
                    var matches = _categoryRegex.Matches(query);

                    var values = matches.OfType<Match>().Select(match => match.Groups[1].Value);

                    var facet = GetFacet<CategoryFacet>("Category");

                    int count = values.Sum(value => facet.Values.Single(v => v.Name.Equals(value)).Count);

                    QueryCache[query] = count;
                    return count;
                }
            }
            return null;
        }

        /// <summary>
        /// Builds the xquery to count the number of object matching the specified predicates (<paramref name="query"/>)
        /// </summary>
        protected virtual string GetXQueryToCount(string query)
        {
            return string.Format("count(//{0}[{1}])", XmlTagName, query);
        }

        /// <summary>
        /// Builds the xquery to get the results to visualize the objects matching the specified predicates (<paramref name="query"/>)
        /// </summary>
        protected virtual string GetXQueryToVisualize(string query)
        {
            return string.Format("//{0}[{1}]", XmlTagName, query);
        }

        #endregion

        #region Simple Queries
        //TODO: SimpleQueries noch verallgemeinern
        protected virtual bool IsSimpleQuery(string query, string facet, int paranthesisCount)
        {
            if (!query.Contains(facet))
                return false;

            return IsSimpleQuery(query, paranthesisCount);
        }

        protected virtual bool IsSimpleQuery(string query, int paranthesisCount)
        {
            return query.Replace("/text()", "").Count(ch => ch == '(') == paranthesisCount;
        }

        protected virtual bool IsSimpleLanguageQuery(string query)
        {
            return IsSimpleQuery(query, "Language", 3);
        }

        protected virtual bool IsSimpleCategoryQuery(string query)
        {
            return IsSimpleQuery(query, "Category", 3);
        }

        protected virtual bool IsSimpleTypeQuery(string query)
        {
            return IsSimpleQuery(query, "Type", 3);
        }

        protected virtual bool IsSimpleYearQuery(string query)
        {
            return IsSimpleQuery(query, "Year", 2);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Replaces all special xml entitites (&quot; &amp; ...) with their matching xml entities (&amp;quot; ...)
        /// Uses <see cref="SecurityElement.Escape"/>
        /// </summary>
        public static string EscapeSpecialXmlCharacters(string str)
        {
            return SecurityElement.Escape(str);
        }

        /// <summary>
        /// Shortens the passed <paramref name="str"/> to a length of <paramref name="maxLength"/> if necessary.
        /// This will be denoted by ending <paramref name="shortenSign"/> like "...". If <paramref name="shortenSign"/> is null, nothing will be appended
        /// The length of the result string including the <paramref name="shortenSign"/> is always smaller/equal <paramref name="maxLength"/>
        /// 
        /// </summary>
        public static string ShortenString(string str, int maxLength, string shortenSign = "...")
        {
            if (str == null)
            {
                return null;
            }
            if (shortenSign != null && shortenSign.Length >= maxLength)
            {
                throw new ArgumentException("shortenSign has more than maxLength characters!", "shortenSign");
            }

            //muss string überhaupt gekürzt werden?
            if (str.Length <= maxLength)
            {
                return str;
            }

            //sonst kürzen und shortenSign anhängen
            shortenSign = shortenSign ?? string.Empty;
            return str.Substring(0, maxLength - shortenSign.Length) + shortenSign;
        }
        #endregion
    }

    public static class XmlDataProviderExtensions
    {
        /// <summary>
        /// Create a Tuple with two items.
        /// Same as: <code>Tuple.Create(item1, item2)</code>
        /// 
        /// For collection inititalizer with <see cref="XmlDataProviderBase.DisplayNamesOfCatecoricalValues"/>: <code>{"myFacet".And("myValue), "myName"}</code>
        /// </summary>
        public static Tuple<string, string> And(this string item1, string item2)
        {
            return Tuple.Create(item1, item2);
        }

        /// <summary>
        /// Create a tuple with <code>Item1 = null</code> and the specified second item
        /// Same as: <code>Tuple.Create((string)null, item2)</code>
        /// 
        /// For collection inititalizer with <see cref="XmlDataProviderBase.DisplayNamesOfCatecoricalValues"/>: <code>{"myValue".AsValue(), "myName"}</code>
        /// </summary>
        public static Tuple<string, string> AsValue(this string item2)
        {
            return Tuple.Create((string)null, item2);
        }
    }
}