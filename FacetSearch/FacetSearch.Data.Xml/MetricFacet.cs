﻿using System;
using System.Collections.Generic;
using System.Linq;
using BlendedLibrary.Data;

namespace BlendedLibrary.FacetStreams.Data.Xml
{
    class MetricFacet : Facet
    {
        #region properties

        // min value
        public int Min { get; set; }

        // max value
        public int Max { get; set; }

        // list with entries
        public Dictionary<String, int> Entries { get; set; }

        public int[] Counts { get; set; }

        #endregion

        #region ctor

        public MetricFacet(string name, string screen, int count, int min, int max, DataRepresentation rep)
            : base(name, screen, count, rep)
        {
            Min = min;
            Max = max;
            Entries = new Dictionary<string, int>();
        }

        #endregion

        public int GetCount(int minValue, int maxValue)
        {
            if(minValue > maxValue)
                return 0;

            return Counts.Skip(minValue).Take(maxValue + 1 - minValue).Sum();
        }
    }
}
