﻿using BlendedLibrary.Data;

namespace BlendedLibrary.FacetStreams.Data.Xml
{
    public class FacetElement : IFacetElement
    {
        #region properties

        public string Name { get; set; }

        public string Screen { get; set; }

        public int Count { get; set; }

        public bool IsMultiValue { get; set; }

        #endregion

        #region ctor

        public FacetElement(string name, string screen, int count, bool isMultiValue)
        {
            Name = name;
            Screen = screen;
            Count = count;
            IsMultiValue = isMultiValue;
        }

        #endregion
    }
}
