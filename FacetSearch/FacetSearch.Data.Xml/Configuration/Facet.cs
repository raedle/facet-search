﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace BlendedLibrary.FacetStreams.Data.Xml.Configuration
{
    [XmlType("facet")]
    public class Facet
    {
        #region properties

        #region Id

        [Category("Facet")]
        [DisplayName("Id")]
        [Description("Id of facet as it is defined in the index. The value must be unique.")]
        [XmlAttribute("id")]
        public string Id { get; set; }

        #endregion

        #region Name

        [Category("Facet")]
        [DisplayName("Name")]
        [Description("Name of facet how it should appear in Facet Search.")]
        [XmlAttribute("name")]
        public string Name { get; set; }

        #endregion

        #region Count

        private int _count = -1;

        [Category("Facet")]
        [DisplayName("Count")]
        [Description("Maximum number of facet values for this facet.")]
        [XmlAttribute("count")]
        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }

        #endregion

        #region Type

        [Category("Facet")]
        [DisplayName("Type")]
        [Description("Type of facet (category or text).")]
        [XmlAttribute("type")]
        public string Type { get; set; }

        #endregion

        #region Values

        [Category("Facet Values")]
        [DisplayName("Values")]
        [Description("Facet values.")]
        [XmlElement("value", Order = 1)]
        public List<FacetValue> Values { get; set; }

        #endregion

        #endregion

        #region public methods

        /// <summary>
        /// Defines whether count attribute is serialized or not.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeCount()
        {
            return Count > -1;
        }

        /// <summary>
        /// Defines whether values are serialized or not.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeValues()
        {
            return Values != null && Values.Count > 0;
        }

        #endregion

        public override string ToString()
        {
            return string.Format("{0} ({1})", Name, Type);
        }
    }
}
