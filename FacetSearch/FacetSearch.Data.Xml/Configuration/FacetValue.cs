﻿using System.Xml.Serialization;

namespace BlendedLibrary.FacetStreams.Data.Xml.Configuration
{
    [XmlType("value")]
    public class FacetValue
    {
        #region properties

        #region Id

        [XmlAttribute("id")]
        public string Id { get; set; }

        #endregion

        #region Name

        [XmlAttribute("name")]
        public string Name { get; set; }

        #endregion

        #region IsMultiValue

        [XmlAttribute("multi-value")]
        public bool IsMultiValue { get; set; }

        #endregion

        #endregion

        public override string ToString()
        {
            return string.Format("{0} (multi={1})", Name, IsMultiValue);
        }
    }
}
