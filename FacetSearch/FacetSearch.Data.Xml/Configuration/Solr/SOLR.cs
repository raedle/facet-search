﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace BlendedLibrary.FacetStreams.Data.Xml.Configuration.Solr
{
    [XmlType("config")]
    public class Solr : BaseConfiguration
    {
        #region Type

        [XmlIgnore]
        public override string Type
        {
            get { return "solr"; }
        }

        #endregion

        #region properties

        #region Base

        [Category("Configuration")]
        [DisplayName("Base Url")]
        [Description("Base Url for Solr Index")]
        [XmlElement("base", Order = 0)]
        public string Base { get; set; }

        #endregion

        #region Facets

        [Category("Configuration")]
        [DisplayName("Facets")]
        [Description("Facets visisble in Facet Search")]
        [XmlArray("facets")]
        [XmlArrayItem("facet")]
        public List<Facet> Facets { get; set; }

        #endregion

        #endregion
    }
}
