﻿using System.Xml.Serialization;
using BlendedLibrary.Data.Configuration;

namespace BlendedLibrary.FacetStreams.Data.Xml.Configuration
{
    public class BaseXDataProviderConfiguration : IDataProviderConfiguration
    {
        #region Type

        [XmlIgnore]
        public string Type
        {
            get { throw new System.NotImplementedException(); }
        }

        #endregion

        #region public properties

        public string Host { get; set; }

        public int Port { get; set; }

        public int EventPort { get; set; }

        public string WorkingDirectory { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string DatabaseName { get; set; }

        public bool CachingEnabled { get; set; }


        #endregion

        #region ctor

        public BaseXDataProviderConfiguration(string host, int port, int eventPort, string workingDirectory, string username, string password, string databaseName, bool cachingEnabled)
        {
            Host = host;
            Port = port;
            EventPort = eventPort;
            WorkingDirectory = workingDirectory;
            Username = username;
            Password = password;
            DatabaseName = databaseName;
            CachingEnabled = cachingEnabled;
        }

        #endregion
    }
}
