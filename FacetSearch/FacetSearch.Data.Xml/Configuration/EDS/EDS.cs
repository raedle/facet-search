﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace BlendedLibrary.FacetStreams.Data.Xml.Configuration.EDS
{
    [XmlType("config")]
    // ReSharper disable once InconsistentNaming
    public class EDS : BaseConfiguration
    {
        #region Type

        [XmlIgnore]
        public override string Type
        {
            get { return "rds"; }
        }

        #endregion

        #region properties

        #region Base

        [Category("Configuration")]
        [DisplayName("Base Url")]
        [Description("Base Url for Rds Index")]
        [XmlElement("base")]
        public string Base { get; set; }

        #endregion

        #region Facets

        [Category("Configuration")]
        [DisplayName("Facets")]
        [Description("Facets visisble in Facet Search")]
        [XmlArray("facets")]
        [XmlArrayItem("facet")]
        public List<EdsFacet> Facets { get; set; }

        #endregion

        #endregion
    }
}
