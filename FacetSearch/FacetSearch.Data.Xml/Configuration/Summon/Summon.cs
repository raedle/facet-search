﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace BlendedLibrary.FacetStreams.Data.Xml.Configuration.Summon
{
    [XmlType("config")]
    public class Summon : BaseConfiguration
    {
        #region Type

        [XmlIgnore]
        public override string Type
        {
            get { return "summon"; }
        }

        #endregion

        #region properties

        #region ApiKey

        [Category("Configuration")]
        [DisplayName("Api Key")]
        [Description("Summon Api Key")]
        [XmlElement("apiKey")]
        public string ApiKey { get; set; }

        #endregion

        #region ApiId

        [Category("Configuration")]
        [DisplayName("Api Id")]
        [Description("Summon Api Id")]
        [XmlElement("apiID")]
        public string ApiId { get; set; }

        #endregion

        #region LocalCatalogOnly

        [Category("Configuration")]
        [DisplayName("Local Catalog Only")]
        [Description("True searches only in local catalog.")]
        [XmlElement("localCatalogOnly")]
        public bool LocalCatalogOnly { get; set; }

        #endregion

        #region Facets

        [Category("Configuration")]
        [DisplayName("Facets")]
        [Description("Facets visisble in Facet Search")]
        [XmlArray("facets")]
        [XmlArrayItem("facet")]
        public List<Facet> Facets { get; set; }

        #endregion

        #endregion

    }
}
