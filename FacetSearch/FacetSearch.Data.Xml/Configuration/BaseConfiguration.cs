﻿using BlendedLibrary.Data.Configuration;

namespace BlendedLibrary.FacetStreams.Data.Xml.Configuration
{
    public abstract class BaseConfiguration : IDataProviderConfiguration
    {
        #region Type

        public abstract string Type { get; }

        #endregion
    }
}
