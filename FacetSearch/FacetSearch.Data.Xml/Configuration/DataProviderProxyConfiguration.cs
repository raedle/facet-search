﻿using System.Xml.Serialization;

namespace BlendedLibrary.FacetStreams.Data.Xml.Configuration
{
    [XmlType("config")]
    public class DataProviderProxyConfiguration
    {
        #region properties

        #region Type

        [XmlElement("type")]
        public string Type { get; set; }

        #endregion

        #region Index

        [XmlElement("index")]
        public string Index { get; set; }

        #endregion

        #region Xslt

        [XmlElement("xslt")]
        public string Xslt { get; set; }

        #endregion

        #region AddAvailabilities

        [XmlElement("addAvailabilities")]
        public bool AddAvailabilities { get; set; }

        #endregion

        #region Debug

        [XmlElement("debug")]
        public bool Debug { get; set; }

        #endregion

        #endregion
    }
}
