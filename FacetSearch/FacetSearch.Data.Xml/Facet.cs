﻿using BlendedLibrary.Data;

namespace BlendedLibrary.FacetStreams.Data.Xml
{
    public class Facet
    {
        #region properties

        public string Name { get; set; }

        public string Screen { get; set; }

        public int Count { get; set; }

        public DataRepresentation Representation { get; set; }

        #endregion

        #region ctor

        public Facet(string name, string screen, int count, DataRepresentation rep)
        {
            Name = name;
            Screen = screen;
            Count = count;
            Representation = rep;
        }

        #endregion
    }
}
