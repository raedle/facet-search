﻿using System.Windows.Controls;

namespace BlendedLibrary.FacetSearch.Application.View
{
    public partial class QueryCanvasView : UserControl
    {
        public QueryCanvasView()
        {
            InitializeComponent();
        }

        internal Canvas DebuggingCanvas
        { 
            get { return debuggingCanvas; }
        }
    }
}