﻿using System.Windows;
using System.Windows.Controls;
using BlendedLibrary.FacetSearch.Application.ViewModel;
using BlendedLibrary.FacetSearch.Application.ViewModel.Link;
using BlendedLibrary.FacetSearch.Application.ViewModel.Node;

namespace BlendedLibrary.FacetSearch.Application.View
{
    public class QueryCanvasItemContainerStyleSelector : StyleSelector
    {
        public override Style SelectStyle(object item, DependencyObject container)
        {

            if (item is QueryCanvasLinkViewModel)
                return LinkStyle;

            if (item is QueryCanvasItemViewModel)
                return NodeStyle;

            if (item is QueryCanvasResultTokenViewModel)
                return ResultTokenStyle;

            return null;
        }

        public Style NodeStyle { get; set; }
        public Style LinkStyle { get; set; }
        public Style ResultTokenStyle { get; set; }
    }
}
