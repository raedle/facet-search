﻿using System.Windows.Controls;
using System.Windows.Media;

namespace BlendedLibrary.FacetSearch.Application.View.Link
{
    /// <summary>
    /// Interaction logic for QueryCanvasExitLinkView.xaml
    /// </summary>
    public partial class QueryCanvasExitLinkView : UserControl
    {
        public QueryCanvasExitLinkView()
        {
            InitializeComponent();
        }

        protected override GeometryHitTestResult HitTestCore(GeometryHitTestParameters hitTestParameters)
        {
            return new GeometryHitTestResult(this, HitTestPath.RenderedGeometry.FillContainsWithDetail(hitTestParameters.HitGeometry));
        }
    }
}
