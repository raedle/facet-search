﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using BlendedLibrary.Common;
using BlendedLibrary.FacetSearch.Application.CircleControl;

namespace BlendedLibrary.FacetSearch.Application.View
{
    public partial class OuterNodeLabels : Grid
    {
        #region events

        public event EventHandler<TouchEventArgs> ContactDownOnFacetLabel;

        public event EventHandler<TouchEventArgs> ContactUpOnFacetLabel;
        
        public event EventHandler<TouchEventArgs> ContactDownOnValueLabel;
        
        public event EventHandler<TouchEventArgs> ContactUpOnValueLabel;

        #endregion

        #region ctor

        public OuterNodeLabels()
        {
            InitializeComponent();
            Loaded += new RoutedEventHandler(OnLoaded);
        }

        #endregion

        #region event handling

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            applyLocalLabelRotation(txtFacetLabel1, Properties.Settings.Default.FacetLabelRadius, 0.0);
            applyLocalLabelRotation(txtFacetLabel2, Properties.Settings.Default.FacetLabelRadius, 120.0);
            applyLocalLabelRotation(txtFacetLabel3, Properties.Settings.Default.FacetLabelRadius, 240.0);
            
            applyLocalLabelRotation(txtValueLabel1, Properties.Settings.Default.ValueLabelRadius, 0.0);
            applyLocalLabelRotation(txtValueLabel2, Properties.Settings.Default.ValueLabelRadius, 120.0);
            applyLocalLabelRotation(txtValueLabel3, Properties.Settings.Default.ValueLabelRadius, 240.0);
            
            registerEventsOnFacetLabels();
            registerEventsOnValueLabels();
            registerEventsOnCircles();
        }

        private void OnContactDownOnFacetLabel(object sender, TouchEventArgs e)
        {
            if (e.TouchDevice.IsFingerRecognized())
            {
                e.Handled = true;

                if (ContactDownOnFacetLabel != null)
                    ContactDownOnFacetLabel(this, e);
            }
        }

        private void OnContactUpOnFacetLabel(object sender, TouchEventArgs e)
        {
            if (e.TouchDevice.IsFingerRecognized())
            {
                e.Handled = true;
                if (ContactUpOnFacetLabel != null)
                    ContactUpOnFacetLabel(this, e);
            }
        }

        private void OnContactDownOnValueLabel(object sender, TouchEventArgs e)
        {
            if (e.TouchDevice.IsFingerRecognized())
            {
                e.Handled = true;

                if (ContactDownOnValueLabel != null)
                    ContactDownOnValueLabel(this, e);
            }
        }

        private void OnContactUpOnValueLabel(object sender, TouchEventArgs e)
        {
            if (e.TouchDevice.IsFingerRecognized())
            {
                e.Handled = true;
                if (ContactUpOnValueLabel != null)
                    ContactUpOnValueLabel(this, e);
            }
        }

        #endregion

        #region private methods

        private void unregisterEventsOnCircles()
        {
            facetCircle.TouchDown -= OnContactDownOnFacetLabel;
            valueCircle.TouchDown -= OnContactDownOnValueLabel;
        }

        private void registerEventsOnCircles()
        {
            facetCircle.TouchDown += OnContactDownOnFacetLabel;
			facetCircle.TouchUp += OnContactUpOnFacetLabel;
            valueCircle.TouchDown += OnContactDownOnValueLabel;
			valueCircle.TouchUp += OnContactUpOnValueLabel;
        }

        private void unregisterEventsOnFacetLabels()
        {
            txtFacetLabel1.TouchDown -= OnContactDownOnFacetLabel;
            txtFacetLabel1.TouchUp -= OnContactUpOnFacetLabel;

            txtFacetLabel2.TouchDown -= OnContactDownOnFacetLabel;
            txtFacetLabel2.TouchUp -= OnContactUpOnFacetLabel;

            txtFacetLabel3.TouchDown -= OnContactDownOnFacetLabel;
            txtFacetLabel3.TouchUp -= OnContactUpOnFacetLabel;
        }

        private void registerEventsOnFacetLabels()
        {
            txtFacetLabel1.TouchDown += OnContactDownOnFacetLabel;
            txtFacetLabel1.TouchUp += OnContactUpOnFacetLabel;

            txtFacetLabel2.TouchDown += OnContactDownOnFacetLabel;
            txtFacetLabel2.TouchUp += OnContactUpOnFacetLabel;

            txtFacetLabel3.TouchDown += OnContactDownOnFacetLabel;
            txtFacetLabel3.TouchUp += OnContactUpOnFacetLabel;
        }

        private void unregisterEventsOnValueLabels()
        {
            txtValueLabel1.TouchDown -= OnContactDownOnValueLabel;
            txtValueLabel1.TouchUp -= OnContactUpOnValueLabel;

            txtValueLabel2.TouchDown -= OnContactDownOnValueLabel;
            txtValueLabel2.TouchUp -= OnContactUpOnValueLabel;

            txtValueLabel3.TouchDown -= OnContactDownOnValueLabel;
            txtValueLabel3.TouchUp -= OnContactUpOnValueLabel;
        }

        private void registerEventsOnValueLabels()
        {
            txtValueLabel1.TouchDown += OnContactDownOnValueLabel;
            txtValueLabel1.TouchUp += OnContactUpOnValueLabel;

            txtValueLabel2.TouchDown += OnContactDownOnValueLabel;
            txtValueLabel2.TouchUp += OnContactUpOnValueLabel;

            txtValueLabel3.TouchDown += OnContactDownOnValueLabel;
            txtValueLabel3.TouchUp += OnContactUpOnValueLabel;
        }

        #endregion

        #region Label Rotation

        private void applyLocalLabelRotation(TextOnArc label, double outerRadius, double startAngle)
        {
            double endAngle = startAngle + 120.0;

            PathFigure figure = new PathFigure();
            ArcSegment arcSegment = new ArcSegment();

            Point startPoint;
            Point endPoint;
            Size arcSize;

            double offsetFromWedge = 10.0;

            startPoint = new Point(
                    -Math.Cos((startAngle + offsetFromWedge) * Math.PI / 180.0) * (outerRadius),
                    Math.Sin((startAngle + offsetFromWedge) * Math.PI / 180.0) * (outerRadius));

            startPoint.Offset(Properties.Settings.Default.WheelRadius, Properties.Settings.Default.WheelRadius);

            endPoint = new Point(
                -Math.Cos((endAngle - offsetFromWedge) * Math.PI / 180.0) * (outerRadius),
                Math.Sin((endAngle - offsetFromWedge) * Math.PI / 180.0) * (outerRadius));

            endPoint.Offset(Properties.Settings.Default.WheelRadius, Properties.Settings.Default.WheelRadius);

            arcSize = new Size(outerRadius, outerRadius);

            figure.StartPoint = startPoint;
            arcSegment.Point = endPoint;
            arcSegment.Size = arcSize;

            figure.Segments.Add(arcSegment);
            label.TextAlignment = TextAlignment.Center;
            label.PathFigure = figure;

            //Path p = new Path();
            //p.Stroke = Brushes.Red;
            //p.StrokeThickness = 2.0;
            //p.Data = new PathGeometry(new PathFigure[] { figure });
            //gridContainer.Children.Add(p);
        }

        #endregion

        #region dp

        #region Facet Label

        public String FacetLabel
        {
            get { return (String)GetValue(FacetLabelProperty); }
            set { SetValue(FacetLabelProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FacetLabel1.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FacetLabelProperty =
            DependencyProperty.Register("FacetLabel", typeof(String), typeof(OuterNodeLabels), new FrameworkPropertyMetadata("FacetLabel"));

        #endregion

        #region Value Label

        public String ValueLabel
        {
            get { return (String)GetValue(ValueLabelProperty); }
            set { SetValue(ValueLabelProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ValueLabel1.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueLabelProperty =
            DependencyProperty.Register("ValueLabel", typeof(String), typeof(OuterNodeLabels), new UIPropertyMetadata("ValueLabel"));

        #endregion

        #region ReceivesContactEvents



        public bool ReceivesContactEvents
        {
            get { return (bool)GetValue(ReceivesContactEventsProperty); }
            set { SetValue(ReceivesContactEventsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ReceivesContactEvents.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ReceivesContactEventsProperty =
            DependencyProperty.Register("ReceivesContactEvents", typeof(bool), typeof(OuterNodeLabels), new UIPropertyMetadata(true, OnReceivesContactEventsChanged));

        private static void OnReceivesContactEventsChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            bool newValue = (bool)e.NewValue;
            if (newValue)
            {
                (sender as OuterNodeLabels).unregisterEventsOnCircles();
                (sender as OuterNodeLabels).unregisterEventsOnFacetLabels();
                (sender as OuterNodeLabels).unregisterEventsOnValueLabels();

                (sender as OuterNodeLabels).registerEventsOnCircles();
                (sender as OuterNodeLabels).registerEventsOnFacetLabels();
                (sender as OuterNodeLabels).registerEventsOnValueLabels();
            }
            else
            {
                (sender as OuterNodeLabels).unregisterEventsOnCircles();
                (sender as OuterNodeLabels).unregisterEventsOnFacetLabels();
                (sender as OuterNodeLabels).unregisterEventsOnValueLabels();
            }
        }

        #endregion

        #region Facet Font Size



        public double FacetFontSize
        {
            get { return (double)GetValue(FacetFontSizeProperty); }
            set { SetValue(FacetFontSizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FacetFontSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FacetFontSizeProperty =
            DependencyProperty.Register("FacetFontSize", typeof(double), typeof(OuterNodeLabels), new UIPropertyMetadata(12.0));



        #endregion

        #region Value Font Size

        public double ValueFontSize
        {
            get { return (double)GetValue(ValueFontSizeProperty); }
            set { SetValue(ValueFontSizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ValueFontSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueFontSizeProperty =
            DependencyProperty.Register("ValueFontSize", typeof(double), typeof(OuterNodeLabels), new UIPropertyMetadata(12.0));



        #endregion

		#endregion
	}
}
