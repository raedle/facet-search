﻿using System.Windows.Controls;

namespace BlendedLibrary.FacetSearch.Application.View.Node
{
    /// <summary>
    /// Interaction logic for QueryCanvasOutOfScreenNodeView.xaml
    /// </summary>
    public partial class QueryCanvasOutOfScreenNodeView : UserControl
    {
        public QueryCanvasOutOfScreenNodeView()
        {
            InitializeComponent();
        }
    }
}
