﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using BlendedLibrary.FacetSearch.Application.CircleControl;
using BlendedLibrary.FacetSearch.Application.Properties;

namespace BlendedLibrary.FacetSearch.Application.View.Node
{
    /// <summary>
    /// Interaction logic for QueryCanvasNodeView.xaml
    /// </summary>
    public partial class QueryCanvasFacetNodeView : UserControl
    {
        public QueryCanvasFacetNodeView()
        {
            InitializeComponent();

            //nur updaten wenn value whele ausgeblendet wird -> weniger anfragen
            ValueWheelSelector.Loaded += (sender, e) =>
                          {
                              var bindingExpression = ValueWheelSelector.GetBindingExpression(ValueWheelSelector.ValueExpressionProperty);
                              Debug.Assert(bindingExpression != null);
                              ValueWheelSelector.IsVisibleChanged += (sender2, e2) =>
                                                                         {
                                                                             if (!(bool) e2.NewValue)
                                                                                 bindingExpression.UpdateSource();
                                                                         };
                          };

            Loaded += (sender, e) =>
                {
                    applyLocalLabelRotation(TxtFacetLabel1, Settings.Default.FacetLabelRadius, 0.0);
                    applyLocalLabelRotation(TxtFacetLabel2, Settings.Default.FacetLabelRadius, 120.0);
                    applyLocalLabelRotation(TxtFacetLabel3, Settings.Default.FacetLabelRadius, 240.0);

                    applyLocalLabelRotation(txtValueLabel1, Settings.Default.ValueLabelRadius, 0.0);
                    applyLocalLabelRotation(txtValueLabel2, Settings.Default.ValueLabelRadius, 120.0);
                    applyLocalLabelRotation(txtValueLabel3, Settings.Default.ValueLabelRadius, 240.0);
                };
        }

        private void applyLocalLabelRotation(TextOnArc label, double outerRadius, double startAngle)
        {
            double endAngle = startAngle + 120.0;

            PathFigure figure = new PathFigure();
            ArcSegment arcSegment = new ArcSegment();

            Point startPoint;
            Point endPoint;
            Size arcSize;

            double offsetFromWedge = 10.0;

            startPoint = new Point(
                    -Math.Cos((startAngle + offsetFromWedge) * Math.PI / 180.0) * (outerRadius),
                    Math.Sin((startAngle + offsetFromWedge) * Math.PI / 180.0) * (outerRadius));

            startPoint.Offset(Settings.Default.WheelRadius, Settings.Default.WheelRadius);

            endPoint = new Point(
                -Math.Cos((endAngle - offsetFromWedge) * Math.PI / 180.0) * (outerRadius),
                Math.Sin((endAngle - offsetFromWedge) * Math.PI / 180.0) * (outerRadius));

            endPoint.Offset(Settings.Default.WheelRadius, Settings.Default.WheelRadius);

            arcSize = new Size(outerRadius, outerRadius);

            figure.StartPoint = startPoint;
            arcSegment.Point = endPoint;
            arcSegment.Size = arcSize;

            figure.Segments.Add(arcSegment);
            label.TextAlignment = TextAlignment.Center;
            label.PathFigure = figure;

            //Path p = new Path();
            //p.Stroke = Brushes.Red;
            //p.StrokeThickness = 2.0;
            //p.Data = new PathGeometry(new PathFigure[] { figure });
            //gridContainer.Children.Add(p);
        }

        private Path _visualDebuggingEllipse;
        private Path _visualDebuggingLink;

        protected override GeometryHitTestResult HitTestCore(GeometryHitTestParameters hitTestParameters)
        {
            //return base.HitTestCore(hitTestParameters);

            //if (_visualDebuggingEllipse == null)
            //{
            //    _visualDebuggingEllipse = new Path();

            //    _visualDebuggingEllipse.Fill = Brushes.Lime;

            //    TreeHelper.TryFindParent<QueryCanvasView>(this).DebuggingCanvas.Children.Add(_visualDebuggingEllipse);
            //}

            //if (_visualDebuggingLink == null)
            //{
            //    _visualDebuggingLink = new Path();
            //    _visualDebuggingLink.Fill = Brushes.DeepPink;
            //    TreeHelper.TryFindParent<QueryCanvasView>(this).DebuggingCanvas.Children.Add(_visualDebuggingLink);
            //}

            var center = new Point(
                Settings.Default.WheelRadius,
                Settings.Default.WheelRadius);

            var ellipse = new EllipseGeometry(center, Settings.Default.NodeRadius, Settings.Default.NodeRadius);

            var originalGeometry = hitTestParameters.HitGeometry;
            
            //var pg = PathGeometry.CreateFromGeometry(originalGeometry);
            
            //_visualDebuggingEllipse.Data = ellipse;
            //_visualDebuggingLink.Data = pg;

            var intersectionDetails = ellipse.FillContainsWithDetail(originalGeometry);
            
            return new GeometryHitTestResult(this, intersectionDetails);
        }
    }
}
