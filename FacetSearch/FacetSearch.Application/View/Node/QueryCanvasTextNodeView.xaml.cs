﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using BlendedLibrary.Common.Control;
using BlendedLibrary.Common.Util;
using BlendedLibrary.FacetSearch.Application.Annotations;
using BlendedLibrary.FacetSearch.Application.ViewModel.Node;
using BlendedLibrary.FacetSearch.Model;

namespace BlendedLibrary.FacetSearch.Application.View.Node
{
    /// <summary>
    /// Interaction logic for QueryCanvasTextNodeView.xaml
    /// </summary>
    public partial class QueryCanvasTextNodeView : UserControl, INotifyPropertyChanged, INotifyPropertyChanging
    {
        #region private fields

        private QueryCanvasView _queryCanvasView;

        #endregion

        #region Properties

        #region Keyboard

        /// <summary>
        /// The <see cref="Keyboard" /> property's name.
        /// </summary>
        public const string KeyboardPropertyName = "Keyboard";

        private VirtualKeyboard _keyboard;

        /// <summary>
        /// Sets and gets the Keyboard property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public VirtualKeyboard Keyboard
        {
            get
            {
                return _keyboard;
            }

            set
            {
                if (_keyboard == value)
                {
                    return;
                }

                RaisePropertyChanging(KeyboardPropertyName);
                _keyboard = value;
                RaisePropertyChanged(KeyboardPropertyName);
            }
        }

        #endregion

        #endregion

        public QueryCanvasTextNodeView()
        {
            InitializeComponent();

            Unloaded += (sender, e) =>
                {
                    if (Keyboard != null)
                    {
                        _queryCanvasView.KeyboardCanvas.Children.Remove(Keyboard);
                        Keyboard = null;
                    }
                };
        }

        protected override GeometryHitTestResult HitTestCore(GeometryHitTestParameters hitTestParameters)
        {
            //var rectangle = new RectangleGeometry(new Rect(0, 30, 110, 45));

            var rectangle = new RectangleGeometry(new Rect(0, 0, Width, Height));

            var originalGeometry = hitTestParameters.HitGeometry;

            //var pg = PathGeometry.CreateFromGeometry(originalGeometry);

            //_visualDebuggingEllipse.Data = ellipse;
            //_visualDebuggingLink.Data = pg;

            var intersectionDetails = rectangle.FillContainsWithDetail(originalGeometry);

            return new GeometryHitTestResult(this, intersectionDetails);
        }

        private void UIElement_OnTouchDown(object sender, TouchEventArgs e)
        {
            sender = sender as FrameworkElement;
            var vm = DataContext as QueryCanvasAndNodeViewModel;

            if (Keyboard == null)
            {
                if (_queryCanvasView == null)
                    _queryCanvasView = TreeHelper.TryFindParent<QueryCanvasView>(sender as DependencyObject);


                Keyboard = new VirtualKeyboard
                           {
                               AssociatedObjectPosition = vm
                           };
                Keyboard.OnEnter += (s, e2) =>
                {
                    _queryCanvasView.KeyboardCanvas.Children.Remove(Keyboard);
                    Keyboard = null;
                    (DataContext as QueryCanvasTextNodeViewModel).ApplyKeyword();
                };

                var b = new Binding(QueryCanvasTextNode.TextPropertyName)
                {
                    Mode = BindingMode.TwoWay,
                    Source = (DataContext as QueryCanvasTextNodeViewModel).Model
                };
                BindingOperations.SetBinding(Keyboard, VirtualKeyboard.TextProperty, b);
                _queryCanvasView.KeyboardCanvas.Children.Add(Keyboard);
            }

            var parentContentControl = TreeHelper.TryFindParent<ContentControl>(sender as DependencyObject);
            var transformGroup = parentContentControl.RenderTransform as TransformGroup;
            var textBlockAngle = 0d;
            foreach (var transform in transformGroup.Children)
            {
                if (transform is RotateTransform)
                {
                    var rotateTransform = transform as RotateTransform;
                    textBlockAngle = rotateTransform.Angle;
                }
            }

            var center = vm.Center;
            var angle = (DataContext as QueryCanvasTextNodeViewModel).Angle;
            var rAngle = (Math.PI / 180) * angle;
            const double offset = 300;

            var offsetX = -Math.Sin(rAngle) * offset;
            var offsetY = Math.Cos(rAngle) * offset;

            if (textBlockAngle > 90 && textBlockAngle < 270)
            {
                offsetX = -offsetX;
                offsetY = -offsetY;
            }

            Keyboard.X = center.X;
            Keyboard.Y = center.Y;
            Keyboard.OffsetX = offsetX;
            Keyboard.OffsetY = offsetY;
            Keyboard.Angle = angle + textBlockAngle;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangingEventHandler PropertyChanging;

        [NotifyPropertyChangingInvocator]
        protected virtual void RaisePropertyChanging([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanging;
            if (handler != null) handler(this, new PropertyChangingEventArgs(propertyName));
        }
    }
}
