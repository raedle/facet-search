﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using BlendedLibrary.FacetSearch.Application.CircleControl;
using BlendedLibrary.FacetSearch.Application.Properties;

namespace BlendedLibrary.FacetSearch.Application.View.Node
{
    /// <summary>
    /// Interaction logic for QueryCanvasResultTokenView.xaml
    /// </summary>
    public partial class QueryCanvasResultTokenView : UserControl
    {
        public QueryCanvasResultTokenView()
        {
            InitializeComponent();

            Loaded += QueryCanvasResultTokenView_Loaded;
        }

        void QueryCanvasResultTokenView_Loaded(object sender, RoutedEventArgs e)
        {
            var s = Settings.Default;
            applyLocalLabelRotation(ResultTxt1, s.ResultTokenRadius * 0.9, 0);
            applyLocalLabelRotation(ResultTxt2, s.ResultTokenRadius * 0.9, 180);
        }

        private void applyLocalLabelRotation(TextOnArc label, double outerRadius, double startAngle)
        {
            var s = Settings.Default;

            double endAngle = startAngle + 180.0;

            PathFigure figure = new PathFigure();
            ArcSegment arcSegment = new ArcSegment();

            Point startPoint;
            Point endPoint;
            Size arcSize;

            double offsetFromWedge = 10.0;

            startPoint = new Point(
                    -Math.Cos((startAngle + offsetFromWedge) * Math.PI / 180.0) * (outerRadius),
                    Math.Sin((startAngle + offsetFromWedge) * Math.PI / 180.0) * (outerRadius));

            startPoint.Offset(s.ResultTokenRadius, s.ResultTokenRadius);

            endPoint = new Point(
                -Math.Cos((endAngle - offsetFromWedge) * Math.PI / 180.0) * (outerRadius),
                Math.Sin((endAngle - offsetFromWedge) * Math.PI / 180.0) * (outerRadius));

            endPoint.Offset(s.ResultTokenRadius, s.ResultTokenRadius);

            arcSize = new Size(outerRadius, outerRadius);

            figure.StartPoint = startPoint;
            arcSegment.Point = endPoint;
            arcSegment.Size = arcSize;

            figure.Segments.Add(arcSegment);
            label.TextAlignment = TextAlignment.Center;
            label.PathFigure = figure;

            //Path p = new Path();
            //p.Stroke = Brushes.Red;
            //p.StrokeThickness = 2.0;
            //p.Data = new PathGeometry(new PathFigure[] { figure });
            //gridContainer.Children.Add(p);
        }

        protected override GeometryHitTestResult HitTestCore(GeometryHitTestParameters hitTestParameters)
        {
            var center = new Point(Settings.Default.ResultTokenRadius, Settings.Default.ResultTokenRadius);

            var ellipse = new EllipseGeometry(center, Settings.Default.ResultTokenRadius, Settings.Default.ResultTokenRadius);

            var originalGeometry = hitTestParameters.HitGeometry;

            var intersectionDetails = ellipse.FillContainsWithDetail(originalGeometry);

            return new GeometryHitTestResult(this, intersectionDetails);
        }
    }
}
