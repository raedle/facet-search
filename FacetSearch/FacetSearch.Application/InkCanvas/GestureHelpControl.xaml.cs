﻿using System.Windows.Controls;

namespace BlendedLibrary.FacetSearch.Application.InkCanvas
{
    /// <summary>
    /// Interaction logic for GestureHelpControl.xaml
    /// </summary>
    public partial class GestureHelpControl : UserControl
    {
        public GestureHelpControl()
        {
            InitializeComponent();
        }
    }
}
