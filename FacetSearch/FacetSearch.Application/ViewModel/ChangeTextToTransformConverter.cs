using System;
using System.Globalization;
using System.Windows.Data;

namespace BlendedLibrary.FacetSearch.Application.ViewModel
{
    public class ChangeTextToTransformConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return 0.0;

            var text = (String)value;

            double transformation = ((25/21)*text.Length + 5);

            return -(transformation < 25 ? 25 : transformation);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}