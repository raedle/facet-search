using BlendedLibrary.FacetSearch.Application.Properties;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.ObjectModel;
using System.Deployment.Application;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using InputKeyboard = System.Windows.Input.Keyboard;
using BlendedLibrary.Common.Control;
using BlendedLibrary.Common.Util;
using BlendedLibrary.Data;
using BlendedLibrary.Data.Configuration;
using BlendedLibrary.FacetSearch.Application.Utils;
using BlendedLibrary.FacetStreams.Data.Xml;
using BlendedLibrary.FacetStreams.Data.Xml.Configuration;
using BlendedLibrary.FacetStreams.Data.Xml.Configuration.EDS;
using BlendedLibrary.FacetStreams.Data.Xml.Configuration.Solr;
using BlendedLibrary.FacetStreams.Data.Xml.Configuration.Summon;
using BlendedLibrary.Service;

namespace BlendedLibrary.FacetSearch.Application.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        #region commands

        public RelayCommand<SenderAwareEventArgs> LoadedCommand { get; private set; }

        public RelayCommand<KeyEventArgs> ShortcutCommand { get; private set; }

        #endregion

        #region properties

        #region ApplicationVersion

        /// <summary>
        /// The <see cref="ApplicationVersion" /> property's name.
        /// </summary>
        public const string ApplicationVersionPropertyName = "ApplicationVersion";

        private string _applicationVersion = string.Empty;

        /// <summary>
        /// Sets and gets the ApplicationVersion property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ApplicationVersion
        {
            get
            {
                return _applicationVersion;
            }

            set
            {
                if (_applicationVersion == value)
                {
                    return;
                }

                _applicationVersion = value;
                RaisePropertyChanged(ApplicationVersionPropertyName);
            }
        }

        #endregion

        #region QueryCanvasViewModel

        /// <summary>
        /// The <see cref="QueryCanvasViewModel" /> property's name.
        /// </summary>
        public const string QueryCanvasViewModelPropertyName = "QueryCanvasViewModel";

        private QueryCanvasViewModel _queryCanvasViewModel = new QueryCanvasViewModel();

        /// <summary>
        /// Sets and gets the QueryCanvasViewModel property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public QueryCanvasViewModel QueryCanvasViewModel
        {
            get
            {
                return _queryCanvasViewModel;
            }

            set
            {
                if (_queryCanvasViewModel == value)
                {
                    return;
                }

                _queryCanvasViewModel = value;
                RaisePropertyChanged(QueryCanvasViewModelPropertyName);
            }
        }

        #endregion

        #region IsConnected

        /// <summary>
        /// The <see cref="IsConnected" /> property's name.
        /// </summary>
        public const string IsConnectedPropertyName = "IsConnected";

        private bool _isConnected;

        /// <summary>
        /// Sets and gets the IsConnected property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsConnected
        {
            get
            {
                return _isConnected;
            }

            set
            {
                if (_isConnected == value)
                {
                    return;
                }

                _isConnected = value;
                RaisePropertyChanged(IsConnectedPropertyName);
            }
        }

        #endregion

        #region UserFeedback

        /// <summary>
        /// The <see cref="UserFeedback" /> property's name.
        /// </summary>
        public const string UserFeedbackPropertyName = "UserFeedback";

        private string _userFeedback = string.Empty;

        /// <summary>
        /// Sets and gets the UserFeedback property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string UserFeedback
        {
            get
            {
                return _userFeedback;
            }

            set
            {
                if (_userFeedback == value)
                {
                    return;
                }

                _userFeedback = value;
                RaisePropertyChanged(UserFeedbackPropertyName);
            }
        }

        #endregion

        #endregion

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            ShortcutCommand = new RelayCommand<KeyEventArgs>(OnShortcut);
            LoadedCommand = new RelayCommand<SenderAwareEventArgs>(OnLoaded);

            QueryCanvasViewModel.FacetTokenIds = Settings.Default.FacetTokenIds;
            QueryCanvasViewModel.TextTokenIds = Settings.Default.TextTokenIds;
            QueryCanvasViewModel.ResultTokenIds = Settings.Default.ResultTokenIds;

            PropertyChanged += (sender, args) =>
            {
                if (QueryCanvasViewModelPropertyName.Equals(args.PropertyName))
                {
                    QueryCanvasViewModel.FacetTokenIds = Settings.Default.FacetTokenIds;
                    QueryCanvasViewModel.TextTokenIds = Settings.Default.TextTokenIds;
                    QueryCanvasViewModel.ResultTokenIds = Settings.Default.ResultTokenIds;
                }
            };

            if (ApplicationDeployment.IsNetworkDeployed)
                ApplicationVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
        }

        #region private methods

        /// <summary>
        /// Key shortcuts to open data provider, settings dialog.
        /// </summary>
        /// <param name="e"></param>
        private void OnShortcut(KeyEventArgs e)
        {
            if (e.IsRepeat) return;

            if ((InputKeyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                switch (e.Key)
                {
                    case Key.D:
                        ConfigureDataProvider();
                        break;
                    case Key.S:
                        ShowSettingsDialog();
                        break;
                }
            }
        }

        private void OnLoaded(SenderAwareEventArgs e)
        {
            // focus this element is required to receive key commands
            var control = e.Sender as UserControl;
            if (control != null)
                control.Focus();

            if (string.IsNullOrWhiteSpace(Settings.Default.DataProviderType))
            {
                ShowDataServiceDialog();
            }

            ApplyDataProvider();
        }

        private void ConfigureDataProvider()
        {
            if (ShowDataServiceDialog())
            {
                // remove old nodes and links
                if (QueryCanvasViewModel != null)
                    QueryCanvasViewModel.Dispose(true);

                QueryCanvasViewModel = new QueryCanvasViewModel();
                IsConnected = false;

                ApplyDataProvider();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static bool ShowDataServiceDialog()
        {
            // Present configuration dialog to user
            var dialog = new SettingsDialog
                         {
                             Title = Resources.DataProviderDialog,
                             SelectedKey = Settings.Default.DataProviderType,
                             Objects = new ObservableCollection<SettingsDialogItem>
                                       {
                                           new SettingsDialogItem(typeof(Summon).Name, typeof(Summon), ComputeDataProviderConfiguration) { FriendlyName = "Summon" },
                                           new SettingsDialogItem(typeof(EDS).Name, typeof(EDS), ComputeDataProviderConfiguration) { FriendlyName = "Rds" },
                                           new SettingsDialogItem(typeof(Solr).Name, typeof(Solr), ComputeDataProviderConfiguration) { FriendlyName = "Solr" }
                                       }
                         };

            var result = dialog.ShowDialog();

            if (result.HasValue && result.Value)
            {
                switch (dialog.Result)
                {
                    case SettingsDialogResult.Ok:

                        // Stop old server instance if running.
                        if (BlendedLibraryService.IsRunning(Settings.Default.DatabasePort))
                        {
                            BlendedLibraryService.Stop(Settings.Default.DatabasePort);
                        }

                        var configuration = dialog.SelectedObject as IDataProviderConfiguration;

                        if (configuration != null)
                        {
                            DataProviderUtility.SaveConfiguration(configuration);
                            DataProviderUtility.SaveProxyConfiguration(configuration.Type);

                            Settings.Default.DataProviderType = dialog.SelectedObject.GetType().Name;
                            Settings.Default.Save();
                            return true;
                        }
                        return false;
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private static object ComputeDataProviderConfiguration(SettingsDialogItem item)
        {
            var type = item.Value as Type;

            if (type == null) return null;

            IDataProviderConfiguration dataProviderConfiguration;
            try
            {
                dataProviderConfiguration = DataProviderUtility.LoadConfiguration(type);
            }
            catch (Exception)
            {
                dataProviderConfiguration = Activator.CreateInstance(type) as IDataProviderConfiguration;
            }
            return dataProviderConfiguration;
        }

        private static void ShowSettingsDialog()
        {
            // Present configuration dialog to user
            var dialog = new SettingsDialog
            {
                Objects = new ObservableCollection<SettingsDialogItem>
                          {
                              new SettingsDialogItem("Settings", Settings.Default),
                              //new SettingsDialogItem("Data Provider Settings", BaseXSe.Default),
                              new SettingsDialogItem("MultiTaction Settings", MultiTactionSettings.Default)
                          }
            };
            dialog.ShowDialog();
        }

        /// <summary>
        /// Connects to data service and sets data provider on query canvas view model.
        /// </summary>
        private void ApplyDataProvider()
        {
            var task = Task.Factory.StartNew<IDataProvider>(ConnectToDataService);

            task.ContinueWith(t =>
            {
                var exception = t.Exception;
                if (exception != null)
                {
                    foreach (var innerException in exception.InnerExceptions)
                    {
                        HandleException(innerException);
                    }

                    MessageBox.Show(Resources.ApplicationWillExit, Resources.ExceptionOccured, MessageBoxButton.OK, MessageBoxImage.Information);
                    Environment.Exit(-100);
                }

                var dataProvider = t.Result;

                QueryCanvasViewModel.DataProvider = dataProvider;
                System.Windows.Application.Current.Exit += (_, __) => dataProvider.Dispose();

                IsConnected = true;
                UserFeedback = null;
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private IDataProvider ConnectToDataService()
        {
            IsConnected = false;
            UserFeedback = Resources.ConnectingToServices;

            var workingDirectory = DataProviderUtility.GetAppDataPath();

            var s = Settings.Default;
            IDataProviderConfiguration dataProviderConfiguration = new BaseXDataProviderConfiguration(s.DatabaseHost, s.DatabasePort, s.DatabaseEventPort, workingDirectory, s.DatabaseUsername, s.DatabasePassword, s.CatalogType.DatabaseName(), s.CachingEnabled);

            IDataProvider dataProvider;
            switch (s.CatalogType)
            {
                case SourceType.Rds:
                    dataProvider = new IndexDataProvider((BaseXDataProviderConfiguration)dataProviderConfiguration, "IndexData.qc");
                    break;
                default:
                    dataProvider = new DebugDataProvider();
                    break;
            }

            var xmlDataProviderBase = dataProvider as XmlDataProviderBase;
            if (xmlDataProviderBase != null)
                try
                {
                    xmlDataProviderBase.Connect();
                }
                catch (Exception e)
                {
                    UserFeedback = e.Message;
                    MessageBox.Show(Resources.ApplicationWillExit, Resources.ExceptionOccured, MessageBoxButton.OK, MessageBoxImage.Information);
                    Environment.Exit(-100);
                }

            return dataProvider;
        }

        /// <summary>
        /// Handle exception that occur during application usage.
        /// </summary>
        /// <param name="e"></param>
        private void HandleException(Exception e)
        {
            var exception = e as BlendedLibraryServiceException;
            if (exception != null)
            {
                switch (exception.Code)
                {
                    case BlendedLibraryService.BEX07:
                    case BlendedLibraryService.BEX08:

                        MessageBox.Show(e.Message, Resources.ExceptionOccured, MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                }
            }
        }

        #endregion
    }
}