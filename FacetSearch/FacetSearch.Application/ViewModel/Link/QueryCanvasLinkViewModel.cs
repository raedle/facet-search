﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;
using BlendedLibrary.Common;
using BlendedLibrary.Common.Model;
using BlendedLibrary.Data;
using BlendedLibrary.FacetSearch.Application.ViewModel.Node;
using BlendedLibrary.FacetSearch.Model;
using GalaSoft.MvvmLight.Command;

namespace BlendedLibrary.FacetSearch.Application.ViewModel.Link
{
    public class QueryCanvasLinkViewModel : QueryCanvasItemViewModel, ILink
    {
        #region private fields

        private readonly QueryCanvasLink _link;

        #endregion

        #region commands

        public RelayCommand UnloadCommand { get; private set; }

        #endregion

        #region public properties

        #region Source

        private QueryCanvasNodeViewModel _source = null;

        /// <summary>
        /// Gets or sets the Source property. This observable property 
        /// indicates ....
        /// </summary>
        public QueryCanvasNodeViewModel Source
        {
            get { return _source; }
            set
            {
                if (_source != value)
                {
                    _source = value;
                    RaisePropertyChanged("Source");
                }
            }
        }

        #endregion

        #region Destination

        private QueryCanvasNodeViewModel _destination = null;

        /// <summary>
        /// Gets or sets the Destination property. This observable property 
        /// indicates ....
        /// </summary>
        public QueryCanvasNodeViewModel Destination
        {
            get { return _destination; }
            set
            {
                if (_destination != value)
                {
                    _destination = value;
                    RaisePropertyChanged("Destination");
                }
            }
        }

        #endregion

        #region LinkWidth

        private double _linkWidth = 14;

        /// <summary>
        /// Gets or sets the LinkWidth property. This observable property 
        /// indicates ....
        /// </summary>
        public double LinkWidth
        {
            get { return _linkWidth; }
            set
            {
                if (_linkWidth != value)
                {
                    _linkWidth = value;
                    RaisePropertyChanged("LinkWidth");
                }
            }
        }

        #endregion

        #region Pulses

        /// <summary>
        /// The <see cref="Pulses" /> property's name.
        /// </summary>
        public const string PulsesPropertyName = "Pulses";

        private ObservableCollection<LinkPulseViewModel> _pulses = new ObservableCollection<LinkPulseViewModel>();

        /// <summary>
        /// Sets and gets the Pulses property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<LinkPulseViewModel> Pulses
        {
            get
            {
                return _pulses;
            }

            set
            {
                if (_pulses == value)
                {
                    return;
                }

                _pulses = value;
                RaisePropertyChanged(PulsesPropertyName);
            }
        }

        #endregion

        #region IsPulseEnabled

        /// <summary>
        /// The <see cref="IsPulseEnabled" /> property's name.
        /// </summary>
        public const string IsPulseEnabledPropertyName = "IsPulseEnabled";

        private bool _isPulseEnabled;

        /// <summary>
        /// Sets and gets the IsPulseEnabled property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsPulseEnabled
        {
            get
            {
                return _isPulseEnabled;
            }

            set
            {
                if (_isPulseEnabled == value)
                {
                    return;
                }

                _isPulseEnabled = value;
                RaisePropertyChanged(IsPulseEnabledPropertyName);
            }
        }

        #endregion

        #endregion

        #region ctor

        public QueryCanvasLinkViewModel(IDataProvider dataProvider, QueryCanvasItem model, QueryCanvasViewModel canvasViewModel)
            : base(dataProvider, model, canvasViewModel)
        {
            _link = Model as QueryCanvasLink;
            Debug.Assert(_link != null, "_link != null");

            Source = canvasViewModel.GetViewModel(_link.Source) as QueryCanvasNodeViewModel;
            Debug.Assert(Source != null, "Source != null");

            if (Source == null)
                throw new Exception("Source == null");

            Destination = canvasViewModel.GetViewModel(_link.Destination) as QueryCanvasNodeViewModel;
            Debug.Assert(Destination != null, "Destination != null");

            if (Destination == null)
                throw new Exception("Destination == null");

            var random = new Random();

            for (var i = 0; i < 5; i++)
            {
                Pulses.Add(new LinkPulseViewModel(new LinkPulse { Speed = random.Next(1, 10), Radius = random.Next(0, 200) }));
            }

            // Unload/dispose view model
            UnloadCommand = new RelayCommand(Dispose);

            Update();

            //Make sure we get the updates of the source and destination nodes when they are being moved around
            RegisterChangeNotificationAction(Destination.Center, Position.XPropertyName, e => OnDestinationPositionChanged());
            RegisterChangeNotificationAction(Destination.Center, Position.YPropertyName, e => OnDestinationPositionChanged());
            RegisterChangeNotificationAction(Source.Center, Position.XPropertyName, e => OnSourcePositionChanged());
            RegisterChangeNotificationAction(Source.Center, Position.YPropertyName, e => OnSourcePositionChanged());

            FrameRenderer.FrameUpdating += PulseRendering;
        }

        #endregion

        void OnSourcePositionChanged()
        {
            AdjustLinkPulses();
        }

        void OnDestinationPositionChanged()
        {
            AdjustLinkPulses();
        }

        /// <summary>
        /// Adjust position of all link pulse objects based on the source position and destination position. This
        /// method calculates the angle between source and destination based on the global coordinate system (screen).
        /// </summary>
        private void AdjustLinkPulses()
        {
            var source = Source;
            var destination = Destination;

            if (source == null || destination == null) return;

            var sCenter = source.Center;
            var dCenter = destination.Center;

            var xDiff = dCenter.X - sCenter.X;
            var yDiff = dCenter.Y - sCenter.Y;

            var angle = Math.Atan2(yDiff, xDiff);

            foreach (var pulse in Pulses)
            {
                pulse.AdjustPosition(sCenter, angle);
            }
        }

        void PulseRendering(object sender, EventArgs e)
        {
            if (!IsPulseEnabled) return;

            Pulse();
            AdjustLinkPulses();
        }

        /// <summary>
        /// Pulses all link pulse objects based on the source position and destination position. This method
        /// calls the beat method of each link pulse object. If the link pulse arrived at the destination
        /// it will be reset.
        /// </summary>
        private void Pulse()
        {
            var source = Source;
            var destination = Destination;

            if (source == null || destination == null) return;

            var sCenter = source.Center;
            var dCenter = destination.Center;

            foreach (var pulse in Pulses)
            {
                pulse.Beat();

                // Reset pulse if arrived at destination
                if (pulse.Model.Radius > (sCenter - dCenter).Length)
                    pulse.Reset();
            }
        }

        internal override Point? GetNearestIntersectionPoint(LineGeometry geometry)
        {
            var l1X1 = geometry.StartPoint.X;
            var l1Y1 = geometry.StartPoint.Y;

            var l1X2 = geometry.EndPoint.X;
            var l1Y2 = geometry.EndPoint.Y;

            var l2X1 = Source.Center.X;
            var l2Y1 = Source.Center.Y;

            var l2X2 = Destination.Center.X;
            var l2Y2 = Destination.Center.Y;

            // Denominator for ua and ub are the same, so store this calculation
            double d = (l2Y2 - l2Y1) * (l1X2 - l1X1) - (l2X2 - l2X1) * (l1Y2 - l1Y1);

            //n_a and n_b are calculated as seperate values for readability
            double n_a = (l2X2 - l2X1) * (l1Y1 - l2Y1) - (l2Y2 - l2Y1) * (l1X1 - l2X1);

            double n_b = (l1X2 - l1X1) * (l1Y1 - l2Y1) - (l1Y2 - l1Y1) * (l1X1 - l2X1);

            // Make sure there is not a division by zero - this also indicates that
            // the lines are parallel.  
            // If n_a and n_b were both equal to zero the lines would be on top of each 
            // other (coincidental).  This check is not done because it is not 
            // necessary for this implementation (the parallel check accounts for this).
            if (d == 0)
                return null;

            // Calculate the intermediate fractional point that the lines potentially intersect.
            double ua = n_a / d;
            double ub = n_b / d;

            // The fractional point will be between 0 and 1 inclusive if the lines
            // intersect.  If the fractional calculation is larger than 1 or smaller
            // than 0 the lines would need to be longer to intersect.
            if (ua >= 0.0 && ua <= 1.0 && ub >= 0.0 && ub <= 1.0)
            {
                Point intersection = new Point();
                intersection.X = l1X1 + (ua * (l1X2 - l1X1));
                intersection.Y = l1Y1 + (ua * (l1Y2 - l1Y1));
                return intersection;
            }
            return null;
        }

        protected override void OnQueryCountChanged(int oldQueryCount, int newQueryCount)
        {
            UpdateLinkWidth();
        }

        internal void TurnIntoExitLink(Position exitLinkDestinationPosition)
        {
            _link.TurnIntoExitLink(exitLinkDestinationPosition);
        }

        private void UpdateLinkWidth()
        {
            LinkWidth = Math.Log10(QueryCount + 1.0) / Math.Log10(DataProvider.TotalResultCount + 1.0) * 12.0 + 2.0;

            var pulseHeight = LinkWidth + 5;
            var pulseWidth = pulseHeight * 1.25;
            foreach (var pulse in Pulses)
            {
                pulse.Width = pulseWidth;
                pulse.Height = pulseHeight;
            }
        }

        internal void Update()
        {
            QueryCount = Source.QueryCount;

            // TODO: It did not work to register and un-register pulsing on composition target renderer during runtime
            // Register pulse animation if results exist, otherwise de-register.
            //if (QueryCount > 0 && !_isPulseAnimationEnabled)
            //{
            //    CompositionTargetEx.FrameUpdating += PulseRendering;
            //    _isPulseAnimationEnabled = true;
            //    Console.WriteLine("Rendering should be enabled: {0}", QueryCount);
            //}
            //else if (QueryCount == 0 && _isPulseAnimationEnabled)
            //{
            //    CompositionTargetEx.FrameUpdating -= PulseRendering;
            //    _isPulseAnimationEnabled = false;
            //    Console.WriteLine("Rendering should be disabled: {0}", QueryCount);
            //}

            IsPulseEnabled = QueryCount > 0;

            UpdateLinkWidth();
        }

        public override void Dispose()
        {
            base.Dispose();

            IsPulseEnabled = false;
            FrameRenderer.FrameUpdating -= PulseRendering;
        }
    }
}
