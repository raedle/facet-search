﻿
using BlendedLibrary.FacetSearch.Application.ViewModel.Node;

namespace BlendedLibrary.FacetSearch.Application.ViewModel.Link
{
    internal interface ILink
    {
        QueryCanvasNodeViewModel Source { get; set; }
        QueryCanvasNodeViewModel Destination { get; set; }
    }
}
