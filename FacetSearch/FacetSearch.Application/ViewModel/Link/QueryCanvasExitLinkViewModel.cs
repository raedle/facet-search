﻿using System.Reactive.Subjects;
using System.Windows.Input;
using BlendedLibrary.Common.Model;
using BlendedLibrary.Data;
using BlendedLibrary.FacetSearch.Model;

namespace BlendedLibrary.FacetSearch.Application.ViewModel.Link
{
    public class QueryCanvasExitLinkViewModel : QueryCanvasLinkViewModel
    {
        private readonly QueryCanvasExitLink _exitLink;

        public QueryCanvasExitLinkViewModel(IDataProvider dataProvider, QueryCanvasExitLink model, QueryCanvasViewModel canvasViewModel)
            : base(dataProvider, model, canvasViewModel)
        {
            _exitLink = model;
            ContactDownTrigger = new Subject<TouchEventArgs>();
        }

        #region ContactDownTrigger

        private Subject<TouchEventArgs> _contactDownTrigger = null;

        /// <summary>
        /// Gets or sets the ContactDownTrigger property. This observable property 
        /// indicates ....
        /// </summary>
        public Subject<TouchEventArgs> ContactDownTrigger
        {
            get { return _contactDownTrigger; }
            set
            {
                if (_contactDownTrigger != value)
                {
                    _contactDownTrigger = value;
                    RaisePropertyChanged("ContactDownTrigger");
                }
            }
        }

        #endregion

        internal void EstablishConnection(QueryCanvasItemViewModel item, Position connectionPoint)
        {
            if(item.Model is QueryCanvasNode)
            _exitLink.EstablishConnection(item.Model as QueryCanvasNode, connectionPoint);
        }

        internal bool IsValidConnectCandidate(QueryCanvasItemViewModel item)
        {
            return _exitLink.IsValidConnectCandidate(item.Model);
        }
    }
}
