﻿using System;
using BlendedLibrary.Common.Model;
using BlendedLibrary.FacetSearch.Model;
using GalaSoft.MvvmLight;

namespace BlendedLibrary.FacetSearch.Application.ViewModel.Link
{
    public class LinkPulseViewModel : ViewModelBase
    {
        #region static members

        private static readonly Random Random = new Random();

        #endregion

        #region public properties

        #region Model

        /// <summary>
        /// The <see cref="Model" /> property's name.
        /// </summary>
        public const string ModelPropertyName = "Model";

        private LinkPulse _model = null;

        /// <summary>
        /// Sets and gets the Model property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public LinkPulse Model
        {
            get
            {
                return _model;
            }

            set
            {
                if (_model == value)
                {
                    return;
                }

                _model = value;
                RaisePropertyChanged(ModelPropertyName);
            }
        }

        #endregion

        #region X

        /// <summary>
        /// The <see cref="X" /> property's name.
        /// </summary>
        public const string XPropertyName = "X";

        private double _x = 0.0;

        /// <summary>
        /// Sets and gets the X property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double X
        {
            get
            {
                return _x;
            }

            set
            {
                if (_x == value)
                {
                    return;
                }

                _x = value;
                RaisePropertyChanged(XPropertyName);
            }
        }

        #endregion

        #region Y

        /// <summary>
        /// The <see cref="Y" /> property's name.
        /// </summary>
        public const string YPropertyName = "Y";

        private double _y = 0.0;

        /// <summary>
        /// Sets and gets the Y property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double Y
        {
            get
            {
                return _y;
            }

            set
            {
                if (_y == value)
                {
                    return;
                }

                _y = value;
                RaisePropertyChanged(YPropertyName);
            }
        }

        #endregion

        #region Width

        /// <summary>
        /// The <see cref="Width" /> property's name.
        /// </summary>
        public const string WidthPropertyName = "Width";

        private double _width = 30.0;

        /// <summary>
        /// Sets and gets the Width property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double Width
        {
            get
            {
                return _width;
            }

            set
            {
                if (_width == value)
                {
                    return;
                }

                _width = value;
                RaisePropertyChanged(WidthPropertyName);
            }
        }

        #endregion

        #region Height

        /// <summary>
        /// The <see cref="Height" /> property's name.
        /// </summary>
        public const string HeightPropertyName = "Height";

        private double _height = 20.0;

        /// <summary>
        /// Sets and gets the Height property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double Height
        {
            get
            {
                return _height;
            }

            set
            {
                if (_height == value)
                {
                    return;
                }

                _height = value;
                RaisePropertyChanged(HeightPropertyName);
            }
        }

        #endregion

        #region Angle

        /// <summary>
        /// The <see cref="Angle" /> property's name.
        /// </summary>
        public const string AnglePropertyName = "Angle";

        private double _angle = 0.0;

        /// <summary>
        /// Sets and gets the Angle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public double Angle
        {
            get
            {
                return _angle;
            }

            set
            {
                if (_angle == value)
                {
                    return;
                }

                _angle = value;
                RaisePropertyChanged(AnglePropertyName);
            }
        }

        #endregion

        #endregion

        #region ctor

        public LinkPulseViewModel(LinkPulse linkPulse)
        {
            Model = linkPulse;
        }

        #endregion

        /// <summary>
        /// It calculates the position of the link pulse from the center point, angle, and
        /// current radius.
        /// </summary>
        /// <param name="center">Origin/center point of the link pulse.</param>
        /// <param name="angle">Angle in which the link pulse moves.</param>
        public void AdjustPosition(Position center, double angle)
        {
            var deltaX = Model.Radius * Math.Cos(angle);
            var deltaY = Model.Radius * Math.Sin(angle);

            X = center.X + deltaX;
            Y = center.Y + deltaY;
            Angle = angle * (180 / Math.PI);
        }

        /// <summary>
        /// Signals a beat of the pulse.
        /// </summary>
        public void Beat()
        {
            Model.Radius += Model.Speed;
            //Model.Radius += Random.Next(1, 50);
        }

        /// <summary>
        /// Resets the link pulse to its source by resetting the radius to zero.
        /// </summary>
        internal void Reset()
        {
            Model.Radius = 0;
        }
    }
}
