﻿
namespace BlendedLibrary.FacetSearch.Application.ViewModel
{
    public enum ValueWheelStates
    {
        ValueWheelVisible,
        ValueWheelInvisible,
        ValueWheelCollapsed,
        NoFingerOnValueWheel
    }
}
