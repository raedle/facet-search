﻿using System;
using System.Windows;
using System.Windows.Media;
using BlendedLibrary.Data;
using BlendedLibrary.FacetSearch.Model;

namespace BlendedLibrary.FacetSearch.Application.ViewModel
{
    public class QueryCanvasItemViewModel : QueryCanvasViewModelBase, IDisposable
    {
        #region ctor

        public QueryCanvasItemViewModel(IDataProvider dataProvider, QueryCanvasItem model, QueryCanvasViewModel canvasViewModel)
        {
            DataProvider = dataProvider;
            Model = model;
            CanvasViewModel = canvasViewModel;
        }

        #endregion

        #region public properties

        public QueryCanvasItem Model { get; private set; }

        public QueryCanvasViewModel CanvasViewModel { get; private set; }

        #region ConnectionTargetCount

        private int _connectionTargetCount;

        /// <summary>
        /// Gets or sets the ConnectionTargetCount property. This observable property 
        /// indicates ....
        /// </summary>
        public int ConnectionTargetCount
        {
            get { return _connectionTargetCount; }
            set
            {
                if (_connectionTargetCount != value)
                {
                    _connectionTargetCount = value;

                    //enforce that count can never ber smaller than zero
                    if (_connectionTargetCount < 0)
                        _connectionTargetCount = 0;

                    RaisePropertyChanged("ConnectionTargetCount");
                }
            }
        }

        #endregion

        #region GlobalExpression

        private string _globalExpression = string.Empty;

        /// <summary>
        /// Gets or sets the GlobalExpression property. This observable property 
        /// indicates
        /// </summary>
        public string GlobalExpression
        {
            get { return _globalExpression; }
            set
            {
                if (_globalExpression != value)
                {
                    var old = _globalExpression;
                    _globalExpression = value;
                    OnGlobalExpressionChanged(old, value);
                    RaisePropertyChanged("GlobalExpression");
                }
            }
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the GlobalExpression property.
        /// </summary>
        protected virtual void OnGlobalExpressionChanged(String oldGlobalExpression, String newGlobalExpression)
        {
        }

        #endregion

        #region QueryCount

        private int _queryCount;

        /// <summary>
        /// Gets or sets the QueryCount property. This observable property 
        /// indicates
        /// </summary>
        public int QueryCount
        {
            get { return _queryCount; }
            set
            {
                if (_queryCount != value)
                {
                    var old = _queryCount;
                    _queryCount = value;
                    OnQueryCountChanged(old, value);
                    RaisePropertyChanged("QueryCount");
                }
            }
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the QueryCount property.
        /// </summary>
        protected virtual void OnQueryCountChanged(int oldQueryCount, int newQueryCount)
        {
        }

        #endregion

        #endregion

        #region virtual methods

        internal virtual Point? GetNearestIntersectionPoint(LineGeometry geometry)
        {
            throw new NotImplementedException();
        }

        public virtual void Dispose()
        {

        }

        #endregion
    }
}
