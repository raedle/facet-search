﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BlendedLibrary.Common.Model;
using BlendedLibrary.Data;
using BlendedLibrary.FacetSearch.Application.ViewModel.Link;
using BlendedLibrary.FacetSearch.Model;

namespace BlendedLibrary.FacetSearch.Application.ViewModel.Node
{
    public class QueryCanvasNodeViewModel : QueryCanvasItemViewModel, IDataReceiver, IPosition
    {
        #region private fields

        private static long _globalId = long.MaxValue;

        private QueryCanvasNode _node;



        #endregion

        #region public properties

        #region Id

        private long _id = -1;
        public long Id
        {
            get
            {
                if (_id == -1)
                    _id = --_globalId;
                return _id;
            }
        }

        #endregion

        #region TokenId

        /// <summary>
        /// The <see cref="TokenId" /> property's name.
        /// </summary>
        public const string TokenIdPropertyName = "TokenId";

        private long _tokenId;

        /// <summary>
        /// Sets and gets the TokenId property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public long TokenId
        {
            get
            {
                return _tokenId;
            }

            set
            {
                if (_tokenId == value)
                {
                    return;
                }

                _tokenId = value;
                RaisePropertyChanged(TokenIdPropertyName);
            }
        }

        #endregion

        #region TopLeft

        /// <summary>
        /// The <see cref="TopLeft" /> property's name.
        /// </summary>
        public const string TopLeftPropertyName = "TopLeft";

        private Position _topLeft = null;

        /// <summary>
        /// Sets and gets the TopLeft property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Position TopLeft
        {
            get
            {
                return _topLeft;
            }

            set
            {
                if (_topLeft == value)
                {
                    return;
                }

                _topLeft = value;
                RaisePropertyChanged(TopLeftPropertyName);
            }
        }

        #endregion

        #region Center

        /// <summary>
        /// The <see cref="Center" /> property's name.
        /// </summary>
        public const string CenterPropertyName = "Center";

        private Position _center = null;

        /// <summary>
        /// Sets and gets the Center property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Position Center
        {
            get
            {
                return _center;
            }

            set
            {
                if (_center == value)
                {
                    return;
                }

                _center = value;
                RaisePropertyChanged(CenterPropertyName);
            }
        }

        #endregion

        #region IsResponsibleForZeroLink

        private bool _isResponsibleForZeroLink = false;

        /// <summary>
        /// Gets or sets the IsResponsibleForZeroLink property. This observable property 
        /// indicates ....
        /// </summary>
        public bool IsResponsibleForZeroLink
        {
            get { return _isResponsibleForZeroLink; }
            set
            {
                if (_isResponsibleForZeroLink != value)
                {
                    _isResponsibleForZeroLink = value;
                    RaisePropertyChanged("IsResponsibleForZeroLink");
                }
            }
        }

        #endregion

        #endregion

        #region ctor

        public QueryCanvasNodeViewModel(IDataProvider dataProvider, QueryCanvasNode model, QueryCanvasViewModel canvasViewModel)
            : base(dataProvider, model, canvasViewModel)
        {
            _id = model.TokenId;

            _node = (model as QueryCanvasNode);
            Debug.Assert(_node != null, "node != null");

            Center = _node.Position;
            TopLeft = new Position(Center.X, Center.Y);

            RegisterChangeNotificationAction(_node, "Position", e => UpdateCenterPosition(_node.Position.X, _node.Position.Y));

            RegisterChangeNotificationAction(this, "QueryCount", e =>
            {
                IsResponsibleForZeroLink = QueryCount == 0;
            });

            GlobalExpression = Model.GlobalExpression;

            QueryCount = 0;
            FetchQueryCount(GlobalExpression);
            DataProvider.TotalResultCount = DataProvider.TotalResultCount;

            RegisterChangeNotificationAction(model, "GlobalExpression").Subscribe(e =>
            {
                GlobalExpression = Model.GlobalExpression;
                FetchQueryCount(GlobalExpression);
            });
        }

        #endregion

        #region IDataReciever

        public virtual String Query
        {
            get
            {
                return GlobalExpression;
            }
        }

        private bool _isFetchingQueryCount;

        public bool IsFetchingQueryCount
        {
            get
            {
                return _isFetchingQueryCount;
            }
            set
            {
                if (_isFetchingQueryCount != value)
                {
                    _isFetchingQueryCount = value;
                    RaisePropertyChanged("IsFetchingQueryCount");
                }
            }
        }

        /// <summary>
        /// does not care if asynchronously called, the DataProvider must check if the request is still valid once the query count is calculated
        /// <see cref=" IDataReciever.GlobalExpression;"/>
        /// </summary>
        /// <param name="query"></param>
        protected virtual void FetchQueryCount(string query)
        {
            DataProvider.UpdateCount(query, this);
        }

        #endregion

        public virtual void UpdateTopLeftPosition(double x, double y)
        {
            TopLeft.X = x;
            TopLeft.Y = y;
        }

        public virtual void UpdateCenterPosition(double x, double y)
        {
            Center.X = x;
            Center.Y = y;
        }

        public IEnumerable<QueryCanvasLinkViewModel> GetIngoingLinks()
        {
            return CanvasViewModel.GetIngoingLinks(this);
        }

        public IEnumerable<QueryCanvasLinkViewModel> GetOutgoingLinks()
        {
            return CanvasViewModel.GetOutgoingLinks(this);
        }

        protected override void OnQueryCountChanged(int oldQueryCount, int newQueryCount)
        {
            base.OnQueryCountChanged(oldQueryCount, newQueryCount);
            var outgoingLinks = GetOutgoingLinks().ToList();
            // update connected links
            foreach (var link in outgoingLinks)
            {
                link.Update();
            }
        }
    }
}
