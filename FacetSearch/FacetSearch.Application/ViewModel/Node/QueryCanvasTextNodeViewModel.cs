﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using BlendedLibrary.Common.Util;
using BlendedLibrary.Data;
using BlendedLibrary.FacetSearch.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace BlendedLibrary.FacetSearch.Application.ViewModel.Node
{
    public class QueryCanvasTextNodeViewModel : QueryCanvasAndNodeViewModel
    {
        #region commands

        public RelayCommand<TouchEventArgs> ContactDownOnChooseFacetCommand { get; set; }

        public RelayCommand<TouchEventArgs> ContactUpOnChooseFacetCommand { get; set; }

        public RelayCommand<FacetItem> ContactDownOnFacetCommand { get; set; }

        #endregion

        #region properties

        #region FacetItems

        private IEnumerable<FacetItem> _facetItems;
        public IEnumerable<FacetItem> FacetItems
        {
            get { return _facetItems; }
            set
            {
                if (!Equals(_facetItems, value))
                {
                    _facetItems = value;
                    RaisePropertyChanged("FacetItems");
                }
            }
        }

        #endregion

        #endregion

        #region ctor

        public QueryCanvasTextNodeViewModel(IDataProvider dataProvider, QueryCanvasTextNode model, QueryCanvasViewModel canvasViewModel)
            : base(dataProvider, model, canvasViewModel)
        {
            //Observable.FromEventPattern<PropertyChangedEventArgs>(this, "PropertyChanged")
            //    .Where(e => Equals("Keyword", e.EventArgs.PropertyName))
            //    .Throttle(TimeSpan.FromMilliseconds(500)).Subscribe(e => UpdateExpression());

            ContactDownOnChooseFacetCommand = new RelayCommand<TouchEventArgs>(OnTouchDownOnChooseFacet);
            ContactUpOnChooseFacetCommand = new RelayCommand<TouchEventArgs>(OnTouchUpOnChooseFacet);

            ContactDownOnFacetCommand = new RelayCommand<FacetItem>(OnTouchDownOnFacet);

            Width = Properties.Settings.Default.TextSnippetWidth;
            Height = Properties.Settings.Default.TextSnippetHeight;

            TopLeft = Center - new Vector(Width / 2.0, Height / 2.0);

            var textFacets = dataProvider.GetFacets(DataRepresentation.Textual);

            FacetItems = textFacets.Select(item => new FacetItem
                {
                    Key = item,
                    DisplayName = dataProvider.GetDisplayNameOfFacet(item),
                    IsVisible = true,
                    IsSelected = true
                }).ToArray();
            UpdateExpression();
        }

        #endregion

        private void OnTextEnter(KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;

            UpdateExpression();
        }

        public void ApplyKeyword()
        {
            UpdateExpression();
        }

        private void UpdateExpression()
        {
            var model = Model as QueryCanvasTextNode;
            if (model == null) return;

            var expression = DataProvider.GetTextExpression(FacetItems.Where(item => item.IsSelected).Select(item => item.Key).ToArray(), SolrQueryUtility.Escape(model.Text));

#if DEBUG
            Console.WriteLine("TextNodeModel: {0}", expression);
#endif

            //if (!string.IsNullOrWhiteSpace(expression))
            model.LocalExpression = expression;
        }

        internal override Point? GetNearestIntersectionPoint(LineGeometry geometry)
        {
            // TODO: Calculate nearest intersection point
            return new Point(Center.X, Center.Y);
        }

        private bool _buttonPressed;

        private void OnTouchDownOnChooseFacet(TouchEventArgs obj)
        {
            _buttonPressed = true;

            foreach (var facetItem in FacetItems)
            {
                facetItem.IsVisible = true;
            }

            obj.Handled = true;
        }

        private void OnTouchUpOnChooseFacet(TouchEventArgs obj)
        {
            foreach (var facetItem in FacetItems)
            {
                if (facetItem.IsSelected == false)
                    facetItem.IsVisible = false;
            }

            _buttonPressed = false;
        }

        private void OnTouchDownOnFacet(FacetItem obj)
        {
            if (obj.IsSelected)
            {
                obj.IsSelected = false;

                if (!_buttonPressed)
                    obj.IsVisible = false;
            }
            else
                obj.IsSelected = true;

            UpdateExpression();
        }
    }

    public class FacetItem : ViewModelBase
    {
        private String _key = null;

        public String Key
        {
            get { return _key; }
            set
            {
                if (_key != value)
                {
                    _key = value;
                    RaisePropertyChanged("Key");
                }
            }
        }

        private String _displayName = null;

        public String DisplayName
        {
            get { return _displayName; }
            set
            {
                if (_displayName != value)
                {
                    _displayName = value;
                    RaisePropertyChanged("DisplayName");
                }
            }
        }

        private Boolean _isVisible = false;

        public Boolean IsVisible
        {
            get { return _isVisible; }
            set
            {
                if (_isVisible != value)
                {
                    _isVisible = value;
                    RaisePropertyChanged("IsVisible");
                }
            }
        }

        private Boolean _isSelected = false;

        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    RaisePropertyChanged("IsSelected");
                }
            }
        }
    }
}
