﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using BlendedLibrary.Common;
using BlendedLibrary.Common.Util;
using BlendedLibrary.Data;
using BlendedLibrary.FacetSearch.Application.CircleControl;
using BlendedLibrary.FacetSearch.Application.Properties;
using BlendedLibrary.FacetSearch.Application.View.Node;
using BlendedLibrary.FacetSearch.Model;
using GalaSoft.MvvmLight.Command;
using ReactiveStateMachine;
using TouchStateMachine;

namespace BlendedLibrary.FacetSearch.Application.ViewModel.Node
{
    public class QueryCanvasFacetNodeViewModel : QueryCanvasAndNodeViewModel
    {
        #region private fields

        private readonly QueryCanvasFacetNode _facetNode;

        private readonly List<TouchDevice> _contactStore = new List<TouchDevice>();

        private readonly object _lastCircleSegmentKey = new object();
        private readonly object _circleSegmentOnContactDownKey = new object();
        private readonly object _contactMovedAwayFromInitialSegmentKey = new object();

        private FacetWheel _facetWheel;
        private ValueWheel _valueWheel;
        private RangeControl _rangeControl;

        #endregion

        #region ctor

        public QueryCanvasFacetNodeViewModel(IDataProvider dataProvider, QueryCanvasFacetNode model, QueryCanvasViewModel canvasViewModel, String selectedFacet, IFacetElement[] selectedValues, double? minMetricValue, double? maxMetricValue) :
            base(dataProvider, model, canvasViewModel)
        {
            SelectedFacet = selectedFacet;
            SelectedValues = selectedValues;
            MinMetricValue = minMetricValue;
            MaxMetricValue = maxMetricValue;

            _facetNode = model;

            var radius = Settings.Default.WheelRadius;
            Width = Height = radius * 2.0;
            TopLeft = Center - new Vector(radius, radius);

            LoadedCommand = new RelayCommand<FrameworkElement>(OnLoaded);

            Facets = DataProvider.GetFacets(DataRepresentation.Categorical | DataRepresentation.Metric);
            FacetDisplayNames = Facets.Select(facet => DataProvider.GetDisplayNameOfFacet(facet)).ToArray();

            ValueWheelStateMachine = new WindowsTouchStateMachine<ValueWheelStates>("ValueWheelStates", SelectedFacet == null ? ValueWheelStates.ValueWheelInvisible : SelectedValues == null ? ValueWheelStates.ValueWheelVisible : ValueWheelStates.ValueWheelCollapsed);
            FacetWheelStateMachine = new WindowsTouchStateMachine<FacetWheelStates>("FacetWheelStates", SelectedFacet == null ? FacetWheelStates.FacetWheelInitial : FacetWheelStates.FacetWheelCollapsed);
        }

        #endregion

        #region private methods

        private void InitEvents()
        {
            ContactDownOnFacetLabel = new Subject<TouchEventArgs>();
            ContactUpOnFacetLabel = new Subject<TouchEventArgs>();
            ContactDownOnFacetWheel = new Subject<TouchEventArgs>();
            ContactUpOnFacetWheel = new Subject<TouchEventArgs>();
            ContactChangedOnFacetWheel = new Subject<TouchEventArgs>();

            ContactDownOnValueLabel = new Subject<TouchEventArgs>();
            ContactUpOnValueLabel = new Subject<TouchEventArgs>();
            ContactDownOnValueWheel = new Subject<TouchEventArgs>();
            ContactUpOnValueWheel = new Subject<TouchEventArgs>();
            ContactChangedOnValueWheel = new Subject<TouchEventArgs>();

            ContactDownOnCloseRing = new Subject<TouchEventArgs>();

            ContactDownOnRangeControl = new Subject<TouchEventArgs>();
            ContactUpOnRangeControl = new Subject<TouchEventArgs>();
        }

        private void InitValueWheelStateMachine()
        {
            ValueWheelStateMachine.StateMachineException += (sender, args) => Console.WriteLine(args.Exception.Message);

#if DEBUG
            ValueWheelStateMachine.StateChanged += (sender, e) =>
            {
                Console.WriteLine("###Value Wheel: New State: {0} (from {1}) (StateMachineCount: {2})", e.ToState,
                    e.FromState, ValueWheelStateMachine.Count);

                //if (Equals(FacetWheelStateMachine.CurrentState, FacetWheelStates.FacetWheelVisible))
                //    ValueWheelStateMachine.AddAutomaticTransition(ValueWheelStates.ValueWheelCollapsed, ValueWheelStates.ValueWheelCollapsed);
            };

#endif

            #region Reacts on FacetWheelStateMachine state changes

            // Open value wheel if facet wheel changes its state to collapsed -> this provides a workflow: first select facet, then select values.
            var obs = Observable.FromEventPattern<StateChangedEventArgs<FacetWheelStates>>(FacetWheelStateMachine, "StateChanged");
            ValueWheelStateMachine.AddTransition(obs)
                                  .From(ValueWheelStates.ValueWheelCollapsed)
                                  .To(ValueWheelStates.ValueWheelVisible)
                                  .Where(e =>
                                      {
                                          var toState = e.EventArgs.ToState;
                                          return toState.Equals(FacetWheelStates.FacetWheelCollapsed);
                                      });
            ValueWheelStateMachine.AddTransition(obs)
                                  .From(ValueWheelStates.ValueWheelInvisible)
                                  .To(ValueWheelStates.ValueWheelVisible)
                                  .Where(e =>
                                  {
                                      var toState = e.EventArgs.ToState;
                                      return toState.Equals(FacetWheelStates.FacetWheelCollapsed);
                                  });
            ValueWheelStateMachine.AddTransition(obs)
                                  .From(ValueWheelStates.ValueWheelVisible)
                                  .To(ValueWheelStates.ValueWheelInvisible)
                                  .Where(e =>
                                  {
                                      var toState = e.EventArgs.ToState;
                                      return toState.Equals(FacetWheelStates.FacetWheelVisible);
                                  });
            #endregion

            #region Value Label

            ValueWheelStateMachine.AddTransition(ContactDownOnValueLabel).From(ValueWheelStates.ValueWheelCollapsed).To(ValueWheelStates.ValueWheelVisible)
                .Where(e => _facetWheel.SelectedFacet != null && DataProvider.GetDataRepresentation(_facetWheel.SelectedFacet) == DataRepresentation.Categorical)
                .Do(e =>
                    {
                        HandleEvent(e);

                        var segment = _valueWheel.GetSegmentByContact(e.TouchDevice);

                        e.TouchDevice.SetUserData(_circleSegmentOnContactDownKey, segment);
                        e.TouchDevice.SetUserData(_contactMovedAwayFromInitialSegmentKey, false);

                        e.TouchDevice.Capture(_txtValueLabelsPath);
                    });

            ValueWheelStateMachine.AddTransition(ContactDownOnValueLabel).From(ValueWheelStates.ValueWheelCollapsed).To(ValueWheelStates.ValueWheelVisible)
                .Where(e => _facetWheel.SelectedFacet != null && DataProvider.GetDataRepresentation(_facetWheel.SelectedFacet) == DataRepresentation.Metric)
                .Do(e =>
                {
                    HandleEvent(e);
                    e.TouchDevice.Capture(_txtValueLabelsPath);
                });

            // This ignores touch up when value wheel is fading in and handles touch + release capture.
            ValueWheelStateMachine.AddTransition(ContactUpOnValueLabel).From(ValueWheelStates.ValueWheelVisible).To(ValueWheelStates.ValueWheelVisible)
                .Do(e =>
                {
                    HandleEvent(e);
                    e.TouchDevice.Capture(null);
                });

            #endregion

            #region Value Wheel

            ValueWheelStateMachine.AddTransition(ContactDownOnValueWheel).From(ValueWheelStates.ValueWheelVisible).To(ValueWheelStates.ValueWheelVisible)
                .Where(e => _facetWheel.SelectedFacet != null &&  DataProvider.GetDataRepresentation(_facetWheel.SelectedFacet) == DataRepresentation.Categorical)
                .Do(e =>
                {
                    HandleEvent(e);

                    var segment = _valueWheel.GetSegmentByContact(e.TouchDevice);

                    e.TouchDevice.SetUserData(_circleSegmentOnContactDownKey, segment);
                    e.TouchDevice.SetUserData(_contactMovedAwayFromInitialSegmentKey, false);

                    CaptureValueWheelContact(e);

                    SelectValueSegment(e);
                });

            ValueWheelStateMachine.AddTransition(ContactChangedOnValueWheel).From(ValueWheelStates.ValueWheelVisible).To(ValueWheelStates.ValueWheelVisible)
                .Do(e =>
                {
                    HandleEvent(e);
                    //CaptureValueWheelContact(e);
                    SelectValueSegment(e);
                });

            ValueWheelStateMachine.AddTransition(ContactDownOnValueWheel).From(ValueWheelStates.NoFingerOnValueWheel).To(ValueWheelStates.ValueWheelVisible)
                .Where(e => _facetWheel.SelectedFacet != null && DataProvider.GetDataRepresentation(_facetWheel.SelectedFacet) == DataRepresentation.Categorical)
                .Do(e =>
                {
                    HandleEvent(e);
                    CaptureValueWheelContact(e);
                    SelectValueSegment(e);
                });

            ValueWheelStateMachine.AddTransition(ContactChangedOnValueWheel).From(ValueWheelStates.NoFingerOnValueWheel).To(ValueWheelStates.ValueWheelVisible)
                .Do(e =>
                {
                    HandleEvent(e);
                    //CaptureValueWheelContact(e);
                    SelectValueSegment(e);
                });

            ValueWheelStateMachine.AddTransition(ContactUpOnValueWheel).From(ValueWheelStates.ValueWheelVisible).To(ValueWheelStates.ValueWheelVisible)
                .Where(e => _valueWheel.TouchesCaptured.Count() > 1)
                .Do(e =>
                {
                    HandleEvent(e);
                    //e.TouchDevice.Capture(null);
                    //CaptureValueWheelContact(e);

                    _valueWheel.ReleaseTouchCapture(e.TouchDevice);
                });

            ValueWheelStateMachine.AddTransition(ContactUpOnValueWheel).From(ValueWheelStates.ValueWheelVisible).To(ValueWheelStates.NoFingerOnValueWheel)
                .Where(e => _valueWheel.TouchesCaptured.Count() <= 1)
                .Do(e =>
                {
                    HandleEvent(e);
                    //e.TouchDevice.Capture(null);
                    //CaptureValueWheelContact(e);

                    _valueWheel.ReleaseTouchCapture(e.TouchDevice);
                });

            ValueWheelStateMachine.AddTimedTransition(TimeSpan.FromMilliseconds(2000))
                .From(ValueWheelStates.NoFingerOnValueWheel)
                .To(ValueWheelStates.ValueWheelCollapsed)
                .Where(() => Equals(FacetWheelStates.FacetWheelCollapsed, FacetWheelStateMachine.CurrentState));

            ValueWheelStateMachine.AddTimedTransition(TimeSpan.FromMilliseconds(2000))
                .From(ValueWheelStates.NoFingerOnValueWheel)
                .To(ValueWheelStates.ValueWheelInvisible)
                .Where(() => !Equals(FacetWheelStates.FacetWheelCollapsed, FacetWheelStateMachine.CurrentState));

            #endregion

            #region Range Control

            ValueWheelStateMachine.AddTransition(ContactDownOnRangeControl).From(ValueWheelStates.ValueWheelVisible).To(ValueWheelStates.ValueWheelVisible)
                .Where(e => DataProvider.GetDataRepresentation(_facetWheel.SelectedFacet) == DataRepresentation.Metric)
                .Do(e =>
                {
                    HandleEvent(e);
                    CaptureValueWheelContact(e);
                });

            ValueWheelStateMachine.AddTransition(ContactUpOnRangeControl).From(ValueWheelStates.ValueWheelVisible).To(ValueWheelStates.ValueWheelVisible)
                                  .Where(e => ValueWheelStateMachine.Count > 1)
                                  .Do(e =>
                                  {
                                      HandleEvent(e);
                                      e.TouchDevice.Capture(null);
                                  });

            ValueWheelStateMachine.AddTransition(ContactUpOnRangeControl).From(ValueWheelStates.ValueWheelVisible).To(ValueWheelStates.NoFingerOnValueWheel)
                                  .Where(e => ValueWheelStateMachine.Count <= 1)
                                  .Do(e =>
                                  {
                                      HandleEvent(e);
                                      e.TouchDevice.Capture(null);
                                  });

            #endregion
        }

        private void InitFacetWheelStateMachine()
        {
            FacetWheelStateMachine.StateMachineException += (sender, args) => Console.WriteLine(args.Exception.Message);
#if DEBUG
            FacetWheelStateMachine.StateChanged += (sender, e) => Console.WriteLine("###Facet Wheel: New State: {0} (from {1}) (StateMachineCount: {2})", e.ToState, e.FromState, FacetWheelStateMachine.Count);
#endif
            #region Facet Label

            FacetWheelStateMachine.AddTransition(ContactDownOnFacetLabel).From(FacetWheelStates.FacetWheelCollapsed).To(FacetWheelStates.FacetWheelVisible)
                .Do(e =>
                {
                    HandleEvent(e);
                    _facetWheel.RotateSelectedCircleSegmentToContact(e.TouchDevice);
                    e.TouchDevice.SetUserData(_circleSegmentOnContactDownKey, _facetWheel.SelectedSegments.First());
                    e.TouchDevice.SetUserData(_contactMovedAwayFromInitialSegmentKey, false);
                    e.TouchDevice.Capture(_txtFacetLabelsPath);
                });

            FacetWheelStateMachine.AddTransition(ContactUpOnFacetLabel).From(FacetWheelStates.FacetWheelVisible).To(FacetWheelStates.FacetWheelVisible)
                .Do(e =>
                    {
                        HandleEvent(e);
                        e.TouchDevice.Capture(null);
                    });

            #endregion

            #region Facet Wheel

            // this avoids an initial visibility of the collapsed value wheel
            FacetWheelStateMachine.AddAutomaticTransition(FacetWheelStates.FacetWheelInitial, FacetWheelStates.FacetWheelVisible);

            // Touch down
            FacetWheelStateMachine.AddTransition(ContactDownOnFacetWheel).From(FacetWheelStates.FacetWheelCollapsed).To(FacetWheelStates.FacetWheelVisible)
                .Do(e =>
                    {
                        HandleEvent(e);
                        e.TouchDevice.SetUserData(_circleSegmentOnContactDownKey, _facetWheel.SelectedSegments.First());
                        e.TouchDevice.SetUserData(_contactMovedAwayFromInitialSegmentKey, false);
                        SelectFacetSegment(e);
                        CaptureFacetWheelContact(e);
                    });

            // Touch up
            FacetWheelStateMachine.AddTransition(ContactUpOnFacetWheel).From(FacetWheelStates.FacetWheelVisible).To(FacetWheelStates.FacetWheelCollapsed)
                .Do(e =>
                    {
                        HandleEvent(e);
                        SelectFacetSegment(e);
                        e.TouchDevice.Capture(null);
                    });

            // Touch moved
            FacetWheelStateMachine.AddTransition(ContactChangedOnFacetWheel).From(FacetWheelStates.FacetWheelVisible).To(FacetWheelStates.FacetWheelVisible)
                .Do(e =>
                    {
                        HandleEvent(e);

                        var segment = _facetWheel.GetSegmentByContact(e.TouchDevice);
                        var initialSegment = e.TouchDevice.GetUserData<CircleSegment>(_circleSegmentOnContactDownKey);

                        if (segment != initialSegment)
                        {
                            e.TouchDevice.SetUserData(_contactMovedAwayFromInitialSegmentKey, true);
                        }

                        var movedAwayFromInitialSegmentValue = e.TouchDevice.GetUserData<bool?>(_contactMovedAwayFromInitialSegmentKey);
                        if (movedAwayFromInitialSegmentValue.HasValue && movedAwayFromInitialSegmentValue.Value)
                        {
                            SelectFacetSegment(e);
                        }
                    });

            #endregion
        }

        private static void HandleEvent(RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void SelectFacetSegment(TouchEventArgs e)
        {
            var segment = _facetWheel.GetSegmentByContact(e.TouchDevice);

            if (segment != null)
            {
                _facetWheel.SelectSegment(segment);
            }
        }

        private void SelectValueSegment(TouchEventArgs e)
        {
            var lastSegment = e.TouchDevice.GetUserData<CircleSegment>(_lastCircleSegmentKey);

            var segment = _valueWheel.GetSegmentByContact(e.TouchDevice);

            if (!Equals(lastSegment, segment) && segment != null)
            {
                _valueWheel.SelectOrDeselectSegment(segment);
                e.TouchDevice.SetUserData(_lastCircleSegmentKey, segment);
            }
        }

        private void CaptureFacetWheelContact(TouchEventArgs e)
        {
            var success = e.TouchDevice.Capture(_facetWheel, CaptureMode.SubTree);

#if DEBUG
            if (!success)
            {
                Console.WriteLine("Failed to capture the contact on FacetWheel");
            }
#endif
        }

        private void CaptureValueWheelContact(TouchEventArgs e)
        {
            var success = e.TouchDevice.Capture(_valueWheel, CaptureMode.SubTree);
        }

        #endregion

        #region Event Handler

        private Path _txtFacetLabelsPath;
        private Path _txtValueLabelsPath;
        private Grid _txtValueLabels;

        private void OnLoaded(FrameworkElement view)
        {
            _facetWheel = (view as QueryCanvasFacetNodeView).FacetWheel;
            _valueWheel = (view as QueryCanvasFacetNodeView).ValueWheel;
            _rangeControl = (view as QueryCanvasFacetNodeView).RangeControl;

            _txtFacetLabelsPath = (view as QueryCanvasFacetNodeView).TxtFacetLabelsPath;
            _txtValueLabelsPath = (view as QueryCanvasFacetNodeView).TxtValueLabelsPath;
            _txtValueLabels = (view as QueryCanvasFacetNodeView).TxtValueLabels;

            InitEvents();
            
            InitValueWheelStateMachine();

            ValueWheelStateMachine.Start();
            InitFacetWheelStateMachine();
            FacetWheelStateMachine.Start();

            _txtValueLabels.Opacity = 0.0;
            _txtValueLabels.Visibility = Visibility.Collapsed;
        }

        #endregion

        #region overrides

        internal override Point? GetNearestIntersectionPoint(System.Windows.Media.LineGeometry geometry)
        {
            var ax = geometry.StartPoint.X;
            var ay = geometry.StartPoint.Y;

            var bx = geometry.EndPoint.X;
            var by = geometry.EndPoint.Y;

            var cx = Center.X;
            var cy = Center.Y;

            var dx = ax - bx;
            var dy = ay - by;

            var lineLength = Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));

            //Normalize
            dx = dx / lineLength;
            dy = dy / lineLength;

            var t = dx * (cx - ax) + dy * (cy - ay);

            var ex = t * dx + ax;
            var ey = t * dy + ay;

            var distance = Math.Sqrt(Math.Pow(ex - cx, 2) + Math.Pow(ey - cy, 2));

            var radius = Settings.Default.NodeRadius;

            if (distance < radius)
            {
                var dt = Math.Sqrt(Math.Pow(radius, 2) - Math.Pow(distance, 2));

                //point 1
                var fx = (t - dt) * dx + ax;
                var fy = (t - dt) * dy + ay;

                var fDstX = ax - fx;
                var fDstY = ay - fy;
                var fDistance = Math.Sqrt(Math.Pow(fDstX, 2) + Math.Pow(fDstY, 2));

                //point 2
                var gx = (t + dt) * dx + ax;
                var gy = (t + dt) * dy + ay;

                var gDstX = ax - gx;
                var gDstY = ay - gy;
                var gDistance = Math.Sqrt(Math.Pow(gDstX, 2) + Math.Pow(gDstY, 2));

                if (fDistance < gDistance)
                {
                    return new Point(fx, fy);
                }

                return new Point(gx, gy);
            }

            if (distance == Settings.Default.NodeRadius)
            {
                return new Point(ex, ey);
            }
            return null;
        }

        #endregion

        #region public properties

        #region ContactDownOnCloseRing

        private Subject<TouchEventArgs> _contactDownOnCloseRing = null;

        /// <summary>
        /// Gets or sets the ContactDownOnCloseRing property. This observable property 
        /// indicates ....
        /// </summary>
        public Subject<TouchEventArgs> ContactDownOnCloseRing
        {
            get { return _contactDownOnCloseRing; }
            set
            {
                if (_contactDownOnCloseRing != value)
                {
                    _contactDownOnCloseRing = value;
                    RaisePropertyChanged("ContactDownOnCloseRing");
                }
            }
        }

        #endregion

        #region ContactDownOnFacetLabel

        private Subject<TouchEventArgs> _contactDownOnFacetLabel = null;

        /// <summary>
        /// Gets or sets the ContactDownOnFacetLabel property. This observable property 
        /// indicates ....
        /// </summary>
        public Subject<TouchEventArgs> ContactDownOnFacetLabel
        {
            get { return _contactDownOnFacetLabel; }
            set
            {
                if (_contactDownOnFacetLabel != value)
                {
                    _contactDownOnFacetLabel = value;
                    RaisePropertyChanged("ContactDownOnFacetLabel");
                }
            }
        }

        #endregion

        #region ContactDownOnValueLabel

        private Subject<TouchEventArgs> _contactDownOnValueLabel = null;

        /// <summary>
        /// Gets or sets the ContactDownOnValueLabel property. This observable property 
        /// indicates ....
        /// </summary>
        public Subject<TouchEventArgs> ContactDownOnValueLabel
        {
            get { return _contactDownOnValueLabel; }
            set
            {
                if (_contactDownOnValueLabel != value)
                {
                    _contactDownOnValueLabel = value;
                    RaisePropertyChanged("ContactDownOnValueLabel");
                }
            }
        }

        #endregion

        #region ContactUpOnFacetLabel

        private Subject<TouchEventArgs> _contactUpOnFacetLabel;

        /// <summary>
        /// Gets or sets the ContactUpOnFacetLabel property. This observable property 
        /// indicates
        /// </summary>
        public Subject<TouchEventArgs> ContactUpOnFacetLabel
        {
            get { return _contactUpOnFacetLabel; }
            set
            {
                if (_contactUpOnFacetLabel != value)
                {
                    Subject<TouchEventArgs> old = _contactUpOnFacetLabel;
                    _contactUpOnFacetLabel = value;
                    OnContactUpOnFacetLabelChanged(old, value);
                    RaisePropertyChanged("ContactUpOnFacetLabel");
                }
            }
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the ContactUpOnFacetLabel property.
        /// </summary>
        protected virtual void OnContactUpOnFacetLabelChanged(Subject<TouchEventArgs> oldContactUpOnFacetLabel, Subject<TouchEventArgs> newContactUpOnFacetLabel)
        {
        }

        #endregion

        #region ContactUpOnValueLabel

        private Subject<TouchEventArgs> _contactUpOnValueLabel = null;

        /// <summary>
        /// Gets or sets the ContactUpOnValueLabel property. This observable property 
        /// indicates ....
        /// </summary>
        public Subject<TouchEventArgs> ContactUpOnValueLabel
        {
            get { return _contactUpOnValueLabel; }
            set
            {
                if (_contactUpOnValueLabel != value)
                {
                    _contactUpOnValueLabel = value;
                    RaisePropertyChanged("ContactUpOnValueLabel");
                }
            }
        }

        #endregion

        #region ContactUpOnFacetWheel

        private Subject<TouchEventArgs> _contactUpOnFacetWheel = null;

        /// <summary>
        /// Gets or sets the ContactUpOnFacetWheel property. This observable property 
        /// indicates ....
        /// </summary>
        public Subject<TouchEventArgs> ContactUpOnFacetWheel
        {
            get { return _contactUpOnFacetWheel; }
            set
            {
                if (_contactUpOnFacetWheel != value)
                {
                    _contactUpOnFacetWheel = value;
                    RaisePropertyChanged("ContactUpOnFacetWheel");
                }
            }
        }

        #endregion

        #region ContactUpOnValueWheel

        private Subject<TouchEventArgs> _contactUpOnValueWheel = null;

        /// <summary>
        /// Gets or sets the ContactUpOnValueWheel property. This observable property 
        /// indicates ....
        /// </summary>
        public Subject<TouchEventArgs> ContactUpOnValueWheel
        {
            get { return _contactUpOnValueWheel; }
            set
            {
                if (_contactUpOnValueWheel != value)
                {
                    _contactUpOnValueWheel = value;
                    RaisePropertyChanged("ContactUpOnValueWheel");
                }
            }
        }

        #endregion

        #region ContactDownOnFacetWheel

        private Subject<TouchEventArgs> _contactDownOnFacetWheel = null;

        /// <summary>
        /// Gets or sets the ContactDownOnFacetWheel property. This observable property 
        /// indicates ....
        /// </summary>
        public Subject<TouchEventArgs> ContactDownOnFacetWheel
        {
            get { return _contactDownOnFacetWheel; }
            set
            {
                if (_contactDownOnFacetWheel != value)
                {
                    _contactDownOnFacetWheel = value;
                    RaisePropertyChanged("ContactDownOnFacetWheel");
                }
            }
        }

        #endregion

        #region ContactChangedOnFacetWheel

        private Subject<TouchEventArgs> _contactChangedOnFacetWheel = null;

        /// <summary>
        /// Gets or sets the ContactChangedOnFacetWheel property. This observable property 
        /// indicates ....
        /// </summary>
        public Subject<TouchEventArgs> ContactChangedOnFacetWheel
        {
            get { return _contactChangedOnFacetWheel; }
            set
            {
                if (_contactChangedOnFacetWheel != value)
                {
                    _contactChangedOnFacetWheel = value;
                    RaisePropertyChanged("ContactChangedOnFacetWheel");
                }
            }
        }

        #endregion

        #region ContactDownOnValueWheel

        private Subject<TouchEventArgs> _contactDownOnValueWheel = null;

        /// <summary>
        /// Gets or sets the ContactDownOnValueWheel property. This observable property 
        /// indicates ....
        /// </summary>
        public Subject<TouchEventArgs> ContactDownOnValueWheel
        {
            get { return _contactDownOnValueWheel; }
            set
            {
                if (_contactDownOnValueWheel != value)
                {
                    _contactDownOnValueWheel = value;
                    RaisePropertyChanged("ContactDownOnValueWheel");
                }
            }
        }

        #endregion

        #region ContactChangedOnValueWheel

        private Subject<TouchEventArgs> _contactChangedOnValueWheel = null;

        /// <summary>
        /// Gets or sets the ContactChangedOnValueWheel property. This observable property 
        /// indicates ....
        /// </summary>
        public Subject<TouchEventArgs> ContactChangedOnValueWheel
        {
            get { return _contactChangedOnValueWheel; }
            set
            {
                if (_contactChangedOnValueWheel != value)
                {
                    _contactChangedOnValueWheel = value;
                    RaisePropertyChanged("ContactChangedOnValueWheel");
                }
            }
        }

        #endregion

        #region ContactDownOnRangeControl

        private Subject<TouchEventArgs> _contactDownOnRangeControl = null;

        /// <summary>
        /// Gets or sets the ContactDownOnRangeControl property. This observable property 
        /// indicates ....
        /// </summary>
        public Subject<TouchEventArgs> ContactDownOnRangeControl
        {
            get { return _contactDownOnRangeControl; }
            set
            {
                if (_contactDownOnRangeControl != value)
                {
                    _contactDownOnRangeControl = value;
                    RaisePropertyChanged("ContactDownOnRangeControl");
                }
            }
        }

        #endregion

        #region ContactUpOnRangeControl

        private Subject<TouchEventArgs> _contactUpOnRangeControl = null;

        /// <summary>
        /// Gets or sets the ContactUpOnRangeControl property. This observable property 
        /// indicates ....
        /// </summary>
        public Subject<TouchEventArgs> ContactUpOnRangeControl
        {
            get { return _contactUpOnRangeControl; }
            set
            {
                if (_contactUpOnRangeControl != value)
                {
                    _contactUpOnRangeControl = value;
                    RaisePropertyChanged("ContactUpOnRangeControl");
                }
            }
        }

        #endregion

        #region ResultCountAngle

        private double _resultCountAngle = 0.0;

        /// <summary>
        /// Gets or sets the ResultCountAngle property. This observable property 
        /// indicates ....
        /// </summary>
        public double ResultCountAngle
        {
            get { return _resultCountAngle; }
            set
            {
                if (_resultCountAngle != value)
                {
                    _resultCountAngle = value;
                    RaisePropertyChanged("ResultCountAngle");
                }
            }
        }

        #endregion

        #region ResultCountPositionX

        private double _resultCountPositionX = 0;

        /// <summary>
        /// Gets or sets the ResultCountPositionX property. This observable property 
        /// indicates ....
        /// </summary>
        public double ResultCountPositionX
        {
            get { return _resultCountPositionX; }
            set
            {
                if (_resultCountPositionX != value)
                {
                    _resultCountPositionX = value;
                    RaisePropertyChanged("ResultCountPositionX");
                }
            }
        }

        #endregion

        #region ResultCountPositionY

        private double _resultCountPositionY = 0;

        /// <summary>
        /// Gets or sets the ResultCountPositionY property. This observable property 
        /// indicates ....
        /// </summary>
        public double ResultCountPositionY
        {
            get { return _resultCountPositionY; }
            set
            {
                if (_resultCountPositionY != value)
                {
                    _resultCountPositionY = value;
                    RaisePropertyChanged("ResultCountPositionY");
                }
            }
        }

        #endregion

        #region Facets

        private String[] _facets = null;

        /// <summary>
        /// Gets or sets the Facets property. This observable property 
        /// indicates ....
        /// </summary>
        public String[] Facets
        {
            get { return _facets; }
            set
            {
                if (_facets != value)
                {
                    _facets = value;
                    RaisePropertyChanged("Facets");
                }
            }
        }

        #endregion

        #region FacetDisplayNames

        private String[] _facetDisplayNames = null;

        /// <summary>
        /// Gets or sets the FacetDisplayNames property. This observable property 
        /// indicates ....
        /// </summary>
        public String[] FacetDisplayNames
        {
            get { return _facetDisplayNames; }
            set
            {
                if (_facetDisplayNames != value)
                {
                    _facetDisplayNames = value;
                    RaisePropertyChanged("FacetDisplayNames");
                }
            }
        }

        #endregion

        #region FacetWheelStateMachine

        private WindowsTouchStateMachine<FacetWheelStates> _facetWheelStateMachine = null;

        /// <summary>
        /// Gets or sets the FacetWheelStateMachine property. This observable property 
        /// indicates ....
        /// </summary>
        public WindowsTouchStateMachine<FacetWheelStates> FacetWheelStateMachine
        {
            get { return _facetWheelStateMachine; }
            set
            {
                if (_facetWheelStateMachine != value)
                {
                    _facetWheelStateMachine = value;
                    RaisePropertyChanged("FacetWheelStateMachine");
                }
            }
        }

        #endregion

        #region ValueWheelStateMachine

        private WindowsTouchStateMachine<ValueWheelStates> _valueWheelStateMachine = null;

        /// <summary>
        /// Gets or sets the ValueWheelStateMachine property. This observable property 
        /// indicates ....
        /// </summary>
        public WindowsTouchStateMachine<ValueWheelStates> ValueWheelStateMachine
        {
            get { return _valueWheelStateMachine; }
            set
            {
                if (_valueWheelStateMachine != value)
                {
                    _valueWheelStateMachine = value;
                    RaisePropertyChanged("ValueWheelStateMachine");
                }
            }
        }

        #endregion

        #region ValueExpression

        private String _valueExpression;

        /// <summary>
        /// Gets or sets the ValueExpression property. This observable property 
        /// indicates
        /// </summary>
        public String ValueExpression
        {
            get { return _valueExpression; }
            set
            {
                if (_valueExpression != value)
                {
                    String old = _valueExpression;
                    _valueExpression = value;
                    OnValueExpressionChanged(old, value);
                    RaisePropertyChanged("ValueExpression");
                }
            }
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the ValueExpression property.
        /// </summary>
        protected virtual void OnValueExpressionChanged(String oldValueExpression, String newValueExpression)
        {
            _facetNode.LocalExpression = newValueExpression;
        }

        #endregion

        #region SelectedFacet

        private string _selectedFacet;

        /// <summary>
        /// Gets or sets the SelectedFacet property. This observable property 
        /// indicates ....
        /// </summary>
        public string SelectedFacet
        {
            get { return _selectedFacet; }
            set
            {
                if (_selectedFacet != value)
                {
                    _selectedFacet = value;
                    RaisePropertyChanged("SelectedFacet");
                }
            }
        }

        #endregion

        #region SelectedValues

        private IFacetElement[] _selectedValues = null;

        /// <summary>
        /// Gets or sets the SelectedValues property. This observable property 
        /// indicates ....
        /// </summary>
        public IFacetElement[] SelectedValues
        {
            get { return _selectedValues; }
            set
            {
                if (_selectedValues != value)
                {
                    _selectedValues = value;
                    RaisePropertyChanged("SelectedValues");
                }
            }
        }

        #endregion

        #region MinMetricValue

        private double? _minMetricValue = null;

        /// <summary>
        /// Gets or sets the MinMetricValue property. This observable property 
        /// indicates ....
        /// </summary>
        public double? MinMetricValue
        {
            get { return _minMetricValue; }
            set
            {
                if (_minMetricValue != value)
                {
                    _minMetricValue = value;
                    RaisePropertyChanged("MinMetricValue");
                }
            }
        }

        #endregion

        #region MaxMetricValue

        private double? _maxMetricValue = null;

        /// <summary>
        /// Gets or sets the MaxMetricValue property. This observable property 
        /// indicates ....
        /// </summary>
        public double? MaxMetricValue
        {
            get { return _maxMetricValue; }
            set
            {
                if (_maxMetricValue != value)
                {
                    _maxMetricValue = value;
                    RaisePropertyChanged("MaxMetricValue");
                }
            }
        }

        #endregion

        #endregion

        #region internal properties

        internal int ContactsOnWheel
        {
            get { return _contactStore.Count; }
        }

        #endregion

        #region commands

        public RelayCommand<FrameworkElement> LoadedCommand { get; set; }

        #endregion
    }
}