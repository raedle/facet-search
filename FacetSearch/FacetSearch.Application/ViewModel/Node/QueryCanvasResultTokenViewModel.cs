﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using BlendedLibrary.Common.Model;
using BlendedLibrary.Data;
using BlendedLibrary.FacetSearch.Application.Properties;
using BlendedLibrary.FacetSearch.Model;

namespace BlendedLibrary.FacetSearch.Application.ViewModel.Node
{
    public class QueryCanvasResultTokenViewModel : QueryCanvasNodeViewModel, IAngle
    {
        #region private fields

        private readonly bool _initialized = false;

        private string _currentQuery;

        #endregion

        #region properties

        #region Model

        private QueryCanvasResultNode _model;

        public new QueryCanvasResultNode Model
        {
            get { return _model; }
            set
            {
                _model = value;
                RaisePropertyChanged("Model");
            }
        }

        #endregion

        #region Width

        private double _width = 0;

        /// <summary>
        /// Gets or sets the Width property. This observable property 
        /// indicates ....
        /// </summary>
        public double Width
        {
            get { return _width; }
            set
            {
                if (_width != value)
                {
                    _width = value;
                    RaisePropertyChanged("Width");
                }
            }
        }

        #endregion

        #region Height

        private double _height = 0.0;

        /// <summary>
        /// Gets or sets the Height property. This observable property 
        /// indicates ....
        /// </summary>
        public double Height
        {
            get { return _height; }
            set
            {
                if (_height != value)
                {
                    _height = value;
                    RaisePropertyChanged("Height");
                }
            }
        }

        public double Angle
        {
            get
            {
                return Model.Angle;
            }
            set
            {
                if (Model.Angle != value)
                {
                    Model.Angle = value;
                    RaisePropertyChanged("Angle");
                }
            }
        }

        #endregion

        #endregion

        internal QueryCanvasResultTokenViewModel(IDataProvider dataProvider, QueryCanvasResultNode model, QueryCanvasViewModel canvasViewModel)
            : base(dataProvider, model, canvasViewModel)
        {
            Model = model;
            DataProvider = dataProvider;

            Width = Height = Settings.Default.ResultTokenRadius * 2;

            Center = model.Position;
            TopLeft = new Position(Center.X, Center.Y);

            UpdateCenterPosition(Model.Position.X, Model.Position.Y);

            Center.PropertyChanged += (sender, e) =>
            {
                if ("X".Equals(e.PropertyName) || "Y".Equals(e.PropertyName))
                    RaisePropertyChanged("Center");
            };

            Model.PropertyChanged += (sender, e) =>
                {
                    if ("ResultExpression".Equals(e.PropertyName))
                    {
                        ExecuteQuery();
                    }
                };
            ExecuteQuery();
            _initialized = true;
        }

        protected override void FetchQueryCount(string globalExpression)
        {
            if (_initialized)
                base.FetchQueryCount(globalExpression);
        }

        public override void UpdateCenterPosition(double x, double y)
        {
            base.UpdateCenterPosition(x, y);
            base.UpdateTopLeftPosition(x - Width / 2.0, y - Height / 2.0);
        }

        public override void UpdateTopLeftPosition(double x, double y)
        {
            base.UpdateTopLeftPosition(x, y);
            base.UpdateCenterPosition(x + Width / 2.0, y + Height / 2.0);
        }

        internal override Point? GetNearestIntersectionPoint(LineGeometry geometry)
        {
            var ax = geometry.StartPoint.X;
            var ay = geometry.StartPoint.Y;

            var bx = geometry.EndPoint.X;
            var by = geometry.EndPoint.Y;

            var cx = Center.X;
            var cy = Center.Y;

            var dx = ax - bx;
            var dy = ay - by;

            var lineLength = Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));

            //Normalize
            dx = dx / lineLength;
            dy = dy / lineLength;

            var t = dx * (cx - ax) + dy * (cy - ay);

            var ex = t * dx + ax;
            var ey = t * dy + ay;

            var distance = Math.Sqrt(Math.Pow(ex - cx, 2) + Math.Pow(ey - cy, 2));

            var radius = Settings.Default.ResultTokenRadius;

            if (distance < radius)
            {
                var dt = Math.Sqrt(Math.Pow(radius, 2) - Math.Pow(distance, 2));

                //point 1
                var fx = (t - dt) * dx + ax;
                var fy = (t - dt) * dy + ay;

                var fDstX = ax - fx;
                var fDstY = ay - fy;
                var fDistance = Math.Sqrt(Math.Pow(fDstX, 2) + Math.Pow(fDstY, 2));

                //point 2
                var gx = (t + dt) * dx + ax;
                var gy = (t + dt) * dy + ay;

                var gDstX = ax - gx;
                var gDstY = ay - gy;
                var gDistance = Math.Sqrt(Math.Pow(gDstX, 2) + Math.Pow(gDstY, 2));

                if (fDistance < gDistance)
                {
                    return new Point(fx, fy);
                }

                return new Point(gx, gy);
            }

            if (distance == Settings.Default.ResultTokenRadius)
            {
                return new Point(ex, ey);
            }
            return null;
        }

        private void ExecuteQuery()
        {
            var dataProvider = DataProvider;
            var tagValue = Model.TokenId;

            if (Model.ResultExpression != null)
            {
                var query = Model.ResultExpression;

                if (_currentQuery == null || !_currentQuery.Equals(query))
                {
                    dataProvider.VisualizeData(query, (byte)tagValue);
                    dataProvider.UpdateCount(query, this);
                    _currentQuery = query;
                }
            }
            else
            {
                _currentQuery = null;
                QueryCount = 0;
                IsFetchingQueryCount = false;
                if (Model.IngoingLinks.Count <= 0)
                    DataProvider.UnvisualizeData((byte)tagValue);
            }
        }

        #region IDataReciever

        public override String Query
        {
            get
            {
                return Model.ResultExpression;
            }
        }

        #endregion

        public override void Dispose()
        {
            Task.Factory.StartNew(() => DataProvider.UnvisualizeData((byte)Model.TokenId));
            base.Dispose();
        }
    }
}
