﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using BlendedLibrary.Common;
using BlendedLibrary.Common.Model;
using BlendedLibrary.Common.Util;
using BlendedLibrary.Data;
using BlendedLibrary.FacetSearch.Application.Properties;
using BlendedLibrary.FacetSearch.Application.ViewModel.Link;
using BlendedLibrary.FacetSearch.Model;
using GalaSoft.MvvmLight.Command;

namespace BlendedLibrary.FacetSearch.Application.ViewModel.Node
{
    public class QueryCanvasAndNodeViewModel : QueryCanvasNodeViewModel, IAngle
    {
        #region private fields

        /// <summary>
        /// Indicates if any finger is currently active, as we limit the interaction that is possible to one finger at a time,
        /// so noone can steal the stream from anyone
        /// </summary>
        private bool _isAnyManipulatorActive;

        private static readonly object IsManipulatorKey = new object();

        private readonly QueryCanvasNode _node;

        #endregion

        #region ctor

        public QueryCanvasAndNodeViewModel(IDataProvider dataProvider, QueryCanvasNode model, QueryCanvasViewModel canvasViewModel)
            : base(dataProvider, model, canvasViewModel)
        {

            _node = (model as QueryCanvasNode);
            Debug.Assert(_node != null, "node != null");
            
            TokenId = _node.TokenId;
            Angle = _node.Angle;

            ContactDownCommand = new RelayCommand<TouchEventArgs>(OnContactDown);
            ContactChangedCommand = new RelayCommand<TouchEventArgs>(OnContactChanged);
            ContactUpCommand = new RelayCommand<TouchEventArgs>(OnContactUp);
        }

        #endregion

        public override void UpdateCenterPosition(double x, double y)
        {
            base.UpdateCenterPosition(x, y);
            base.UpdateTopLeftPosition(x - Width/2.0, y-Height/2.0);
        }

        public override void UpdateTopLeftPosition(double x, double y)
        {
            base.UpdateTopLeftPosition(x, y);
            base.UpdateCenterPosition(x + Width/2.0, y + Height/2.0);
        }

        #region event handler

        private void OnContactDown(TouchEventArgs e)
        {
            //we quit, when the contact is not a finger or another finger is currently manipulating
            if ((!e.TouchDevice.IsFingerRecognized()) || _isAnyManipulatorActive)
                return;
            
            //Contacts.CaptureContact(e.Contact, e.Source as IInputElement);
            //e.Contact.SetUserData(IsManipulatorKey, true);
            //_isAnyManipulatorActive = true;

            

            //Add an exit link if no one exists
            if (_node.ExitLink == null)
            {

                Vector diff = new Point(Center.X, Center.Y) - e.TouchDevice.GetPosition(null);

                double alpha = Math.Atan2(diff.Y, diff.X) + Math.PI;

                var s = Settings.Default;
                var destinationX = Center.X + Math.Cos(alpha) * s.DisplayWidth;
                var destinationY = Center.Y + Math.Sin(alpha) * s.DisplayWidth;

                var exitLinkDestinationPosition = new Position(destinationX, destinationY);

                //add a new Exit Link to the model
                _node.AddExitLink(exitLinkDestinationPosition);

                //we have to wait for the DataBinding of the newly created Exit Link to succeed
                DispatcherHelper.WaitForPriority(DispatcherPriority.Render);

                ExitLink = CanvasViewModel.GetViewModel(_node.ExitLink) as QueryCanvasExitLinkViewModel;
                Debug.Assert(ExitLink != null, "ExitLink != null");
            }
            
            //TODO: ExitLunk != null may cause inconsisten Facet-Streams model state
            //route contact down event to RadialDragBehavior of ExitLink
            if (ExitLink != null)
                ExitLink.ContactDownTrigger.OnNext(e);

            e.Handled = true;
        }

        private void OnContactChanged(TouchEventArgs e)
        {
            var isManipulator = (bool?)e.TouchDevice.GetUserData(IsManipulatorKey);

            //we quit, when the contact is not the current manipulator
            if (!isManipulator.HasValue || !isManipulator.Value)
                return;

            //TODO: implement further

            //var alpha = GetLineAngle(e);

            //this.Model.UpdateExitLine(this.CanvasView.Model, alpha);

            //this.Model.HighlightExitLine();

            //// fix for crossed links
            //if (this.Model.GetExitLine() != null)
            //{
            //    this.Model.GetExitLine().IsAnyManipulatorActive = true;
            //    //Console.WriteLine("Set isAnyManipulatorActive of " +
            //    //    this.Model.GetExitLine().ObjectID + " to " +
            //    //    this.Model.GetExitLine().isAnyManipulatorActive);
            //}

            //this.Model.FindAndHighlightConnectCandidate();

            e.Handled = true;
        }

        private void OnContactUp(TouchEventArgs e)
        {
            var isManipulator = (bool?)e.TouchDevice.GetUserData(IsManipulatorKey);

            //we quit, when the contact is not the current manipulator
            if (!isManipulator.HasValue || !isManipulator.Value)
                return;

            //TODO: implement further
            //this.Model.UnHighlightExitLine();

            //// fix for crossed links
            //// test if link to connect to is a currently manipulated link
            //if (this.Model.ConnectCandidate is FacetCanvasLinkModel &&
            //    (this.Model.ConnectCandidate as FacetCanvasLinkModel).IsAnyManipulatorActive)
            //{
            //    Console.WriteLine("NodeView ignored " + this.Model.ObjectID);

            //    if (this.Model.GetExitLine() != null)
            //    {
            //        this.Model.GetExitLine().IsAnyManipulatorActive = false;
            //        //Console.WriteLine("Set isAnyManipulatorActive of " +
            //        //this.Model.GetExitLine().ObjectID + " to " +
            //        //this.Model.GetExitLine().isAnyManipulatorActive);
            //    }

            //    _isAnyManipulatorActive = false;
            //    Contacts.ReleaseContactCapture(e.Contact);
            //    e.Handled = true;
            //    return;
            //}
            //else
            //{
            //    // check if the new connection to the node would form cycle
            //    if (this.Model.ConnectCandidate is FacetCanvasNodeModel)
            //    {
            //        FacetCanvasNodeModel connectNode = this.Model.ConnectCandidate as FacetCanvasNodeModel;

            //        // add temp link 
            //        this.Model.CanvasModel.AddLink(
            //            new FacetCanvasTempLinkModel("tmpLink" + this.Model.ObjectID, this.Model, connectNode)
            //            );

            //        bool cycle = this.Model.CanvasModel.FindCycle(this.Model);

            //        this.Model.CanvasModel.RemoveLink("tmpLink" + this.Model.ObjectID);

            //        if (cycle)
            //        {
            //            Console.WriteLine("NodeView ignored " + this.Model.ObjectID);

            //            if (this.Model.GetExitLine() != null)
            //            {
            //                this.Model.GetExitLine().IsAnyManipulatorActive = false;
            //                //Console.WriteLine("Set isAnyManipulatorActive of " +
            //                //this.Model.GetExitLine().ObjectID + " to " +
            //                //this.Model.GetExitLine().isAnyManipulatorActive);
            //            }

            //            _isAnyManipulatorActive = false;
            //            Contacts.ReleaseContactCapture(e.Contact);
            //            e.Handled = true;
            //            return;
            //        }
            //    }

            //    Console.WriteLine("NodeView accepted " + this.Model.ObjectID);
            //    // fix for crossed links
            //    if (this.Model.GetExitLine() != null)
            //    {
            //        this.Model.GetExitLine().IsAnyManipulatorActive = false;
            //        //Console.WriteLine("Set isAnyManipulatorActive of " +
            //        //this.Model.GetExitLine().ObjectID + " to " +
            //        //this.Model.GetExitLine().isAnyManipulatorActive);
            //    }
            //}

            //// add new link                        
            //if (this.Model.ConnectCandidate != null && this.Model.GetExitLine() != null)
            //{
            //    // redirect exit line to connect candidate and turn it into an ordinary link
            //    FacetCanvasLinkModel thisExitLine = this.Model.GetExitLine();
            //    FacetCanvasNodeModel thisExitNode = thisExitLine.Dest as FacetCanvasNodeModel;

            //    if (thisExitLine.Dest is FacetCanvasLinkModel && (thisExitLine.Dest as FacetCanvasLinkModel).Dest is FacetCanvasLinkModel)
            //        return;

            //    thisExitLine.Dest = this.Model.ConnectCandidate;
            //    if (this.Model.ConnectCandidate is FacetCanvasNodeModel)
            //        thisExitLine.ObjectID = thisExitLine.Source.ObjectID + "->" + thisExitLine.Dest.ObjectID;
            //    else
            //        thisExitLine.ObjectID = thisExitLine.Source.ObjectID + "->(" + thisExitLine.Dest.ObjectID + ")";
            //    thisExitLine.IsExitLine = false;
            //    ((FacetCanvasNodeModel)thisExitLine.Source).SetExitLine(null);
            //    this.Model.CanvasModel.RemoveNode(thisExitNode);

            //    thisExitLine.UpdateView();
            //    thisExitLine.RestartAnimations(500);

            //    if (this.Model.ConnectCandidate is FacetCanvasLinkModel)
            //    {
            //        // remaining problem: all inbound links to exit line are now an illegal link-to-link
            //        // they have to be redirected to ConnectCandidate as destination
            //        if (thisExitLine.GetAllInboundLinks() != null)
            //        {
            //            foreach (FacetCanvasLinkModel fli in thisExitLine.GetAllInboundLinks())
            //            {
            //                fli.Dest = this.Model.ConnectCandidate;
            //                fli.ObjectID = fli.Source.ObjectID + "->(" + fli.Dest.ObjectID + ")";
            //                fli.UpdateView();
            //            }
            //        }
            //    }
            //}

            //this.Model.VisualState = VisualStates.Solid;
            //this.Model.UpdateView();
            //this.Model.FindAndHighlightConnectCandidate();


            _isAnyManipulatorActive = false;

            e.TouchDevice.Capture(null);

            e.Handled = true;
        }

        #endregion

        #region public properties

        public double Width { get; set; }

        public double Height { get; set; }

        public QueryCanvasExitLinkViewModel ExitLink { get; private set; }

        #region Angle

        private double _angle = 0.0;

        /// <summary>
        /// Gets or sets the Angle property. This observable property 
        /// indicates ....
        /// </summary>
        public double Angle
        {
            get { return _angle; }
            set
            {
                if (_angle != value)
                {
                    _angle = value;
                    RaisePropertyChanged("Angle");
                }
            }
        }

        #endregion

        #endregion

        #region Commands

        public RelayCommand<TouchEventArgs> ContactDownCommand { get; private set; }
        public RelayCommand<TouchEventArgs> ContactChangedCommand { get; private set; }
        public RelayCommand<TouchEventArgs> ContactUpCommand { get; private set; }

        #endregion
    }
}
