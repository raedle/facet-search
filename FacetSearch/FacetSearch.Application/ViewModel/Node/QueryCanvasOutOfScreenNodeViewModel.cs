﻿using BlendedLibrary.Common.Model;
using BlendedLibrary.Data;
using BlendedLibrary.FacetSearch.Model;

namespace BlendedLibrary.FacetSearch.Application.ViewModel.Node
{
    public class QueryCanvasOutOfScreenNodeViewModel : QueryCanvasNodeViewModel, IAngle
    {
        public QueryCanvasOutOfScreenNodeViewModel(IDataProvider dataProvider, QueryCanvasOutOfScreenNode model, QueryCanvasViewModel canvasViewModel)
            : base(dataProvider, model, canvasViewModel)
        {
            
        }

        public override void UpdateCenterPosition(double x, double y)
        {
            base.UpdateCenterPosition(x, y);
            base.UpdateTopLeftPosition(x, y);
        }

        public double Angle
        {
            get;
            set;
        }
    }
}
