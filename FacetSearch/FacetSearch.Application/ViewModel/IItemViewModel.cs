﻿using BlendedLibrary.FacetSearch.Model;

namespace BlendedLibrary.FacetSearch.Application.ViewModel
{
    public interface IItemViewModel<out T> where T:QueryCanvasItem
    {
        T Model { get; }
    }
}
