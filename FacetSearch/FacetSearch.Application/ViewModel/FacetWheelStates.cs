﻿
namespace BlendedLibrary.FacetSearch.Application.ViewModel
{
    public enum FacetWheelStates
    {
        FacetWheelInitial,
        FacetWheelVisible,
        FacetWheelCollapsed
    }
}
