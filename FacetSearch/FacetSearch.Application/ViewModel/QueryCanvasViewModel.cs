﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using BlendedLibrary.Common;
using BlendedLibrary.Common.Model;
using BlendedLibrary.Common.Util;
using BlendedLibrary.Data;
using BlendedLibrary.FacetSearch.Application.InkCanvas;
using BlendedLibrary.FacetSearch.Application.Properties;
using BlendedLibrary.FacetSearch.Application.ViewModel.Link;
using BlendedLibrary.FacetSearch.Application.ViewModel.Node;
using BlendedLibrary.FacetSearch.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using NLog;
using PDollarGestureRecognizer;

namespace BlendedLibrary.FacetSearch.Application.ViewModel
{
    public class QueryCanvasViewModel : QueryCanvasViewModelBase
    {
        #region Logger

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region private fields

        private int _virtualNodeId = int.MaxValue;

        private readonly Dictionary<long, DispatcherTimer> _timers = new Dictionary<long, DispatcherTimer>();

        private readonly Dictionary<long, string> _removedTextNodesDict;
        private readonly Dictionary<long, RemovedItem> _removedItemsDict;

        private AdvancedInkCanvas _canvas;

        private Gesture[] _gestures;

        #endregion

        #region commands

        public RelayCommand LoadedCommand { get; private set; }

        public RelayCommand<SenderAwareEventArgs> KeyUpCommand { get; private set; }

        public RelayCommand<SenderAwareEventArgs> StrokeCollectedCommand { get; private set; }

        public RelayCommand<TouchEventArgs> TouchDownCommand { get; private set; }

        public RelayCommand<TouchEventArgs> TouchMoveCommand { get; private set; }

        public RelayCommand<TouchEventArgs> TouchUpCommand { get; private set; }

        #endregion

        #region properties

        #region Model

        /// <summary>
        /// The <see cref="Model" /> property's name.
        /// </summary>
        public const string ModelPropertyName = "Model";

        private QueryCanvasModel _model;

        /// <summary>
        /// Sets and gets the Model property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public QueryCanvasModel Model
        {
            get
            {
                return _model;
            }

            set
            {
                if (_model == value)
                {
                    return;
                }

                _model = value;
                RaisePropertyChanged(ModelPropertyName);
            }
        }

        #endregion

        #region IsDescriptionVisible

        /// <summary>
        /// The <see cref="IsDescriptionVisible" /> property's name.
        /// </summary>
        public const string IsDescriptionVisiblePropertyName = "IsDescriptionVisible";

        private bool _isDescriptionVisible = true;

        /// <summary>
        /// Sets and gets the IsDescriptionVisible property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsDescriptionVisible
        {
            get
            {
                return _isDescriptionVisible;
            }

            set
            {
                if (_isDescriptionVisible == value)
                {
                    return;
                }

                _isDescriptionVisible = value;
                RaisePropertyChanged(IsDescriptionVisiblePropertyName);
            }
        }

        #endregion

        #endregion

        #region ctor

        public QueryCanvasViewModel()
        {
            Model = new QueryCanvasModel(Settings.Default.DisplayWidth, Settings.Default.DisplayHeight);

            Model.LinkAdded += OnLinkAdded;
            Model.LinkRemoved += OnLinkRemoved;
            Model.NodeAdded += OnNodeAdded;
            Model.NodeRemoved += OnNodeRemoved;

            LoadedCommand = new RelayCommand(OnLoaded);
            KeyUpCommand = new RelayCommand<SenderAwareEventArgs>(OnKeyUp);
            StrokeCollectedCommand = new RelayCommand<SenderAwareEventArgs>(OnStrokeCollected);
            TouchDownCommand = new RelayCommand<TouchEventArgs>(OnTouchDown);
            TouchMoveCommand = new RelayCommand<TouchEventArgs>(OnTouchMove);
            TouchUpCommand = new RelayCommand<TouchEventArgs>(OnTouchUp);
            Items = new ObservableCollection<ViewModelBase>();

            _removedTextNodesDict = new Dictionary<long, string>();
            _removedItemsDict = new Dictionary<long, RemovedItem>();

            _gestures = LoadTrainingSet();

#if DEBUG
            if (DataProvider != null)
            {
                AddDebugContent();
            }
            else
            {
                RegisterChangeNotificationAction(this, "DataProvider", e => { AddDebugContent(); });
            }
#endif
        }

        #endregion

        private void OnLoaded()
        {
        }

        private long GetVirtualNodeId()
        {
            return --_virtualNodeId;
        }

        private void AddDebugContent()
        {
            var facetNode1 = new QueryCanvasFacetNode(new Position(300, 800), 43, GetVirtualNodeId());
            var textNode1 = new QueryCanvasTextNode(new Position(1500, 800), 37, GetVirtualNodeId()) { Text = "Tarantino" };
            var resultNode = new QueryCanvasResultNode(new Position(1100, 800), 87, GetVirtualNodeId());

            Model.AddNode(new QueryCanvasFacetNode(new Position(300, 300), 90, GetVirtualNodeId()));
            Model.AddNode(new QueryCanvasFacetNode(new Position(700, 300), 0, GetVirtualNodeId()));
            Model.AddNode(new QueryCanvasFacetNode(new Position(1100, 300), 197, GetVirtualNodeId()));
            Model.AddNode(facetNode1);
            Model.AddNode(new QueryCanvasTextNode(new Position(700, 800), 14, GetVirtualNodeId()) { Text = "Wessenberg" });
            Model.AddNode(textNode1);
            Model.AddNode(resultNode);

            Model.AddExitLink(textNode1, new Position(3000, 0));

            Model.TurnExitLinkIntoLink(textNode1.ExitLink, resultNode, new Position(0.0, 0.0));
        }

        private void OnKeyUp(SenderAwareEventArgs e)
        {
            var originalEvent = e.OriginalEventArgs as KeyEventArgs;

            if (originalEvent.Key == Key.R)
            {
                Items.Clear();
            }
        }

        /// <summary>
        /// Loads training gesture samples from XML files
        /// </summary>
        /// <returns></returns>
        private static Gesture[] LoadTrainingSet()
        {
            var gestures = new List<Gesture>();

            var assemblyLocation = Assembly.GetExecutingAssembly().Location;
            var parentDirectory = Directory.GetParent(assemblyLocation).FullName;

            var gestureFolders = Directory.GetDirectories(Path.Combine(parentDirectory, "Resources", "GestureSet"));
            foreach (var folder in gestureFolders)
            {
                var gestureFiles = Directory.GetFiles(folder, "*.xml");
                foreach (string file in gestureFiles)
                    gestures.Add(GestureIO.ReadGesture(file));
            }
            return gestures.ToArray();
        }

        #region event handling

        private void OnStrokeCollected(SenderAwareEventArgs e)
        {
            var inkCanvas = e.Sender as AdvancedInkCanvas;
            var eventArgs = e.OriginalEventArgs as StrokeEventArgs;

            var pg = PathGeometry.CreateFromGeometry(eventArgs.Stroke.GetGeometry());

            if (eventArgs.Device == Device.Stylus || (eventArgs.Device == Device.Touch && Settings.Default.IsFingerGesturesEnabled))
            {
                //throw new Exception("Analyze text");
                Console.WriteLine("STROKE {0}", AnalyzeStroke(eventArgs.Stroke));
                RecognizeGesture(pg);
            }
            else if (eventArgs.Device == Device.StylusInverted)
            {
                var nodesToDelete = HitTestHelper.GetElementsInGeometry<UserControl>(pg, inkCanvas)
                    .Select(view => view.DataContext)
                    .OfType<QueryCanvasNodeViewModel>().ToArray();

                foreach (var vm in nodesToDelete)
                {
                    var node = vm.Model as QueryCanvasNode;
                    Debug.Assert(node != null, "node != null");

                    Model.RemoveNode(node);
                }
            }

            var linksToDelete = HitTestHelper.GetElementsInGeometry<UserControl>(pg, inkCanvas)
                .Select(view => view.DataContext)
                .OfType<QueryCanvasLinkViewModel>().ToArray();

            //TODO: add method to model, which is capable of deleting more than one link safely
            foreach (var vm in linksToDelete)
            {
                var link = vm.Model as QueryCanvasLink;
                Debug.Assert(link != null, "link != null");

                Model.DeleteLink(link);
            }
        }

        public string AnalyzeStroke(Stroke stroke)
        {
            using (var inkAnalyzer = new InkAnalyzer())
            {
                // analyzes stroke collection
                analyzeStrokes(inkAnalyzer, stroke);

                var cnc = inkAnalyzer.FindNodesOfType(ContextNodeType.InkWord);
                foreach (InkWordNode wordNode in cnc)
                {
                    return wordNode.GetRecognizedString();
                }
            }
            return null;
        }

        /// <summary>
        /// Analyzes stroke collection.
        /// </summary>
        /// <param name="inkAnalyzer"></param>
        private void analyzeStrokes(InkAnalyzer inkAnalyzer, Stroke stroke)
        {
            inkAnalyzer.AddStroke(stroke, CultureInfo.CurrentUICulture.LCID);
            inkAnalyzer.SetStrokeType(stroke, StrokeType.Writing);
            var status = inkAnalyzer.Analyze();

            if (!status.Successful)
            {
                #region handle analysis warnings

                string message = "";
                foreach (AnalysisWarning warning in status.Warnings)
                {
                    switch (warning.WarningCode)
                    {
                        case AnalysisWarningCode.Aborted:
                            message += "Analysis operation was aborted. ";
                            break;
                        case AnalysisWarningCode.BackgroundException:
                            // This is a fatal warning. Throw an exception.
                            // First, attempt to save as much doc state as possible 
                            // ...

                            // Rethrow the exception so that it can be caught by an exception
                            // handler (or if there is no exception handler, a program error 
                            // debugger such as Dr. Watson can be invoked)
                            throw (warning.BackgroundException);
                        case AnalysisWarningCode.ConfirmedWithoutInkRecognition:
                            message += "Node was confirmed without ink recognition having been performed. ";
                            break;
                        case AnalysisWarningCode.ContextNodeLocationNotSet:
                            message += "Node does not have a proper location set. ";
                            break;
                        case AnalysisWarningCode.FactoidCoercionNotSupported:
                            message += "Factoid coercion failed ";
                            if (warning.AnalysisHint != null && warning.AnalysisHint.Factoid != null)
                            {
                                message += "for factoid: " + warning.AnalysisHint.Factoid + ". ";
                            }
                            break;
                        case AnalysisWarningCode.FactoidNotSupported:
                            if (warning.AnalysisHint != null && warning.AnalysisHint.Factoid != null)
                            {
                                message += warning.AnalysisHint.Factoid + " factoid was not respected. ";
                            }
                            break;
                        case AnalysisWarningCode.GuideNotSupported:
                            message += "Guide was not respected. ";
                            break;
                        case AnalysisWarningCode.AddInkToRecognizerFailed:
                            message += "Ink could not be added to the InkRecognizer. ";
                            break;
                        case AnalysisWarningCode.InkRecognizerInitializationFailed:
                            message += "The InkRecognizer failed to initialize. ";
                            break;
                        case AnalysisWarningCode.NoMatchingInkRecognizerFound:
                            message += "There are no ink recognizers meeting the language or capabilities needed. ";
                            break;
                        case AnalysisWarningCode.LanguageIdNotRespected:
                            message += "The language ID set on a stroke did not match the language ID of the InkRecognizer. ";
                            break;
                        case AnalysisWarningCode.PartialDictionaryTermsNotSupported:
                            message += "Partial dictionary terms could not be returned from the text recognizer. ";
                            break;
                        case AnalysisWarningCode.TextRecognitionProcessFailed:
                            message += "The text recognition process failed. ";
                            break;
                        case AnalysisWarningCode.SetPrefixSuffixFailed:
                            message += "The text recognizer was unable to respect either the prefix or suffix. ";
                            if (warning.AnalysisHint != null && warning.AnalysisHint.PrefixText != null)
                            {
                                message += "Prefix: " + warning.AnalysisHint.PrefixText + ". ";
                            }
                            if (warning.AnalysisHint != null && warning.AnalysisHint.SuffixText != null)
                            {
                                message += "Suffix: " + warning.AnalysisHint.SuffixText + ". ";
                            }
                            break;
                        case AnalysisWarningCode.WordlistNotSupported:
                            message += "Wordlist was not respected. ";
                            break;
                        case AnalysisWarningCode.WordModeNotSupported:
                            message += "Word mode was not respected. ";
                            break;
                    }
                }

                #endregion
            }
        }

        private void RecognizeGesture(PathGeometry geometry)
        {
            var points = new List<PDollarGestureRecognizer.Point>();
            foreach (var f in geometry.Figures)
                foreach (var s in f.Segments)
                    if (s is PolyLineSegment)
                        foreach (var pt in ((PolyLineSegment)s).Points)
                            points.Add(new PDollarGestureRecognizer.Point((float)pt.X, (float)pt.Y, 0));

            if (!points.Any())
                return;

            var candidate = new Gesture(points.ToArray());
            var gestureClass = PointCloudRecognizer.Classify(candidate, _gestures);

            var bounds = geometry.Bounds;
            var position = bounds;

            QueryCanvasNode node = null;

            if ("Circle".Equals(gestureClass))
                node = new QueryCanvasFacetNode(new Position(position.X, position.Y), 0, GetVirtualNodeId());
            else if ("Rectangle".Equals(gestureClass))
                node = new QueryCanvasTextNode(new Position(position.X, position.Y), 0, GetVirtualNodeId());
            else if ("Triangle".Equals(gestureClass))
                node = new QueryCanvasResultNode(new Position(position.X, position.Y), 0, GetVirtualNodeId());

            if (node != null)
                Model.AddNode(node);
        }

        private void OnNodeRemoved(object sender, QueryCanvasItemEventArgs<QueryCanvasNode> e)
        {
            var vm = Items.OfType<QueryCanvasItemViewModel>().Single(item => item.Model == e.Item);

            if (vm is QueryCanvasFacetNodeViewModel)
            {
                var facetVm = vm as QueryCanvasFacetNodeViewModel;

                var removedItem = new RemovedItem
                {
                    MaxMetricValue = facetVm.MaxMetricValue,
                    MinMetricValue = facetVm.MinMetricValue,
                    SelectedFacet = facetVm.SelectedFacet,
                    SelectedValues = facetVm.SelectedValues
                };

                _removedItemsDict.Add(facetVm.TokenId, removedItem);
            }
            else if (vm is QueryCanvasTextNodeViewModel)
            {
                var textVm = vm as QueryCanvasTextNodeViewModel;

                _removedTextNodesDict.Add(textVm.TokenId, (textVm.Model as QueryCanvasTextNode).Text);
            }

            Items.Remove(vm);
            vm.Dispose();
        }

        private void OnNodeAdded(object sender, QueryCanvasItemEventArgs<QueryCanvasNode> e)
        {
            ViewModelBase vm = null;

            if (e.Item is QueryCanvasFacetNode)
            {
                var facetNode = e.Item as QueryCanvasFacetNode;

                if (_removedItemsDict.ContainsKey(facetNode.TokenId))
                {
                    vm = new QueryCanvasFacetNodeViewModel(DataProvider, e.Item as QueryCanvasFacetNode, this, _removedItemsDict[facetNode.TokenId].SelectedFacet, _removedItemsDict[facetNode.TokenId].SelectedValues, _removedItemsDict[facetNode.TokenId].MinMetricValue, _removedItemsDict[facetNode.TokenId].MaxMetricValue);
                    _removedItemsDict.Remove(facetNode.TokenId);
                }
                else
                    vm = new QueryCanvasFacetNodeViewModel(DataProvider, e.Item as QueryCanvasFacetNode, this, null, null, null, null);
            }
            else if (e.Item is QueryCanvasTextNode)
            {
                var tvm = new QueryCanvasTextNodeViewModel(DataProvider, e.Item as QueryCanvasTextNode, this);

                if (_removedTextNodesDict.ContainsKey(tvm.TokenId))
                {
                    (tvm.Model as QueryCanvasTextNode).Text = _removedTextNodesDict[tvm.TokenId];
                    tvm.ApplyKeyword();
                    _removedTextNodesDict.Remove(tvm.TokenId);
                }

                vm = tvm;
            }
            else if (e.Item is QueryCanvasResultNode)
                vm = new QueryCanvasResultTokenViewModel(DataProvider, e.Item as QueryCanvasResultNode, this);
            else if (e.Item is QueryCanvasOutOfScreenNode)
                vm = new QueryCanvasOutOfScreenNodeViewModel(DataProvider, e.Item as QueryCanvasOutOfScreenNode, this);

            if (vm != null)
                Items.Add(vm);
        }

        private void OnLinkRemoved(object sender, QueryCanvasItemEventArgs<QueryCanvasLink> e)
        {
            try
            {
                var vm = Items.OfType<QueryCanvasItemViewModel>().Single(item => item.Model == e.Item);
                Items.Remove(vm);
                //vm.Dispose();
            }
            catch (Exception)
            {
                // ignore
            }
        }

        private void OnLinkAdded(object sender, QueryCanvasItemEventArgs<QueryCanvasLink> e)
        {
            try
            {
                ViewModelBase vm;

                if (e.Item is QueryCanvasExitLink)
                    vm = new QueryCanvasExitLinkViewModel(DataProvider, e.Item as QueryCanvasExitLink, this);
                else
                    vm = new QueryCanvasLinkViewModel(DataProvider, e.Item, this);

                Items.Add(vm);
            }
            catch (Exception ex)
            {
                if (Logger.IsErrorEnabled)
                    Logger.ErrorException(ex.Message, ex);
            }
        }

        private void OnTouchDown(TouchEventArgs e)
        {
            if (_canvas == null)
                _canvas = TreeHelper.TryFindChild<AdvancedInkCanvas>(e.Source as DependencyObject);

            if (Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                var touchPosition = e.TouchDevice.GetPosition(null);

                var virtualNode = new QueryCanvasFacetNode(new Position(touchPosition.X, touchPosition.Y), e.TouchDevice.GetOrientation(null), GetVirtualNodeId());
                Model.AddNode(virtualNode);

                return;
            }
            if (Keyboard.IsKeyDown(Key.RightCtrl))
            {
                var touchPosition = e.TouchDevice.GetPosition(null);

                var virtualNode = new QueryCanvasTextNode(new Position(touchPosition.X, touchPosition.Y), e.TouchDevice.GetOrientation(null), GetVirtualNodeId());
                Model.AddNode(virtualNode);

                return;
            }


            if (!e.TouchDevice.IsTagRecognized())
                return;

            // Display description visible
            IsDescriptionVisible = false;

            var tagValue = e.TouchDevice.GetTagData().Value;

            e.TouchDevice.Capture(e.OriginalSource as IInputElement);

            var position = e.TouchDevice.GetPosition(null);
            var center = e.TouchDevice.GetPosition(null);

            QueryCanvasNode node = null;

            if (_timers.ContainsKey(tagValue))
            {
                if (_timers[tagValue].IsEnabled)
                {
                    _timers[tagValue].Stop();

                    try
                    {
                        node = Model.GetNode(tagValue);
                        node.WillBeDeletedSoon = false;

                        node.Position.X = position.X;
                        node.Position.Y = position.Y;
                        node.Angle = e.TouchDevice.GetOrientation(null);

                        return;
                    }
                    catch
                    {
                        // ignore
                    }
                }
            }

            // Geometry is used to calculate a possible hit with a link.
            System.Windows.Media.Geometry geometry = null;

            if (IsFacetToken(e.TouchDevice) && !Model.ContainsNode(tagValue))
            {
                node = new QueryCanvasFacetNode(new Position(position.X, position.Y), e.TouchDevice.GetOrientation(null), tagValue);

                geometry = new EllipseGeometry(center, Settings.Default.TokenRadius, Settings.Default.TokenRadius);
            }
            else if (IsTextToken(e.TouchDevice) && !Model.ContainsNode(tagValue))
            {
                node = new QueryCanvasTextNode(new Position(position.X, position.Y), e.TouchDevice.GetOrientation(null) + Settings.Default.RotationOffset, tagValue);

                geometry = new RectangleGeometry(new Rect(center.X - Settings.Default.TextSnippetWidth / 4.0, center.Y - Settings.Default.TextSnippetHeight / 4.0, Settings.Default.TextSnippetWidth / 2.0, Settings.Default.TextSnippetHeight / 2.0))
                {
                    Transform = new RotateTransform(e.TouchDevice.GetOrientation(null) + Settings.Default.RotationOffset, center.X, center.Y)
                };
            }
            else if (IsResultToken(e.TouchDevice) && !Model.ContainsNode(tagValue))
            {
                node = new QueryCanvasResultNode(new Position(position.X, position.Y), e.TouchDevice.GetOrientation(null) + Settings.Default.RotationOffset, tagValue);

                geometry = new EllipseGeometry(center, Settings.Default.ResultTokenRadius, Settings.Default.ResultTokenRadius);
            }

            if (node == null)
                return;

            var hitTestResults = HitTestHelper.GetElementsInGeometry<UserControl>(geometry, _canvas).Select(view => view.DataContext).OfType<QueryCanvasLinkViewModel>().ToArray();
            if (hitTestResults.Any())
            {
                var link = hitTestResults.First().Model as QueryCanvasLink;
                Model.PlaceNodeOntoLink(node, link);
            }
            else
            {
                Model.AddNode(node);
            }
        }

        private void OnTouchMove(TouchEventArgs e)
        {
            if (!e.TouchDevice.IsTagRecognized())
                return;

            var tagValue = e.TouchDevice.GetTagData().Value;

            if (!Model.HasNode(tagValue))
            {
                // Add node on touch move if touch down was not performed.
                OnTouchDown(e);
            }
        }

        private void OnTouchUp(TouchEventArgs e)
        {
            if (!e.TouchDevice.IsTagRecognized())
                return;

            try
            {
                long tagValue = e.TouchDevice.GetTagData().Value;

                // node does not exist in model
                if (!Model.HasNode(tagValue)) return;

                if (_timers.ContainsKey(tagValue))
                {
                    if (!_timers[tagValue].IsEnabled)
                        _timers[tagValue].Start();

                    try
                    {
                        var node = Model.GetNode(tagValue);
                        node.WillBeDeletedSoon = true;
                    }
                    catch
                    {
                        // ignore
                    }
                    return;
                }

                var timer = new DispatcherTimer { Interval = Settings.Default.NodeRemoveDelay };
                timer.Tick += (sender, e2) =>
                {
                    timer.Stop();

                    RemoveNode(e.TouchDevice);
                };
                _timers.Add(tagValue, timer);
                timer.Start();

                try
                {
                    var node = Model.GetNode(tagValue);
                    node.WillBeDeletedSoon = true;
                }
                catch
                {
                    // ignore
                }
            }
            catch
            {
                // ignore
            }
        }

        private void RemoveNode(TouchDevice touchDevice)
        {
            if (!touchDevice.IsTagRecognized())
                return;

            long tokenId = touchDevice.GetTagData().Value;

            if ((IsFacetToken(touchDevice) || IsTextToken(touchDevice) || IsResultToken(touchDevice)) && Model.ContainsNode(touchDevice.GetTagData().Value))
            {
                var node = Model.GetNode(tokenId);

                //TODO: save node for a short time, in case it has just been removed temporarily

                Model.RemoveNode(node);
            }

            // Display description visible
            if (Model.NodeCount <= 0)
                IsDescriptionVisible = true;
        }

        #endregion

        #region public properties

        #region Items

        private ObservableCollection<ViewModelBase> _items = null;

        /// <summary>
        /// Gets or sets the Items property. This observable property 
        /// indicates ....
        /// </summary>
        public ObservableCollection<ViewModelBase> Items
        {
            get { return _items; }
            private set
            {
                if (_items != value)
                {
                    _items = value;
                    RaisePropertyChanged("Items");
                }
            }
        }

        #endregion

        #region TextTokenIds

        private int[] _textTokenIds;

        /// <summary>
        /// Gets or sets the TextTokenIds property. This observable property 
        /// indicates ....
        /// </summary>
        public int[] TextTokenIds
        {
            get { return _textTokenIds; }
            set
            {
                if (_textTokenIds != value)
                {
                    _textTokenIds = value;
                    RaisePropertyChanged("TextTokenIds");
                }
            }
        }

        #endregion

        #region FacetTokenIds

        private int[] _facetTokenIds;

        /// <summary>
        /// Gets or sets the FacetTokenIds property. This observable property 
        /// indicates ....
        /// </summary>
        public int[] FacetTokenIds
        {
            get { return _facetTokenIds; }
            set
            {
                if (_facetTokenIds != value)
                {
                    _facetTokenIds = value;
                    RaisePropertyChanged("FacetTokenIds");
                }
            }
        }

        #endregion

        #region ResultTokenIds

        private int[] _resultTokenIds;

        /// <summary>
        /// Gets or sets the ResultTokenIds property. This observable property 
        /// indicates ....
        /// </summary>
        public int[] ResultTokenIds
        {
            get { return _resultTokenIds; }
            set
            {
                if (_resultTokenIds != value)
                {
                    _resultTokenIds = value;
                    RaisePropertyChanged("ResultTokenIds");
                }
            }
        }

        #endregion

        #endregion

        #region internal methods

        internal QueryCanvasItemViewModel GetViewModel(QueryCanvasItem item)
        {
            return Items.OfType<QueryCanvasItemViewModel>().Where(i => i.Model == item).SingleOrDefault();
        }

        #endregion

        #region private methods

        private bool IsFacetToken(TouchDevice c)
        {
            var tagValue = (int)c.GetTagData().Value;
            return (FacetTokenIds.Contains(tagValue));
        }

        private bool IsTextToken(TouchDevice c)
        {
            var tagValue = (int)c.GetTagData().Value;
            return TextTokenIds.Contains(tagValue);
        }

        public bool IsResultToken(TouchDevice c)
        {
            var tagValue = (int)c.GetTagData().Value;
            return ResultTokenIds.Contains(tagValue);
        }

        #endregion

        internal IEnumerable<QueryCanvasLinkViewModel> GetIngoingLinks(QueryCanvasNodeViewModel queryCanvasNodeViewModel)
        {
            return Items.ToArray().OfType<QueryCanvasLinkViewModel>().Where(link => link.Destination == queryCanvasNodeViewModel);
        }

        internal IEnumerable<QueryCanvasLinkViewModel> GetOutgoingLinks(QueryCanvasNodeViewModel queryCanvasNodeViewModel)
        {
            return Items.ToArray().OfType<QueryCanvasLinkViewModel>().Where(link => link.Source == queryCanvasNodeViewModel);
        }
        public new void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                Model.Clear();
            }
        }
    }

    internal class RemovedItem
    {
        private double? _maxMetricValue = null;

        public double? MaxMetricValue
        {
            get { return _maxMetricValue; }
            set
            {
                if (_maxMetricValue != value)
                    _maxMetricValue = value;
            }
        }

        private double? _minMetricValue = null;

        public double? MinMetricValue
        {
            get { return _minMetricValue; }
            set
            {
                if (_minMetricValue != value)
                    _minMetricValue = value;
            }
        }

        private String _selectedFacet = null;

        public String SelectedFacet
        {
            get { return _selectedFacet; }
            set
            {
                if (_selectedFacet != value)
                    _selectedFacet = value;
            }
        }

        private IFacetElement[] _selectedValues = null;

        public IFacetElement[] SelectedValues
        {
            get { return _selectedValues; }
            set
            {
                if (_selectedValues != value)
                    _selectedValues = value;
            }
        }
    }
}
