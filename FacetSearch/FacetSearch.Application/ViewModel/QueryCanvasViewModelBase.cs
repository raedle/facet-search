﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reactive.Linq;
using BlendedLibrary.Data;
using GalaSoft.MvvmLight;

namespace BlendedLibrary.FacetSearch.Application.ViewModel
{
    public class QueryCanvasViewModelBase : ViewModelBase
    {
        private readonly List<IDisposable> _changeNotificationActionSubscriptions = new List<IDisposable>();

        protected void AddChangeNotificationActionSubscription(IDisposable subscription)
        {
            _changeNotificationActionSubscriptions.Add(subscription);
        }

        /// <summary>
        /// !Subscription muss manuell zum Disposen per <see cref="AddChangeNotificationActionSubscription"/> eingetragen werden
        /// </summary>
        protected IObservable<PropertyChangedEventArgs> RegisterChangeNotificationAction(INotifyPropertyChanged source, String sourceProperty)
        {
            return Observable.FromEventPattern<PropertyChangedEventArgs>(source, "PropertyChanged").Where(e => e.EventArgs.PropertyName == sourceProperty).Select(e => e.EventArgs);
        }
        protected IDisposable RegisterChangeNotificationAction(INotifyPropertyChanged source, String sourceProperty, Action<PropertyChangedEventArgs> action)
        {
            var subscription = Observable.FromEventPattern<PropertyChangedEventArgs>(source, "PropertyChanged").Where(e => e.EventArgs.PropertyName == sourceProperty).Subscribe(e => action(e.EventArgs));
            AddChangeNotificationActionSubscription(subscription);
            return subscription;
        }

        #region DataProvider

        private IDataProvider _dataProvider;

        /// <summary>
        /// Gets or sets the DataProvider property. This observable property 
        /// indicates ....
        /// </summary>
        public IDataProvider DataProvider
        {
            get { return _dataProvider; }
            set
            {
                if (_dataProvider != value)
                {
                    _dataProvider = value;
                    RaisePropertyChanged("DataProvider");
                }
            }
        }

        #endregion

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                foreach (var changeNotificationAction in _changeNotificationActionSubscriptions)
                    changeNotificationAction.Dispose();
            }
        }

    }
}