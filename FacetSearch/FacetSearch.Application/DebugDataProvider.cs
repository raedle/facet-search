﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BlendedLibrary.Data;
using BlendedLibrary.Data.Configuration;
using BlendedLibrary.FacetStreams.Data.Xml;

namespace BlendedLibrary.FacetSearch.Application
{
    public class DebugDataProvider : IDataProvider
    {
        private readonly Random _random = new Random();

        #region ctor

        public DebugDataProvider()
        {
            DataProviderConfiguration = new DebugDataProviderConfiguration();
        }

        #endregion

        public string[] GetFacets()
        {
            return new[] { "Facet 1", "Facet 2", "Facet 3", "Facet 4", "Facet 5" };
        }

        public IDataProviderConfiguration DataProviderConfiguration { get; set; }

        public string[] GetFacets(DataRepresentation representation)
        {
            return GetFacets();
        }

        public string GetDisplayNameOfFacet(string facet)
        {
            return facet;
        }

        public IFacetElement[] GetValuesOfCategoricalFacet(string facet)
        {
            return new[]
                   {
                       new FacetElement("Value 1", "Value 1 Screen", _random.Next(0, int.MaxValue), false),
                       new FacetElement("Value 2", "Value 2 Screen", _random.Next(0, int.MaxValue), false),
                       new FacetElement("Value 3", "Value 3 Screen", _random.Next(0, int.MaxValue), false),
                       new FacetElement("Value 4", "Value 4 Screen", _random.Next(0, int.MaxValue), false),
                       new FacetElement("Value 5", "Value 5 Screen", _random.Next(0, int.MaxValue), false),
                       new FacetElement("Value 6", "Value 6 Screen", _random.Next(0, int.MaxValue), false),
                       new FacetElement("Value 7", "Value 7 Screen", _random.Next(0, int.MaxValue), false),
                       new FacetElement("Value 8", "Value 8 Screen", _random.Next(0, int.MaxValue), false),
                       new FacetElement("Value 9", "Value 9 Screen", _random.Next(0, int.MaxValue), false),
                       new FacetElement("Value 10", "Value 10 Screen", _random.Next(0, int.MaxValue), false),
                       new FacetElement("Value 11", "Value 11 Screen", _random.Next(0, int.MaxValue), false),
                       new FacetElement("Value 12", "Value 12 Screen", _random.Next(0, int.MaxValue), false)
                   };
        }

        public string GetDisplayNameOfCategoricalValue(string facet, string value)
        {
            return value;
        }

        public DataRepresentation GetDataRepresentation(string facet)
        {
            if (GetFacets()[0].Equals(facet))
                return DataRepresentation.Metric;
            return DataRepresentation.Categorical;
        }

        public MetricRange GetMetricRange(string facet)
        {
            return new MetricRange(1492, 2013);
        }

        public void VisualizeData(string query, byte targetId)
        {

        }

        public void UnvisualizeData(byte targetId)
        {

        }

        public string GetCategoricalExpression(string facet, IFacetElement[] values)
        {
            return string.Format("categoricalExpression(facet={0},values={1})", facet, string.Join(",", values.Select(v => v.Name)));
        }

        public string GetMetricRangeExpression(string facet, int minValue, int maxValue)
        {
            return string.Format("metricExpression(facet={0},minValue={1},maxValue={2})", facet, minValue, maxValue);
        }

        public string GetTextExpression(string[] facets, string value)
        {
            return string.Format("textExpression(facets={0},value={1})", string.Join(",", facets), value);
        }

        public void UpdateCount(string query, IDataReceiver receiver)
        {
            Task.Factory.StartNew(() =>
                {
                    receiver.IsFetchingQueryCount = true;

                    // Simulate query result latency
                    Thread.Sleep(_random.Next(150, 3000));

                    receiver.QueryCount = _random.Next(1344242);

                    receiver.IsFetchingQueryCount = false;
                });
        }

        public int GetCount(string query)
        {
            return _random.Next(1000);
        }

        public Task<int> GetCountAsync(string query)
        {
            return Task.Factory.StartNew(() => _random.Next(1000));
        }

        public Task<int> GetCancelableCountAsync(string query, CancellationToken token)
        {
            return Task.Factory.StartNew(() => _random.Next(1000), token);
        }

        #region TotalResultCount

        private int _totalResultCount = 1000;

        public int TotalResultCount
        {
            set { _totalResultCount = value; }
            get { return _totalResultCount; }
        }

        #endregion

        public void SaveCache()
        {

        }

        public void Dispose()
        {

        }
    }

    internal class DebugDataProviderConfiguration : IDataProviderConfiguration
    {
        #region Type

        public string Type
        {
            get { return "debug"; }
        }

        #endregion
    }
}
