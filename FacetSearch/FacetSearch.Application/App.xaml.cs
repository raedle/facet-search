﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Threading;
using BlendedLibrary.FacetSearch.Application.Properties;
using Tools.CommonUtilities.WPF.Input;

namespace BlendedLibrary.FacetSearch.Application
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        public App()
        {
            Startup += AppStartup;
            DispatcherUnhandledException += AppDispatcherUnhandledException;

            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.GetCultureInfo(Settings.Default.Language);
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.GetCultureInfo(Settings.Default.Language);

            StartupUri = InputExtensions.GetIsPixelSenseTechnology() ? new Uri("Windows/MainSurfaceWindow.xaml", UriKind.Relative) : new Uri("Windows/MainWindow.xaml", UriKind.Relative);
        }

        void AppStartup(object sender, StartupEventArgs e)
        {
            //foreach (var arg in e.Args)
            //{
            //    switch (arg)
            //    {
            //        case "--reset":
            //            Settings.Default.IsFirstStart = true;
            //            Settings.Default.Save();
            //            break;
            //    }
            //}
        }

        static void AppDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.StackTrace ?? "no stack trace", e.Exception.Message);
            e.Handled = true;
        }
    }
}
