<xsl:stylesheet version='2.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output method='xml'/>
    <xsl:template match="/">
      <results>
      <xsl:for-each select='response/result/doc'>
        <medium>
        <xsl:attribute name="medium_id"><xsl:apply-templates select='str[@name="id"]/text()'/></xsl:attribute>
        <xsl:attribute name="ppn"><xsl:apply-templates select='str[@name="ppn"]/text()'/></xsl:attribute>
          <xsl:for-each select='arr[@name="au_display_short"]/str'>
            <author><xsl:apply-templates select='text()'/></author>
          </xsl:for-each>
        <title><xsl:apply-templates select='str[@name="ti_short"]/text()'/></title>
          <xsl:for-each select='arr[@name="la"]/str'>
            <language><xsl:apply-templates select='text()'/></language>
          </xsl:for-each>
          <xsl:for-each select='arr[@name="sb"]/str'>
            <isbn><xsl:apply-templates select='text()'/></isbn>
          </xsl:for-each>
          <xsl:choose>
             <xsl:when test='arr[@name="mt_facet"]/str/text() = "article"'>
               <cover_url>https://katalog.ub.uni-tuebingen.de/opac/interface/themes/tuebingen/icons/medienicon/zeitschrift_a.gif</cover_url>
             </xsl:when>
             <xsl:when test='arr[@name="mt_facet"]/str/text() = "book"'>
               <cover_url>https://katalog.ub.uni-tuebingen.de/opac/interface/themes/tuebingen/icons/medienicon/buch_rot_a.gif</cover_url>
             </xsl:when>
             <xsl:when test='arr[@name="mt_facet"]/str/text() = "binary"'>
               <cover_url>https://katalog.ub.uni-tuebingen.de/opac/interface/themes/tuebingen/icons/medienicon/elektr_ressource_a.gif</cover_url>
             </xsl:when>
             <xsl:otherwise>
                <cover_url>https://katalog.ub.uni-tuebingen.de/opac/interface/themes/tuebingen/icons/medienicon/aufsatz_a.gif</cover_url>
             </xsl:otherwise>
          </xsl:choose>
        <xsl:if test='arr[@name="py_display"]/str/text()'><year><xsl:apply-templates select='arr[@name="py_display"]/str/text()'/></year></xsl:if>
        <xsl:if test='arr[@name="pu_pp_display"]/str/text()'><description><xsl:apply-templates select='arr[@name="pu_pp_display"]/str/text()'/></description></xsl:if>
        <xsl:if test='arr[@name="umfang"]/str/text()'><note><xsl:apply-templates select='arr[@name="umfang"]/str/text()'/></note></xsl:if>
        </medium>
        <xsl:text>&#xa;</xsl:text>
      </xsl:for-each>
      </results>
  </xsl:template>
</xsl:stylesheet>