<!-- Das Feld clientKey muss manuell eingetragen werden (SummonClientKey). -->
<xsl:stylesheet version='2.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output method='xml'/>
    <xsl:template match="/">
      <xsl:variable name="clientKey">ME8XH5LS2S</xsl:variable>
      <xsl:variable name="image_url">http://api.summon.serialssolutions.com/2.0.0/image/isbn/<xsl:copy-of select="$clientKey" />/</xsl:variable>
      <results>
      <availabilityPath>http://api.summon.serialssolutions.com<xsl:value-of select='response/@availabilityPath'/></availabilityPath>
      <xsl:for-each select='response/documents/document'>
        <medium>
        <xsl:attribute name="medium_id"><xsl:apply-templates select='field[@name="ExternalDocumentID"]/value/text()'/></xsl:attribute>
        <xsl:if test='@availabilityId'>
          <xsl:attribute name="availability_id"><xsl:value-of select='@availabilityId'/></xsl:attribute>
        </xsl:if>
          <xsl:for-each select='field[@name="Author"]/value'>
            <author><xsl:apply-templates select='text()'/></author>
          </xsl:for-each>
        <title><xsl:apply-templates select='field[@name="Title"]/value/text()'/></title>
          <xsl:for-each select='field[@name="Language"]/value'>
            <language><xsl:apply-templates select='text()'/></language>
          </xsl:for-each>
          <xsl:for-each select='field[@name="ISBN"]/value'>
            <isbn><xsl:apply-templates select='text()'/></isbn>
          </xsl:for-each>
            <xsl:variable name="cover_url">
              <xsl:for-each select='field[@name="ISBN"]/value'><xsl:apply-templates select='text()'/>_</xsl:for-each>
            </xsl:variable>
            <xsl:choose>
             <xsl:when test="string-length($cover_url) > 0">
               <cover_url><xsl:copy-of select="$image_url"/><xsl:copy-of select="substring($cover_url, 0, string-length($cover_url))"/>/medium</cover_url>
             </xsl:when>
             <xsl:otherwise>
             </xsl:otherwise>
             </xsl:choose>
        <xsl:if test='field[@name="PublicationYear"]/value/text()'><year><xsl:apply-templates select='field[@name="PublicationYear"]/value/text()'/></year></xsl:if>
        <xsl:if test='field[@name="Subtitle"]/value/text()'><description><xsl:apply-templates select='field[@name="Subtitle"]/value/text()'/></description></xsl:if>
        <xsl:if test='field[@name="Discipline"]/value/text()'><note><xsl:apply-templates select='field[@name="Discipline"]/value/text()'/></note></xsl:if>
        </medium>
        <xsl:text>&#xa;</xsl:text>
      </xsl:for-each>
      </results>
  </xsl:template>
</xsl:stylesheet>