<xsl:stylesheet version='2.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output method='xml'/>
    <xsl:template match="/">
      <results>
      <xsl:for-each select='json/response/docs/id'>
        <medium>
        <xsl:attribute name="medium_id"><xsl:apply-templates select='id/text()'/></xsl:attribute>
        <xsl:attribute name="ppn"><xsl:apply-templates select='str[@name="ppn"]/text()'/></xsl:attribute>
          <xsl:for-each select='authors/id'>
            <author><xsl:apply-templates select='text()'/></author>
          </xsl:for-each>
        <title><xsl:apply-templates select='title/text()'/></title>
          <xsl:for-each select='languages/id'>
            <language><xsl:apply-templates select='text()'/></language>
          </xsl:for-each>
        <xsl:if test='an/text()'><isbn><xsl:apply-templates select='an/text()'/></isbn></xsl:if>
        <xsl:if test='source/dates/published/year/text()'><year><xsl:apply-templates select='source/dates/published/year/text()'/></year></xsl:if>
        <xsl:if test='source/display/text()'><description><xsl:apply-templates select='source/display/text()'/></description></xsl:if>
        <xsl:if test='source/publicationType/text()'><note><xsl:apply-templates select='source/publicationType/text()'/></note></xsl:if>
        </medium>
        <xsl:text>&#xa;</xsl:text>
      </xsl:for-each>
      </results>
  </xsl:template>
</xsl:stylesheet>