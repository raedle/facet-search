﻿using System;
using System.IO;
using System.Xml.Serialization;
using BlendedLibrary.Data.Configuration;
using BlendedLibrary.FacetStreams.Data.Xml.Configuration;

namespace BlendedLibrary.FacetSearch.Application.Utils
{
    public static class DataProviderUtility
    {
        #region static fields

        private const string AppGroupName = "Blended Library";

        private const string AppName = "Facet Search";

        private const string ConfigurationFilename = "rds-config.xml";

        private const string ProxyConfigurationFilename = "application_config.xml";

        #endregion

        #region IDataProviderConfiguration Load/Save

        public static void SaveConfiguration(IDataProviderConfiguration configuartion)
        {
            var serializer = new XmlSerializer(configuartion.GetType());

            var configFilename = GetConfigurationFilename();

            using (var stream = new FileStream(configFilename, FileMode.Create))
            {
                serializer.Serialize(stream, configuartion);
            }
        }

        public static IDataProviderConfiguration LoadConfiguration(Type configurationType)
        {
            var serializer = new XmlSerializer(configurationType);

            var configFilename = GetConfigurationFilename();

            using (var stream = new FileStream(configFilename, FileMode.Open))
            {
                return serializer.Deserialize(stream) as IDataProviderConfiguration;
            }
        }

        #endregion

        #region DataProviderProxyConfiguration Load/Save

        public static void SaveProxyConfiguration(string type)
        {
            var config = new DataProviderProxyConfiguration
            {
                Type = type,
                Index = ConfigurationFilename,
                Xslt = string.Format("{0}.xslt", type),
                AddAvailabilities = true,
                Debug = true
            };

            var serializer = new XmlSerializer(typeof(DataProviderProxyConfiguration));

            var proxyConfigFilename = GetProxyConfigurationFilename();

            using (var stream = new FileStream(proxyConfigFilename, FileMode.Create))
            {
                serializer.Serialize(stream, config);
            }
        }

        #endregion

        #region helpers

        public static string GetConfigurationFilename()
        {
            var appDataPath = GetAppDataPath();
            return Path.Combine(appDataPath, ConfigurationFilename);
        }

        public static string GetProxyConfigurationFilename()
        {
            var appDataPath = GetAppDataPath();
            return Path.Combine(appDataPath, ProxyConfigurationFilename);
        }

        public static string GetAppDataPath()
        {
            var commonAppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var appDataPath = Path.Combine(commonAppDataPath, AppGroupName, AppName);

            if (!Directory.Exists(appDataPath))
                Directory.CreateDirectory(appDataPath);

            return appDataPath;
        }

        #endregion
    }
}
