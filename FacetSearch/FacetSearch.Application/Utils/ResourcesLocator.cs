﻿using System.Resources;

namespace BlendedLibrary.FacetSearch.Application.Utils
{
    internal class ResourcesLocator
    {
        private static readonly ResourceManager ResourceManager = new ResourceManager("BlendedLibrary.FacetSearch.Application.Properties.Resources", typeof(ResourcesLocator).Assembly);

        public static string GetString(string name)
        {
            return ResourceManager.GetString(name);
        }
    }
}
