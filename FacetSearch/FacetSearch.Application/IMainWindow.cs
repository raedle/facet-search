﻿namespace BlendedLibrary.FacetSearch.Application
{
    /// <summary>
    /// An interface of the application's main window to be independent from the finally used window instance
    /// </summary>
    public interface IMainWindow
    {
        /// <summary>
        /// The instance of the main application panel
        /// </summary>
        MainPanel MainPanel { get; }
    }
}
