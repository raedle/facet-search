﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    public class CircleSegment : ContentControl
    {
        #region private fields

        private Grid _contentGrid;
        private PiePiece _segmentShape;
        private TextOnArc _textLabel;
        private TextOnArc _valueCountLabel;
        private TransformGroup _transformGroup;
        private TranslateTransform _translateTransform;
        private RotateTransform _rotateTransform;

        #endregion

        #region ctor

        public CircleSegment()
        {
            BuildVisualParts();
            ApplyTransformations();
        }

        #endregion

        #region private methods

        private void BuildVisualParts()
        {
            _contentGrid = new Grid();
            _segmentShape = new PiePiece();

            _textLabel = new TextOnArc { Foreground = Brushes.White, FontSize = FontSize };
            _textLabel.Loaded += OnTextLabelLoaded;

            _valueCountLabel = new TextOnArc { FontWeight = FontWeights.Bold, Foreground = Brushes.WhiteSmoke, FontSize = 20.0 };
            Panel.SetZIndex(_valueCountLabel, 4711);
            _valueCountLabel.Loaded += OnValueCountLabelLoaded;

            _contentGrid.Children.Add(_segmentShape);
            _contentGrid.Children.Add(_textLabel);
            _contentGrid.Children.Add(_valueCountLabel);

            Content = _contentGrid;
        }

        private void BuildTextLabel()
        {
            Point startPoint;
            Point endPoint;
            Size arcSize;

            var figure = new PathFigure();
            var arcSegment = new ArcSegment();
            var radius = OuterRadius - TextOffsetFromRadius;

            if (TextOrientation == TextOrientations.OnRadius)
            {
                #region text on radius

                var startAngle = TextOffsetFromWedge * Math.PI / 180.0;
                var endAngle = (WedgeAngle - TextOffsetFromWedge) * Math.PI / 180.0;

                startPoint = new Point(-Math.Cos(startAngle) * (radius), Math.Sin(startAngle) * (radius));
                startPoint.Offset(OuterRadius, 0);

                endPoint = new Point(-Math.Cos(endAngle) * (radius), Math.Sin(endAngle) * (radius));
                endPoint.Offset(OuterRadius, 0);

                arcSize = new Size(radius, radius);

                #endregion
            }
            else
            {
                #region text rotated towards center

                var angle = (WedgeAngle / 2.0 + TextOffsetFromWedge) * Math.PI / 180.0;

                startPoint = new Point(-Math.Cos(angle) * radius, Math.Sin(angle) * radius);
                startPoint.Offset(OuterRadius, 0.0);

                endPoint = new Point(-Math.Cos(angle) * InnerRadius, Math.Sin(angle) * InnerRadius);
                endPoint.Offset(OuterRadius, 0.0);

                arcSize = new Size(0.0, 0.0);

                #endregion
            }

            figure.StartPoint = startPoint;
            arcSegment.Point = endPoint;
            arcSegment.Size = arcSize;

            figure.Segments.Add(arcSegment);
            _textLabel.TextAlignment = TextAlignment;
            _textLabel.PathFigure = figure;

            ////Visual debugging
            //Path p = new Path();
            //p.Stroke = Brushes.Red;
            //p.StrokeThickness = 2.0;
            //p.Data = new PathGeometry(new PathFigure[] { figure });
            //_contentGrid.Children.Add(p);
        }

        private void BuildValueCountLabel()
        {
            var figure = new PathFigure();
            var arcSegment = new ArcSegment();

            var radius = OuterRadius - ValueCountOffestFromRadius;
            var startAngle = ValueCountOffsetFromWedge * Math.PI / 180.0;
            var endAngle = (WedgeAngle - ValueCountOffsetFromWedge) * Math.PI / 180.0;

            var startPoint = new Point(-Math.Cos(startAngle) * radius, Math.Sin(startAngle) * radius);
            startPoint.Offset(OuterRadius, 0);

            var endPoint = new Point(-Math.Cos(endAngle) * radius, Math.Sin(endAngle) * radius);
            endPoint.Offset(OuterRadius, 0);

            var arcSize = new Size(radius, radius);

            figure.StartPoint = startPoint;
            arcSegment.Point = endPoint;
            arcSegment.Size = arcSize;

            figure.Segments.Add(arcSegment);
            _valueCountLabel.PathFigure = figure;

            ////Visual debugging
            //Path p = new Path();
            //p.Stroke = Brushes.Red;
            //p.StrokeThickness = 2.0;
            //p.Data = new PathGeometry(new PathFigure[] { figure });
            //_contentGrid.Children.Add(p);
        }

        private void ApplyDeselection()
        {
            _segmentShape.Magnification = 1.0;
            _segmentShape.PushOut = 0.0;
            Width /= Magnification;
            Width -= PushOut;
            Height /= Magnification;
            Height -= PushOut;
            Panel.SetZIndex(this, -1);
        }

        private void ApplySelection()
        {
            _segmentShape.Magnification = Magnification;
            _segmentShape.PushOut = PushOut;
            Width += PushOut;
            Width *= Magnification;
            Height += PushOut;
            Height *= Magnification;
            Panel.SetZIndex(this, 1000);
        }

        private void ApplyTransformations()
        {
            //aply render transform to the whole segment
            _transformGroup = new TransformGroup();
            _translateTransform = new TranslateTransform();
            _rotateTransform = new RotateTransform();

            _transformGroup.Children.Add(_translateTransform);
            _transformGroup.Children.Add(_rotateTransform);
            RenderTransform = _transformGroup;
        }

        #endregion

        #region event handler

        private void OnTextLabelLoaded(object sender, EventArgs e)
        {
            BuildTextLabel();
        }

        private void OnValueCountLabelLoaded(object sender, EventArgs e)
        {
            BuildValueCountLabel();
        }

        #endregion

        #region public properties

        public Shape SegmentShape
        {
            get
            {
                return _segmentShape;
            }
        }

        #endregion

        #region dependency properties

        #region RotationAngle

        /// <summary>
        /// RotationAngle Dependency Property
        /// </summary>
        public static readonly DependencyProperty RotationAngleProperty =
            DependencyProperty.Register("RotationAngle", typeof(double), typeof(CircleSegment),
                new FrameworkPropertyMetadata((double)0.0,
                    new PropertyChangedCallback(OnRotationAngleChanged)));

        /// <summary>
        /// Gets or sets the RotationAngle property. This dependency property 
        /// indicates ....
        /// </summary>
        public double RotationAngle
        {
            get { return (double)GetValue(RotationAngleProperty); }
            set { SetValue(RotationAngleProperty, value); }
        }

        /// <summary>
        /// Handles changes to the RotationAngle property.
        /// </summary>
        private static void OnRotationAngleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CircleSegment target = (CircleSegment)d;
            double oldRotationAngle = (double)e.OldValue;
            double newRotationAngle = target.RotationAngle;
            target.OnRotationAngleChanged(oldRotationAngle, newRotationAngle);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the RotationAngle property.
        /// </summary>
        protected virtual void OnRotationAngleChanged(double oldRotationAngle, double newRotationAngle)
        {
            _segmentShape.RotationAngle = newRotationAngle;
            _rotateTransform.Angle = newRotationAngle;
        }

        #endregion

        #region WedgeAngle

        /// <summary>
        /// WedgeAngle Dependency Property
        /// </summary>
        public static readonly DependencyProperty WedgeAngleProperty =
            DependencyProperty.Register("WedgeAngle", typeof(double), typeof(CircleSegment),
                new FrameworkPropertyMetadata((double)0.0,
                    new PropertyChangedCallback(OnWedgeAngleChanged)));

        /// <summary>
        /// Gets or sets the WedgeAngle property. This dependency property 
        /// indicates ....
        /// </summary>
        public double WedgeAngle
        {
            get { return (double)GetValue(WedgeAngleProperty); }
            set { SetValue(WedgeAngleProperty, value); }
        }

        /// <summary>
        /// Handles changes to the WedgeAngle property.
        /// </summary>
        private static void OnWedgeAngleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CircleSegment target = (CircleSegment)d;
            double oldWedgeAngle = (double)e.OldValue;
            double newWedgeAngle = target.WedgeAngle;
            target.OnWedgeAngleChanged(oldWedgeAngle, newWedgeAngle);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the WedgeAngle property.
        /// </summary>
        protected virtual void OnWedgeAngleChanged(double oldWedgeAngle, double newWedgeAngle)
        {
            _segmentShape.WedgeAngle = newWedgeAngle;
        }

        #endregion

        #region Fill

        /// <summary>
        /// Fill Dependency Property
        /// </summary>
        public static readonly DependencyProperty FillProperty =
            DependencyProperty.Register("Fill", typeof(Brush), typeof(CircleSegment),
                new FrameworkPropertyMetadata((Brush)Brushes.Transparent,
                    new PropertyChangedCallback(OnFillChanged)));

        /// <summary>
        /// Gets or sets the Fill property. This dependency property 
        /// indicates ....
        /// </summary>
        public Brush Fill
        {
            get { return (Brush)GetValue(FillProperty); }
            set { SetValue(FillProperty, value); }
        }

        /// <summary>
        /// Handles changes to the Fill property.
        /// </summary>
        private static void OnFillChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CircleSegment target = (CircleSegment)d;
            Brush oldFill = (Brush)e.OldValue;
            Brush newFill = target.Fill;
            target.OnFillChanged(oldFill, newFill);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Fill property.
        /// </summary>
        protected virtual void OnFillChanged(Brush oldFill, Brush newFill)
        {
            _segmentShape.Fill = newFill;
        }

        #endregion

        #region InnerRadius

        /// <summary>
        /// InnerRadius Dependency Property
        /// </summary>
        public static readonly DependencyProperty InnerRadiusProperty =
            DependencyProperty.Register("InnerRadius", typeof(double), typeof(CircleSegment),
                new FrameworkPropertyMetadata((double)0.0,
                    new PropertyChangedCallback(OnInnerRadiusChanged)));

        /// <summary>
        /// Gets or sets the InnerRadius property. This dependency property 
        /// indicates ....
        /// </summary>
        public double InnerRadius
        {
            get { return (double)GetValue(InnerRadiusProperty); }
            set { SetValue(InnerRadiusProperty, value); }
        }

        /// <summary>
        /// Handles changes to the InnerRadius property.
        /// </summary>
        private static void OnInnerRadiusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CircleSegment target = (CircleSegment)d;
            double oldInnerRadius = (double)e.OldValue;
            double newInnerRadius = target.InnerRadius;
            target.OnInnerRadiusChanged(oldInnerRadius, newInnerRadius);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the InnerRadius property.
        /// </summary>
        protected virtual void OnInnerRadiusChanged(double oldInnerRadius, double newInnerRadius)
        {
            _segmentShape.InnerRadius = newInnerRadius;
        }

        #endregion

        #region OuterRadius

        /// <summary>
        /// OuterRadius Dependency Property
        /// </summary>
        public static readonly DependencyProperty OuterRadiusProperty =
            DependencyProperty.Register("OuterRadius", typeof(double), typeof(CircleSegment),
                new FrameworkPropertyMetadata((double)0.0,
                    new PropertyChangedCallback(OnOuterRadiusChanged)));

        /// <summary>
        /// Gets or sets the OuterRadius property. This dependency property 
        /// indicates ....
        /// </summary>
        public double OuterRadius
        {
            get { return (double)GetValue(OuterRadiusProperty); }
            set { SetValue(OuterRadiusProperty, value); }
        }

        /// <summary>
        /// Handles changes to the OuterRadius property.
        /// </summary>
        private static void OnOuterRadiusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CircleSegment target = (CircleSegment)d;
            double oldOuterRadius = (double)e.OldValue;
            double newOuterRadius = target.OuterRadius;
            target.OnOuterRadiusChanged(oldOuterRadius, newOuterRadius);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the OuterRadius property.
        /// </summary>
        protected virtual void OnOuterRadiusChanged(double oldOuterRadius, double newOuterRadius)
        {
            _segmentShape.OuterRadius = newOuterRadius;
            _rotateTransform.CenterX = newOuterRadius;
            _rotateTransform.CenterY = newOuterRadius;
            _translateTransform.Y = newOuterRadius;
        }

        #endregion

        #region Magnification

        /// <summary>
        /// Magnification Dependency Property
        /// </summary>
        public static readonly DependencyProperty MagnificationProperty =
            DependencyProperty.Register("Magnification", typeof(double), typeof(CircleSegment),
                new FrameworkPropertyMetadata((double)1.0,
                    new PropertyChangedCallback(OnMagnificationChanged)));

        /// <summary>
        /// Gets or sets the Magnification property. This dependency property 
        /// indicates ....
        /// </summary>
        public double Magnification
        {
            get { return (double)GetValue(MagnificationProperty); }
            set { SetValue(MagnificationProperty, value); }
        }

        /// <summary>
        /// Handles changes to the Magnification property.
        /// </summary>
        private static void OnMagnificationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CircleSegment target = (CircleSegment)d;
            double oldMagnification = (double)e.OldValue;
            double newMagnification = target.Magnification;
            target.OnMagnificationChanged(oldMagnification, newMagnification);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Magnification property.
        /// </summary>
        protected virtual void OnMagnificationChanged(double oldMagnification, double newMagnification)
        {
            if (IsSelected)
                _segmentShape.Magnification = newMagnification;
        }

        #endregion

        #region IsSelected

        /// <summary>
        /// IsSelected Dependency Property
        /// </summary>
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(CircleSegment),
                new FrameworkPropertyMetadata((bool)false,
                    new PropertyChangedCallback(OnIsSelectedChanged)));

        /// <summary>
        /// Gets or sets the IsSelected property. This dependency property 
        /// indicates ....
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        /// <summary>
        /// Handles changes to the IsSelected property.
        /// </summary>
        private static void OnIsSelectedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CircleSegment target = (CircleSegment)d;
            bool oldIsSelected = (bool)e.OldValue;
            bool newIsSelected = target.IsSelected;
            target.OnIsSelectedChanged(oldIsSelected, newIsSelected);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the IsSelected property.
        /// </summary>
        protected virtual void OnIsSelectedChanged(bool oldIsSelected, bool newIsSelected)
        {
            if (oldIsSelected != newIsSelected)
            {
                if (newIsSelected)
                    ApplySelection();
                else
                    ApplyDeselection();
            }
        }

        #endregion

        #region PushOut

        /// <summary>
        /// PushOut Dependency Property
        /// </summary>
        public static readonly DependencyProperty PushOutProperty =
            DependencyProperty.Register("PushOut", typeof(double), typeof(CircleSegment),
                new FrameworkPropertyMetadata((double)0.0,
                    new PropertyChangedCallback(OnPushOutChanged)));

        /// <summary>
        /// Gets or sets the PushOut property. This dependency property 
        /// indicates ....
        /// </summary>
        public double PushOut
        {
            get { return (double)GetValue(PushOutProperty); }
            set { SetValue(PushOutProperty, value); }
        }

        /// <summary>
        /// Handles changes to the PushOut property.
        /// </summary>
        private static void OnPushOutChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CircleSegment target = (CircleSegment)d;
            double oldPushOut = (double)e.OldValue;
            double newPushOut = target.PushOut;
            target.OnPushOutChanged(oldPushOut, newPushOut);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the PushOut property.
        /// </summary>
        protected virtual void OnPushOutChanged(double oldPushOut, double newPushOut)
        {
            if (IsSelected)
                _segmentShape.PushOut = newPushOut;
        }

        #endregion

        #region Stroke

        /// <summary>
        /// Stroke Dependency Property
        /// </summary>
        public static readonly DependencyProperty StrokeProperty =
            DependencyProperty.Register("Stroke", typeof(Brush), typeof(CircleSegment),
                new FrameworkPropertyMetadata((Brush)Brushes.Transparent,
                    new PropertyChangedCallback(OnStrokeChanged)));

        /// <summary>
        /// Gets or sets the Stroke property. This dependency property 
        /// indicates ....
        /// </summary>
        public Brush Stroke
        {
            get { return (Brush)GetValue(StrokeProperty); }
            set { SetValue(StrokeProperty, value); }
        }

        /// <summary>
        /// Handles changes to the Stroke property.
        /// </summary>
        private static void OnStrokeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CircleSegment target = (CircleSegment)d;
            Brush oldStroke = (Brush)e.OldValue;
            Brush newStroke = target.Stroke;
            target.OnStrokeChanged(oldStroke, newStroke);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Stroke property.
        /// </summary>
        protected virtual void OnStrokeChanged(Brush oldStroke, Brush newStroke)
        {
            _segmentShape.Stroke = newStroke;
        }

        #endregion

        #region StrokeThickness

        /// <summary>
        /// StrokeThickness Dependency Property
        /// </summary>
        public static readonly DependencyProperty StrokeThicknessProperty =
            DependencyProperty.Register("StrokeThickness", typeof(double), typeof(CircleSegment),
                new FrameworkPropertyMetadata((double)0.0,
                    new PropertyChangedCallback(OnStrokeThicknessChanged)));

        /// <summary>
        /// Gets or sets the StrokeThickness property. This dependency property 
        /// indicates ....
        /// </summary>
        public double StrokeThickness
        {
            get { return (double)GetValue(StrokeThicknessProperty); }
            set { SetValue(StrokeThicknessProperty, value); }
        }

        /// <summary>
        /// Handles changes to the StrokeThickness property.
        /// </summary>
        private static void OnStrokeThicknessChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CircleSegment target = (CircleSegment)d;
            double oldStrokeThickness = (double)e.OldValue;
            double newStrokeThickness = target.StrokeThickness;
            target.OnStrokeThicknessChanged(oldStrokeThickness, newStrokeThickness);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the StrokeThickness property.
        /// </summary>
        protected virtual void OnStrokeThicknessChanged(double oldStrokeThickness, double newStrokeThickness)
        {
            _segmentShape.StrokeThickness = newStrokeThickness;
        }

        #endregion

        #region Text

        public String Text
        {
            get { return (String)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(String), typeof(CircleSegment), new UIPropertyMetadata(null, OnTextChanged));

        private static void OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CircleSegment target = (CircleSegment)d;
            string oldText = (string)e.OldValue;
            string newText = target.Text;
            target.OnTextChanged(oldText, newText);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Text property.
        /// </summary>
        protected virtual void OnTextChanged(string oldText, string newText)
        {
            _textLabel.Text = newText;
        }

        #endregion

        #region TextOrientation

        public TextOrientations TextOrientation
        {
            get { return (TextOrientations)GetValue(TextOrientationProperty); }
            set { SetValue(TextOrientationProperty, (object)value); }
        }

        // Using a DependencyProperty as the backing store for TextOrientation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextOrientationProperty =
            DependencyProperty.Register("TextOrientation", typeof(TextOrientations), typeof(CircleSegment), new UIPropertyMetadata(TextOrientations.TowardsCenter));

        #endregion

        #region TextOffsetFromRadius

        public double TextOffsetFromRadius
        {
            get { return (double)GetValue(TextOffsetFromRadiusProperty); }
            set { SetValue(TextOffsetFromRadiusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextOffsetFromRadius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextOffsetFromRadiusProperty =
            DependencyProperty.Register("TextOffsetFromRadius", typeof(double), typeof(CircleSegment), new UIPropertyMetadata(5.0));

        #endregion

        #region TextOffsetFromWedge

        public double TextOffsetFromWedge
        {
            get { return (double)GetValue(TextOffsetFromWedgeProperty); }
            set { SetValue(TextOffsetFromWedgeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextOffsetFromWedge.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextOffsetFromWedgeProperty =
            DependencyProperty.Register("TextOffsetFromWedge", typeof(double), typeof(CircleSegment), new UIPropertyMetadata(5.0));

        #endregion

        #region TextAlignment

        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, (object)value); }
        }

        // Using a DependencyProperty as the backing store for TextAlignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextAlignmentProperty =
            DependencyProperty.Register("TextAlignment", typeof(TextAlignment), typeof(CircleSegment), new UIPropertyMetadata(TextAlignment.Center));

        #endregion

        #region ValueCount

        /// <summary>
        /// ValueCount Dependency Property
        /// </summary>
        public static readonly DependencyProperty ValueCountProperty =
            DependencyProperty.Register("ValueCount", typeof(int), typeof(CircleSegment),
                new FrameworkPropertyMetadata((int)0,
                    new PropertyChangedCallback(OnValueCountChanged)));

        /// <summary>
        /// Gets or sets the ValueCount property. This dependency property 
        /// indicates ....
        /// </summary>
        public int ValueCount
        {
            get { return (int)GetValue(ValueCountProperty); }
            set { SetValue(ValueCountProperty, value); }
        }

        /// <summary>
        /// Handles changes to the ValueCount property.
        /// </summary>
        private static void OnValueCountChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CircleSegment target = (CircleSegment)d;
            int oldValueCount = (int)e.OldValue;
            int newValueCount = target.ValueCount;
            target.OnValueCountChanged(oldValueCount, newValueCount);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the ValueCount property.
        /// </summary>
        protected virtual void OnValueCountChanged(int oldValueCount, int newValueCount)
        {
            _valueCountLabel.Text = newValueCount.ToString();
        }

        #endregion

        #region ValueCountOffsetFromRadius

        public double ValueCountOffestFromRadius
        {
            get { return (double)GetValue(ValueCountOffestFromRadiusProperty); }
            set { SetValue(ValueCountOffestFromRadiusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ValueCountOffestFromRadius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueCountOffestFromRadiusProperty =
            DependencyProperty.Register("ValueCountOffestFromRadius", typeof(double), typeof(CircleSegment), new UIPropertyMetadata(40.0));

        #endregion

        #region ValueCountOffsetFromWedge

        public double ValueCountOffsetFromWedge
        {
            get { return (double)GetValue(ValueCountOffsetFromWedgeProperty); }
            set { SetValue(ValueCountOffsetFromWedgeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ValueCountOffsetFromWedge.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueCountOffsetFromWedgeProperty =
            DependencyProperty.Register("ValueCountOffsetFromWedge", typeof(double), typeof(CircleSegment), new UIPropertyMetadata(10.0));

        #endregion

        #endregion
    }
}
