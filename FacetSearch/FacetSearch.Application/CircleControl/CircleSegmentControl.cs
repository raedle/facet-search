﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using BlendedLibrary.Common.Util;
using Microsoft.Surface.Presentation.Input;

namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    [ContentProperty("CircleSegments")]
    public class CircleSegmentControl : UserControl
    {
        #region private fields

        private readonly Canvas _contentCanvas = new Canvas();

        private RotateTransform _controlRotateTransform;

        #endregion

        #region ctor

        public CircleSegmentControl()
        {
            Content = _contentCanvas;

            Loaded += OnLoaded;
        }

        #endregion

        #region event handling

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            UpdatePiePieces();

            PrepareControlRotation();
        }

        protected virtual void OnSelectionChanged()
        {

        }

        #endregion

        #region private methods

        private void UpdatePiePieces()
        {
            var segmentAngle = 360.0 / CircleSegments.Count;
            var accumulativeAngle = 0.0;

            foreach (var segment in CircleSegments)
            {
                segment.WedgeAngle = segmentAngle;
                segment.RotationAngle = accumulativeAngle;
                accumulativeAngle += segmentAngle;
            }
        }

        private void AddCircleSegmentInternal(CircleSegment circleSegment)
        {
            CircleSegments.Add(circleSegment);

            circleSegment.InnerRadius = InnerRadius;
            circleSegment.OuterRadius = OuterRadius;
            circleSegment.Magnification = Magnification;
            circleSegment.PushOut = Pushout;

            if (!_contentCanvas.Children.Contains(circleSegment))
                _contentCanvas.Children.Add(circleSegment);
        }

        private void PrepareControlRotation()
        {
            _controlRotateTransform = new RotateTransform();

            if (RenderTransform != null)
            {
                TransformGroup transform;
                if (RenderTransform is TransformGroup)
                    transform = (TransformGroup)RenderTransform;
                else
                {
                    transform = new TransformGroup();
                    transform.Children.Add(RenderTransform);
                }

                transform.Children.Add(_controlRotateTransform);
                RenderTransform = transform;
            }
            else
            {
                RenderTransform = _controlRotateTransform;
            }

            RenderTransformOrigin = new Point(0.5, 0.5);
        }

        private void SelectInternal(CircleSegment segment, bool select)
        {
            if (segment.IsSelected == select)
                return;

            segment.IsSelected = select;

            if (segment.IsSelected)
            {
                //if we don't allow multiselection, we have to
                if (!AllowMultiSelection)
                {
                    //remove the currently selected segment and set it to deselected
                    if (SelectedSegments.Count > 0)
                    {
                        var currentlySelectedSegment = SelectedSegments.First();
                        SelectedSegments.Remove(currentlySelectedSegment);
                        currentlySelectedSegment.IsSelected = false;
                    }
                }

                //then we have to add it to the SelectedSegments collection
                if (!SelectedSegments.Contains(segment))
                    SelectedSegments.Add(segment);
            }
            else
            {
                SelectedSegments.Remove(segment);
            }

            IsAnySegmentSelected = SelectedSegments.Count > 0;
            SelectedSegmentsLabel = GetSelectedSegmentsLabel();
            OnSelectionChanged();
            OnSelectionChanged(EventArgs.Empty);
        }

        #endregion

        #region protected methods

        protected void RemoveCircleSegments()
        {
            _contentCanvas.Children.Clear();
            CircleSegments.Clear();
        }

        protected void AddCircleSegments(IEnumerable<CircleSegment> circleSegments)
        {
            foreach (var circleSegment in circleSegments)
                AddCircleSegmentInternal(circleSegment);
            UpdatePiePieces();
        }

        protected void AddCircleSegment(CircleSegment circleSegment)
        {
            AddCircleSegmentInternal(circleSegment);
            UpdatePiePieces();
        }

        protected virtual String GetSelectedSegmentsLabel()
        {
            return IsAnySegmentSelected ? SelectedSegments.First().Text : "All";
        }

        #endregion

        #region public methods

        public void RotateByAngleDiff(double angleDiff)
        {
            _controlRotateTransform.Angle = AngleHelper.NormalizeAngle(_controlRotateTransform.Angle + angleDiff);
        }

        public CircleSegment GetSegmentByContact(TouchDevice c)
        {
            return GetSegmentByPoint(c.GetCenterPosition(this));
        }

        public CircleSegment GetSegmentByPoint(Point p)
        {
            var centerPosition = new Point(Width / 2.0, Height / 2.0);
            var vPointToCenter = p - centerPosition;
            var angleOfPoint = AngleHelper.NormalizeAngle(Math.Atan2(vPointToCenter.Y, vPointToCenter.X) / Math.PI * 180.0 + 180);
            var controlAngle = 0.0;

            // TODO: Why is the rotate transform null?
            if (_controlRotateTransform != null)
            {
                controlAngle = AngleHelper.NormalizeAngle(_controlRotateTransform.Angle);
                angleOfPoint = AngleHelper.NormalizeAngle(angleOfPoint + controlAngle);
            }

            var segmentUnderPoint = (from CircleSegment segment in CircleSegments
                                     let minAngle = AngleHelper.NormalizeAngle(segment.RotationAngle + controlAngle - segment.WedgeAngle)
                                     let maxAngle = AngleHelper.NormalizeAngle(segment.RotationAngle + controlAngle)
                                     where AngleHelper.IsAngleBetween(minAngle, maxAngle, angleOfPoint)
                                     select segment).ToArray();


            return segmentUnderPoint.SingleOrDefault();
        }

        public void RotateCircleSegmentToContact(CircleSegment s, TouchDevice c)
        {
            var contactPosition = c.GetCenterPosition(Parent as UIElement);
            var centerPosition = new Point(Width / 2.0, Height / 2.0);

            var pointDiff = contactPosition - centerPosition;

            var angleOfContactPosition = Math.Atan2(pointDiff.Y, pointDiff.X) / Math.PI * 180.0 + 180;

            var angleDiff = angleOfContactPosition - s.RotationAngle;

            var resultAngle = angleDiff + s.WedgeAngle / 2.0;

            _controlRotateTransform.Angle = AngleHelper.NormalizeAngle(resultAngle);

            //Console.WriteLine("RotateTransform: " + _controlRotateTransform.Angle);
        }

        public void RotateSelectedCircleSegmentToContact(TouchDevice c)
        {
            if (IsAnySegmentSelected)
                RotateCircleSegmentToContact(SelectedSegments.First(), c);
        }

        public void RotateByAngle(double angle)
        {
            _controlRotateTransform.Angle = AngleHelper.NormalizeAngle(_controlRotateTransform.Angle + angle);
        }

        public double GetResultingSegmentAngle(CircleSegment s)
        {
            return _controlRotateTransform.Angle + s.RotationAngle;
        }

        public void SelectSegment(CircleSegment segment)
        {
            SelectInternal(segment, true);
        }

        public void DeselectSegment(CircleSegment segment)
        {
            SelectInternal(segment, false);
        }

        public void SelectOrDeselectSegment(CircleSegment segment)
        {
            SelectInternal(segment, !segment.IsSelected);
        }

        public void ClearSelection()
        {
            SelectedSegments.Clear();
            IsAnySegmentSelected = SelectedSegments.Count > 0;
            SelectedSegmentsLabel = GetSelectedSegmentsLabel();
        }

        #endregion

        #region public properties

        #region Circle Segments

        private ObservableCollection<CircleSegment> _circleSegments;
        public ObservableCollection<CircleSegment> CircleSegments
        {
            get
            {
                return _circleSegments ?? (_circleSegments = new ObservableCollection<CircleSegment>());
            }
        }

        #endregion

        #region Selected Segments

        private ObservableCollection<CircleSegment> _selectedSegments;
        public ObservableCollection<CircleSegment> SelectedSegments
        {
            get
            {
                return _selectedSegments ?? (_selectedSegments = new ObservableCollection<CircleSegment>());
            }
        }

        #endregion

        #endregion

        #region dp

        #region Inner Radius

        public double InnerRadius
        {
            get { return (double)GetValue(InnerRadiusProperty); }
            set { SetValue(InnerRadiusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for InnerRadius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty InnerRadiusProperty =
            DependencyProperty.Register("InnerRadius", typeof(double), typeof(CircleSegmentControl), new UIPropertyMetadata(0.0));

        #endregion

        #region OuterRadius

        /// <summary>
        /// OuterRadius Dependency Property
        /// </summary>
        public static readonly DependencyProperty OuterRadiusProperty =
            DependencyProperty.Register("OuterRadius", typeof(double), typeof(CircleSegmentControl),
                new FrameworkPropertyMetadata((double)0.0,
                    new PropertyChangedCallback(OnOuterRadiusChanged)));

        /// <summary>
        /// Gets or sets the OuterRadius property. This dependency property 
        /// indicates ....
        /// </summary>
        public double OuterRadius
        {
            get { return (double)GetValue(OuterRadiusProperty); }
            set { SetValue(OuterRadiusProperty, value); }
        }

        /// <summary>
        /// Handles changes to the OuterRadius property.
        /// </summary>
        private static void OnOuterRadiusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CircleSegmentControl target = (CircleSegmentControl)d;
            double oldOuterRadius = (double)e.OldValue;
            double newOuterRadius = target.OuterRadius;
            target.OnOuterRadiusChanged(oldOuterRadius, newOuterRadius);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the OuterRadius property.
        /// </summary>
        protected virtual void OnOuterRadiusChanged(double oldOuterRadius, double newOuterRadius)
        {
            Width = Height = 2.0 * newOuterRadius;
        }

        #endregion

        #region Magnification

        public double Magnification
        {
            get { return (double)GetValue(MagnificationProperty); }
            set { SetValue(MagnificationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Magnification.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MagnificationProperty =
            DependencyProperty.Register("Magnification", typeof(double), typeof(CircleSegmentControl), new UIPropertyMetadata(1.0));

        #endregion

        #region Pushout

        public double Pushout
        {
            get { return (double)GetValue(PushoutProperty); }
            set { SetValue(PushoutProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Popout.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PushoutProperty =
            DependencyProperty.Register("Pushout", typeof(double), typeof(CircleSegmentControl), new UIPropertyMetadata(0.0));

        #endregion

        #region IsAnySegmentSelected

        public bool IsAnySegmentSelected
        {
            get { return (bool)GetValue(IsAnySegmentSelectedProperty); }
            set { SetValue(IsAnySegmentSelectedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsAnySegmentSelected.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsAnySegmentSelectedProperty =
            DependencyProperty.Register("IsAnySegmentSelected", typeof(bool), typeof(CircleSegmentControl), new UIPropertyMetadata(false));

        #endregion

        #region AllowMultiSelection

        public bool AllowMultiSelection
        {
            get { return (bool)GetValue(AllowMultiSelectionProperty); }
            set { SetValue(AllowMultiSelectionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AllowMultiSelection.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowMultiSelectionProperty =
            DependencyProperty.Register("AllowMultiSelection", typeof(bool), typeof(CircleSegmentControl), new UIPropertyMetadata(true));

        #endregion

        #region SelectedSegmentsLabel

        public String SelectedSegmentsLabel
        {
            get { return (String)GetValue(SelectedSegmentsLabelProperty); }
            set { SetValue(SelectedSegmentsLabelProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedSegmentsLabel.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedSegmentsLabelProperty =
            DependencyProperty.Register("SelectedSegmentsLabel", typeof(String), typeof(CircleSegmentControl), new UIPropertyMetadata(""));

        #endregion

        #region State

        public CircleSegmentState State
        {
            get { return (CircleSegmentState)GetValue(StateProperty); }
            set { SetValue(StateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for State.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StateProperty =
            DependencyProperty.Register("State", typeof(CircleSegmentState), typeof(CircleSegmentControl), new UIPropertyMetadata(CircleSegmentState.Open, OnStateChanged));

        private static void OnStateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //var control = d as CircleSegmentControl;

            //if (control == null) return;

            //var state = (CircleSegmentState) e.NewValue;

            //control.Background = state == CircleSegmentState.Open ? Brushes.Yellow : Brushes.DeepSkyBlue;
        }

        #endregion

        #endregion

        #region events

        public event EventHandler SelectionChanged;

        public void OnSelectionChanged(EventArgs e)
        {
            EventHandler handler = SelectionChanged;
            if (handler != null) handler(this, e);
        }

        #endregion

    }
}
