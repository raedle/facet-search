﻿// TextOnPathBase.cs by Charles Petzold, September 2008

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    public class TextOnPathBase : FrameworkElement
    {
        #region protected fields

        protected Typeface Typeface;

        protected VisualCollection VisualChildren;

        protected List<FormattedText> FormattedChars = new List<FormattedText>();

        protected double PathLength;

        protected double TextLength;

        protected Rect BoundingRect;

        #endregion

        #region ctor

        public TextOnPathBase()
        {
            Typeface = new Typeface(FontFamily, FontStyle, FontWeight, FontStretch);
            VisualChildren = new VisualCollection(this);

            //Path p = new Path();
            //var pg = p.Data.GetFlattenedPathGeometry();
            //pg.Figures
        }

        #endregion

        #region overrides

        protected override int VisualChildrenCount
        {
            get
            {
                return VisualChildren.Count;
            }
        }

        protected override Visual GetVisualChild(int index)
        {
            if (index < 0 || index >= VisualChildren.Count)
                throw new ArgumentOutOfRangeException("index");

            return VisualChildren[index];
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            return (Size)BoundingRect.BottomRight;
        }

        #endregion

        #region virtuals

        protected virtual void GenerateVisualChildren()
        {
            VisualChildren.Clear();

            foreach (var formText in FormattedChars)
            {
                var drawingVisual = new DrawingVisual();

                var transformGroup = new TransformGroup();
                transformGroup.Children.Add(new ScaleTransform());
                transformGroup.Children.Add(new RotateTransform());
                transformGroup.Children.Add(new TranslateTransform());
                drawingVisual.Transform = transformGroup;

                var dc = drawingVisual.RenderOpen();
                dc.DrawText(formText, new Point(0, 0));
                dc.Close();

                VisualChildren.Add(drawingVisual);
            }

            TransformVisualChildren();
        }

        protected virtual void TransformVisualChildren()
        {
            BoundingRect = new Rect();

            if (PathLength == 0 || TextLength == 0)
                return;

            if (FormattedChars.Count != VisualChildren.Count)
                return;

            var scalingFactor = PathLength / TextLength;
            var pathGeometry = new PathGeometry(new PathFigure[] { PathFigure });
            double progress = 0;
            BoundingRect = new Rect();

            for (var index = 0; index < VisualChildren.Count; index++)
            {
                var formText = FormattedChars[index];

                var width = scalingFactor * formText.WidthIncludingTrailingWhitespace;

                var baseline = scalingFactor * formText.Baseline;

                progress += width / 2 / PathLength;

                Point point, tangent;

                pathGeometry.GetPointAtFractionLength(progress, out point, out tangent);

                var drawingVisual = VisualChildren[index] as DrawingVisual;
                var transformGroup = drawingVisual.Transform as TransformGroup;
                var scaleTransform = transformGroup.Children[0] as ScaleTransform;
                var rotateTransform = transformGroup.Children[1] as RotateTransform;
                var translateTransform = transformGroup.Children[2] as TranslateTransform;

                scaleTransform.ScaleX = scalingFactor;
                scaleTransform.ScaleY = scalingFactor;
                rotateTransform.Angle = Math.Atan2(tangent.Y, tangent.X) * 180 / Math.PI;
                rotateTransform.CenterX = width / 2;
                rotateTransform.CenterY = baseline;
                translateTransform.X = point.X - width / 2;
                translateTransform.Y = point.Y - baseline;

                var rect = drawingVisual.ContentBounds;
                rect.Transform(transformGroup.Value);
                BoundingRect.Union(rect);

                progress += width / 2 / PathLength;
            }
            InvalidateMeasure();
        }

        #endregion

        #region dp

        #region FontFamily

        public static readonly DependencyProperty FontFamilyProperty = TextElement.FontFamilyProperty.AddOwner(typeof(TextOnPathBase), new FrameworkPropertyMetadata(OnFontFamilyChanged));

        public FontFamily FontFamily
        {
            set { SetValue(FontFamilyProperty, value); }
            get { return (FontFamily)GetValue(FontFamilyProperty); }
        }

        private static void OnFontFamilyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var target = (TextOnPathBase)d;
            var oldFont = (FontFamily)e.OldValue;
            var newFont = target.FontFamily;
            target.OnFontFamilyChanged(oldFont, newFont);
        }

        protected virtual void OnFontFamilyChanged(FontFamily oldFont, FontFamily newFont)
        {
            Typeface = new Typeface(FontFamily, FontStyle, FontWeight, FontStretch);
        }

        #endregion

        #region FontStyle

        /// <summary>
        /// FontStyle Dependency Property
        /// </summary>
        public static readonly DependencyProperty FontStyleProperty =
            TextElement.FontStyleProperty.AddOwner(typeof(TextOnPathBase),
                new FrameworkPropertyMetadata(OnFontStyleChanged));

        /// <summary>
        /// Gets or sets the FontStyle property. This dependency property 
        /// indicates ....
        /// </summary>
        public FontStyle FontStyle
        {
            get { return (FontStyle)GetValue(FontStyleProperty); }
            set { SetValue(FontStyleProperty, value); }
        }

        /// <summary>
        /// Handles changes to the FontStyle property.
        /// </summary>
        private static void OnFontStyleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextOnPathBase target = (TextOnPathBase)d;
            FontStyle oldFontStyle = (FontStyle)e.OldValue;
            FontStyle newFontStyle = target.FontStyle;
            target.OnFontStyleChanged(oldFontStyle, newFontStyle);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the FontStyle property.
        /// </summary>
        protected virtual void OnFontStyleChanged(FontStyle oldFontStyle, FontStyle newFontStyle)
        {
        }

        #endregion

        #region FontWeight

        /// <summary>
        /// FontWeight Dependency Property
        /// </summary>
        public static readonly DependencyProperty FontWeightProperty =
            TextElement.FontWeightProperty.AddOwner(typeof(TextOnPathBase),
                new FrameworkPropertyMetadata(OnFontWeightChanged));

        /// <summary>
        /// Gets or sets the FontWeight property. This dependency property 
        /// indicates ....
        /// </summary>
        public FontWeight FontWeight
        {
            get { return (FontWeight)GetValue(FontWeightProperty); }
            set { SetValue(FontWeightProperty, value); }
        }

        /// <summary>
        /// Handles changes to the FontWeight property.
        /// </summary>
        private static void OnFontWeightChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextOnPathBase target = (TextOnPathBase)d;
            FontWeight oldFontWeight = (FontWeight)e.OldValue;
            FontWeight newFontWeight = target.FontWeight;
            target.OnFontWeightChanged(oldFontWeight, newFontWeight);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the FontWeight property.
        /// </summary>
        protected virtual void OnFontWeightChanged(FontWeight oldFontWeight, FontWeight newFontWeight)
        {
        }

        #endregion

        #region FontStretch

        /// <summary>
        /// FontStretch Dependency Property
        /// </summary>
        public static readonly DependencyProperty FontStretchProperty =
            TextElement.FontStretchProperty.AddOwner(typeof(TextOnPathBase),
                new FrameworkPropertyMetadata(OnFontStretchChanged));

        /// <summary>
        /// Gets or sets the FontStretch property. This dependency property 
        /// indicates ....
        /// </summary>
        public FontStretch FontStretch
        {
            get { return (FontStretch)GetValue(FontStretchProperty); }
            set { SetValue(FontStretchProperty, value); }
        }

        /// <summary>
        /// Handles changes to the FontStretch property.
        /// </summary>
        private static void OnFontStretchChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextOnPathBase target = (TextOnPathBase)d;
            FontStretch oldFontStretch = (FontStretch)e.OldValue;
            FontStretch newFontStretch = target.FontStretch;
            target.OnFontStretchChanged(oldFontStretch, newFontStretch);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the FontStretch property.
        /// </summary>
        protected virtual void OnFontStretchChanged(FontStretch oldFontStretch, FontStretch newFontStretch)
        {
        }

        #endregion

        #region FontSize

        /// <summary>
        /// FontSize Dependency Property
        /// </summary>
        public static readonly DependencyProperty FontSizeProperty = TextElement.FontSizeProperty.AddOwner(typeof(TextOnPathBase),
            new FrameworkPropertyMetadata(20.0, OnFontSizeChanged));

        /// <summary>
        /// Gets or sets the FontSize property. This dependency property 
        /// indicates ....
        /// </summary>
        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        /// <summary>
        /// Handles changes to the FontSize property.
        /// </summary>
        private static void OnFontSizeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextOnPathBase target = (TextOnPathBase)d;
            double oldFontSize = (double)e.OldValue;
            double newFontSize = target.FontSize;
            target.OnFontSizeChanged(oldFontSize, newFontSize);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the FontSize property.
        /// </summary>
        protected virtual void OnFontSizeChanged(double oldFontSize, double newFontSize)
        {
        }

        #endregion

        #region Foreground

        public static readonly DependencyProperty ForegroundProperty =
            TextElement.ForegroundProperty.AddOwner(typeof(TextOnPathBase),
                new FrameworkPropertyMetadata(OnForegroundChanged));

        public Brush Foreground
        {
            set { SetValue(ForegroundProperty, value); }
            get { return (Brush)GetValue(ForegroundProperty); }
        }

        private static void OnForegroundChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextOnPathBase target = (TextOnPathBase)d;
            Brush oldForeground = (Brush)e.OldValue;
            Brush newForeground = target.Foreground;
            target.OnForegroundChanged(oldForeground, newForeground);
        }

        protected virtual void OnForegroundChanged(Brush oldForeground, Brush newForeground)
        {
            
        }
        
        #endregion

        #region Text

        public static readonly DependencyProperty TextProperty = TextBlock.TextProperty.AddOwner(typeof(TextOnPathBase),
            new FrameworkPropertyMetadata(null, OnTextChanged));

        public string Text
        {
            set { SetValue(TextProperty, value); }
            get { return (string)GetValue(TextProperty); }
        }

        /// <summary>
        /// Handles changes to the Text property.
        /// </summary>
        private static void OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextOnPathBase target = (TextOnPathBase)d;
            string oldText = (string)e.OldValue;
            string newText = target.Text;
            target.OnTextChanged(oldText, newText);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Text property.
        /// </summary>
        protected virtual void OnTextChanged(string oldText, string newText)
        {
            if (DesignerProperties.GetIsInDesignMode(this))
                return;

            FormattedChars.Clear();
            TextLength = 0;

            foreach (var formattedText in Text.Select(ch => new FormattedText(ch.ToString(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, Typeface, 100, Foreground)))
            {
                FormattedChars.Add(formattedText);
                TextLength += formattedText.WidthIncludingTrailingWhitespace;
            }

            GenerateVisualChildren();
        }

        #endregion

        #region PathFigure

        public static readonly DependencyProperty PathFigureProperty =
            DependencyProperty.Register("PathFigure",
                typeof(PathFigure),
                typeof(TextOnPathBase),
                new FrameworkPropertyMetadata(null, OnPathFigureChanged));

        public PathFigure PathFigure
        {
            set { SetValue(PathFigureProperty, value); }
            get { return (PathFigure)GetValue(PathFigureProperty); }
        }

        static void OnPathFigureChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextOnPathBase target = (TextOnPathBase)d;
            PathFigure oldPathFigure = (PathFigure)e.OldValue;
            PathFigure newPathFigure = target.PathFigure;
            target.OnPathFigureChanged(oldPathFigure, newPathFigure);
        }

        protected virtual void OnPathFigureChanged(PathFigure oldPathFigure, PathFigure newPathFigure)
        {
            PathLength = GetPathFigureLength(PathFigure);
            TransformVisualChildren();
        }

        #endregion

        #region TextAlignment

        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextAlignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextAlignmentProperty = TextBlock.TextAlignmentProperty.AddOwner(typeof(TextOnPathBase),
            new FrameworkPropertyMetadata(TextAlignment.Center, OnTextAlignmentChanged));

        private static void OnTextAlignmentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextOnPathBase target = (TextOnPathBase)d;
            TextAlignment oldTextAlignment = (TextAlignment)e.OldValue;
            TextAlignment newTextAlignment = target.TextAlignment;
            target.OnTextAlignmentChanged(oldTextAlignment, newTextAlignment);
        }

        protected virtual void OnTextAlignmentChanged(TextAlignment oldTextAlignment, TextAlignment newTextAlignment)
        {
            
        }

        #endregion

        #endregion

        #region static

        public static double GetPathFigureLength(PathFigure pathFigure)
        {
            if (pathFigure == null)
                return 0;

            var isAlreadyFlattened = pathFigure.Segments.All(pathSegment => (pathSegment is PolyLineSegment) || (pathSegment is LineSegment));

            var pathFigureFlattened = isAlreadyFlattened ? pathFigure : pathFigure.GetFlattenedPathFigure();
            double length = 0;
            var pt1 = pathFigureFlattened.StartPoint;

            foreach (var pathSegment in pathFigureFlattened.Segments)
            {
                if (pathSegment is LineSegment)
                {
                    var pt2 = (pathSegment as LineSegment).Point;
                    length += (pt2 - pt1).Length;
                    pt1 = pt2;
                }
                else if (pathSegment is PolyLineSegment)
                {
                    var pointCollection = (pathSegment as PolyLineSegment).Points;
                    foreach (var pt2 in pointCollection)
                    {
                        length += (pt2 - pt1).Length;
                        pt1 = pt2;
                    }
                }
            }
            return length;
        }

        #endregion
    }
}
