﻿namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    public enum SelectionModes
    {
        Selecting,
        Deselecting,
        None
    }
}
