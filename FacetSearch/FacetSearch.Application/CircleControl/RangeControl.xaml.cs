﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using BlendedLibrary.Common;
using BlendedLibrary.FacetSearch.Application.ViewModel.Node;
using Microsoft.Expression.Shapes;

namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    /// <summary>
    /// Interaction logic for RangeControl.xaml
    /// </summary>
    public partial class RangeControl
    {
        #region dependency properties

        /// <summary>
        /// size of inner radius
        /// </summary>
        public double InnerRadius
        {
            get { return (double)GetValue(InnerRadiusProperty); }
            set { SetValue(InnerRadiusProperty, value); }
        }

        public static readonly DependencyProperty InnerRadiusProperty =
            DependencyProperty.Register("InnerRadius", typeof(double), typeof(RangeControl), new UIPropertyMetadata(50.0));

        /// <summary>
        /// size of outer radius
        /// </summary>
        public double OuterRadius
        {
            get { return (double)GetValue(OuterRadiusProperty); }
            set { SetValue(OuterRadiusProperty, value); }
        }

        public static readonly DependencyProperty OuterRadiusProperty =
            DependencyProperty.Register("OuterRadius", typeof(double), typeof(RangeControl), new UIPropertyMetadata(150.0));

        #region MinCurrentValue

        /// <summary>
        /// MinCurrentValue Dependency Property
        /// </summary>
        public static readonly DependencyProperty MinCurrentValueProperty =
            DependencyProperty.Register("MinCurrentValue", typeof(double), typeof(RangeControl),
                new FrameworkPropertyMetadata(0.0,
                    OnMinCurrentValueChanged));

        /// <summary>
        /// Gets or sets the MinCurrentValue property. This dependency property 
        /// indicates ....
        /// </summary>
        public double MinCurrentValue
        {
            get { return (double)GetValue(MinCurrentValueProperty); }
            set { SetValue(MinCurrentValueProperty, value); }
        }

        /// <summary>
        /// Handles changes to the MinCurrentValue property.
        /// </summary>
        private static void OnMinCurrentValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var target = (RangeControl)d;
            var oldMinCurrentValue = (double)e.OldValue;
            var newMinCurrentValue = target.MinCurrentValue;
            target.OnMinCurrentValueChanged(oldMinCurrentValue, newMinCurrentValue);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the MinCurrentValue property.
        /// </summary>
        protected virtual void OnMinCurrentValueChanged(double oldMinCurrentValue, double newMinCurrentValue)
        {
            OnRangeChanged(EventArgs.Empty);
        }

        #endregion

        #region MaxCurrentValue

        /// <summary>
        /// MaxCurrentValue Dependency Property
        /// </summary>
        public static readonly DependencyProperty MaxCurrentValueProperty =
            DependencyProperty.Register("MaxCurrentValue", typeof(double), typeof(RangeControl),
                new FrameworkPropertyMetadata(0.0,
                    OnMaxCurrentValueChanged));

        /// <summary>
        /// Gets or sets the MaxCurrentValue property. This dependency property 
        /// indicates ....
        /// </summary>
        public double MaxCurrentValue
        {
            get { return (double)GetValue(MaxCurrentValueProperty); }
            set { SetValue(MaxCurrentValueProperty, value); }
        }

        /// <summary>
        /// Handles changes to the MaxCurrentValue property.
        /// </summary>
        private static void OnMaxCurrentValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var target = (RangeControl)d;
            var oldMaxCurrentValue = (double)e.OldValue;
            var newMaxCurrentValue = target.MaxCurrentValue;
            target.OnMaxCurrentValueChanged(oldMaxCurrentValue, newMaxCurrentValue);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the MaxCurrentValue property.
        /// </summary>
        protected virtual void OnMaxCurrentValueChanged(double oldMaxCurrentValue, double newMaxCurrentValue)
        {
            OnRangeChanged(EventArgs.Empty);
        }

        #endregion

        /// <summary>
        /// selected minimum value of range
        /// </summary>
        public double MinOverallRange
        {
            get { return (double)GetValue(MinOverallRangeProperty); }
            set { SetValue(MinOverallRangeProperty, value); }
        }

        public static readonly DependencyProperty MinOverallRangeProperty =
            DependencyProperty.Register("MinOverallRange", typeof(double), typeof(RangeControl), new UIPropertyMetadata(0.0));

        /// <summary>
        /// selected maximum value of range
        /// </summary>
        public double MaxOverallRange
        {
            get { return (double)GetValue(MaxOverallRangeProperty); }
            set { SetValue(MaxOverallRangeProperty, value); }
        }

        public static readonly DependencyProperty MaxOverallRangeProperty =
            DependencyProperty.Register("MaxOverallRange", typeof(double), typeof(RangeControl), new UIPropertyMetadata(0.0));

        /// <summary>
        /// 
        /// </summary>
        public double StartAngle
        {
            get { return (double)GetValue(StartAngleProperty); }
            set { SetValue(StartAngleProperty, value); }
        }

        public static readonly DependencyProperty StartAngleProperty =
            DependencyProperty.Register("StartAngle", typeof(double), typeof(RangeControl), new UIPropertyMetadata(40.0));


        /// <summary>
        /// 
        /// </summary>
        public double EndAngle
        {
            get { return (double)GetValue(EndAngleProperty); }
            set { SetValue(EndAngleProperty, value); }
        }

        public static readonly DependencyProperty EndAngleProperty =
            DependencyProperty.Register("EndAngle", typeof(double), typeof(RangeControl), new UIPropertyMetadata(320.0));


        /// <summary>
        /// 
        /// </summary>
        public double RangePieStartAngle
        {
            get { return (double)GetValue(RangePieStartAngleProperty); }
            set { SetValue(RangePieStartAngleProperty, value); }
        }

        public static readonly DependencyProperty RangePieStartAngleProperty =
            DependencyProperty.Register("RangePieStartAngle", typeof(double), typeof(RangeControl), new UIPropertyMetadata(40.0));








        /// <summary>
        /// name of attribute
        /// </summary>
        public string AttributeName
        {
            get { return (string)GetValue(AttributeNameProperty); }
            set { SetValue(AttributeNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AttributeName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AttributeNameProperty =
            DependencyProperty.Register("AttributeName", typeof(string), typeof(RangeControl), new UIPropertyMetadata("no name"));

        public Type TypeOfValue
        {
            get { return (Type)GetValue(TypeOfValueProperty); }
            set { SetValue(TypeOfValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ValueType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TypeOfValueProperty =
            DependencyProperty.Register("TypeOfValue", typeof(Type), typeof(RangeControl), new UIPropertyMetadata(typeof(int)));

        #endregion

        #region private properties

        private int _minContactID;
        private int _maxContactID;

        private double internalSpin = 6;

        private const double StartAngleMin = 40.0;
        private const double EndAngleMax = 320.0;

        private double min_lastXTranslate = 0.0;
        private double min_lastYTranslate = 0.0;

        private double max_lastXTranslate = 0.0;
        private double max_lastYTranslate = 0.0;

        private double distanceOfHandleMax = -16.0;
        private double distanceOfHandleMin = -55.0;

        private double distanceOfMaxText = -65.0;
        private double distanceOfMinText = -80.0;

        #endregion

        public String RangeLabel
        {
            get
            {
                if (TypeOfValue == typeof(int))
                    return String.Format("{0:f0} - {1:f0}", MinCurrentValue, MaxCurrentValue);
                
                return String.Format("{0:f} - {1:f}", MinCurrentValue, MaxCurrentValue);
            }
        }

        public event EventHandler RangeChanged;

        public void OnRangeChanged(EventArgs e)
        {
            var handler = RangeChanged;
            if (handler != null) handler(this, e);
        }

        #region ctor

        public RangeControl()
        {
            InitializeComponent();
        }

        public void InitRangeControl()
        {
            var vm = DataContext as QueryCanvasFacetNodeViewModel;

            if (vm.MinMetricValue.HasValue && vm.MaxMetricValue.HasValue)
            {
                MinCurrentValue = vm.MinMetricValue.Value;
                MaxCurrentValue = vm.MaxMetricValue.Value;
            }
            else
            {
                MinCurrentValue = MinOverallRange;
                MaxCurrentValue = MaxOverallRange;
            }

            RangeControlName.Margin = new Thickness(0, ((OuterRadius - InnerRadius) / 2) - 20, 0, 0);

            StartAngle = CalculateAngleByValue(MinCurrentValue);
            EndAngle = CalculateAngleByValue(MaxCurrentValue);

            TransformElement(StartAngle, -90.0, distanceOfHandleMax, -5.0, RangeControlMaxValue);
            TransformElement(EndAngle, 90.0, distanceOfHandleMin, -5.0, RangeControlMinValue);

            TransformElement(StartAngleMin, -94.0, -75.0, -17.0, OverallMaxValue);
            TransformElement(EndAngleMax, 94.0, -90.0, 7.0, OverallMinValue);

            // white Border: 35, crossing it with the handle cube: 26
            TransformElement(StartAngle, internalSpin, distanceOfHandleMax, -0.0, SecondMaxHandle);
            // white Border: 325, crossing it with the handle cube: 334
            TransformElement(EndAngle, -internalSpin, distanceOfHandleMin, 0.0, SecondMinHandle);
 
        }

        #endregion

        #region private methods

        private double CalculateAngle(Point position)
        {
            double distanceC = Math.Sqrt(Math.Pow((position.X - OuterRadius), 2) + Math.Pow(position.Y - OuterRadius, 2));
            double distanceA = Math.Sqrt(Math.Pow((position.X - OuterRadius), 2) + Math.Pow(position.Y - 0.0, 2));
            double distanceB = Math.Sqrt(Math.Pow((0.0), 2) + Math.Pow(OuterRadius - 0.0, 2));

            double radian = Math.Acos((Math.Pow(distanceB, 2) + Math.Pow(distanceC, 2) - Math.Pow(distanceA, 2)) / (2 * distanceB * distanceC));

            double degree = ConvertRadianToDegree(radian);

            if (position.X <= OuterRadius)
                degree = 360 - degree;
//            if (degree > 320)
//                degree = 320;
//            if (degree < 40)
//                degree = 40;
            //wert.Text = Convert.ToString(degree);
            return degree;
        }

        private Point CalculateCoordinate(double angle, double radiusModification)
        {
            double x = (OuterRadius + radiusModification) * Math.Cos(ConvertDegreeToRadian(angle - 90));
            double y = (OuterRadius + radiusModification) * Math.Sin(ConvertDegreeToRadian(angle - 90));

            return new Point(x, y);
        }

        private double CalculateCurrentValue(double angle)
        {
            return MaxOverallRange - (((MaxOverallRange - MinOverallRange) / (EndAngleMax - StartAngleMin)) * (angle - StartAngleMin));
        }

        private double CalculateAngleByValue(double value)
        {
            return EndAngleMax - (((EndAngleMax - StartAngleMin) / (MaxOverallRange - MinOverallRange)) * (MaxOverallRange - value));
        }

        private void TransformElement(double angle, double angleRotateMod, double radiusModification, double angleModification, FrameworkElement element)
        {
            var point = CalculateCoordinate(angle, radiusModification);
            
            var transGroup = new TransformGroup();

            //if (element is Rectangle)
            //    point.Y += OuterRadius;

            var translateTrans = new TranslateTransform(point.X, point.Y);


            var rotateTrans = new RotateTransform(angle + angleRotateMod);

            transGroup.Children.Add(rotateTrans);
            transGroup.Children.Add(translateTrans);
            element.RenderTransform = transGroup;

            
        }

        private static double ConvertDegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        private static double ConvertRadianToDegree(double angle)
        {
            return angle * 180 / Math.PI;
        }

        #endregion

        #region surface events

        private void OnRangeRangeHandleMaxContactDown(object sender, TouchEventArgs e)
        {
            if (!e.TouchDevice.IsFingerRecognized())
                return;

            _maxContactID = e.TouchDevice.Id;

            var arc = e.OriginalSource as Arc;
            e.TouchDevice.Capture(arc);

            e.Handled = true;

            OnContactDownOnRangeControl(e);
        }

        private void OnRangeRangeHandleMinContactDown(object sender, TouchEventArgs e)
        {
            if (!e.TouchDevice.IsFingerRecognized())
                return;

            _minContactID = e.TouchDevice.Id;

            var arc = e.OriginalSource as Arc;
            e.TouchDevice.Capture(arc);

            e.Handled = true;

            OnContactDownOnRangeControl(e);
        }

        private void OnRangeTextHandleMaxContactDown(object sender, TouchEventArgs e)
        {
            if (!e.TouchDevice.IsFingerRecognized())
                return;

            _maxContactID = e.TouchDevice.Id;

            var textBlock = e.OriginalSource as TextBlock;
            e.TouchDevice.Capture(textBlock);

            e.Handled = true;

            OnContactDownOnRangeControl(e);
        }

        private void OnRangeTextHandleMinContactDown(object sender, TouchEventArgs e)
        {
            if (!e.TouchDevice.IsFingerRecognized())
                return;

            _minContactID = e.TouchDevice.Id;

            var textBlock = e.OriginalSource as TextBlock;
            e.TouchDevice.Capture(textBlock);

            e.Handled = true;

            OnContactDownOnRangeControl(e);
        }

        private void OnSecondMaxHandleContactDown(object sender, TouchEventArgs e)
        {
            if (!e.TouchDevice.IsFingerRecognized())
                return;

            _maxContactID = e.TouchDevice.Id;

            var rectangle = e.OriginalSource as Ellipse;
            e.TouchDevice.Capture(rectangle);

            e.Handled = true;
            SecondMaxHandle.Width = SecondMaxHandle.Height = 60;
            OnContactDownOnRangeControl(e);
        }

        private void OnSecondMinHandleContactDown(object sender, TouchEventArgs e)
        {
            if (!e.TouchDevice.IsFingerRecognized())
                return;

            _minContactID = e.TouchDevice.Id;

            var rectangle = e.OriginalSource as Ellipse;
            e.TouchDevice.Capture(rectangle);

            e.Handled = true;

            SecondMinHandle.Width = SecondMinHandle.Height = 35;

            OnContactDownOnRangeControl(e);
        }

        private void SelfContactChanged(object sender, TouchEventArgs e)
        {
            Point position = e.GetTouchPoint(this).Position;
            double radius = Math.Sqrt(Math.Pow((position.X - OuterRadius), 2) + Math.Pow(position.Y - OuterRadius, 2));
            radius = radius - OuterRadius;

            if (e.TouchDevice.Id == _minContactID)
            {
                double oldEndAngle = EndAngle;

                EndAngle = CalculateAngle(e.TouchDevice.GetCenterPosition(this));
                
                if (EndAngle > 320)
                    EndAngle = 320;
                

                if (EndAngle < StartAngle)
                    EndAngle = StartAngle;


                double minValue = CalculateCurrentValue(EndAngle);

                if (TypeOfValue == typeof(int))
                    minValue = Math.Round(minValue, 0);

                MinCurrentValue = minValue;

                TransformElement(EndAngle, 90.0, -25.0, -5.0, RangeControlMinValue);

                var point = CalculateCoordinate(EndAngle, 0);
                min_lastXTranslate = point.X;
                min_lastYTranslate = point.Y;
                TransformElement(EndAngle, -internalSpin, radius, 0, SecondMinHandle);

                if (EndAngle < 180)
                    TransformElement(EndAngle, -90.0, radius + 30, -5.0, RangeControlMinValue);
                else
                    TransformElement(EndAngle, 90.0, radius + 30, -5.0, RangeControlMinValue);
            }

            else if (e.TouchDevice.Id == _maxContactID)
            {
                
                
                double oldStartAngle = StartAngle;
                StartAngle = CalculateAngle(e.TouchDevice.GetPosition(this));
                
                if (StartAngle < 40)
                    StartAngle = 40;
                if (StartAngle > EndAngle)
                    StartAngle = EndAngle;

                double maxValue = CalculateCurrentValue(StartAngle);

                if (TypeOfValue == typeof(int))
                    maxValue = Math.Round(maxValue, 0);

                MaxCurrentValue = maxValue;
                TransformElement(StartAngle, -90.0, -20.0, -5.0, RangeControlMaxValue);

                var point = CalculateCoordinate(StartAngle, 0);
                max_lastXTranslate = point.X;
                max_lastYTranslate = point.Y;
               
                TransformElement(StartAngle, internalSpin, radius, 0, SecondMaxHandle);


                if (StartAngle > 180)
                    TransformElement(StartAngle, 90.0, radius + 60, -5.0, RangeControlMaxValue);
                else
                    TransformElement(StartAngle, -90.0, radius + 60, -5.0, RangeControlMaxValue);
            }

        }

        private void SelfContactUp(object sender, TouchEventArgs e)
        {
            if (e.TouchDevice.Id == _minContactID)
            {
                _minContactID = 0;
                
                Point position = new Point(min_lastXTranslate, min_lastYTranslate);
                //werty.Text = "min restored: " + Convert.ToString(position);
                //double radius = Math.Sqrt(Math.Pow((position.X - OuterRadius), 2) + Math.Pow(position.Y - OuterRadius, 2));

                //EndAngle = CalculateAngle(position);

  
                if (EndAngle < 180)
                    TransformElement(EndAngle, -90.0, distanceOfHandleMin, -5.0, RangeControlMinValue);
                else
                    TransformElement(EndAngle, 90.0, distanceOfHandleMin, -5.0, RangeControlMinValue);



                TransformElement(EndAngle, -internalSpin, distanceOfHandleMin, 0, SecondMinHandle);
                SecondMinHandle.Width = SecondMinHandle.Height = 25;
            }
            else if (e.TouchDevice.Id == _maxContactID)
            {
                _maxContactID = 0;

                Point position = new Point(max_lastXTranslate, max_lastYTranslate);
                // double radius = Math.Sqrt(Math.Pow((position.X - OuterRadius), 2) + Math.Pow(position.Y - OuterRadius, 2));
                //StartAngle = CalculateAngle(position);

              
                TransformElement(StartAngle, -internalSpin, distanceOfHandleMax, 0, SecondMaxHandle);

                if (StartAngle > 180)
                    TransformElement(StartAngle, 90.0, distanceOfHandleMax, -5.0, RangeControlMaxValue);
                else
                    TransformElement(StartAngle, -90.0, distanceOfHandleMax, -5.0, RangeControlMaxValue);


                SecondMaxHandle.Width = SecondMaxHandle.Height = 50;
            }
            e.TouchDevice.Capture(null);

            e.Handled = true;

            OnContactUpOnRangeControl(e);
        }

        private void SelfContactDown(object sender, TouchEventArgs e)
        {
            //wenn auf kreis down: capturen, damit auch up wenn von Kreis runter erkannt wird
            e.TouchDevice.Capture(this);

            e.Handled = true;
            OnContactDownOnRangeControl(e);
        }

        #endregion

        public event EventHandler<TouchEventArgs> ContactDownOnRangeControl;

        private void OnContactDownOnRangeControl(TouchEventArgs e)
        {
            EventHandler<TouchEventArgs> handler = ContactDownOnRangeControl;
            if (handler != null) handler(this, e);
        }

        public event EventHandler<TouchEventArgs> ContactUpOnRangeControl;

        private void OnContactUpOnRangeControl(TouchEventArgs e)
        {
            EventHandler<TouchEventArgs> handler = ContactUpOnRangeControl;
            if (handler != null) handler(this, e);
        }




    }

    public class SizeCalculator : IMultiValueConverter
    {
        // This calculates the size of the inner circle.
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                return 100.0;
            }
            // Retrieve the size and calculate new value
            var outerRadius = (double)values[0];
            var innerRadius = (double)values[1];

            return (outerRadius - innerRadius);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class AngleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var angle = (Double)value;

            if (parameter != null && parameter.Equals("0"))
                return angle - 5;

            return angle + 5;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class ValueTypeConverter : DependencyObject, IValueConverter 
    {
        public Type ValueType
        {
            get { return (Type)GetValue(ValueTypeProperty); }
            set { SetValue(ValueTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ValueType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueTypeProperty =
            DependencyProperty.Register("ValueType", typeof(Type), typeof(ValueTypeConverter), new UIPropertyMetadata(typeof(int)));


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (ValueType == typeof(int))
                return System.Convert.ToInt32(value);
            if (ValueType == typeof(double))
                return value;
            
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
