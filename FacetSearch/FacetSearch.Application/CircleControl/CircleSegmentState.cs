﻿namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    public enum CircleSegmentState
    {
        Open,
        Close
    }
}
