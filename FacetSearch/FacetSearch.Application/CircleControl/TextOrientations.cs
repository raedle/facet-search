﻿namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    public enum TextOrientations
    {
        TowardsCenter,
        OnRadius
    }
}
