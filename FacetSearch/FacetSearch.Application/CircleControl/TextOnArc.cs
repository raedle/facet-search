﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    public class TextOnArc : TextOnPathBase
    {
        #region private fields

        private FormattedText _ellipsisText;

        private readonly List<DrawingVisual> _ellipsis = new List<DrawingVisual>();

        #endregion

        #region overrides

        protected override void OnTextChanged(String oldText, String newText)
        {
            FormattedChars.Clear();
            TextLength = 0;

            _ellipsisText = new FormattedText(".", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, Typeface, FontSize, Foreground);

            foreach (var formattedText in Text.Select(ch => new FormattedText(ch.ToString(), CultureInfo.CurrentCulture,
                                                                              FlowDirection.LeftToRight, Typeface, FontSize, Foreground)))
            {
                FormattedChars.Add(formattedText);
                TextLength += formattedText.WidthIncludingTrailingWhitespace;
            }

            GenerateVisualChildren();
        }

        protected override void TransformVisualChildren()
        {
            BoundingRect = new Rect();

            if (PathLength == 0 || TextLength == 0)
                return;

            if (FormattedChars.Count != VisualChildren.Count)
                return;

            var scalingFactor = PathLength / TextLength;
            var pathGeometry = new PathGeometry(new PathFigure[] { PathFigure });
            double progress = 0;
            BoundingRect = new Rect();


            //der text soll mittig auf dem kreisbogen platziert werden, daher muss als erstes der startpunkt
            //des textes festgelegt werden

            //wenn der text kleiner ist als der pfad
            if (TextLength < PathLength)
            {
                double diff = PathLength - TextLength;

                //um den startpunkt zu bekommen muss auf beiden seiten die hälfte der differenz zwischen
                //pfad und text abgezogen werden
                //dies wird nur dann gemacht, wenn der text zentriert angezeigt werden soll
                if (TextAlignment == TextAlignment.Center)
                    progress = diff / 2.0 / PathLength;
            }

            var reallyUseEllipsis = false;
            var fractionOfEllipsis = 0.0;

            if (UseEllipsis)
            {
                var ellipsisLength = 3.0 * _ellipsisText.WidthIncludingTrailingWhitespace;

                if ((TextLength + ellipsisLength) > PathLength)
                {
                    reallyUseEllipsis = true;
                    fractionOfEllipsis = ellipsisLength / PathLength + 0.05;
                    GenerateEllipsis();
                }

            }


            var usingEllipsis = false;

            //für jeden Buchstaben des Textes
            for (int index = 0; index < VisualChildren.Count; index++)
            {
                var formText = FormattedChars[index];

                ////rechne aus, wie groß der Buchstabe ist, wenn er fertig skaliert ist
                //double width = scalingFactor * formText.WidthIncludingTrailingWhitespace;

                ////rechne aus, wie groß die Baseline ist, wenn sie fertig skaliert ist
                //double baseline = scalingFactor * formText.Baseline;



                //determine if we must draw the ellipsis
                if (reallyUseEllipsis && (progress + fractionOfEllipsis) >= 1.0)
                {
                    usingEllipsis = true;
                    VisualChildren.RemoveRange(index, VisualChildren.Count - index);

                    break;
                }

                //verschiebe den progress wert auf den mittelpunkt des aktuellen buchstabens
                progress += formText.WidthIncludingTrailingWhitespace / 2.0 / PathLength;

                //gibt an, an welcher stelle im pfad der Center-Punkt des Buchstabens ist
                //progress += width / 2 / pathLength;

                Point point, tangent;

                //rechnet den konkreten Punkt und seine Tangenbte aus, der an der Stelle 'progress' im Pfad ist
                pathGeometry.GetPointAtFractionLength(progress, out point, out tangent);

                var drawingVisual = VisualChildren[index] as DrawingVisual;
                var transformGroup = drawingVisual.Transform as TransformGroup;
                var scaleTransform = transformGroup.Children[0] as ScaleTransform;
                var rotateTransform = transformGroup.Children[1] as RotateTransform;
                var translateTransform = transformGroup.Children[2] as TranslateTransform;

                //scaleTransform.ScaleX = scalingFactor;
                //scaleTransform.ScaleY = scalingFactor;
                //dreht den buchstaben basierend auf der tangente des aktuellen punkts
                rotateTransform.Angle = Math.Atan2(tangent.Y, tangent.X) * 180.0 / Math.PI;
                rotateTransform.CenterX = formText.WidthIncludingTrailingWhitespace / 2.0;
                rotateTransform.CenterY = formText.Baseline / 2.0 + (formText.Height - formText.Baseline);
                //verschiebt den buchstaben an die stelle des Punktes auf dem Pfad, dann in die Mitte (Width/2.0 und Baseline)

                translateTransform.X = point.X - formText.WidthIncludingTrailingWhitespace / 2.0;

                translateTransform.Y = point.Y;
                translateTransform.Y -= formText.Baseline / 2.0;
                translateTransform.Y -= formText.Height - formText.Baseline;

                var rect = drawingVisual.ContentBounds;
                rect.Transform(transformGroup.Value);
                BoundingRect.Union(rect);

                //verschiebe den 'progress' auf dem pfad um die zweite hälfte des buchstabens nach rechts
                progress += formText.WidthIncludingTrailingWhitespace / 2.0 / PathLength;

            }

            if (usingEllipsis)
            {
                for (var i = 0; i < 3; i++)
                {
                    VisualChildren.Add(_ellipsis[i]);

                    var formText = _ellipsisText;

                    //verschiebe den progress wert auf den mittelpunkt des aktuellen buchstabens
                    progress += formText.WidthIncludingTrailingWhitespace / 2.0 / PathLength;


                    Point point, tangent;

                    //rechnet den konkreten Punkt und seine Tangenbte aus, der an der Stelle 'progress' im Pfad ist
                    pathGeometry.GetPointAtFractionLength(progress, out point, out tangent);

                    var drawingVisual = _ellipsis[i] as DrawingVisual;
                    var transformGroup = drawingVisual.Transform as TransformGroup;
                    var scaleTransform = transformGroup.Children[0] as ScaleTransform;
                    var rotateTransform = transformGroup.Children[1] as RotateTransform;
                    var translateTransform = transformGroup.Children[2] as TranslateTransform;

                    //dreht den buchstaben basierend auf der tangente des aktuellen punkts
                    rotateTransform.Angle = Math.Atan2(tangent.Y, tangent.X) * 180.0 / Math.PI;
                    rotateTransform.CenterX = formText.WidthIncludingTrailingWhitespace / 2.0;
                    rotateTransform.CenterY = formText.Baseline / 2.0 + (formText.Height - formText.Baseline);
                    //verschiebt den buchstaben an die stelle des Punktes auf dem Pfad, dann in die Mitte (Width/2.0 und Baseline)

                    translateTransform.X = point.X - formText.WidthIncludingTrailingWhitespace / 2.0;

                    translateTransform.Y = point.Y;
                    translateTransform.Y -= formText.Baseline / 2.0;
                    translateTransform.Y -= formText.Height - formText.Baseline;

                    var rect = drawingVisual.ContentBounds;
                    rect.Transform(transformGroup.Value);
                    BoundingRect.Union(rect);

                    //verschiebe den 'progress' auf dem pfad um die zweite hälfte des buchstabens nach rechts
                    progress += formText.WidthIncludingTrailingWhitespace / 2.0 / PathLength;

                }
            }

            InvalidateMeasure();
        }

        #endregion

        #region private methods

        private void GenerateEllipsis()
        {
            _ellipsis.Clear();

            for (int i = 0; i < 3; i++)
            {
                var drawingVisual = new DrawingVisual();

                var transformGroup = new TransformGroup();
                transformGroup.Children.Add(new ScaleTransform());
                transformGroup.Children.Add(new RotateTransform());
                transformGroup.Children.Add(new TranslateTransform());
                drawingVisual.Transform = transformGroup;

                var dc = drawingVisual.RenderOpen();
                dc.DrawText(_ellipsisText, new Point(0, 0));
                dc.Close();
                _ellipsis.Add(drawingVisual);
            }
        }

        private void UpdateText()
        {
            PathFigure figure = new PathFigure();
            ArcSegment arcSegment = new ArcSegment();

            Point startPoint;
            Point endPoint;
            Size arcSize;

            double offsetFromWedge = 10.0;

            startPoint = new Point(
                    -Math.Cos((StartAngle + offsetFromWedge) * Math.PI / 180.0) * (Radius),
                    Math.Sin((StartAngle + offsetFromWedge) * Math.PI / 180.0) * (Radius));

            startPoint.Offset(Properties.Settings.Default.WheelRadius, Properties.Settings.Default.WheelRadius);

            endPoint = new Point(
                -Math.Cos((EndAngle - offsetFromWedge) * Math.PI / 180.0) * (Radius),
                Math.Sin((EndAngle - offsetFromWedge) * Math.PI / 180.0) * (Radius));

            endPoint.Offset(Properties.Settings.Default.WheelRadius, Properties.Settings.Default.WheelRadius);

            arcSize = new Size(Radius, Radius);

            figure.StartPoint = startPoint;
            arcSegment.Point = endPoint;
            arcSegment.Size = arcSize;

            figure.Segments.Add(arcSegment);
            PathFigure = figure;
        }

        #endregion

        #region dp

        

        

        #region UseEllipsis

        public bool UseEllipsis
        {
            get { return (bool)GetValue(UseEllipsisProperty); }
            set { SetValue(UseEllipsisProperty, value); }
        }

        // Using a DependencyProperty as the backing store for UseEllipsis.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UseEllipsisProperty =
            DependencyProperty.Register("UseEllipsis", typeof(bool), typeof(TextOnArc), new UIPropertyMetadata(false));

        #endregion

        #region Radius

        /// <summary>
        /// Radius Dependency Property
        /// </summary>
        public static readonly DependencyProperty RadiusProperty =
            DependencyProperty.Register("Radius", typeof(double), typeof(TextOnArc),
                new FrameworkPropertyMetadata((double)0.0,
                    new PropertyChangedCallback(OnRadiusChanged)));

        /// <summary>
        /// Gets or sets the Radius property. This dependency property 
        /// indicates ....
        /// </summary>
        public double Radius
        {
            get { return (double)GetValue(RadiusProperty); }
            set { SetValue(RadiusProperty, value); }
        }

        /// <summary>
        /// Handles changes to the Radius property.
        /// </summary>
        private static void OnRadiusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextOnArc target = (TextOnArc)d;
            double oldRadius = (double)e.OldValue;
            double newRadius = target.Radius;
            target.OnRadiusChanged(oldRadius, newRadius);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Radius property.
        /// </summary>
        protected virtual void OnRadiusChanged(double oldRadius, double newRadius)
        {
            UpdateText();
        }

        #endregion

        #region StartAngle

        /// <summary>
        /// StartAngle Dependency Property
        /// </summary>
        public static readonly DependencyProperty StartAngleProperty =
            DependencyProperty.Register("StartAngle", typeof(double), typeof(TextOnArc),
                new FrameworkPropertyMetadata((double)0.0,
                    new PropertyChangedCallback(OnStartAngleChanged)));

        /// <summary>
        /// Gets or sets the StartAngle property. This dependency property 
        /// indicates ....
        /// </summary>
        public double StartAngle
        {
            get { return (double)GetValue(StartAngleProperty); }
            set { SetValue(StartAngleProperty, value); }
        }

        /// <summary>
        /// Handles changes to the StartAngle property.
        /// </summary>
        private static void OnStartAngleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextOnArc target = (TextOnArc)d;
            double oldStartAngle = (double)e.OldValue;
            double newStartAngle = target.StartAngle;
            target.OnStartAngleChanged(oldStartAngle, newStartAngle);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the StartAngle property.
        /// </summary>
        protected virtual void OnStartAngleChanged(double oldStartAngle, double newStartAngle)
        {
            UpdateText();
        }

        #endregion

        #region EndAngle

        /// <summary>
        /// EndAngle Dependency Property
        /// </summary>
        public static readonly DependencyProperty EndAngleProperty =
            DependencyProperty.Register("EndAngle", typeof(double), typeof(TextOnArc),
                new FrameworkPropertyMetadata((double)0.0,
                    new PropertyChangedCallback(OnEndAngleChanged)));

        /// <summary>
        /// Gets or sets the EndAngle property. This dependency property 
        /// indicates ....
        /// </summary>
        public double EndAngle
        {
            get { return (double)GetValue(EndAngleProperty); }
            set { SetValue(EndAngleProperty, value); }
        }

        /// <summary>
        /// Handles changes to the EndAngle property.
        /// </summary>
        private static void OnEndAngleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextOnArc target = (TextOnArc)d;
            double oldEndAngle = (double)e.OldValue;
            double newEndAngle = target.EndAngle;
            target.OnEndAngleChanged(oldEndAngle, newEndAngle);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the EndAngle property.
        /// </summary>
        protected virtual void OnEndAngleChanged(double oldEndAngle, double newEndAngle)
        {
            UpdateText();
        }

        #endregion

        #endregion
    }
}
