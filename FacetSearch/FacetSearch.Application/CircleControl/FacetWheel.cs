﻿using System;
using System.Linq;
using System.Windows;
using BlendedLibrary.Common.Util;
using BlendedLibrary.FacetSearch.Application.Properties;
using BlendedLibrary.FacetSearch.Application.ViewModel.Node;

namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    public class FacetWheel : CircleSegmentControl
    {
        #region private fields

        private readonly InitializationTrigger _initializationTrigger; 

        #endregion

        #region dependency properties

        #region SelectedFacet

        /// <summary>
        /// SelectedFacet Dependency Property
        /// </summary>
        public static readonly DependencyProperty SelectedFacetProperty = DependencyProperty.Register("SelectedFacet", typeof(string), typeof(FacetWheel), new FrameworkPropertyMetadata(null, OnSelectedFacetChanged));

        /// <summary>
        /// Gets or sets the SelectedFacet property. This dependency property 
        /// indicates ....
        /// </summary>
        public string SelectedFacet
        {
            get { return (string)GetValue(SelectedFacetProperty); }
            set { SetValue(SelectedFacetProperty, value); }
        }

        /// <summary>
        /// Handles changes to the SelectedFacet property.
        /// </summary>
        private static void OnSelectedFacetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var target = (FacetWheel)d;
            var oldSelectedFacet = (string)e.OldValue;
            var newSelectedFacet = target.SelectedFacet;
            target.OnSelectedFacetChanged(oldSelectedFacet, newSelectedFacet);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the SelectedFacet property.
        /// </summary>
        protected virtual void OnSelectedFacetChanged(string oldSelectedFacet, string newSelectedFacet)
        {
            (DataContext as QueryCanvasFacetNodeViewModel).SelectedFacet = newSelectedFacet;
        }

        #endregion

        #region Facets

        /// <summary>
        /// Facets Dependency Property
        /// </summary>
        public static readonly DependencyProperty FacetsProperty = DependencyProperty.Register("Facets", typeof(String[]), typeof(FacetWheel), new FrameworkPropertyMetadata(null, OnFacetsChanged));

        /// <summary>
        /// Gets or sets the Facets property. This dependency property 
        /// indicates ....
        /// </summary>
        public string[] Facets
        {
            get { return (string[])GetValue(FacetsProperty); }
            set { SetValue(FacetsProperty, value); }
        }

        /// <summary>
        /// Handles changes to the Facets property.
        /// </summary>
        private static void OnFacetsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var target = (FacetWheel)d;
            var oldFacets = (string[])e.OldValue;
            var newFacets = target.Facets;
            target.OnFacetsChanged(oldFacets, newFacets);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Facets property.
        /// </summary>
        protected virtual void OnFacetsChanged(string[] oldFacets, string[] newFacets)
        {
            if (newFacets != null && !_initializationTrigger.HasFired)
                _initializationTrigger.SetTrigger("Facets");
        }

        #endregion

        #region DisplayNames

        /// <summary>
        /// DisplayNames Dependency Property
        /// </summary>
        public static readonly DependencyProperty DisplayNamesProperty = DependencyProperty.Register("DisplayNames", typeof(String[]), typeof(FacetWheel), new FrameworkPropertyMetadata(null, OnDisplayNamesChanged));

        /// <summary>
        /// Gets or sets the DisplayNames property. This dependency property 
        /// indicates ....
        /// </summary>
        public string[] DisplayNames
        {
            get { return (string[])GetValue(DisplayNamesProperty); }
            set { SetValue(DisplayNamesProperty, value); }
        }

        /// <summary>
        /// Handles changes to the DisplayNames property.
        /// </summary>
        private static void OnDisplayNamesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var target = (FacetWheel)d;
            var oldDisplayNames = (string[])e.OldValue;
            var newDisplayNames = target.DisplayNames;
            target.OnDisplayNamesChanged(oldDisplayNames, newDisplayNames);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the DisplayNames property.
        /// </summary>
        protected virtual void OnDisplayNamesChanged(string[] oldDisplayNames, string[] newDisplayNames)
        {
            if (newDisplayNames != null && !_initializationTrigger.HasFired)
                _initializationTrigger.SetTrigger("DisplayNames");
        }

        #endregion

        #endregion

        #region ctor

        public FacetWheel()
        {
            AllowMultiSelection = false;

            _initializationTrigger = new InitializationTrigger(InitializeFacetWheel, "Facets", "DisplayNames");
        }

        #endregion

        #region event handling

        void InitializeFacetWheel()
        {
            BuildCircleSegments();

            var vm = DataContext as QueryCanvasFacetNodeViewModel;
            if (!String.IsNullOrEmpty(vm.SelectedFacet))
                SelectSegment(CircleSegments.Single(segment => segment.Tag as String == vm.SelectedFacet));
            //else
            //    SelectSegment(CircleSegments.First());
        }

        #endregion

        #region private methods

        private void BuildCircleSegments()
        {
            var tuples = Facets.Zip(DisplayNames, (facet, name) => new Tuple<String, String>(facet, name));

            var segments = tuples.Select(tuple => new CircleSegment
                                       {
                                           TextOffsetFromRadius = 10.0,
                                           TextOffsetFromWedge = 0.0,
                                           FontSize = Settings.Default.FacetNameFontSize,
                                           TextAlignment = TextAlignment.Left,
                                           Text = tuple.Item2,
                                           Tag = tuple.Item1
                                       });

            AddCircleSegments(segments);
        }

        #endregion

        #region overrides

        protected override void OnSelectionChanged()
        {
            SelectedFacet = SelectedSegments.First().Tag as String;
        }

        protected override string GetSelectedSegmentsLabel()
        {
            return SelectedSegments.First().Text;
        }

        #endregion
    }
}
