﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using BlendedLibrary.Data;
using BlendedLibrary.FacetSearch.Application.ViewModel.Node;
using BlendedLibrary.FacetStreams.Data.Xml;

namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    public class ValueWheelSelector : ContentControl
    {

        private void UpdateValueWheel()
        {
            if (DataProvider != null && !String.IsNullOrEmpty(Facet))
            {
                var dataRepresentation = DataProvider.GetDataRepresentation(Facet);

                if (dataRepresentation == DataRepresentation.Categorical)
                {
                    ValueWheel.ClearSelection();

                    ValueWheel.Values = DataProvider.GetValuesOfCategoricalFacet(Facet);
                    ValueWheel.DisplayNames = ValueWheel.Values.Select(value => DataProvider.GetDisplayNameOfCategoricalValue(Facet, value.Name)).ToArray();
                    ValueWheel.DataContext = DataContext;

                    ValueWheel.InitWheel();

                    Content = ValueWheel;

                    ValueLabel = ValueWheel.SelectedSegmentsLabel;
                    ValueExpression = DataProvider.GetCategoricalExpression(Facet, ValueWheel.SelectedSegments.Select(segment => segment.Tag as IFacetElement).ToArray());
                }
                else if (dataRepresentation == DataRepresentation.Metric)
                {
                    RangeControl.DataContext = DataContext;

                    RangeControl.AttributeName = DataProvider.GetDisplayNameOfFacet(Facet);
                    var range = DataProvider.GetMetricRange(Facet);

                    RangeControl.MinOverallRange = range.MinValue;
                    RangeControl.MaxOverallRange = range.MaxValue;

                    RangeControl.InitRangeControl();

                    Content = RangeControl;
                    ValueLabel = RangeControl.RangeLabel;
                    ValueExpression = DataProvider.GetMetricRangeExpression(Facet, Convert.ToInt32(RangeControl.MinCurrentValue), Convert.ToInt32(RangeControl.MaxCurrentValue));
                }
            }
        }

        private void OnValueWheelSelectionChanged(object sender, EventArgs e)
        {
            //only update the label, if any of the first 35 characters has changed --> performance imporovement
            if(ValueWheel.SelectedSegmentsLabel.Length >= 35 && ValueLabel.Length >= 35)
            {
                if (ValueWheel.SelectedSegmentsLabel.Substring(0, 35) != ValueLabel.Substring(0, 35))
                    ValueLabel = ValueWheel.SelectedSegmentsLabel;
            }
            else
                ValueLabel = ValueWheel.SelectedSegmentsLabel;
            
            ValueExpression = DataProvider.GetCategoricalExpression(Facet, ValueWheel.SelectedSegments.Select(segment => segment.Tag as IFacetElement).ToArray());
        }

        private void OnRangeControlRangeChanged(object sender, EventArgs e)
        {
            ValueLabel = RangeControl.RangeLabel;

            ValueExpression = DataProvider.GetMetricRangeExpression(Facet, Convert.ToInt32(RangeControl.MinCurrentValue), Convert.ToInt32(RangeControl.MaxCurrentValue));
        }

        #region dp

        #region Facet

        /// <summary>
        /// Facet Dependency Property
        /// </summary>
        public static readonly DependencyProperty FacetProperty = DependencyProperty.Register("Facet", typeof(String), typeof(ValueWheelSelector), new FrameworkPropertyMetadata(null, OnFacetChanged));

        /// <summary>
        /// Gets or sets the Facet property. This dependency property 
        /// indicates ....
        /// </summary>
        public String Facet
        {
            get { return (String)GetValue(FacetProperty); }
            set { SetValue(FacetProperty, value); }
        }

        /// <summary>
        /// Handles changes to the Facet property.
        /// </summary>
        private static void OnFacetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ValueWheelSelector target = (ValueWheelSelector)d;
            String oldFacet = (String)e.OldValue;
            String newFacet = target.Facet;
            target.OnFacetChanged(oldFacet, newFacet);
        }

        private bool _loaded;

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Facet property.
        /// </summary>
        protected virtual void OnFacetChanged(String oldFacet, String newFacet)
        {
            if (_loaded)
            {
                //clear all saved values
                var vm = DataContext as QueryCanvasFacetNodeViewModel;
                if (vm != null)
                {
                    vm.MinMetricValue = null;
                    vm.MaxMetricValue = null;
                    vm.SelectedValues = null;
                }
            }

            if (!_loaded)
                _loaded = true;

            if (!String.IsNullOrEmpty(newFacet))
            {
                UpdateValueWheel();
            }
        }

        #endregion

        #region ValueLabel

        /// <summary>
        /// ValueLabel Dependency Property
        /// </summary>
        public static readonly DependencyProperty ValueLabelProperty =
            DependencyProperty.Register("ValueLabel", typeof(String), typeof(ValueWheelSelector),
                new FrameworkPropertyMetadata("Value Label",
                    OnValueLabelChanged));

        /// <summary>
        /// Gets or sets the ValueLabel property. This dependency property 
        /// indicates ....
        /// </summary>
        public String ValueLabel
        {
            get { return (String)GetValue(ValueLabelProperty); }
            set { SetValue(ValueLabelProperty, value); }
        }

        /// <summary>
        /// Handles changes to the ValueLabel property.
        /// </summary>
        private static void OnValueLabelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ValueWheelSelector target = (ValueWheelSelector)d;
            String oldValueLabel = (String)e.OldValue;
            String newValueLabel = target.ValueLabel;
            target.OnValueLabelChanged(oldValueLabel, newValueLabel);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the ValueLabel property.
        /// </summary>
        protected virtual void OnValueLabelChanged(String oldValueLabel, String newValueLabel)
        {
        }

        #endregion

        #region ValueExpression

        /// <summary>
        /// ValueExpression Dependency Property
        /// </summary>
        public static readonly DependencyProperty ValueExpressionProperty =
            DependencyProperty.Register("ValueExpression", typeof(String), typeof(ValueWheelSelector),
                new FrameworkPropertyMetadata((String)null));

        /// <summary>
        /// Gets or sets the ValueExpression property. This dependency property 
        /// indicates ....
        /// </summary>
        public String ValueExpression
        {
            get { return (String)GetValue(ValueExpressionProperty); }
            set { SetValue(ValueExpressionProperty, value); }
        }

        #endregion

        #region DataProvider

        /// <summary>
        /// DataProvider Dependency Property
        /// </summary>
        public static readonly DependencyProperty DataProviderProperty =
            DependencyProperty.Register("DataProvider", typeof(IDataProvider), typeof(ValueWheelSelector),
                new FrameworkPropertyMetadata(null,
                    OnDataProviderChanged));

        /// <summary>
        /// Gets or sets the DataProvider property. This dependency property 
        /// indicates ....
        /// </summary>
        public IDataProvider DataProvider
        {
            get { return (IDataProvider)GetValue(DataProviderProperty); }
            set { SetValue(DataProviderProperty, value); }
        }

        /// <summary>
        /// Handles changes to the DataProvider property.
        /// </summary>
        private static void OnDataProviderChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ValueWheelSelector target = (ValueWheelSelector)d;
            IDataProvider oldDataProvider = (IDataProvider)e.OldValue;
            IDataProvider newDataProvider = target.DataProvider;
            target.OnDataProviderChanged(oldDataProvider, newDataProvider);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the DataProvider property.
        /// </summary>
        protected virtual void OnDataProviderChanged(IDataProvider oldDataProvider, IDataProvider newDataProvider)
        {
            UpdateValueWheel();
        }

        #endregion

        #region ValueWheel

        /// <summary>
        /// ValueWheel Dependency Property
        /// </summary>
        public static readonly DependencyProperty ValueWheelProperty =
            DependencyProperty.Register("ValueWheel", typeof(ValueWheel), typeof(ValueWheelSelector),
                new FrameworkPropertyMetadata(null, OnValueWheelChanged));

        /// <summary>
        /// Gets or sets the ValueWheel property. This dependency property 
        /// indicates ....
        /// </summary>
        public ValueWheel ValueWheel
        {
            get { return (ValueWheel)GetValue(ValueWheelProperty); }
            set { SetValue(ValueWheelProperty, value); }
        }

        private static void OnValueWheelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ValueWheelSelector target = (ValueWheelSelector)d;
            ValueWheel oldValueWheel = (ValueWheel)e.OldValue;
            ValueWheel newValueWheel = target.ValueWheel;
            target.OnValueWheelChanged(oldValueWheel, newValueWheel);
        }

        protected virtual void OnValueWheelChanged(ValueWheel oldValueWheel, ValueWheel newValueWheel)
        {
            DependencyPropertyDescriptor dpd = DependencyPropertyDescriptor.FromProperty(CircleSegmentControl.SelectedSegmentsLabelProperty, typeof (CircleSegmentControl));
            
            if(oldValueWheel != null)
            {
                oldValueWheel.SelectionChanged -= OnValueWheelSelectionChanged;
            }

            if (newValueWheel != null)
            {
                newValueWheel.SelectionChanged += OnValueWheelSelectionChanged;
            }
        }

        #endregion

        #region RangeControl

        /// <summary>
        /// RangeControl Dependency Property
        /// </summary>
        public static readonly DependencyProperty RangeControlProperty =
            DependencyProperty.Register("RangeControl", typeof(RangeControl), typeof(ValueWheelSelector),
                new FrameworkPropertyMetadata(null,
                    OnRangeControlChanged));

        /// <summary>
        /// Gets or sets the RangeControl property. This dependency property 
        /// indicates ....
        /// </summary>
        public RangeControl RangeControl
        {
            get { return (RangeControl)GetValue(RangeControlProperty); }
            set { SetValue(RangeControlProperty, value); }
        }

        /// <summary>
        /// Handles changes to the RangeControl property.
        /// </summary>
        private static void OnRangeControlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ValueWheelSelector target = (ValueWheelSelector)d;
            RangeControl oldRangeControl = (RangeControl)e.OldValue;
            RangeControl newRangeControl = target.RangeControl;
            target.OnRangeControlChanged(oldRangeControl, newRangeControl);
        }
 
        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the RangeControl property.
        /// </summary>
        protected virtual void OnRangeControlChanged(RangeControl oldRangeControl, RangeControl newRangeControl)
        {
            if (oldRangeControl != null)
                oldRangeControl.RangeChanged -= OnRangeControlRangeChanged;

            if (newRangeControl != null)
                newRangeControl.RangeChanged += OnRangeControlRangeChanged;
        }

        #endregion

        #endregion
    }
}