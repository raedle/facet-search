﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using BlendedLibrary.Data;
using BlendedLibrary.FacetSearch.Application.ViewModel.Node;

namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    public class ValueWheel : CircleSegmentControl
    {
        #region overrides

        protected override string GetSelectedSegmentsLabel()
        {
            if (IsAnySegmentSelected)
            {
                var sb = new StringBuilder();
                var segments = SelectedSegments.OrderBy(segment => segment.Text);
                foreach (var segment in segments)
                {
                    sb.Append(segment.Text);
                    if (segment != segments.Last())
                        sb.Append(", ");
                }
                return sb.ToString();
            }
            return "All";
        }

        protected override void OnSelectionChanged()
        {
            (DataContext as QueryCanvasFacetNodeViewModel).SelectedValues = SelectedSegments.Select(segment => segment.Tag as IFacetElement).ToArray();
        }

        #endregion

        #region public methods

        internal void InitWheel()
        {
            if (Values == null || DisplayNames == null)
                return;

            RemoveCircleSegments();

            var tuples = Values.Zip(DisplayNames, (value, name) => new Tuple<IFacetElement, String>(value, name));

            var segments = tuples.Select(tuple => new CircleSegment
            {
                TextOffsetFromRadius = 10.0,
                TextOffsetFromWedge = 0.0,
                FontSize = 14.0,
                TextAlignment = TextAlignment.Left,
                Text = tuple.Item2,
                Tag = tuple.Item1
            });

            AddCircleSegments(segments);

            var vm = DataContext as QueryCanvasFacetNodeViewModel;

            if (vm.SelectedValues != null)
            {
                foreach (var value in vm.SelectedValues.ToArray())
                {
                    var targetSegment = CircleSegments.SingleOrDefault(segment => (segment.Tag as IFacetElement) == value);
                    if (targetSegment != null)
                        SelectSegment(targetSegment);
                }
            }

            SelectedSegmentsLabel = GetSelectedSegmentsLabel();
        }

        #endregion

        #region abstract methods

        protected string GetSegmentValue(CircleSegment segment)
        {
            throw new NotImplementedException();
        }



        protected String GetSegmentExpression(CircleSegment segment)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region dp

        #region Values

        /// <summary>
        /// Values Dependency Property
        /// </summary>
        public static readonly DependencyProperty ValuesProperty =
            DependencyProperty.Register("Values", typeof(IFacetElement[]), typeof(ValueWheel),
                new FrameworkPropertyMetadata((IFacetElement[])null));

        /// <summary>
        /// Gets or sets the Values property. This dependency property 
        /// indicates ....
        /// </summary>
        public IFacetElement[] Values
        {
            get { return (IFacetElement[])GetValue(ValuesProperty); }
            set { SetValue(ValuesProperty, value); }
        }

        #endregion

        #region DisplayNames

        /// <summary>
        /// DisplayNames Dependency Property
        /// </summary>
        public static readonly DependencyProperty DisplayNamesProperty =
            DependencyProperty.Register("DisplayNames", typeof(String[]), typeof(ValueWheel),
                new FrameworkPropertyMetadata((String[])null));

        /// <summary>
        /// Gets or sets the ValueDisplayNames property. This dependency property 
        /// indicates ....
        /// </summary>
        public String[] DisplayNames
        {
            get { return (String[])GetValue(DisplayNamesProperty); }
            set { SetValue(DisplayNamesProperty, value); }
        }

        #endregion

        #endregion
    }
}
