﻿using System;

namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    public class CircleSegmentSelectionChangedEventArgs : EventArgs
    {
        public CircleSegment Segment { get; set; }

        public CircleSegmentSelectionChangedEventArgs(CircleSegment segment)
        {
            Segment = segment;
        }
    }
}
