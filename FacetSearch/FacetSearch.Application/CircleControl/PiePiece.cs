﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    /// <summary>
    /// A pie piece shape
    /// </summary>
    public class PiePiece : Shape
    {
        #region dependency properties

        #region Magnification

        public static readonly DependencyProperty MagnificationProperty =
            DependencyProperty.Register("Magnification", typeof(double), typeof(PiePiece), new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));
        
        public double Magnification
        {
            get { return (double)GetValue(MagnificationProperty); }
            set { SetValue(MagnificationProperty, value); }
        }

        #endregion

        #region Outer Radius

        public static readonly DependencyProperty OuterRadiusProperty =
            DependencyProperty.Register("OuterRadius", typeof(double), typeof(PiePiece),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));

        public double OuterRadius
        {
            get { return (double)GetValue(OuterRadiusProperty); }
            set { SetValue(OuterRadiusProperty, value); }
        }

        #endregion

        #region PushOut
        
        public static readonly DependencyProperty PushOutProperty =
            DependencyProperty.Register("PushOut", typeof(double), typeof(PiePiece),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));

        public double PushOut
        {
            get { return (double)GetValue(PushOutProperty); }
            set { SetValue(PushOutProperty, value); }
        }

        #endregion

        #region Inner Radius

        public static readonly DependencyProperty InnerRadiusProperty =
            DependencyProperty.Register("InnerRadius", typeof(double), typeof(PiePiece),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));

        public double InnerRadius
        {
            get { return (double)GetValue(InnerRadiusProperty); }
            set { SetValue(InnerRadiusProperty, value); }
        }

        #endregion

        #region Wedge Angle

        public static readonly DependencyProperty WedgeAngleProperty =
            DependencyProperty.Register("WedgeAngle", typeof(double), typeof(PiePiece),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));

        public double WedgeAngle
        {
            get { return (double)GetValue(WedgeAngleProperty); }
            set { SetValue(WedgeAngleProperty, value); }
        }

        #endregion

        #region Rotation Angle

        public static readonly DependencyProperty RotationAngleProperty =
            DependencyProperty.Register("RotationAngle", typeof(double), typeof(PiePiece),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));

        /// <summary>
        /// The rotation, in degrees, from the Y axis vector of this pie piece.
        /// </summary>
        public double RotationAngle
        {
            get { return (double)GetValue(RotationAngleProperty); }
            set { SetValue(RotationAngleProperty, value); }
        }

        #endregion

        #endregion

        #region geometry building

        /// <summary>
        /// this one is automatically called from the wpf render mechanism
        /// </summary>
        protected override Geometry DefiningGeometry
        {
            get
            {
                // Create a StreamGeometry for describing the shape
                var geometry = new StreamGeometry {FillRule = FillRule.EvenOdd};

                using (var context = geometry.Open())
                {
                    DrawGeometry(context);
                }

                // Freeze the geometry for performance benefits
                geometry.Freeze();

                return geometry;
            }
        }

        private void DrawGeometry(StreamGeometryContext context)
        {
            var largeArc = WedgeAngle > 180.0;

            if (largeArc)
            {

            }
            else
            {
                //start at the origin
                var outerArcStartPoint = new Point(-OuterRadius * Magnification, 0.0);
                outerArcStartPoint.Offset(OuterRadius, 0.0);

                //the inner arc is translated by the inner radius on the x axis
                var innerArcStartPoint = new Point(-InnerRadius * Magnification, 0.0);
                innerArcStartPoint.Offset(OuterRadius, 0.0);
                
                //the inner arc end point is computed by the Wedge Angle and the inner radius
                var innerArcEndPoint = new Point(-Math.Cos(WedgeAngle * Math.PI / 180.0) * InnerRadius * Magnification, Math.Sin(WedgeAngle * Math.PI / 180.0) * InnerRadius * Magnification); 
                innerArcEndPoint.Offset(OuterRadius, 0.0);
                
                //the outer arc end point is computed by the Wedge Angle and the outer radius
                var outerArcEndPoint = new Point(-Math.Cos(WedgeAngle * Math.PI / 180.0) * OuterRadius * Magnification, Math.Sin(WedgeAngle * Math.PI / 180.0) * OuterRadius * Magnification);
                outerArcEndPoint.Offset(OuterRadius, 0.0);
                
                if (PushOut > 0)
                {
                    var offset = ComputeCartesianCoordinate(0.0 + WedgeAngle / 2.0, PushOut);
                    innerArcStartPoint.Offset(offset.X, offset.Y);
                    innerArcEndPoint.Offset(offset.X, offset.Y);
                    outerArcStartPoint.Offset(offset.X, offset.Y);
                    outerArcEndPoint.Offset(offset.X, offset.Y);
                }

                var outerArcSize = new Size(OuterRadius * Magnification, OuterRadius * Magnification);
                var innerArcSize = new Size(InnerRadius * Magnification, InnerRadius * Magnification);

                context.BeginFigure(outerArcStartPoint, true, true);
                context.LineTo(innerArcStartPoint, true, true);
                context.ArcTo(innerArcEndPoint, innerArcSize, 0, false, SweepDirection.Counterclockwise, true, true);
                context.LineTo(outerArcEndPoint, true, true);
                context.ArcTo(outerArcStartPoint, outerArcSize, 0, false, SweepDirection.Clockwise, true, true);
            }
        }

        public static Point ComputeCartesianCoordinate(double angle, double radius)
        {
            // convert to radians
            var angleRad = (Math.PI / 180.0) * (angle);

            var x = radius * (-1) * Math.Cos(angleRad);
            var y = radius * Math.Sin(angleRad);

            return new Point(x, y);
        }

        #endregion
    }

}
