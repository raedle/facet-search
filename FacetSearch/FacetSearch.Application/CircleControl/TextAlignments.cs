﻿namespace BlendedLibrary.FacetSearch.Application.CircleControl
{
    public enum TextAlignments
    {
        Left,
        Center,
        Right
    }
}
