﻿namespace BlendedLibrary.FacetSearch.Application.Windows
{
    public partial class MainSurfaceWindow : IMainWindow
    {
        public MainSurfaceWindow()
        {
            InitializeComponent();
//#if DEBUG
//            WindowState = System.Windows.WindowState.Normal;
//            WindowStyle = System.Windows.WindowStyle.SingleBorderWindow;
//#else
            WindowState = System.Windows.WindowState.Maximized;
            WindowStyle = System.Windows.WindowStyle.None;
//#endif
            this.Loaded += new System.Windows.RoutedEventHandler(MainSurfaceWindow_Loaded);
        }

        void MainSurfaceWindow_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            
        }

        public MainPanel MainPanel
        {
            get { return Main; }
        }
    }
}
