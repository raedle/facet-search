﻿using System.Windows;

namespace BlendedLibrary.FacetSearch.Application.Windows
{
    public partial class MainWindow : IMainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            //#if DEBUG
            //            WindowState = System.Windows.WindowState.Normal;
            //            WindowStyle = System.Windows.WindowStyle.SingleBorderWindow;
            //#else
            WindowState = WindowState.Maximized;
            WindowStyle = WindowStyle.None;
            //#endif
        }

        public MainPanel MainPanel
        {
            get { return Main; }
        }
    }
}
