﻿using System;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Threading;
using BlendedLibrary.Common;
using BlendedLibrary.Common.Model;
using BlendedLibrary.Common.Util;
using BlendedLibrary.FacetSearch.Application.Properties;
using BlendedLibrary.FacetSearch.Application.ViewModel.Link;
using BlendedLibrary.FacetSearch.Model;

namespace BlendedLibrary.FacetSearch.Application.Behaviors
{
    public class ReleaseLinkBehavior : Behavior<FrameworkElement>
    {
        private InitializationTrigger _initializationTrigger;

        private IObservable<TouchEventArgs> _contactDownEvent;
        private IObservable<TouchEventArgs> _contactUpEvent;

        public ReleaseLinkBehavior()
        {
            _initializationTrigger = new InitializationTrigger(InitializeEvents, "Delay", "Attached");   
        }

        private void InitializeEvents()
        {
            _contactDownEvent = Observable.FromEventPattern<EventHandler<TouchEventArgs>, TouchEventArgs>(handler => AssociatedObject.TouchDown += handler, handler => AssociatedObject.TouchDown -= handler).Select(evt => evt.EventArgs).Where(e => e.TouchDevice.IsFingerRecognized());
            _contactUpEvent = Observable.FromEventPattern<EventHandler<TouchEventArgs>, TouchEventArgs>(handler => AssociatedObject.TouchUp += handler, handler => AssociatedObject.TouchUp -= handler).Select(evt => evt.EventArgs).Where(e => e.TouchDevice.IsFingerRecognized());

            var trigger = _contactDownEvent.Do(OnContactDown).Delay(TimeSpan.FromMilliseconds(Delay)).TakeUntil(_contactUpEvent.Do(OnContactUp));

//            DispatcherObservable.ObserveOnDispatcher(Observable.Defer(() => trigger).Repeat()).Subscribe(OnDelayElapsed);
            //TODO: Warum findet ReSharper ObserveOnDispatcher nicht
            Observable.Defer(() => trigger).Repeat().ObserveOnDispatcher().Subscribe(OnDelayElapsed);
        }

        private void OnContactDown(TouchEventArgs e)
        {
            e.TouchDevice.Capture(AssociatedObject);
            e.Handled = true;
        }

        private void OnContactUp(TouchEventArgs e)
        {
            e.TouchDevice.Capture(null);
            e.Handled = true;

        }

        private void OnDelayElapsed(TouchEventArgs e)
        {
            var vm = (AssociatedObject.DataContext as QueryCanvasLinkViewModel);
            var source = vm.Source;


            Vector diff = new Point(vm.Source.Center.X, vm.Source.Center.Y) - new Point(vm.Destination.Center.X, vm.Destination.Center.Y);

            double alpha = Math.Atan2(diff.Y, diff.X) + Math.PI;

            var s = Settings.Default;
            var destinationX = vm.Source.Center.X + Math.Cos(alpha) * s.DisplayWidth;
            var destinationY = vm.Source.Center.Y + Math.Sin(alpha) * s.DisplayWidth;

            var exitLinkDestinationPosition = new Position(destinationX, destinationY);

            vm.TurnIntoExitLink(exitLinkDestinationPosition);

            //we have to wait for the DataBinding of the newly created Exit Link to succeed
            DispatcherHelper.WaitForPriority(DispatcherPriority.Render);

            var exitLink = source.CanvasViewModel.GetViewModel((source.Model as QueryCanvasNode).ExitLink) as QueryCanvasExitLinkViewModel;
            exitLink.ContactDownTrigger.OnNext(e);
        }

        protected override void OnAttached()
        {
            if (!_initializationTrigger.HasFired)
            {
                _initializationTrigger.SetTrigger("Attached");
            }
        }




        #region Delay

        /// <summary>
        /// Delay Dependency Property
        /// </summary>
        public static readonly DependencyProperty DelayProperty =
            DependencyProperty.Register("Delay", typeof(double), typeof(ReleaseLinkBehavior),
                new FrameworkPropertyMetadata((double)0.0,
                    new PropertyChangedCallback(OnDelayChanged)));

        /// <summary>
        /// Gets or sets the Delay property. This dependency property 
        /// indicates ....
        /// </summary>
        public double Delay
        {
            get { return (double)GetValue(DelayProperty); }
            set { SetValue(DelayProperty, value); }
        }

        /// <summary>
        /// Handles changes to the Delay property.
        /// </summary>
        private static void OnDelayChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ReleaseLinkBehavior target = (ReleaseLinkBehavior)d;
            double oldDelay = (double)e.OldValue;
            double newDelay = target.Delay;
            target.OnDelayChanged(oldDelay, newDelay);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Delay property.
        /// </summary>
        protected virtual void OnDelayChanged(double oldDelay, double newDelay)
        {
            if (!_initializationTrigger.HasFired)
                _initializationTrigger.SetTrigger("Delay");
        }

        #endregion


    }
}
