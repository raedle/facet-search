﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Shapes;
using BlendedLibrary.Common;
using BlendedLibrary.Common.Model;
using BlendedLibrary.Common.Util;
using BlendedLibrary.FacetSearch.Application.Properties;
using BlendedLibrary.FacetSearch.Application.View.Link;
using BlendedLibrary.FacetSearch.Application.ViewModel;
using BlendedLibrary.FacetSearch.Application.ViewModel.Link;
using BlendedLibrary.FacetSearch.Application.ViewModel.Node;
using BlendedLibrary.FacetSearch.Model;

namespace BlendedLibrary.FacetSearch.Application.Behaviors
{
    internal class RadialDragBehavior : Behavior<FrameworkElement>
    {
        /// Radial Drag Behavior to allow dragging round a center point
        /// handles hit testing (to detect possible connection targets) and throws respective events (to highlight the target and make the link smaller)
        /// handles offsets or timeouts (to delay link releasing)

        private IObservable<TouchEventArgs> _contactDownEvent;

        private Subject<TouchEventArgs> _contactDownEventProxy;

        private IObservable<TouchEventArgs> _contactChangedEvent;

        private IObservable<TouchEventArgs> _contactUpEvent;

        private InitializationTrigger _initializationTrigger;

        private Canvas _parentCanvas;

        private QueryCanvasItemViewModel _lastConnectionTarget;
        private Position _lastConnectionPoint;

        private double _windowWidth;
        private double _windowHeight;

        protected override void OnAttached()
        {
            _initializationTrigger = new InitializationTrigger(InitEvents, "Source", "Destination", "ExternalTrigger");
        }

        private void InitEvents()
        {
            _parentCanvas = TreeHelper.TryFindParent<Canvas>(AssociatedObject);

            var window = Window.GetWindow(AssociatedObject);
            _windowWidth = window.ActualWidth;
            _windowHeight = window.ActualHeight;

            _contactDownEventProxy = new Subject<TouchEventArgs>();

            _contactChangedEvent = Observable.FromEventPattern<EventHandler<TouchEventArgs>, TouchEventArgs>(handler => AssociatedObject.TouchMove += handler, handler => AssociatedObject.TouchMove -= handler).Select(evt => evt.EventArgs).Where(e => e.TouchDevice.IsFingerRecognized());
            _contactDownEvent = Observable.FromEventPattern<EventHandler<TouchEventArgs>, TouchEventArgs>(handler => AssociatedObject.TouchDown += handler, handler => AssociatedObject.TouchDown -= handler).Select(evt => evt.EventArgs).Where(e => e.TouchDevice.IsFingerRecognized());
            _contactUpEvent = Observable.FromEventPattern<EventHandler<TouchEventArgs>, TouchEventArgs>(handler => AssociatedObject.TouchUp += handler, handler => AssociatedObject.TouchUp -= handler).Select(evt => evt.EventArgs).Where(e => e.TouchDevice.IsFingerRecognized());

            //Route external contact down events and internal contact down events to the proxy
            _contactDownEvent.Subscribe(e => _contactDownEventProxy.OnNext(e));
            ExternalTrigger.Subscribe(e => _contactDownEventProxy.OnNext(e));

            var radialDrag = from contactDown in _contactDownEventProxy.Do(OnContactDown)
                             from contactChanged in _contactChangedEvent.TakeUntil(_contactUpEvent.Do(OnContactUp))
                             select contactChanged;

            radialDrag.Subscribe(OnRadialDrag);

            //var mouseDrag = from mlbd in mouseLeftButtonDown.Select(evt => new Selection(evt.EventArgs.GetPosition(null))).Do(s => { StartSelection(s); Mouse.Capture(AssociatedObject); })
            //                from md in mouseMoves.Do(point => mlbd.Points.Add(point)).TakeUntil(mouseLeftButtonUp.Do((evt) => mlbd.Points.Add(evt.EventArgs.GetPosition(null))).Do((evt) => { FinishSelection(mlbd); AssociatedObject.ReleaseMouseCapture(); }))
            //                select mlbd;

            //return mouseDrag.Subscribe(ChangeSelection);

        }

        private void OnContactDown(TouchEventArgs e)
        {
            e.Handled = true;
            e.TouchDevice.Capture(AssociatedObject);
            RepositionDestination(e.TouchDevice);
            PerformHitTesting();
        }

        //private Path _debugPath;

        private Path _debugEllipse;

        private void PerformHitTesting()
        {
            if (AssociatedObject is QueryCanvasExitLinkView)
            {
                var geometry = HitTestGeometry.GetWidenedPathGeometry(new Pen(Brushes.Transparent, 10));

                //if(_debugPath == null)
                //{
                //    _debugPath = new Path();
                //    _debugPath.Fill = Brushes.LightBlue;

                //    TreeHelper.TryFindParent<QueryCanvasView>(AssociatedObject).DebuggingCanvas.Children.Add(_debugPath);
                //}

                //_debugPath.Data = geometry;

                //if (_debugEllipse == null)
                //{
                //    _debugEllipse = new Path();
                //    _debugEllipse.Fill = Brushes.Lime;
                //    TreeHelper.TryFindParent<QueryCanvasView>(AssociatedObject).DebuggingCanvas.Children.Add(_debugEllipse);
                //}

                var hitTestResults = HitTestHelper.GetElementsInGeometry<UserControl>(geometry, _parentCanvas).ToArray();

                var newConnectionTarget = GetConnectionTarget(hitTestResults);

                //if we have found a new connection target
                if (newConnectionTarget != null)
                {
                    //and it is the same as the old, we do nothing
                    if (newConnectionTarget == _lastConnectionTarget)
                        return;

                    //we have to decrease the count of the old target first
                    if (_lastConnectionTarget != null)
                        _lastConnectionTarget.ConnectionTargetCount--;

                    //and then increase the count of the new target
                    newConnectionTarget.ConnectionTargetCount++;
                    _lastConnectionTarget = newConnectionTarget;
                }
                else
                {
                    //if we didn't find any connection target, we have to decrease the count of the old target and release it
                    if (_lastConnectionTarget != null)
                    {
                        _lastConnectionTarget.ConnectionTargetCount--;
                        _lastConnectionTarget = null;
                    }
                }
            }
        }

        private bool IsInWindow(Point p)
        {
            if (p.X < 0 || p.Y < 0 || p.X > _windowWidth || p.Y > _windowHeight)
                return false;
            return true;
        }

        private QueryCanvasItemViewModel GetConnectionTarget(UserControl[] hitTestResults)
        {
            //1. HitTest against all Elements in the Canvas
            //2. Get ViewModels of all found Elements
            //3. Throw away the Source Node and the Ingoing and Outgoing Links of the source node
            //4. Calculate the Nearest Intersection Point of every Element
            //5. Order By smallest Distance to Intersection Point

            var connectionCandidates = hitTestResults
                .Select(view => view.DataContext as QueryCanvasItemViewModel)
                .Except(new QueryCanvasItemViewModel[] { Source })
                .Except(Source.GetIngoingLinks())
                .Except(Source.GetOutgoingLinks())
                .Select(vm => new { ViewModel = vm, NearestPoint = vm.GetNearestIntersectionPoint(HitTestGeometry as LineGeometry) })
                .Where(anonym => anonym.NearestPoint != null)
                .Where(anonym => IsInWindow(anonym.NearestPoint.Value))
                .Where(anonym =>
                    {
                        var vm = anonym.ViewModel as QueryCanvasLinkViewModel;
                        if (vm != null)
                            return false;
                        return true;
                    })
                .OrderBy(anonym => Point.Subtract(anonym.NearestPoint.Value, new Point(Source.Center.X, Source.Center.Y)).Length)
                .ToArray();

            //if we have found any possible target
            if (connectionCandidates.Length > 0)
            {
                //we have to check if it is valid to connect to this target (cycle check !)
                var newConnectionTarget = connectionCandidates.First();

                if ((AssociatedObject.DataContext as QueryCanvasExitLinkViewModel).IsValidConnectCandidate(newConnectionTarget.ViewModel))
                {
                    _lastConnectionPoint = new Position(newConnectionTarget.NearestPoint.Value.X, newConnectionTarget.NearestPoint.Value.Y);
                    return newConnectionTarget.ViewModel;
                }
            }
            return null;
        }



        private void OnContactUp(TouchEventArgs e)
        {
            e.TouchDevice.Capture(null);

            if (_lastConnectionTarget != null)
            {
                var vm = AssociatedObject.DataContext as QueryCanvasExitLinkViewModel;
                vm.EstablishConnection(_lastConnectionTarget, _lastConnectionPoint);
                _lastConnectionTarget.ConnectionTargetCount--;
                _lastConnectionTarget = null;
            }

        }

        private void OnRadialDrag(TouchEventArgs e)
        {
            RepositionDestination(e.TouchDevice);
            PerformHitTesting();
        }

        private void RepositionDestination(TouchDevice contact)
        {
            if (AssociatedObject is QueryCanvasExitLinkView)
            {
                Vector diff = new Point(Source.Center.X, Source.Center.Y) - contact.GetPosition(null);

                double alpha = Math.Atan2(diff.Y, diff.X) + Math.PI;

                var s = Settings.Default;
                var destinationX = Source.Center.X + Math.Cos(alpha) * s.DisplayWidth;
                var destinationY = Source.Center.Y + Math.Sin(alpha) * s.DisplayWidth;
                Destination.UpdateCenterPosition(destinationX, destinationY);
                return;
            }
        }

        #region Source

        /// <summary>
        /// Source Dependency Property
        /// </summary>
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(QueryCanvasNodeViewModel), typeof(RadialDragBehavior),
                new FrameworkPropertyMetadata(null,
                    new PropertyChangedCallback(OnSourceChanged)));

        /// <summary>
        /// Gets or sets the Source property. This dependency property 
        /// indicates ....
        /// </summary>
        public QueryCanvasNodeViewModel Source
        {
            get { return (QueryCanvasNodeViewModel)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        /// <summary>
        /// Handles changes to the Source property.
        /// </summary>
        private static void OnSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RadialDragBehavior target = (RadialDragBehavior)d;
            QueryCanvasNodeViewModel oldSource = (QueryCanvasNodeViewModel)e.OldValue;
            QueryCanvasNodeViewModel newSource = target.Source;
            target.OnSourceChanged(oldSource, newSource);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Source property.
        /// </summary>
        protected virtual void OnSourceChanged(QueryCanvasNodeViewModel oldSource, QueryCanvasNodeViewModel newSource)
        {
            if (newSource != null && !_initializationTrigger.HasFired)
                _initializationTrigger.SetTrigger("Source");
        }

        #endregion

        #region Destination

        /// <summary>
        /// Destination Dependency Property
        /// </summary>
        public static readonly DependencyProperty DestinationProperty =
            DependencyProperty.Register("Destination", typeof(QueryCanvasNodeViewModel), typeof(RadialDragBehavior),
                new FrameworkPropertyMetadata(null,
                    new PropertyChangedCallback(OnDestinationChanged)));

        /// <summary>
        /// Gets or sets the Destination property. This dependency property 
        /// indicates ....
        /// </summary>
        public QueryCanvasNodeViewModel Destination
        {
            get { return (QueryCanvasNodeViewModel)GetValue(DestinationProperty); }
            set { SetValue(DestinationProperty, value); }
        }

        /// <summary>
        /// Handles changes to the Destination property.
        /// </summary>
        private static void OnDestinationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RadialDragBehavior target = (RadialDragBehavior)d;
            QueryCanvasNodeViewModel oldDestination = (QueryCanvasNodeViewModel)e.OldValue;
            QueryCanvasNodeViewModel newDestination = target.Destination;
            target.OnDestinationChanged(oldDestination, newDestination);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Destination property.
        /// </summary>
        protected virtual void OnDestinationChanged(QueryCanvasNodeViewModel oldDestination, QueryCanvasNodeViewModel newDestination)
        {
            if (newDestination != null && !_initializationTrigger.HasFired)
                _initializationTrigger.SetTrigger("Destination");
        }

        #endregion

        #region ExternalTrigger

        /// <summary>
        /// ExternalTrigger Dependency Property
        /// </summary>
        public static readonly DependencyProperty ExternalTriggerProperty =
            DependencyProperty.Register("ExternalTrigger", typeof(IObservable<TouchEventArgs>), typeof(RadialDragBehavior),
                new FrameworkPropertyMetadata((IObservable<TouchEventArgs>)null,
                    new PropertyChangedCallback(OnExternalTriggerChanged)));

        /// <summary>
        /// Gets or sets the ExternalTrigger property. This dependency property 
        /// indicates ....
        /// </summary>
        public IObservable<TouchEventArgs> ExternalTrigger
        {
            get { return (IObservable<TouchEventArgs>)GetValue(ExternalTriggerProperty); }
            set { SetValue(ExternalTriggerProperty, value); }
        }

        /// <summary>
        /// Handles changes to the ExternalTrigger property.
        /// </summary>
        private static void OnExternalTriggerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RadialDragBehavior target = (RadialDragBehavior)d;
            IObservable<TouchEventArgs> oldExternalTrigger = (IObservable<TouchEventArgs>)e.OldValue;
            IObservable<TouchEventArgs> newExternalTrigger = target.ExternalTrigger;
            target.OnExternalTriggerChanged(oldExternalTrigger, newExternalTrigger);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the ExternalTrigger property.
        /// </summary>
        protected virtual void OnExternalTriggerChanged(IObservable<TouchEventArgs> oldExternalTrigger, IObservable<TouchEventArgs> newExternalTrigger)
        {
            if (newExternalTrigger != null && !_initializationTrigger.HasFired)
                _initializationTrigger.SetTrigger("ExternalTrigger");
        }

        #endregion

        #region HitTestGeometry

        /// <summary>
        /// HitTestGeometry Dependency Property
        /// </summary>
        public static readonly DependencyProperty HitTestGeometryProperty =
            DependencyProperty.Register("HitTestGeometry", typeof(Geometry), typeof(RadialDragBehavior),
                new FrameworkPropertyMetadata((Geometry)null,
                    new PropertyChangedCallback(OnHitTestGeometryChanged)));

        /// <summary>
        /// Gets or sets the HitTestGeometry property. This dependency property 
        /// indicates ....
        /// </summary>
        public Geometry HitTestGeometry
        {
            get { return (Geometry)GetValue(HitTestGeometryProperty); }
            set { SetValue(HitTestGeometryProperty, value); }
        }

        /// <summary>
        /// Handles changes to the HitTestGeometry property.
        /// </summary>
        private static void OnHitTestGeometryChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RadialDragBehavior target = (RadialDragBehavior)d;
            Geometry oldHitTestGeometry = (Geometry)e.OldValue;
            Geometry newHitTestGeometry = target.HitTestGeometry;
            target.OnHitTestGeometryChanged(oldHitTestGeometry, newHitTestGeometry);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the HitTestGeometry property.
        /// </summary>
        protected virtual void OnHitTestGeometryChanged(Geometry oldHitTestGeometry, Geometry newHitTestGeometry)
        {
        }

        #endregion

    }
}
