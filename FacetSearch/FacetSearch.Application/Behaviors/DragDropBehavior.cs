﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Shapes;
using BlendedLibrary.Common;
using BlendedLibrary.Common.Model;
using BlendedLibrary.FacetSearch.Application.ViewModel;

namespace BlendedLibrary.FacetSearch.Application.Behaviors
{
    public class DragDropBehavior : Behavior<FrameworkElement>
    {
        public static readonly DependencyProperty AffectedObjectProperty = DependencyProperty.Register("AffectedObject", typeof(FrameworkElement), typeof(DragDropBehavior), new PropertyMetadata(default(FrameworkElement)));

        #region private fields

        private Window _window;
        private Path _debugShape;
        //private VisualDebuggingCanvas _canvas;

        private bool _isMoving = false;
        private int _id = -1;

        #endregion

        #region properties

        #region IsEnabled

        public bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsEnabled.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsEnabledProperty =
            DependencyProperty.Register("IsEnabled", typeof(bool), typeof(DragDropBehavior), new PropertyMetadata(true));

        #endregion

        #region IsRotationEnabled

        public bool IsRotationEnabled
        {
            get { return (bool)GetValue(IsRotationEnabledProperty); }
            set { SetValue(IsRotationEnabledProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsRotationEnabled.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsRotationEnabledProperty =
            DependencyProperty.Register("IsRotationEnabled", typeof(bool), typeof(DragDropBehavior), new PropertyMetadata(true));

        #endregion

        #region RotationOffset

        private int _rotationOffset = 0;

        public int RotationOffset
        {
            get { return _rotationOffset; }
            set { _rotationOffset = value; }
        }

        public FrameworkElement AffectedObject
        {
            get { return (FrameworkElement) GetValue(AffectedObjectProperty); }
            set { SetValue(AffectedObjectProperty, value); }
        }

        #endregion

        #endregion

        #region overrides

        protected override void OnAttached()
        {
            if (!IsEnabled) return;

            base.OnAttached();

            //AssociatedObject.MouseDown += OnInputDown;
            //AssociatedObject.MouseUp += OnInputUp;

            AssociatedObject.TouchDown += OnInputDown;
            AssociatedObject.TouchUp += OnInputUp;

            AssociatedObject.ManipulationStarting += OnManipulationStarting;
            AssociatedObject.ManipulationStarted += OnManipulationStarted;
            AssociatedObject.ManipulationDelta += OnManipulationDelta;
            AssociatedObject.ManipulationCompleted += OnManipulationCompleted;

            AssociatedObject.IsManipulationEnabled = true;
        }

        protected override void OnDetaching()
        {
            if (!IsEnabled) return;

            //AssociatedObject.MouseDown -= OnInputDown;
            //AssociatedObject.MouseUp -= OnInputUp;

            AssociatedObject.TouchDown -= OnInputDown;
            AssociatedObject.TouchUp -= OnInputUp;

            AssociatedObject.ManipulationStarting -= OnManipulationStarting;
            AssociatedObject.ManipulationStarted -= OnManipulationStarted;
            AssociatedObject.ManipulationDelta -= OnManipulationDelta;
            AssociatedObject.ManipulationCompleted -= OnManipulationCompleted;

            base.OnDetaching();
        }

        #endregion

        #region event handling

        private void OnInputDown(object sender, TouchEventArgs e)
        {
            if (!e.TouchDevice.IsFingerRecognized())
                return;

            _window = Window.GetWindow(AssociatedObject);

            e.Handled = true;

            e.Device.Capture(AssociatedObject, CaptureMode.SubTree);
            Manipulation.AddManipulator(AssociatedObject, e.Device.AsManipulator());
        }

        private void OnInputUp(object sender, TouchEventArgs e)
        {
            if (!e.TouchDevice.IsFingerRecognized())
                return;

            if (Manipulation.IsManipulationActive(AssociatedObject))
            {
                Manipulation.CompleteManipulation(AssociatedObject);

                e.Handled = true;
            }
        }

        private void OnManipulationStarting(object sender, ManipulationStartingEventArgs e)
        {
            e.Handled = true;
        }

        private void OnManipulationStarted(object sender, ManipulationStartedEventArgs e)
        {
            Manipulation.SetManipulationContainer(AssociatedObject, _window);
            Manipulation.SetManipulationMode(AssociatedObject,
                IsRotationEnabled ?
                ManipulationModes.TranslateX |
                ManipulationModes.TranslateY |
                ManipulationModes.Rotate
                :
                ManipulationModes.TranslateX |
                ManipulationModes.TranslateY);
        }

        private bool _firstCallToManipulationDelta;
        
        private void OnManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            if (!_firstCallToManipulationDelta)
            {
                _firstCallToManipulationDelta = true;
                return;
            }

            if (e.DeltaManipulation.Translation.Length > 1.0 || e.DeltaManipulation.Rotation > 0)
                _isMoving = true;

            if (AffectedObject == null) return;

            var vm = AffectedObject.DataContext as IPosition;

            var oldX = Canvas.GetLeft(AffectedObject);
            var oldY = Canvas.GetTop(AffectedObject);

            if (vm != null)
            {
                oldX = vm.TopLeft.X;
                oldY = vm.TopLeft.Y;
            }

            var newX = oldX;
            if (!Double.IsInfinity(e.DeltaManipulation.Translation.X))
                newX += e.DeltaManipulation.Translation.X;

            var newY = oldY;
            if (!Double.IsInfinity(e.DeltaManipulation.Translation.Y))
                newY += e.DeltaManipulation.Translation.Y;

            var angleViewModel = AssociatedObject.DataContext as IAngle;
            if (angleViewModel != null)
            {
                angleViewModel.Angle += e.DeltaManipulation.Rotation;// + Settings.Default.RotationOffset;
            }

            var positionViewModel = AssociatedObject.DataContext as IPosition;
            if (positionViewModel != null)
            {
                positionViewModel.UpdateTopLeftPosition(newX, newY);
            }
        }

        private void OnManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            _isMoving = false;

            _firstCallToManipulationDelta = false;
            //_canvas.Children.Remove(_debugShape);   
        }

        #endregion
    }
}
