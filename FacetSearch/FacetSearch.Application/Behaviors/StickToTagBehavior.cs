﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using BlendedLibrary.Common;
using BlendedLibrary.Common.Model;
using BlendedLibrary.Common.Util;
using BlendedLibrary.FacetSearch.Application.Properties;
using BlendedLibrary.FacetSearch.Application.ViewModel;

namespace BlendedLibrary.FacetSearch.Application.Behaviors
{
    public class StickToTagBehavior : Behavior<FrameworkElement>
    {
        #region private fields

        private IObservable<EventPattern<TouchEventArgs>> _contactChangedEvent;
        private IDisposable _contactChangedSubscription;

        private bool _loaded = true;

        private Canvas _parentCanvas;

        private UIElement _container;

        #endregion

        #region overrides

        protected override void OnAttached()
        {
            if (TagValue.HasValue)
            {
                Activate();
            }
        }

        protected override void OnDetaching()
        {
            _contactChangedSubscription.Dispose();
        }

        #endregion

        #region dp

        #region TagValue

        /// <summary>
        /// TagValue Dependency Property
        /// </summary>
        public static readonly DependencyProperty TagValueProperty =
            DependencyProperty.Register("TagValue", typeof(int?), typeof(StickToTagBehavior),
                new FrameworkPropertyMetadata(null,
                    new PropertyChangedCallback(OnTagValueChanged)));

        /// <summary>
        /// Gets or sets the TagValue property. This dependency property 
        /// indicates ....
        /// </summary>
        public int? TagValue
        {
            get { return (int?)GetValue(TagValueProperty); }
            set { SetValue(TagValueProperty, value); }
        }

        /// <summary>
        /// Handles changes to the TagValue property.
        /// </summary>
        private static void OnTagValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var target = (StickToTagBehavior)d;
            var oldTagValue = (int?)e.OldValue;
            var newTagValue = target.TagValue;
            target.OnTagValueChanged(oldTagValue, newTagValue);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the TagValue property.
        /// </summary>
        protected virtual void OnTagValueChanged(int? oldTagValue, int? newTagValue)
        {
            if (newTagValue.HasValue && AssociatedObject != null)
            {
                Activate();
            }
        }

        #endregion

        #region OffsetX

        /// <summary>
        /// OffsetX Dependency Property
        /// </summary>
        public static readonly DependencyProperty OffsetXProperty =
            DependencyProperty.Register("OffsetX", typeof(double), typeof(StickToTagBehavior),
                new FrameworkPropertyMetadata((double)0.0,
                    new PropertyChangedCallback(OnOffsetXChanged)));

        /// <summary>
        /// Gets or sets the OffsetX property. This dependency property 
        /// indicates ....
        /// </summary>
        public double OffsetX
        {
            get { return (double)GetValue(OffsetXProperty); }
            set { SetValue(OffsetXProperty, value); }
        }

        /// <summary>
        /// Handles changes to the OffsetX property.
        /// </summary>
        private static void OnOffsetXChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            StickToTagBehavior target = (StickToTagBehavior)d;
            double oldOffsetX = (double)e.OldValue;
            double newOffsetX = target.OffsetX;
            target.OnOffsetXChanged(oldOffsetX, newOffsetX);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the OffsetX property.
        /// </summary>
        protected virtual void OnOffsetXChanged(double oldOffsetX, double newOffsetX)
        {
        }

        #endregion

        #region OffsetY

        /// <summary>
        /// OffsetY Dependency Property
        /// </summary>
        public static readonly DependencyProperty OffsetYProperty =
            DependencyProperty.Register("OffsetY", typeof(double), typeof(StickToTagBehavior),
                new FrameworkPropertyMetadata((double)0.0,
                    new PropertyChangedCallback(OnOffsetYChanged)));

        /// <summary>
        /// Gets or sets the OffsetY property. This dependency property 
        /// indicates ....
        /// </summary>
        public double OffsetY
        {
            get { return (double)GetValue(OffsetYProperty); }
            set { SetValue(OffsetYProperty, value); }
        }

        /// <summary>
        /// Handles changes to the OffsetY property.
        /// </summary>
        private static void OnOffsetYChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            StickToTagBehavior target = (StickToTagBehavior)d;
            double oldOffsetY = (double)e.OldValue;
            double newOffsetY = target.OffsetY;
            target.OnOffsetYChanged(oldOffsetY, newOffsetY);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the OffsetY property.
        /// </summary>
        protected virtual void OnOffsetYChanged(double oldOffsetY, double newOffsetY)
        {
        }

        #endregion

        #endregion

        #region private methods

        private void Activate()
        {
            _parentCanvas = TreeHelper.TryFindParent<Canvas>(AssociatedObject);

            var itemsControl = TreeHelper.TryFindParent<ItemsControl>(AssociatedObject);

            if (itemsControl != null)
            {
                _container = itemsControl.ContainerFromElement(AssociatedObject) as UIElement;
            }

            _loaded = true;

            if (_parentCanvas != null)
            {
                _contactChangedEvent = Observable.FromEventPattern<EventHandler<TouchEventArgs>, TouchEventArgs>(handler => _parentCanvas.TouchMove += handler, handler => _parentCanvas.TouchMove -= handler);
                _contactChangedSubscription = _contactChangedEvent.Select(evt => evt.EventArgs).Where(args =>
                    (args.TouchDevice.IsTagRecognized() && args.TouchDevice.GetTagData().Value == TagValue)
                    ).Subscribe(OnContactChanged);

                AssociatedObject.Unloaded += OnUnloaded;
                AssociatedObject.Loaded += OnLoaded;
            }
        }

        #endregion

        #region event handler

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (!_loaded)
                Activate();
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            _loaded = false;
            _contactChangedSubscription.Dispose();
        }

        private void OnContactChanged(TouchEventArgs args)
        {
            Point position = args.TouchDevice.GetPosition(_parentCanvas);

            var x = position.X + OffsetX;
            var y = position.Y + OffsetY;

            //if (_container != null)
            //{
            //    Canvas.SetLeft(_container, x);
            //    Canvas.SetTop(_container, y);
            //}
            //else
            //{
            //    Canvas.SetLeft(AssociatedObject, x);
            //    Canvas.SetTop(AssociatedObject, y);
            //}

            var angleViewModel = AssociatedObject.DataContext as IAngle;
            if (angleViewModel != null)
            {

                var oldAngle = angleViewModel.Angle;

                //var oldAngleTranslated = TranslateOrientation(_parentCanvas, AssociatedObject, oldAngle);

                var newAngle = args.TouchDevice.GetOrientation(_parentCanvas);

                //Console.WriteLine("[n] Contact Angle: " + angle);

                //var newAngleTranslated = TranslateOrientation(_parentCanvas, AssociatedObject, newAngle);

                //Console.WriteLine("[n] Contact Angle: " + angle);

                angleViewModel.Angle = newAngle + Settings.Default.RotationOffset;
            }

            var positionViewModel = AssociatedObject.DataContext as IPosition;
            if (positionViewModel != null)
            {
                positionViewModel.UpdateTopLeftPosition(x, y);
            }
        }

        public static double TranslateOrientation(UIElement root, UIElement relativeTo, double orientationRelativeToRoot)
        {
            double num;
            if ((relativeTo == null) || object.ReferenceEquals(relativeTo, root))
            {
                num = orientationRelativeToRoot;
            }
            else
            {
                Vector vector = new Vector(1.0, 0.0);
                MatrixTransform transform = root.TransformToVisual(relativeTo) as MatrixTransform;
                Vector vector2 = vector * transform.Matrix;
                num = GetDegreesFromRadians(Math.Atan2(vector2.Y, vector2.X)) + orientationRelativeToRoot;
            }
            return AdjustAngle(num);
        }

        public static double GetDegreesFromRadians(double radians)
        {
            return ((radians * 180.0) / 3.1415926535897931);
        }

        private static double AdjustAngle(double angleDegrees)
        {
            angleDegrees = angleDegrees % 360.0;
            if (angleDegrees < 0.0)
            {
                angleDegrees += 360.0;
            }
            return angleDegrees;
        }

        #endregion

        public event EventHandler<EventArgs> DragDelta;
    }
}
