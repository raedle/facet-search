# README #

Facet Search is a visual, tangible information seeking system for interactive tabletops. It enables multiple users to simultaneously formulate database queries and allow them to seamlessly switch between loosely coupled parallel work and tightly coupled collaboration. A visual pipe-and-filter metaphor abstracts away complex Boolean logic, keeps track of search history, and allows for branching and merging of search paths. The visual and tangible concepts have been studied at the HCI Group, University of Konstanz and Microsoft Research Cambridge, UK. More details on the studies can be found at: [Research Paper](http://hci.uni-konstanz.de/downloads/final_CR_7.pdf). Facet Search was completely implemented from scratch with the concepts of keyword input in addition to facets and a plugin mechanism for easy exchange of data providers.

This project was funded by the Ministry of Research, Science, and Art Baden Württemberg, Germany. It implements connectors to three resource discovery systems (RDS) (Solr, Ebsco EDS, and Serial Solutions Summon). More details on how to download, setup, and configure the application can be found in the Wiki.

### THIRD PARTY LIBRARIES ###

We would like to acknowledge the many third party toolkits, frameworks, and libraries that were used to implement Facet Search. Those open source toolkits, frameworks, and libraries make programmers' live a lot easier and less frustrating ;)

* [Extended.Wpf.Toolkit](http://wpftoolkit.codeplex.com)
* [MvvmLight Toolkit](https://mvvmlight.codeplex.com)
* [Reactive Extensions](https://rx.codeplex.com)
* [NLog](http://nlog-project.org)
* [Reactive State Machine](https://reactivestatemachine.codeplex.com)
* [iTextSharp](https://github.com/itext/itextsharp)
* [MessagingToolkit.QRCode](http://platform.twit88.com/projects/mt-qrcode)
* [WpfAnimatedGif](https://wpfanimatedgif.codeplex.com)
* [Awesomeium](http://www.awesomium.com) a HTML UI engine to render web pages.
* [$P Gesture Recognizer](https://depts.washington.edu/aimgroup/proj/dollar/pdollar.html) for creating nodes when pen input is activated.

### LICENSE ###
```
#!text
The MIT License (MIT)

Copyright (c) 2014 Blended Library

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```