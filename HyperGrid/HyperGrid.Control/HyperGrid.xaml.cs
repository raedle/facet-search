﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using Microsoft.Surface.Presentation.Controls.Primitives;

namespace BlendedLibrary.HyperGrid.Control
{
    /// <summary>
    /// Interaction logic for VHyperGrid.xaml
    /// </summary>
    public partial class HyperGrid
    {
        #region Attributes
        /// <summary>
        /// datastructure for storing all attributes of all (also differing) objects of the current landscape (respectively the rootcollection) (movies, persons, coutries, emails, ...)
        /// the surrounding dictionary holds all object types (e.g. movies, emails, websites, ...)
        /// the inner hashset holds all the metadata-types (respectively PropertyInfos)
        /// </summary>
        //TODO auslagern in extra klasse des frameworks
        public Dictionary<Type, HashSet<PropertyInfo>> Attributes = new Dictionary<Type, HashSet<PropertyInfo>>();

        /// <summary>
        /// datastructure for storing the attribute bindings for each object-type (ZPhoto, Movie, ...)
        /// </summary>
        private readonly Dictionary<Type, Dictionary<int, ObservableCollection<Binding>>> _attributeBindings = new Dictionary<Type, Dictionary<int, ObservableCollection<Binding>>>();

        /// <summary>
        /// datastructure for storing the textual filters
        /// </summary>
        private readonly ObservableCollection<TextBox> _filterTextBoxes = new ObservableCollection<TextBox>();

        /// <summary>
        /// used to set opacity while filtering items / loading new rows
        /// </summary>
        private readonly ObservableCollection<Object> _filteredItems = new ObservableCollection<object>();
        private bool _isFiltering;

        #region RowHeightZoomed DependencyProperty

        /// <summary>
        /// RowHeightZoomed Dependency Property
        /// </summary>
        public static readonly DependencyProperty RowHeightZoomedProperty =
            DependencyProperty.Register("RowHeightZoomed", typeof(double), typeof(HyperGrid),
                new FrameworkPropertyMetadata((double)184));

        /// <summary>
        /// Gets or sets the RowHeightZoomed property.  This dependency property
        /// indicates the default height of a row when zoomed into.
        /// </summary>
        public double RowHeightZoomed
        {
            get { return (double)GetValue(RowHeightZoomedProperty); }
            set { SetValue(RowHeightZoomedProperty, value); }
        }

        #endregion RowHeightZoomed DependencyProperty

        #region zoomInDuration & zoomOutDuration DependencyProperty

        /// <summary>
        /// zoomInDuration Dependency Property
        /// </summary>
        public static readonly DependencyProperty ZoomInDurationProperty =
            DependencyProperty.Register("zoomInDuration", typeof(int), typeof(HyperGrid),
                new FrameworkPropertyMetadata(100));

        /// <summary>
        /// Gets or sets the zoomInDuration property.  This dependency property
        /// indicates the time needed for zooming into a row or cell (in milliseconds).
        /// </summary>
        public int ZoomInDuration
        {
            get { return (int)GetValue(ZoomInDurationProperty); }
            set { SetValue(ZoomInDurationProperty, value); }
        }

        /// <summary>
        /// zoomOutDuration Dependency Property
        /// </summary>
        public static readonly DependencyProperty ZoomOutDurationProperty =
            DependencyProperty.Register("zoomOutDuration", typeof(int), typeof(HyperGrid),
                new FrameworkPropertyMetadata(80));

        /// <summary>
        /// Gets or sets the zoomOutDuration property.  This dependency property
        /// indicates the time needed for zooming out of a row or cell (in milliseconds).
        /// </summary>
        public int ZoomOutDuration
        {
            get { return (int)GetValue(ZoomOutDurationProperty); }
            set { SetValue(ZoomOutDurationProperty, value); }
        }

        #endregion zoomInDuration & zoomOutDuration DependencyProperty

        #region Setting DependendencyProperties

        public bool DynamicZoomedRowHeight
        {
            get { return (bool)GetValue(DynamicZoomedRowHeightProperty); }
            set { SetValue(DynamicZoomedRowHeightProperty, value); }
        }

        public static readonly DependencyProperty DynamicZoomedRowHeightProperty =
            DependencyProperty.Register("DynamicZoomedRowHeight", typeof(bool), typeof(HyperGrid), new PropertyMetadata(false));



        public bool DisplayDetailOnZoom
        {
            get { return (bool)GetValue(DisplayDetailOnZoomProperty); }
            set { SetValue(DisplayDetailOnZoomProperty, value); }
        }

        public static readonly DependencyProperty DisplayDetailOnZoomProperty =
            DependencyProperty.Register("DisplayDetailOnZoom", typeof(bool), typeof(HyperGrid), new PropertyMetadata(false));



        public bool CanUserZoomRows
        {
            get { return (bool)GetValue(CanUserZoomRowsProperty); }
            set { SetValue(CanUserZoomRowsProperty, value); }
        }

        public static readonly DependencyProperty CanUserZoomRowsProperty =
            DependencyProperty.Register("CanUserZoomRows", typeof(bool), typeof(HyperGrid), new PropertyMetadata(true));

        #endregion

        #region DynamicItemsSource DependencyProperty
        public IList DynamicItemsSource
        {
            get { return (IList)GetValue(DynamicItemsSourceProperty); }
            set { SetValue(DynamicItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty DynamicItemsSourceProperty =
            DependencyProperty.Register("DynamicItemsSource", typeof(IList), typeof(HyperGrid));


        public bool UseDynamicItemsSource
        {
            get { return (bool)GetValue(UseDynamicItemsSourceProperty); }
            set { SetValue(UseDynamicItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty UseDynamicItemsSourceProperty =
            DependencyProperty.Register("UseDynamicItemsSource", typeof(bool), typeof(HyperGrid), new PropertyMetadata(false));

        /// <summary>
        /// <para>Determines when the list should start cleaning up elements, if DynamicItemsSource is used. </para>
        /// <para>Buffer size can exceed specified size, especially if a lot of continuous scrolling via touch is enabled.</para>
        /// <para>Default at 500. (Too much can get critical for memory, too little can result in jumps due to clean up)</para>
        /// </summary>
        public int DynamicBufferSize
        {
            get { return (int)GetValue(DynamicBufferSizeProperty); }
            set { SetValue(DynamicBufferSizeProperty, value); }
        }

        public static readonly DependencyProperty DynamicBufferSizeProperty =
            DependencyProperty.Register("DynamicBufferSize", typeof(int), typeof(HyperGrid), new PropertyMetadata(500));


        public double DynamicScrollBarHeight
        {
            get { return (double)GetValue(DynamicScrollBarHeightProperty); }
            set { SetValue(DynamicScrollBarHeightProperty, value); }
        }

        public static readonly DependencyProperty DynamicScrollBarHeightProperty =
            DependencyProperty.Register("DynamicScrollBarHeight", typeof(double), typeof(HyperGrid), new PropertyMetadata(0.0));

        public double DynamicScrollBarValue
        {
            get { return (double) GetValue(DynamicScrollBarValueProperty); }
            set { SetValue(DynamicScrollBarValueProperty, value); }
        }

        public static readonly DependencyProperty DynamicScrollBarValueProperty =
            DependencyProperty.Register("DynamicScrollBarValue", typeof(double), typeof(HyperGrid), new PropertyMetadata(0.0));

        public int DynamicInitialSize
        {
            get { return (int) GetValue(DynamicInitialSizeProperty); }
            set { SetValue(DynamicInitialSizeProperty, value); }
        }

        public static readonly DependencyProperty DynamicInitialSizeProperty =
            DependencyProperty.Register("DynamicInitialSize", typeof(int), typeof(HyperGrid), new PropertyMetadata(50));
        
        #endregion

        #endregion

        #region HyperGrid Related

        static HyperGrid()
        {
            #region set default values for hypergrid (the ones which differ from datagrid)

            Type ownerType = typeof(HyperGrid);

            //auto generate columns
            AutoGenerateColumnsProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(false));
            ColumnWidthProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(new DataGridLength(1, DataGridLengthUnitType.Star)));

            //scrollbars
            HorizontalScrollBarVisibilityProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(ScrollBarVisibility.Hidden));
            VerticalScrollBarVisibilityProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(ScrollBarVisibility.Visible));

            //gridlines
            GridLinesVisibilityProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(DataGridGridLinesVisibility.Horizontal));

            //columns
            HeadersVisibilityProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(DataGridHeadersVisibility.All));
            CanUserSortColumnsProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(true));
            CanUserReorderColumnsProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(true));
            CanUserResizeColumnsProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(true));
            ColumnHeaderHeightProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(50.0));
            EnableColumnVirtualizationProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(true));

            //rows
            RowHeightProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(20.0));
            if (MinRowHeightProperty != null)
                MinRowHeightProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(20.0));
            CanUserAddRowsProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(false));
            CanUserResizeRowsProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(true));
            EnableRowVirtualizationProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(true));

            //alternating row background
            AlternationCountProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(2));
            AlternatingRowBackgroundProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(Brushes.Gray));

            //selection mode
            SelectionModeProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(DataGridSelectionMode.Extended));

            #endregion set default values for hypergrid (the ones which differ from datagrid)
        }

        public HyperGrid()
        {
            Initialized += InitializeDynamicItemsSource;

            InitializeComponent();

            Sorting += HyperGrid_CustomSorting;
            
            Loaded += VHyperGrid_Loaded;
            Loaded += InitializeFilterTimer;

            LoadingRow += VHyperGrid_LoadingRow;
            ItemContainerGenerator.StatusChanged += ItemContainerGenerator_StatusChanged;
        }

        void VHyperGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Loaded += (_sender, _e) =>
                {
                    DataGridRow row = _sender as DataGridRow;
                    var cellsPresenter = TreeHelper.TryFindChild<DataGridCellsPresenter>(row);

                    // If the row was zoomed and unloaded, zoom it in again
                    if(UseDynamicItemsSource && _zoomedRows.ContainsKey(e.Row.DataContext))
                    {
                        // Send zoom in event or formatting can be messed up
                        if(OnRowZoomInto != null)
                            OnRowZoomInto(TreeHelper.TryFindChildren<DataGridCell>(cellsPresenter), new EventArgs());
                        if (DynamicZoomedRowHeight)
                            BindToTallestElement(cellsPresenter);
                        else
                            cellsPresenter.Height = RowHeightZoomed;

                        if(DisplayDetailOnZoom)
                            e.Row.DetailsVisibility = Visibility.Visible;
                    }


                    if (!_isFiltering)
                        return;

                    // Prevent other datagrids from being affected
                    DataGrid parentGrid = TreeHelper.TryFindParent<DataGrid>(e.Row);
                    if (!Equals(parentGrid, this))
                        return;

                    // Set opacity if items are filtered
                    cellsPresenter.Opacity = _filteredItems.Contains(e.Row.DataContext) ? 1 : 0.3;
                };
        }

        /// <summary>
        /// sets some properties which couldn't be set in static-part of class and some styles/templates
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VHyperGrid_Loaded(object sender, RoutedEventArgs e)
        {
            // set styles, templates & events
            SetTemplates();
        }

        protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            ScrollViewer scrollviewer = TreeHelper.TryFindChild<ScrollViewer>(this);
            if (scrollviewer != null)
                scrollviewer.ScrollToHome();

            ClearSorting();

            base.OnItemsSourceChanged(oldValue, newValue);
        }

        /// <summary>
        /// helper for setting attributes only after ItemContainerGenerator thread (which fills items into the grid) is finished
        /// </summary>
        /// <param name="sender">HyperGrid itself</param>
        /// <param name="e">ItemContainerGenerator.StatusChanged Event</param>
        private void ItemContainerGenerator_StatusChanged(object sender, EventArgs e)
        {
            if (ItemContainerGenerator == null || ItemContainerGenerator.Status != GeneratorStatus.ContainersGenerated)
                return;
            ItemContainerGenerator.StatusChanged -= ItemContainerGenerator_StatusChanged;
            Dispatcher.BeginInvoke(DispatcherPriority.Input, new VoidDelegate(DelayedFillAttributes));
        }

        private delegate void VoidDelegate();

        /// <summary>
        /// method for delaying setting of attribute-bindings and sortmemberpaths of columns only after items are present
        /// </summary>
        private void DelayedFillAttributes()
        {
            //fill datastructure with attributes of each object-type (movie, zphoto, ...)
            FillAttributeBindings();
        }

        #endregion

        #region styling
        //TODO probably on "Initialized" or "OnApplyTemplate"
        private void SetTemplates()
        {
            //set ColumnHeaderStyle (if not set already)
            if (ColumnHeaderStyle == null)
            {
                Style hgColumnHeaderStyle = (Style)FindResource("HGColumnHeaderStyle");
                ColumnHeaderStyle = hgColumnHeaderStyle;
            }

            // set RowHeaderTemplate (if not set already)
            if (RowHeaderTemplate == null)
            {
                DataTemplate rowHeaderTemplate = (DataTemplate)FindResource("RowHeaderTemplate");
                RowHeaderTemplate = rowHeaderTemplate;
            }
        }

        #endregion styling

        #region filtering

        #region filter events

        public event EventHandler<FilterEventArgs> OnFiltering;
        public event EventHandler OnFilterClear;
        public event EventHandler OnFilterStart;

        #endregion filter event sending

        #region filter event handling

        private readonly DispatcherTimer _filterTimer = new DispatcherTimer();
        private bool _isFilteringInProgress;

        private void InitializeFilterTimer(object sender, RoutedEventArgs e)
        {
            _filterTimer.Interval = TimeSpan.FromSeconds(1);
            _filterTimer.Tick += (o, args) =>
                {
                    _filterTimer.Stop();
                    StartFiltering();
            };
        }

        private void StartFiltering()
        {
            SendNotification("Suchen...");

            // Start a new background thread.. UpdateLayout doesn't work with forcing to redraw 
            // notification, therefore this hack
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += (sender1, eventArgs) =>
                {
                    // Wait 50ms for UI to draw notification (can't be in dispatcherthread or ui would freeze)
                    Thread.Sleep(50);
                    if (!_filterTimer.IsEnabled)
                    {
                        // filter in dispatcher thread 
                        Dispatcher.Invoke(FilterItems);
                    }
                };

            bw.RunWorkerAsync();
        }

        private bool _filterTextBoxSelected;

        private void Header_OnPreviewTouch(object sender, TouchEventArgs e)
        {
            // TODO: hotfix; if filter textbox is selected via touch the column gets sorted
            _filterTextBoxSelected = true;
        }

        private void Filter_TextBox_Loaded(object sender, RoutedEventArgs e)
        {
            //TODO: dont do it everytime the text changes...
            FillFilterTextBoxesCollection();
        }

        public void ClearFilters()
        {
            foreach (var textbox in _filterTextBoxes)
                Filter_Textbox_Reset(textbox, null);
        }

        private void Filter_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb != null && String.IsNullOrEmpty(tb.Text))
            {
                // If the textbox is empty, don't wait for user input but filter directly
                _filterTimer.Stop();
                StartFiltering();
            }
            else
            {
                // Delay filtering, so that the list is only filtered once if a word is typed in
                _filterTimer.Stop();
                _filterTimer.Start();
            }
            // Set sorting to null
            ClearSorting();
        }

        private void FilterItems()
        {
            // Filter by changing opacity depending on match and bring matching items to top
            if (ItemsSource == null || Items.Count == 0)
                return;

            _isFilteringInProgress = true;
            if (OnFilterStart != null)
                OnFilterStart(this, new EventArgs());
            _isFiltering = true;

            _filteredItems.Clear();
            ClearSorting();

            if (UseDynamicItemsSource)
            {
                foreach (var entry in _dynamicItems)
                {
                    if (FilterRow(entry))
                        _filteredItems.Add(entry);
                }

                int i = 0;

                var scroller = TreeHelper.TryFindChild<ScrollViewer>(this);
                scroller.ScrollToHome();

                foreach (var entry in _filteredItems)
                {
                    if (i++ >= DynamicBufferSize)
                        break;

                    _dynamicItems.Remove(entry);
                    _dynamicItems.Insert(0, entry);
                }

                RefreshCurrentItems();
            }
            else
            {
                foreach (var entry in ItemsSource)
                {
                    if (FilterRow(entry))
                        _filteredItems.Add(entry);
                }

                // move matching items to top
                ListCollectionView lcv = (ListCollectionView)CollectionViewSource.GetDefaultView(ItemsSource);
                FilterSorter mySort = new FilterSorter(_filteredItems);
                try
                {
                    // Without commitedit and canceledit the customsort doesn't work sometimes (??)
                    CommitEdit();
                    CancelEdit();

                    lcv.CustomSort = mySort;
                }
                catch
                { }
            }

            // Redraw all elements
            UpdateLayout();

            // And scroll to top
            ScrollViewer scrollviewer = TreeHelper.TryFindChild<ScrollViewer>(this);
            scrollviewer.ScrollToHome();

            // change opacity
            IEnumerable<DataGridCellsPresenter> visibleItems = TreeHelper.TryFindChildren<DataGridCellsPresenter>(this);
            foreach (DataGridCellsPresenter item in visibleItems)
            {
                if (_filteredItems.Contains(item.DataContext)) //Matches
                    item.Opacity = 1;
                else
                    item.Opacity = 0.3;
            }

            if (OnFiltering != null)
            {
                OnFiltering(this, new FilterEventArgs(_filteredItems));
            }

            _isFilteringInProgress = false;
        }

        public class FilterSorter : IComparer
        {
            public FilterSorter(IEnumerable<Object> filteredItems)
            {
                FilteredItems = filteredItems;
            }

            public IEnumerable<Object> FilteredItems
            {
                get;
                set;
            }

            int IComparer.Compare(object x, object y)
            {
                bool containsX = FilteredItems.Contains(x);
                bool containsY = FilteredItems.Contains(y);

                if (containsX && containsY)
                    return 0;
                if (containsX)
                    return -1;
                if (containsY)
                    return 1;
                return 0;
            }
        }

        private bool FilterRow(object sender)
        {
            if (sender == null)
                return false;

            //TODO: make it faster
            Object currObject = sender;

            int numberOfFiltersEmpty = 0;
            int numberOfFiltersMatching = 0;
            for (int i = 0; i < _filterTextBoxes.Count; i++)
            {
                TextBox currentColFilter = _filterTextBoxes[i];

                if (currentColFilter.Text.Length == 0)
                {
                    numberOfFiltersEmpty++;
                }
                else
                {
                    // get the filter-text
                    String currFilterText = currentColFilter.Text;

                    // get the attributes to search for
                    Type currentObjectType = currObject.GetType();

                    if (!_attributeBindings.ContainsKey(currentObjectType) || _attributeBindings[currentObjectType].Count <= i)
                        continue;

                    DataGridColumn currentCol = Columns[i];

                    //Search only attributes corresponding to sortmemberpath
                    String currValue = GetFullStringOfAttribute(currObject, currentCol.SortMemberPath);
                    if (currValue.ToLower().Contains(currFilterText.ToLower()))
                        numberOfFiltersMatching++;
                }
            }
            if (_filterTextBoxes.Count == numberOfFiltersEmpty)
            {
                if (OnFilterClear != null)
                    OnFilterClear(this, new EventArgs());
                _isFiltering = false;
                return true;
            }
            return (_filterTextBoxes.Count == (numberOfFiltersEmpty + numberOfFiltersMatching));
        }

        private string GetFullStringOfAttribute(Object currObject, string currAtt)
        {
            var attributeNames = currAtt.Split('.');

            Type currType = currObject.GetType();
            PropertyInfo currPropInfo = currType.GetProperty(attributeNames.First());

            //WPF binds to Attribute1.Attribute2.Attribute3 etc, but (as far as I know)
            //this won't work with reflections, so loop through each attribute separated
            //by '.'
            for (int i = 1; i < attributeNames.Count() && currPropInfo != null; i++ )
            {
                var attr = attributeNames[i];
                currObject = currPropInfo.GetValue(currObject);
                currType = currPropInfo.PropertyType;
                currPropInfo = currType.GetProperty(attr);
            }

            if (currPropInfo == null || currPropInfo.GetValue(currObject) == null)
                return "";
            return currPropInfo.GetValue(currObject).ToString();
        }

        private void Filter_Textbox_Reset(object sender, RoutedEventArgs e)
        {
            //get textbox from column where filter-text should be removed
            FrameworkElement filterButton = (FrameworkElement)sender;
            DataGridColumnHeader myGvch = TreeHelper.TryFindParent<DataGridColumnHeader>(filterButton);

            TextBox filterTextBox = TreeHelper.TryFindChild<TextBox>(myGvch);
            filterTextBox.Text = "";
        }

/*
        private void Touch_Filter_Textbox_Reset(object sender, TouchEventArgs e)
        {
            Filter_Textbox_Reset(sender, null);
        }
*/

        #endregion filter event handling

        #region filter helpers (get the Binding of all Attributes in all columns)

        private void FillFilterTextBoxesCollection()
        {
            //very simple method to get all textfilters (by any keypress in any of the textboxes - just delete the datastructure and rebuild it from scratch)
            _filterTextBoxes.Clear();
            DataGridCellsPanel hgCellsPanel = TreeHelper.TryFindChild<DataGridCellsPanel>(this);

            //get all HG-column-headers
            foreach (DataGridColumnHeader currentColumnHeader in hgCellsPanel.Children)
            {
                //get their filter textbox
                TextBox currentFilterTextbox = TreeHelper.TryFindChild<TextBox>(currentColumnHeader);

                //add filterbox to datastructure
                _filterTextBoxes.Add(currentFilterTextbox);
            }
        }

        /// <summary>
        /// fills the attributeBindings datastructure with type-specific (zphoto, movie, ...) bindings of each column
        /// </summary>
        //TODO not really used yet
        private void FillAttributeBindings()
        {
            //iterate over all items and get their type
            for (int rowId = 0; rowId < Items.Count; rowId++)
            {
                Type rowType = Items[rowId].GetType();

                //add type if it is not contained in attributeBindings
                if (!_attributeBindings.Keys.Contains(rowType))
                {
                    _attributeBindings.Add(rowType, GetAttributesFromCellTemplate(rowId));
                }
            }
        }

        /// <summary>
        /// gets the bindings for all cells of a row
        /// </summary>
        /// <param name="rowId">ID of the row to get the Bindings for</param>
        /// <returns>Dictionary with columnId and Collection of all Bindings for each column</returns>
        public Dictionary<int, ObservableCollection<Binding>> GetAttributesFromCellTemplate(int rowId)
        {
            Dictionary<int, ObservableCollection<Binding>> columnBindings = new Dictionary<int, ObservableCollection<Binding>>();

            //get row and its columns to search for databound elements
            DataGridRow dataGridRow = (DataGridRow)ItemContainerGenerator.ContainerFromItem(Items[rowId]);
            if (dataGridRow == null)
                return columnBindings;

            DataGridCellsPanel dataGridRowColumns = TreeHelper.TryFindChild<DataGridCellsPanel>(dataGridRow);

            //iterate over all columns
            for (int columnId = 0; columnId < dataGridRowColumns.Children.Count; columnId++)
            {
                ObservableCollection<Binding> currColumnBindings = new ObservableCollection<Binding>();

                //get current cell
                FrameworkElement currCell = (FrameworkElement)dataGridRowColumns.Children[columnId];

                //get bound items for this frameworkelement recursively (iterate over whole childtree)
                GetAllBindings(currColumnBindings, currCell);

                //add bindings of current column to datastructure
                columnBindings.Add(columnId, currColumnBindings);
            }

            return columnBindings;
        }

        /// <summary>
        /// recursively gets all bindings of a frameworkelement and its children
        /// </summary>
        /// <param name="columnAttributes">datastrurcture to store bindings</param>
        /// <param name="element">frameworkelement to search for a binding (also searched in its children)</param>
        private void GetAllBindings(ObservableCollection<Binding> columnAttributes, FrameworkElement element)
        {
            //see: http://blogs.msdn.com/marlat/archive/2009/05/24/getbindingexpression-a-good-way-to-iterate-over-all-dependency-properties-in-visual-tree-in-silverlight-3.aspx
            FieldInfo[] infos = element.GetType().GetFields(BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance | BindingFlags.Static);

            foreach (FieldInfo field in infos)
            {
                if (field.FieldType == typeof(DependencyProperty))
                {
                    DependencyProperty dp = (DependencyProperty)field.GetValue(null);
                    BindingExpression ex = element.GetBindingExpression(dp);

                    if (ex != null && ex.ParentBinding.Path.Path.Length > 0)
                    {
                        columnAttributes.Add(ex.ParentBinding);
                    }
                }
            }

            int children = VisualTreeHelper.GetChildrenCount(element);

            for (int i = 0; i < children; i++)
            {
                FrameworkElement child = VisualTreeHelper.GetChild(element, i) as FrameworkElement;

                if (child != null)
                {
                    GetAllBindings(columnAttributes, child);
                }
            }
        }

        #endregion filter helpers (get the Binding of all Attributes in all columns)

        #endregion filtering

        #region sorting

        #region custom sorting

        private DataGridColumn _currentSortColumn;

        public void ClearSorting()
        {
            if (_currentSortColumn != null)
                _currentSortColumn.SortDirection = null;
        }

        /// <summary>
        /// custom sorting algotrithm to improve performance and enable sorting of special-types (e.g. Uri, IList(String), ...)
        /// but performance is not very fast - other algorithm with delegate (HyperGrid2_CustomSorting_new) is testet
        /// (gets datasource & column, sets sorting direction & sorter)
        /// </summary>
        /// <param name="sender">HyperGrid ItemsSource</param>
        /// <param name="e">Column which shall be sorted</param>
        private void HyperGrid_CustomSorting(object sender, DataGridSortingEventArgs e)
        {
            //see http://yogesh.jagotagroup.com/blog/post/2008/11/01/Filtering-WPF-toolkit-DataGrid.aspx

            // respect sorting methods from user, especially if datatype isn't implemented here
            if (e.Handled || !CanUserSortColumns)
                return;

            e.Handled = true; // ignore standard-sorting behaviour     

            // Don't filter and sort at the same time
            if (_filterTimer.IsEnabled || _isFilteringInProgress)
                return;

            // TODO: hack because sorting happening if filtertextbox is selected via touch
            if (_filterTextBoxSelected)
            {
                _filterTextBoxSelected = false;
                return;
            }

            // TODO If no direction is available (i.e. if it was reset during filtering) remove listcollectionview
            //if (_currentSortColumn == null)
            //{
            //    if (UseDynamicItemsSource)
            //    {

            //    }
            //    else
            //    {

            //    }
            //}           

            DataGridColumn currentCol = e.Column;
            _currentSortColumn = e.Column;
            ListSortDirection direction = (currentCol.SortDirection != ListSortDirection.Ascending)
                                              ? ListSortDirection.Ascending
                                              : ListSortDirection.Descending;

            currentCol.SortDirection = direction;

            if (UseDynamicItemsSource)
            {
                ListCollectionView dynamicLcv = new ListCollectionView(DynamicItemsSource);
                CustomSorter dynamicSort = new CustomSorter(direction, currentCol);
                dynamicLcv.CustomSort = dynamicSort;

                foreach (var item in dynamicLcv)
                {
                    _dynamicItems.Remove(item);
                    _dynamicItems.Add(item);
                }

                RefreshCurrentItems();

            }
            else
            {
                ListCollectionView lcv = (ListCollectionView)CollectionViewSource.GetDefaultView(ItemsSource);
                CustomSorter mySort = new CustomSorter(direction, currentCol);
                try
                {
                    // Without commitedit and canceledit the customsort doesn't work sometimes (??)
                    CommitEdit();
                    CancelEdit();
                    lcv.CustomSort = mySort;
                }
                catch (Exception)
                {
                }
            }


        }

        /// <summary>
        /// custom comparer, not very fast because of large if/else part (couldn't use switch because typeof isn't supported by switch)
        /// </summary>
        public class CustomSorter : IComparer
        {
            public CustomSorter(ListSortDirection direction, DataGridColumn column)
            {
                Direction = direction;
                Column = column;
            }

            public ListSortDirection Direction
            {
                get;
                private set;
            }

            public DataGridColumn Column
            {
                get;
                private set;
            }

            int IComparer.Compare(object x, object y)
            {
                //TODO how to sort heterogenous data?
                //TODO parsing of special datatypes (e.g. Date, url, mpaa-certificate (order by age of admittance), Color, Images, ...)

                #region Get Properties via reflection
                object currObjectX = x;
                object currObjectY = y;

                var attributeNames = Column.SortMemberPath.Split('.');
                //If Sortmemberpath is empty or X and Y are of not the same type sorting is impossible
                if (attributeNames.Length == 0 || !(x.GetType().IsEquivalentTo(y.GetType())))
                    return 0;

                Type currTypeX = x.GetType();
                Type currTypeY = y.GetType();
                PropertyInfo currPropInfoX = currTypeX.GetProperty(attributeNames.First());
                PropertyInfo currPropInfoY = currTypeY.GetProperty(attributeNames.First());

                //WPF binds to Attribute1.Attribute2.Attribute3 etc, but (as far as I know)
                //this won't work with reflections, so we loop through each attribute separated
                //by '.'
                for (int i = 1; i < attributeNames.Count() && currPropInfoX != null && currPropInfoY != null; i++)
                {
                    var attr = attributeNames[i];
                    currObjectX = currPropInfoX.GetValue(currObjectX);
                    currObjectY = currPropInfoY.GetValue(currObjectY);
                    currTypeX = currPropInfoX.PropertyType;
                    currTypeY = currPropInfoY.PropertyType;
                    currPropInfoX = currTypeX.GetProperty(attr);
                    currPropInfoY = currTypeY.GetProperty(attr);
                }

                if (currObjectX != null && currPropInfoX != null)
                    currObjectX = currPropInfoX.GetValue(currObjectX);
                else
                    currObjectX = null;

                if (currObjectY != null && currPropInfoY != null)
                    currObjectY = currPropInfoY.GetValue(currObjectY);
                else
                    currObjectY = null;
                #endregion

                int dir = (Direction == ListSortDirection.Ascending) ? 1 : -1;

                #region handling of null values (performance hack)

                if (currObjectX == null && currObjectY == null)
                    return 0;
                if (currObjectX == null)
                    return 1 * dir;
                if (currObjectY == null)
                    return -1 * dir;

                #endregion handling of null values (performance hack)

                #region start type specific compare

                Type attributeType = currObjectX.GetType();
                if (attributeType == typeof(String))
                {
                    return StringCompare((String)currObjectX, (String)currObjectY) * dir;
                }
                if (attributeType == typeof(Double))
                {
                    return DoubleCompare((Double)currObjectX, (Double)currObjectY) * dir;
                }
                if (attributeType == typeof(int?))
                {
                    return IntCompare((int?)currObjectX, (int?)currObjectY) * dir;
                }
                if (attributeType == typeof(int))
                {
                    return IntCompare((int)currObjectX, (int)currObjectY) * dir;
                }
                if (attributeType == typeof(DateTime?))
                {
                    return DateTimeCompare((DateTime?)currObjectX, (DateTime?)currObjectY) * dir;
                }
                if (attributeType == typeof(DateTime))
                {
                    return DateTimeCompare((DateTime)currObjectX, (DateTime)currObjectY) * dir;
                }
                if (attributeType == typeof(Uri))
                {
                    return UriCompare((Uri)currObjectX, (Uri)currObjectY) * dir;
                }
                if (attributeType == typeof(IList<String>))
                {
                    StringBuilder iList1String = new StringBuilder();
                    StringBuilder iList2String = new StringBuilder();

                    foreach (String currOcc in ((IList<String>)currObjectX))
                    {
                        iList1String.Append(currOcc);

                        //speed up by breaking after 50characters (sorting makes no sense anymore - in 99.99% of the cases)...
                        if (iList1String.Length > 50) break;
                    }
                    foreach (String currOcc in ((IList<String>)currObjectY))
                    {
                        iList2String.Append(currOcc);

                        //speed up by breaking after 50characters (sorting makes no sense anymore - in 99.99% of the cases)...
                        if (iList2String.Length > 50) break;
                    }
                    return StringCompare(iList1String.ToString(), iList2String.ToString()) * dir;
                }
                
                throw new NotImplementedException();

                #endregion start type specific compare
            }

            #region Comparer helper classes

            private int StringCompare(string s1, string s2)
            {
                return s1.CompareTo(s2);
            }

            private int DoubleCompare(double? doub1, double? doub2)
            {
                return doub1.Value.CompareTo(doub2.Value);
            }

            private int IntCompare(int? int1, int? int2)
            {
                return int1.Value.CompareTo(int2);
            }

            private int BoolCompare(bool bool1, bool bool2)
            {
                return bool1.CompareTo(bool2);
            }

            private int DateTimeCompare(DateTime? date1, DateTime? date2)
            {
                return date1.Value.CompareTo(date2.Value);
            }

            private int UriCompare(Uri uri1, Uri uri2)
            {
                return uri1.ToString().CompareTo(uri2.ToString());
            }

            #endregion Comparer helper classes
        }

        #endregion custom sorting

        #endregion sorting

        #region grouping
        // Todo.
        #endregion grouping

        #region zooming

        public event EventHandler OnRowZoomInto;
        public event EventHandler OnRowZoomOut;

        private FrameworkElement GetTallestElement(DependencyObject parent)
        {
            FrameworkElement tallestElement = null;

            //Iterate through each cell
            if (parent is DataGridCellsPresenter)
            {
                DataGridCellsPresenter presenter = parent as DataGridCellsPresenter;
                var cells = TreeHelper.TryFindChildren<DataGridCell>(presenter);

                foreach (FrameworkElement temp in cells.Select(GetTallestElement))
                {
                    if (tallestElement == null)
                        tallestElement = temp;
                    else if (temp != null && temp.ActualHeight > tallestElement.ActualHeight)
                        tallestElement = temp;
                }
            }
            else
            {
                IEnumerable<Panel> panels = TreeHelper.TryFindChildren<Panel>(parent);

                foreach (var panel in panels)
                {
                    if (tallestElement == null || panel.ActualHeight > tallestElement.ActualHeight)
                        tallestElement = panel;

                    FrameworkElement temp = GetTallestElement(panel);
                    if (temp != null && temp.ActualHeight > tallestElement.ActualHeight)
                        tallestElement = temp;
                }
            }

            return tallestElement;
        }


        private readonly List<DependencyObject> _hitResultsList = new List<DependencyObject>();

        public HitTestResultBehavior MyHitTestResult(HitTestResult result)
        {
            // Add the hit test result to the list that will be processed after the enumeration.
            _hitResultsList.Add(result.VisualHit);

            // Set the behavior to return visuals at all z-order levels. 
            return HitTestResultBehavior.Continue;
        }

        /// <summary>
        /// Gets the list of routed event handlers subscribed to the specified routed event.
        /// </summary>
        /// <param name="element">The UI element on which the event is defined.</param>
        /// <param name="routedEvent">The routed event for which to retrieve the event handlers.</param>
        /// <returns>The list of subscribed routed event handlers.</returns>
        /// <see cref="http://stackoverflow.com/questions/9434817/how-to-remove-all-click-event-handler"/>
        public int CountRoutedEventHandlers(UIElement element, RoutedEvent routedEvent)
        {
            // Get the EventHandlersStore instance which holds event handlers for the specified element.
            // The EventHandlersStore class is declared as internal.
            var eventHandlersStoreProperty = typeof(UIElement).GetProperty(
                "EventHandlersStore", BindingFlags.Instance | BindingFlags.NonPublic);
            object eventHandlersStore = eventHandlersStoreProperty.GetValue(element, null);

            // Invoke the GetRoutedEventHandlers method on the EventHandlersStore instance 
            // for getting an array of the subscribed event handlers.
            if (eventHandlersStore == null)
                return 0;
            var getRoutedEventHandlers = eventHandlersStore.GetType().GetMethod(
                "GetRoutedEventHandlers", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            if (getRoutedEventHandlers == null)
                return 0;
            var routedEventHandlers = (RoutedEventHandlerInfo[])getRoutedEventHandlers.Invoke(
                eventHandlersStore, new object[] { routedEvent });

            if (routedEventHandlers == null)
                return 0;

            return routedEventHandlers.Length;
        }

        private void RowZoomInto(object sender, MouseButtonEventArgs e)
        {
            // TODO, see RowZoomToggle
        }

        private void RowZoomToggle(object sender, MouseButtonEventArgs e)
        {
            if (!CanUserZoomRows)
                return;

            Window mainWindow = TreeHelper.TryFindParent<Window>(sender as DependencyObject);

            // Retrieve the coordinate of the mouse position.
            Point pt = e.GetPosition(mainWindow);

            // Clear the contents of the list used for hit test results.
            _hitResultsList.Clear();
            mainWindow.UpdateLayout();

            // Set up a callback to receive the hit test result enumeration.
            VisualTreeHelper.HitTest(this, null,
                MyHitTestResult,
                new PointHitTestParameters(pt));

            if (_hitResultsList.Count == 0)
                return;

            // If any element calls PreviewMouseDown etc, stop zooming (e.Handled doesn't work on PreviewEventargs)
            if ((from hit in _hitResultsList select hit as UIElement into uiHit 
                 let count = CountRoutedEventHandlers(uiHit, PreviewMouseLeftButtonDownEvent) 
                 select count + CountRoutedEventHandlers(uiHit, PreviewMouseDownEvent)
                              + CountRoutedEventHandlers(uiHit, PreviewTouchDownEvent)).Any(count => count != 0))
                              
            {
                return;
            }

            DataGridRow currentRow = TreeHelper.TryFindParent<DataGridRow>(_hitResultsList[0]);
            if (currentRow == null) // Click was outside of datagrids
                return;

            DataGridCellsPresenter currentCellsPresenter = TreeHelper.TryFindChild<DataGridCellsPresenter>(currentRow);
            DataGridDetailsPresenter detailsPresenter = TreeHelper.TryFindChild<DataGridDetailsPresenter>(currentRow);

            DataGrid parentGrid = TreeHelper.TryFindParent<DataGrid>(currentCellsPresenter);
            // Prevent cells from a datagrid in the VHyperGrid from zooming
            if (!(parentGrid is HyperGrid))
                return;

            // Use the found parenthypergrid as point of reference instead of [this], in case of stacked VHyperGrids
            HyperGrid parentHyperGrid = (HyperGrid)parentGrid;

            // look if row was already zoomed
            if (Math.Abs(currentCellsPresenter.ActualHeight - parentHyperGrid.RowHeight) < 1)
            {
                double height = parentHyperGrid.RowHeightZoomed;
                if (parentHyperGrid.DynamicZoomedRowHeight)
                {
                    FrameworkElement element = GetTallestElement(currentCellsPresenter);
                    if(element != null)
                        height = element.ActualHeight;
                }
                
                // Zoom row
                StartRowZoomAnimation(currentCellsPresenter, currentCellsPresenter.ActualHeight, height, ZoomInDuration);

                // Show detailview
                if (parentHyperGrid.DisplayDetailOnZoom)
                    StartDetailZoomAnimation(detailsPresenter, ZoomInDuration, true);

                // Save row context so it gets loaded with the correct height
                _zoomedRows.Add(currentCellsPresenter.DataContext, TreeHelper.TryFindParent<DataGridRow>(currentCellsPresenter).ActualHeight);

            }
            else
            {
                // Zoom row
                StartRowZoomAnimation(currentCellsPresenter, currentCellsPresenter.ActualHeight, parentHyperGrid.RowHeight, ZoomOutDuration);

                // Close detailview
                if (parentHyperGrid.DisplayDetailOnZoom)
                    StartDetailZoomAnimation(detailsPresenter, ZoomOutDuration, false);

                // Remove row context so it gets loaded with the correct height
                _zoomedRows.Remove(currentCellsPresenter.DataContext);
            }

            e.Handled = true;
        }

        private void RowZoomOut(object sender, MouseButtonEventArgs e)
        {
            // TODO, see RowZoomToggle
        }

        private void StartDetailZoomAnimation(FrameworkElement myFE, int duration, bool zoomIn)
        {
            if (myFE == null)
                return;

            myFE.Visibility = Visibility.Hidden;
            myFE.UpdateLayout();

            // Set animation properties
            Duration zoomDuration = new Duration(TimeSpan.FromMilliseconds(duration));
            DoubleAnimation zoomHeightAnimation = new DoubleAnimation
                {
                    From = (zoomIn ? 0 : myFE.ActualHeight),
                    To = (zoomIn ? myFE.ActualHeight : 0),
                    Duration = zoomDuration,
                    FillBehavior = FillBehavior.Stop
                };

            // Close detailspanel on zooming out
            if (!zoomIn)
            {
                zoomHeightAnimation.Completed += (obj, e) =>
                    {
                        myFE.Visibility = Visibility.Collapsed;
                    };
            }

            // start animation
            myFE.BeginAnimation(HeightProperty, null);
            myFE.BeginAnimation(HeightProperty, zoomHeightAnimation);
            myFE.Height = Double.NaN;
            myFE.Visibility = Visibility.Visible;
        }

        private void StartRowZoomAnimation(FrameworkElement myFE, double fromHeight, double toHeight, int duration)
        {
            //check if rowheight is smaller than initial rowheight
            if (toHeight < RowHeight)
                toHeight = RowHeight;

            //set animation properties
            Duration zoomDuration = new Duration(TimeSpan.FromMilliseconds(duration));
            DoubleAnimation zoomHeightAnimation = new DoubleAnimation
                {
                    From = fromHeight,
                    To = toHeight,
                    Duration = zoomDuration,
                    FillBehavior = FillBehavior.Stop
                };

            // prevent that the last 5 elements of the list get zoomed out of view so user has to scroll down again
            IList itemsource = UseDynamicItemsSource ? DynamicItemsSource : Items;
            ScrollViewer scrollviewer = TreeHelper.TryFindParent<ScrollViewer>(myFE);
            if (toHeight > fromHeight && itemsource.Count - itemsource.IndexOf(myFE.DataContext) <= 5
                && Math.Abs(scrollviewer.VerticalOffset - scrollviewer.ScrollableHeight) < 1)
            {
                zoomHeightAnimation.Changed += (obj, e) => scrollviewer.ScrollToBottom();
            }

            // Activate appropriate event
            if (toHeight > fromHeight && OnRowZoomInto != null)
                OnRowZoomInto(TreeHelper.TryFindChildren<DataGridCell>(myFE), new EventArgs());
            else if (toHeight < fromHeight && OnRowZoomOut != null)
                OnRowZoomOut(TreeHelper.TryFindChildren<DataGridCell>(myFE), new EventArgs());

            // start animation
            myFE.BeginAnimation(HeightProperty, null);
            myFE.BeginAnimation(HeightProperty, zoomHeightAnimation);

            if (DynamicZoomedRowHeight && toHeight > fromHeight)
            {
                // enable binding if dynamic height is activated and is zoomed in, in case height changes 
                BindToTallestElement(myFE);

                // If height changes, animate the change
                GetTallestElement(myFE).SizeChanged += DataGridRow_SizeChanged;
            }
            else
            {
                GetTallestElement(myFE).SizeChanged -= DataGridRow_SizeChanged;
                myFE.Height = toHeight;
            }
        }

        void DataGridRow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            DataGridRow row = TreeHelper.TryFindParent<DataGridRow>(sender as DependencyObject);

            if (row != null && _zoomedRows.ContainsKey(row.DataContext))
                _zoomedRows[row.DataContext] = row.ActualHeight;

            // TODO: animate sizechanged smoothly, the commented code results in more jumping than without zooming

        }

        private void BindToTallestElement(FrameworkElement myFE)
        {
            FrameworkElement tallestElement = GetTallestElement(myFE);

            Binding heightBinding = new Binding {Source = tallestElement, Path = new PropertyPath("ActualHeight")};
            myFE.SetBinding(HeightProperty, heightBinding);
        }

        #endregion zooming

        #region Dynamic row loading

        private void InitializeDynamicItemsSource(object sender, EventArgs eventArgs)
        {
            // Subscribe to events relating to DynamicItemsSource
            DependencyPropertyDescriptor dpd = DependencyPropertyDescriptor.FromProperty(DynamicItemsSourceProperty,
                                                                                         typeof (HyperGrid));
            if (dpd != null)
            {
                dpd.AddValueChanged(this, delegate
                    {
                        var source = DynamicItemsSource as INotifyCollectionChanged; 
                        if (source != null)
                            source.CollectionChanged += VHyperGrid_CollectionChanged;

                        RefreshDynamicItemsSource();
                        ClearSorting();
                        ClearFilters();
                    });
            }

            dpd = DependencyPropertyDescriptor.FromProperty(DynamicItemsSourceProperty, typeof (HyperGrid));
            if (dpd != null)
            {
                dpd.AddValueChanged(this, delegate
                    {
                        if (UseDynamicItemsSource)
                            ItemsSource = _currentItems;
                    });
            }

            dpd = DependencyPropertyDescriptor.FromProperty(UseDynamicItemsSourceProperty, typeof(HyperGrid));
            if (dpd != null)
            {
                dpd.AddValueChanged(this, delegate
                    {
                        if (UseDynamicItemsSource)
                        {
                            ItemsSource = _currentItems;

                            // Bind custom scrollbar values
                            var maxHeightBinding = new Binding { Path = new PropertyPath(DynamicScrollBarHeightProperty), Source = this };
                            var valueBinding = new Binding { Path = new PropertyPath(DynamicScrollBarValueProperty), Source = this };

                            Loaded += (o, args) =>
                                {
                                    SurfaceScrollBar scrollBar = TreeHelper.TryFindChild<SurfaceScrollBar>(this);
                                    if (scrollBar != null)
                                    {
                                        scrollBar.SetBinding(RangeBase.MaximumProperty, maxHeightBinding);
                                        scrollBar.SetBinding(RangeBase.ValueProperty, valueBinding);
                                    }

                                    ScrollViewer scrollViewer = TreeHelper.TryFindChild<ScrollViewer>(this);
                                    scrollViewer.CanContentScroll = false;
                                };
                        }
                    });
            }

            var dSource = DynamicItemsSource as INotifyCollectionChanged;
            if (dSource != null)
                dSource.CollectionChanged += VHyperGrid_CollectionChanged;

            // Clean up after scrolling
            _cleanupTimer.Interval = TimeSpan.FromMilliseconds(300);
            _cleanupTimer.Tick += DynamicItemsCleanUp;

            // Keep Scrollbarheight up to date on zoom
            LoadingRow += (o, args) => { args.Row.SizeChanged += UpdateScrollbarHeight; };
            UnloadingRow += (o, args) => { args.Row.SizeChanged -= UpdateScrollbarHeight; };

        }

        void VHyperGrid_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // method can run in non-ui-thread
            try
            {
                Dispatcher.Invoke(() => {
                    if (UseDynamicItemsSource)
                        UpdateDynamicItemsSource(e);
                }, DispatcherPriority.Background);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); 
            }
        }

        private void UpdateDynamicItemsSource(NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in e.NewItems)
                    {
                        // if the new item is "in range" of _currentitems, add it directly
                        if (e.NewStartingIndex - 1 >= _listBegin
                            && e.NewStartingIndex <= _listBegin + _currentItems.Count
                            && _currentItems.Count < DynamicBufferSize)
                        {
                            if (e.NewStartingIndex == _listBegin - 1)
                                _listBegin--;

                            _currentItems.Insert(e.NewStartingIndex - _listBegin, item);
                            UpdateLayout();
                        }

                        // if it's before _currentItems it means that _currentItems begins one item "later" and
                        // the scrollbar has to scroll down
                        if (e.NewStartingIndex <= _listBegin)
                        {
                            _listBegin++;
                            DynamicScrollBarValue += RowHeight;
                        }

                        // Add it to the list and update max height
                        if (_dynamicItems.Count > e.NewStartingIndex)
                            _dynamicItems.Insert(e.NewStartingIndex, item);
                        DynamicScrollBarHeight += RowHeight;

                        // Check if buffer values are still correct and load new items in before list
                        _cleanupTimer.Stop();
                        _cleanupTimer.Start();
                    }
                    break;
                case NotifyCollectionChangedAction.Move:
                    // TODO: ???
                    throw new NotSupportedException();
                    break;
                case NotifyCollectionChangedAction.Remove:
                    // Can't manipulate _dynamicItems during foreach over _dynamicItems, so save them
                    var removedObjects = new List<object>();

                    foreach (var item in _dynamicItems)
                    {
                        if (!e.NewItems.Contains(item))
                        {
                            removedObjects.Add(item);
                            // if the item is currently displayed, delete it
                            if (_currentItems.Contains(item))
                                _currentItems.Remove(item);
                        }
                    }

                    // Finally remove objects from working list
                    foreach (var removedObject in removedObjects)
                        _dynamicItems.Remove(removedObject);


                    // Maybe load additional items if necessary
                    _cleanupTimer.Stop();
                    _cleanupTimer.Start();

                    break;
                case NotifyCollectionChangedAction.Replace:
                    // TODO: ???
                    throw new NotSupportedException();
                    break;
                case NotifyCollectionChangedAction.Reset:
                    RefreshDynamicItemsSource();
                    break;
            }
            
        }

        void RefreshDynamicItemsSource()
        {
            // Completely rebuild list from DynamicItemsSource
            _dynamicItems.Clear();

            if (DynamicItemsSource != null)
            {
                try
                {
                    foreach (var item in DynamicItemsSource)
                        _dynamicItems.Add(item);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }

                // Reset scrollbar height
                ScrollViewer sv = TreeHelper.TryFindChild<ScrollViewer>(this);
                DynamicScrollBarHeight = -sv.ActualHeight + RowHeight * DynamicItemsSource.Count;
            }

            // And refresh active items
            RefreshCurrentItems();
        }

        void RefreshCurrentItems()
        {
            // Reset attributes
            _currentItems.Clear();
            _listBegin = 0;
            DynamicScrollBarValue = 0;

            // Rebuild list
            int i = 0;
            foreach (var item in _dynamicItems.TakeWhile(item => i++ < DynamicInitialSize))
                _currentItems.Add(item);

            // Start at top again
            ScrollViewer scrollviewer = TreeHelper.TryFindChild<ScrollViewer>(this);
            scrollviewer.ScrollToHome();

            // Reset scrolloffset
            scrollviewer.UpdateLayout();
            _scrollOffset = 0;
        }

        private int _listBegin;
        private double _scrollOffset;
        private bool _isTimerActive;
        private readonly ObservableCollection<Object> _currentItems = new ObservableCollection<Object>();
        private readonly ObservableCollection<Object> _dynamicItems = new ObservableCollection<Object>();
        private readonly Dictionary<Object, double> _zoomedRows = new Dictionary<Object, double>();
        private readonly DispatcherTimer _cleanupTimer = new DispatcherTimer();

        void DynamicItemsCleanUp(Object sender, EventArgs e)
        {
            // Scrolling while touches are captured results in jumps, so wait until scrollviewer is "free"
            ScrollViewer scrollviewer = TreeHelper.TryFindChild<ScrollViewer>(this);
            if (scrollviewer.AreAnyTouchesCaptured)
                return; // Timer isn't stopped so it will check again after timerinterval

            _isTimerActive = true;
            _cleanupTimer.Stop();

            int firstVisibleItemIndex = _listBegin;
            double currScrollOffset = 0;
            IEnumerable<DataGridRow> rows = TreeHelper.TryFindChildren<DataGridRow>(scrollviewer);
            bool addedItems = false;

            foreach (var dataGridRow in rows)
            {
                currScrollOffset += dataGridRow.ActualHeight;
                
                if (currScrollOffset >= scrollviewer.VerticalOffset)
                    break;

                firstVisibleItemIndex++;
            }

            if (_currentItems.Count == 0)
                return;

            // Load items into beginning of list until 1/2 of buffersize is full
            // This needs to be done while no scrolling is active, as loading items into
            // the beginning of the list pushes rows down and code-scrolling while touch is
            // active will jump back to where the touch-scroll was, which results in jumps
            while (firstVisibleItemIndex - _dynamicItems.IndexOf(_currentItems.First()) < DynamicBufferSize / 2 && _listBegin >= AlternationCount)
            {
                addedItems = true;
                // Also only load in packs of AlternationCount, or the color of the row switches
                for (int i = 0; i < AlternationCount; i++)
                {
                    _currentItems.Insert(0, _dynamicItems[--_listBegin]);

                    // Scroll up to prevent jumps
                    scrollviewer.UpdateLayout();
                    _isTimerActive = true;

                    // React to zoomed-in rows
                    double scrollDistance = RowHeight;
                    if (_zoomedRows.ContainsKey(_dynamicItems[_listBegin]))
                        scrollDistance = _zoomedRows[_dynamicItems[_listBegin]];

                    scrollviewer.ScrollToVerticalOffset(scrollviewer.ContentVerticalOffset + scrollDistance);
                }
            }

            // Remove excess items (everything that exceeds 1/2 of buffersize) in beginning of list
            while (!addedItems && _dynamicItems.IndexOf(_currentItems.First()) + AlternationCount < firstVisibleItemIndex - DynamicBufferSize / 2)
            {
                // Again only in packs of AlternationCount
                for (int i = 0; i < AlternationCount; i++)
                {
                    // React to zoomed in rows
                    double scrollDistance = RowHeight;
                    if (_zoomedRows.ContainsKey(_currentItems.First()))
                        scrollDistance = _zoomedRows[_currentItems.First()];

                    _currentItems.RemoveAt(0);
                    _listBegin++;

                    // Scroll down to prevent jumps
                    scrollviewer.UpdateLayout();
                    _isTimerActive = true;
                    if (DynamicScrollBarHeight - DynamicScrollBarValue < RowHeight / 3)
                        scrollviewer.ScrollToBottom();
                    else
                        scrollviewer.ScrollToVerticalOffset(scrollviewer.ContentVerticalOffset - scrollDistance);
                }

            }

            // Clean up "behind" / after / under list
            // Scrolling is handled by inserting top items as scrollviewer doesn't really care about items on bottom
            // and row alternation isn't affected if items disappear on bottom
            // However, because entries only get deleted in AlternationCount packs, 
            // don't delete last items even if it means that the list is slightly over dynamiclistsize
            for (int i = _currentItems.Count; i > firstVisibleItemIndex + DynamicBufferSize / 2; i--)
                _currentItems.RemoveAt(_currentItems.Count - 1);
        }

        private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (_isTimerActive)
            {
                _isTimerActive = false;
                return;
            }
            if (!(sender is ScrollViewer) || !UseDynamicItemsSource)
                return;


            _cleanupTimer.Stop();
            if(e.VerticalChange > 0)
                _scrollOffset += e.VerticalChange;

            if (_scrollOffset > RowHeight && _listBegin + _currentItems.Count < _dynamicItems.Count)
            {
                // Append to end
                _scrollOffset -= RowHeight;
                _currentItems.Add(_dynamicItems[_listBegin + _currentItems.Count]);
            }

            // Restart timer for cleanup (doing cleanup while scrolling results in "jumps")
            if (e.VerticalChange > 0 || e.VerticalChange < 0)
                _cleanupTimer.Start();

            // Set scrollbar manually (scrollviewer gets wrong value if elements get unloaded)               
            DynamicScrollBarValue += e.VerticalChange;
            // Check if dynamicscrollbarvalue is within bounds
            DynamicScrollBarValue = Math.Min(DynamicScrollBarHeight, Math.Max(0, DynamicScrollBarValue));

            // Send notification if scrollviewer reached beginning and items will be loaded into scrollviewer
            ScrollViewer scrollViewer = sender as ScrollViewer;
            if (scrollViewer.VerticalOffset < RowHeight && _listBegin > 0)
                SendNotification(@"Bitte warten Sie während neue Elemente geladen werden.");
        }

        void UpdateScrollbarHeight(object sender, SizeChangedEventArgs e)
        {
            if (e.PreviousSize.Height < 1)
                return;
            DynamicScrollBarHeight += e.NewSize.Height - e.PreviousSize.Height;
        }

        #endregion

        #region Send notifications

        public event EventHandler<string> OnNotification;

        private void SendNotification(string message)
        {
            if (OnNotification != null)
                OnNotification(this, message);
        }

        #endregion
    }
}