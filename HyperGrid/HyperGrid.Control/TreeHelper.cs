﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace BlendedLibrary.HyperGrid.Control
{
    public static class TreeHelper
    {
        public static T TryFindChild<T>(DependencyObject obj) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child is T)
                    return (T)child;
                
                T childOfChild = TryFindChild<T>(child);
                if (childOfChild != null)
                    return childOfChild;
            }
            return null;
        }

        public static IEnumerable<T> TryFindChildren<T>(DependencyObject obj) where T : DependencyObject
        {
            List<T> children = new List<T>();

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child is T)
                    children.Add((T)child);
                else
                {
                    IEnumerable<T> childOfChild = TryFindChildren<T>(child);
                    if (childOfChild != null)
                        children.AddRange(childOfChild);
                }
            }
            return children;
        }

        public static T TryFindParent<T>(DependencyObject child) where T : DependencyObject
        {
            if (child == null)
                return null;

            //get parent item
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            //we’ve reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we’re looking for
            T parent = parentObject as T;
            //use recursion to proceed with next level
            return parent ?? TryFindParent<T>(parentObject);
        }
    }
}
