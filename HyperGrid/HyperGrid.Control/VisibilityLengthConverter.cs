﻿using System;
using System.Windows;
using System.Windows.Data;

namespace BlendedLibrary.HyperGrid.Control
{
    internal class VisibilityLengthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is int)
                return (int)value == 0 ? Visibility.Hidden : Visibility.Visible;

            return value == null ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
              System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    internal class VisibilityLengthConverterInverse : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (int)value != 0 ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
              System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}