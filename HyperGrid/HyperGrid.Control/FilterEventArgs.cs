﻿using System;
using System.Collections.ObjectModel;

namespace BlendedLibrary.HyperGrid.Control
{
    public class FilterEventArgs : EventArgs
    {
        public ObservableCollection<Object> Items { get; private set; }

        public FilterEventArgs(ObservableCollection<Object> items)
        {
            Items = items;
        }
    }
}
