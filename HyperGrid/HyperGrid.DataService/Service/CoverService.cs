﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using BlendedLibrary.HyperGrid.DataService.Domain;
using BlendedLibrary.HyperGrid.DataService.Extensions;
using BlendedLibrary.HyperGrid.DataService.Generator.Lookup;
using BlendedLibrary.HyperGrid.DataService.Properties;

namespace BlendedLibrary.HyperGrid.DataService.Service
{
    public class CoverService
    {
        private readonly Timer _timer;

        private readonly Queue<Medium2> _queue = new Queue<Medium2>();

        private readonly IList<IMetadataLookup<Medium2>> _metadataLookups;

        private readonly DirectoryInfo _imageDirectory;

        private readonly object _lock = new object();

        private static CoverService _instance;
        public static CoverService Instance
        {
            get { return _instance ?? (_instance = new CoverService()); }
        }

        private CoverService()
        {
            _timer = new Timer(500) { AutoReset = true };
            _timer.Elapsed += TimerElapsed;

            _imageDirectory = new DirectoryInfo(Settings.Default.ImageDirectory);
            if (!_imageDirectory.Exists)
                _imageDirectory.Create();

            _metadataLookups = new List<IMetadataLookup<Medium2>>
                                   {
                                       /*new GoogleMetadataLookup(_imageDirectory),*/
                                       new NoCoverBookMetadataLookup(_imageDirectory, mediums => (from medium in mediums where medium.ImageUrl == null select medium)),
                                       new LiberoAvailabilityMetadataLookup(_imageDirectory)
                                   };
        }

        void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            // Dequeue remaining items
            lock (_lock)
            {
                if (!_queue.Any()) return;

                DoLookup(_queue.ToArray());
                _queue.Clear();
            }

            _timer.Stop();
        }

        public void Resolve(Medium2 medium)
        {
            _timer.Stop();

            lock (_lock)
            {
                if (_queue.Contains(medium))
                    return;

                _queue.Enqueue(medium);

                while (_queue.Count >= 10)
                {
                    DoLookup(_queue.Dequeue(10).ToArray());
                }
            }

            _timer.Start();
        }

        private void DoLookup(IEnumerable<Medium2> items)
        {
            Task.Factory.StartNew(() =>
                                      {
                                          foreach (var lookup in _metadataLookups)
                                          {
                                              lookup.Lookup(items);
                                          }
                                      });
        }
    }
}
