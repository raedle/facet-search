﻿using System;
using System.Drawing;
using System.Linq;
using System.Text;
using BlendedLibrary.Data.Domain;
using BlendedLibrary.HyperGrid.DataService.Properties;

namespace BlendedLibrary.HyperGrid.DataService.Domain
{
    public static class MediumExtensions
    {
        public static string GetImageIdentifier(this Medium medium)
        {
            var id = medium.GetCleanISBN() ??
                     new string((from char c in medium.LibraryId where char.IsLetterOrDigit(c) select c).ToArray());

            if (string.IsNullOrEmpty(id))
                throw new Exception("Image identifier is null");

            return id;
        }

        public static string GetCleanISBN(this Medium medium)
        {
            if (medium.ISBNs == null || medium.ISBNs.Count < 1)
                return null;

            if (string.IsNullOrWhiteSpace(medium.ISBNs.First()))
                return null;

            return new string((from char c in medium.ISBNs.First() where char.IsLetterOrDigit(c) select c).ToArray());
        }

        public static bool IsAvailable(this Medium2 medium)
        {
            if (medium.Availabilities == null)
                return false;

            return medium.Availabilities.Count(a => "available".Equals(a.Status) && !string.IsNullOrWhiteSpace(a.CallNumber)) > 0;
        }

        public static string AvailabilitiesToString(this Medium2 medium)
        {
            if (medium.Availabilities.Count == 0)
                return "-";

            var sb = new StringBuilder();
            foreach (var availability in medium.Availabilities.Take(medium.Availabilities.Count - 1))
            {
                sb.AppendFormat("{0}\r\n", availability);
            }
            sb.AppendFormat("{0}", medium.Availabilities.Last());

            return sb.ToString();
        }

        public static Bitmap GetBitmap(this Medium medium)
        {
            switch (medium.Type)
            {
                case "Buch":
                    return Resources.BuchImage;
                case "DVD":
                    return Resources.DVDImage;
                case "Zeitschrift":
                    return Resources.DefaultMediumImage;
                case "Kartenmaterial":
                    return Resources.DefaultMediumImage;
                case "Mikrofiche":
                    return Resources.MikroficheImage;
                case "Video":
                    return Resources.VideoImage;
                case "Diskette":
                    return Resources.DefaultMediumImage;
                case "Tonträger":
                    return Resources.DefaultMediumImage;
                case "CD-Rom":
                    return Resources.CdRomImage;
                case "diverse":
                    return Resources.DefaultMediumImage;
                default:
                    return Resources.DefaultMediumImage;
            }
        }
    }
}
