﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Xml.Serialization;
using BlendedLibrary.Data.Domain;
using BlendedLibrary.HyperGrid.DataService.Generator.Lookup;
using BlendedLibrary.HyperGrid.DataService.Properties;

namespace BlendedLibrary.HyperGrid.DataService.Domain
{
    [XmlRoot("medium")]
    public class Medium2 : Medium, ILookable, INotifyPropertyChanged
    {
        #region private fields

        private bool _isResolving;

        #endregion

        #region properties

        #region ImageUrl

        private string _imageUrl;

        [XmlElement("cover_url")]
        public new string ImageUrl
        {
            get
            {
                return _imageUrl;
            }

            set
            {
                _imageUrl = value;
                OnPropertyChanged("ImageUrl");
            }
        }

        #endregion

        #region Availabilities

        private bool _isFetchingAvailabilities = true;

        public bool IsFetchingAvailabilities
        {
            get
            {
                return _isFetchingAvailabilities;
            }

            set
            {
                _isFetchingAvailabilities = value;

                if (!_isFetchingAvailabilities && BestStatus.Equals("Suchen..."))
                    BestStatus = "Keine Exemplare gefunden!";

                OnPropertyChanged("IsFetchingAvailabilities");
            }
        }

        private object _availabilitiesLock = new object();

        private ObservableCollection<Availability> _availabilities;

        [XmlArray("availabilities")]
        [XmlArrayItem("availability")]
        public ObservableCollection<Availability> Availabilities
        {
            get { return _availabilities; }
            set
            {
                if (_availabilities != null)
                    _availabilities.CollectionChanged -= AvailabilitiesCollectionChanged;
                
                _availabilities = value;
                OnPropertyChanged("Availabilities");
                OnPropertyChanged("Signature0");

                BindingOperations.EnableCollectionSynchronization(_availabilities, _availabilitiesLock);

                if (_availabilities != null)
                    _availabilities.CollectionChanged += AvailabilitiesCollectionChanged;
            }
        }

        #region FilteredAvailabilities

        private ObservableCollection<Availability> _filteredAvailabilities;
        [XmlIgnore]
        public ObservableCollection<Availability> FilteredAvailabilities
        {
            get
            {
                UpdateFilteredAvailabilities();
                return _filteredAvailabilities;
            }
            private set
            {
            }
        }

        private void UpdateFilteredAvailabilities()
        {
            _filteredAvailabilities = new ObservableCollection<Availability>();
            foreach (var a in _availabilities)
            {
                if ((a.LocationString != null &&
                    a.StatusMessage != null &&
                    a.CallNumber != null) &&
                    !(a.LocationString.Equals(".") && a.StatusMessage.Equals(".") && a.CallNumber.Contains("weitere Bücher")))
                    _filteredAvailabilities.Add(a);
            }
        }

        #endregion 

        void AvailabilitiesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("Signature0");

            //Set BestStatus
            BestStatus = "Keine Exemplare gefunden!";

            foreach (var item in _availabilities)
            {
                Availability avail = item as Availability;

                if (avail.StatusMessage == null)
                    continue;
                if (avail.StatusMessage.Equals("Verfügbar") || avail.StatusMessage.StartsWith("Verfügbar"))
                {
                    BestStatus = "Verfügbar";
                    break;
                }
                else if (avail.StatusMessage.Equals("Apparat") || avail.StatusMessage.StartsWith("laufende"))
                    BestStatus = avail.StatusMessage;
                else if (BestStatus.Equals("Keine Exemplare gefunden!"))  //Only set this if no other can be found!
                    BestStatus = avail.StatusMessage;
            }
        }

        #endregion

        #region Signature0

        public string Signature0
        {
            get
            {
                return Availabilities.Count == 0 ? "" : Availabilities.First().CallNumber;
            }
        }

        #endregion

        #region BestStatus
        private string _bestStatus = "Suchen...";

        public string BestStatus
        {
            get
            {
                return _bestStatus;
            }

            set
            {
                _bestStatus = value;
                OnPropertyChanged("BestStatus");
            }
        }

        #endregion

        #endregion

        public override string ToString()
        {
            return "Medium[Id=" + ID + ",LibraryId=" + LibraryId + ",ParallelId=" + ParallelId + ",Title=" + Title + ",Subtitle=" + Subtitle + ",Authors=" + Authors + ",Institution=" + Institution + ",Year=" + Year + ",Format=" + Format + ",Note=" + Note + ",Signature=" + Signature + ",Url=" + Url + ",ISBN=" + ISBNs + ",ISSN=" + ISSN + ",Description=" + Description + ",Town=" + Town + ",Publisher=" + Publisher + ",Category=" + Category + ",Detail=" + Detail + ",Keywords=" + Keywords + ",Language=" + Language + ",ImageUrl=" + ImageUrl + "]";
        }

        #region Implement INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
