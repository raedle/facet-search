﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace BlendedLibrary.HyperGrid.DataService.Domain
{
    [XmlRoot("availabilityItems")]
    public class AvailabilityItems
    {
        [XmlElement("availabilities")]
        public List<Availabilities> Availabilities;

        public Availabilities GetAvailabilities(string id)
        {
            return Availabilities == null ? null : Availabilities.First(a => a.ID.Equals(id));
        }
    }

    [XmlType("availabilities")]
    public class Availabilities
    {
        [XmlAttribute("id")]
        public string ID { get; set; }

        [XmlElement("availability")]
        public List<Availability> Availability;
    }

    [XmlType("availability")]
    public class Availability
    {
        [XmlIgnore]
        public string ID { get; set; }

        [XmlAttribute("callNumber")]
        public string CallNumber { get; set; }

        [XmlAttribute("locationString")]
        public string LocationString { get; set; }

        [XmlAttribute("status")]
        public string Status { get; set; }

        [XmlAttribute("statusMessage")]
        public string StatusMessage { get; set; }

        [XmlAttribute("displayString")]
        public string DisplayString { get; set; }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}, {3}, {4}", CallNumber, LocationString, Status, StatusMessage, DisplayString);
        }
    }
}
