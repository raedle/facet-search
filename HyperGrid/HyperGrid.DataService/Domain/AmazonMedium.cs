﻿using System.Xml.Serialization;

namespace BlendedLibrary.HyperGrid.DataService.Domain
{
    [XmlRoot("Medium")]
    public class AmazonMedium : Medium2
    {
        [XmlElement("Rating")]
        public decimal Rating
        {
            get;
            set;
        }

        [XmlElement("DetailPageUrl")]
        public string DetailPageUrl
        {
            get;
            set;
        }
    }
}
