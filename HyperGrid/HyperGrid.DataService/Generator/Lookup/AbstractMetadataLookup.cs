﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace BlendedLibrary.HyperGrid.DataService.Generator.Lookup
{
    public abstract class AbstractMetadataLookup<T> : IMetadataLookup<T> where T : ILookable
    {
        #region private

        private readonly Func<IEnumerable<T>, IEnumerable<T>> _constrainValues;

        #endregion

        #region protected

        protected readonly DirectoryInfo ImageDirectory;

        #endregion

        #region ctor

        protected AbstractMetadataLookup(DirectoryInfo imageDirectory, Func<IEnumerable<T>, IEnumerable<T>> constrainValues)
        {
            ImageDirectory = imageDirectory;
            _constrainValues = constrainValues;
        }

        #endregion

        /// <summary>
        /// Saves an image to hard disk.
        /// </summary>
        /// <param name="image">image origin</param>
        /// <param name="imageFileUrl">image will be saved at given url</param>
        protected static void SaveImageToDisk(Image image, string imageFileUrl)
        {
            var file = new FileInfo(imageFileUrl);
            if (file.Directory != null && !file.Directory.Exists)
                file.Directory.Create();

            try
            {
                image.Save(imageFileUrl);
                image.Dispose();
            }
            catch (Exception)
            {
                Console.WriteLine(@"Fatal Exception on saving or disposing image");
            }
            
        }

        /// <summary>
        /// Generates an image file name.
        /// </summary>
        /// <param name="id">id will be used for image name</param>
        /// <param name="imageType">type of the image</param>
        /// <param name="subFolders">a bunch of sub folders</param>
        /// <returns>image file name</returns>
        protected string GetImageFileName(string id, string imageType, params string[] subFolders)
        {
            var imageFile = new StringBuilder();
            imageFile.AppendFormat(@"{0}", ImageDirectory.FullName);

            if (subFolders.Length > 0)
                foreach (var subFolder in subFolders)
                {
                    imageFile.AppendFormat(@"\{0}", subFolder);
                }
            return imageFile.AppendFormat(@"\{0}\{1}.{2}", id.First(), id, imageType).ToString();
        }

        #region Implementation of IMetadataLookup<T>

        public abstract string Name { get; }

        public int Lookup(IEnumerable<T> values)
        {
            var constrainedValues = values;
            if (_constrainValues != null)
                 constrainedValues = _constrainValues(values).ToArray();

            var count = constrainedValues.Count();

            Console.WriteLine(@"Request metadata for {0} mediums using '{1}'", count, Name);

            var foundMetadata = LookupWithConstrainedValues(constrainedValues);

            Console.WriteLine(@"Metadata for {0} mediums requested with {1} and found {2}", count, Name, foundMetadata);

            return foundMetadata;
        }

        public abstract int LookupWithConstrainedValues(IEnumerable<T> values);

        #endregion
    }
}
