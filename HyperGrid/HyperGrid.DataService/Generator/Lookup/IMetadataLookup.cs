﻿using System.Collections.Generic;

namespace BlendedLibrary.HyperGrid.DataService.Generator.Lookup
{
    public interface IMetadataLookup<in T> where T : ILookable
    {
        string Name { get; }
        int Lookup(IEnumerable<T> mediums);
    }
}
