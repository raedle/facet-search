﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Serialization;
using BlendedLibrary.HyperGrid.DataService.Domain;
using BlendedLibrary.HyperGrid.DataService.Extensions;
using BlendedLibrary.HyperGrid.DataService.Properties;
using GalaSoft.MvvmLight.Threading;
using NLog;
using Tools.BaseX;

namespace BlendedLibrary.HyperGrid.DataService.Generator.Lookup
{
    public class LiberoAvailabilityMetadataLookup : AbstractMetadataLookup<Medium2>
    {
        #region Logger

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region private

        private const string HttpRequest =
            "http:send-request(<http:request method='get' status-only='false'/>, '{0}')/response/availabilityItems";

        private const string SummonServiceUrl = "http://libero.ub.uni-konstanz.de/ub/User.SummonXMLServer.cls?{0}";

        //private readonly BaseXSession _session;
        private readonly BaseXClient _client;

        private readonly XmlSerializer _serializer;

        #endregion

        public LiberoAvailabilityMetadataLookup(DirectoryInfo imageDirectory)
            : base(imageDirectory, null)
        {
            var s = Settings.Default;
            //_session = new BaseXSession(s.DatabaseHost, s.DatabasePort, s.DatabaseUsername, s.DatabasePassword);
            _client = new BaseXClient(s.DatabaseHost, s.DatabasePort, s.DatabaseUsername, s.DatabasePassword, s.DatabaseName);

            _serializer = new XmlSerializer(typeof(AvailabilityItems));
        }

        #region Overrides of AbstractMetadataLookup<Medium2>

        public override string Name
        {
            get { return "Libero Availability Metadata Lookup"; }
        }

        public override int LookupWithConstrainedValues(IEnumerable<Medium2> values)
        {
            var sb = new StringBuilder();

            var copy = values.ToArray();
            foreach (var chunks in copy.Chunk(Settings.Default.LiberoParallelStatusRequest))
            {
                foreach (var medium in chunks.Take(chunks.Count() - 1))
                {
                    sb.AppendFormat("s.id={0}&", medium.LibraryId);
                }
                sb.AppendFormat("s.id={0}", chunks.Last().LibraryId);

                var url = string.Format(SummonServiceUrl, HttpUtility.HtmlEncode(sb));
                var query = string.Format(HttpRequest, url);

                string[] result = null;
                try
                {
                    //result = _session.Query(query).ToArray();
                    result = _client.Query(query).ToArray();
                }
                catch (Exception e)
                {
                    if (Logger.IsErrorEnabled)
                        Logger.ErrorException("Could not retrieve availabilites", e);
                }

                if (result != null && result.Any())
                {
                    var reader = new StringReader(result.Single());
                    var availabilityItems = (AvailabilityItems)_serializer.Deserialize(reader);

                    foreach (var medium in chunks)
                    {
                        var availabilities = availabilityItems.GetAvailabilities(medium.LibraryId);

                        if (medium.Availabilities == null)
                            medium.Availabilities = new ObservableCollection<Availability>();

                        DispatcherHelper.RunAsync(delegate
                                                         {
                                                             foreach (var availability in availabilities.Availability)
                                                             {
                                                                 medium.Availabilities.Add(availability);
                                                             }

                                                             // the last availablility seems to be additional libero info
                                                             if (medium.Availabilities.Count > 0)
                                                                 medium.Availabilities.Remove(medium.Availabilities.Last());

                                                             foreach (var availability in medium.Availabilities)
                                                             {
                                                                 availability.ID = availabilities.ID;
                                                             }

                                                             medium.IsFetchingAvailabilities = false;
                                                         });
                    }
                }

                sb.Clear();
            };

            return values.Count();
        }

        #endregion
    }
}
