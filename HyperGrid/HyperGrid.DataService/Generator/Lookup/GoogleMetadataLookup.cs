﻿using System;
using System.Collections.Generic;
using System.IO;
using BlendedLibrary.HyperGrid.DataService.Domain;

namespace BlendedLibrary.HyperGrid.DataService.Generator.Lookup
{
    public class GoogleMetadataLookup : AbstractMetadataLookup<Medium2>
    {
        #region ctor

        public GoogleMetadataLookup(DirectoryInfo imageDirectory, Func<IEnumerable<Medium2>, IEnumerable<Medium2>> constrainValues)
            : base(imageDirectory, constrainValues)
        {
        }

        #endregion

        #region IMetadataLookup

        public override string Name
        {
            get { return "Google Metadata Lookup"; }
        }

        public override int LookupWithConstrainedValues(IEnumerable<Medium2> mediums)
        {
            //new Google.GData.Client.Service("print")
            throw new NotImplementedException();
        }

        #endregion
    }
}
