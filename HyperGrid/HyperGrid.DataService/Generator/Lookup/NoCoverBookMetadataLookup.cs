﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BlendedLibrary.HyperGrid.DataService.Domain;
using BlendedLibrary.HyperGrid.DataService.Utils;

namespace BlendedLibrary.HyperGrid.DataService.Generator.Lookup
{
    public class NoCoverBookMetadataLookup : AbstractMetadataLookup<Medium2>
    {
        #region ctor

        public NoCoverBookMetadataLookup(DirectoryInfo imageDirectory, Func<IEnumerable<Medium2>, IEnumerable<Medium2>> constrainValues)
            : base(imageDirectory, constrainValues)
        {
        }

        #endregion

        #region Overrides of AbstractMetadataLookup<Medium2>

        public override string Name
        {
            get { return "No Cover Book Metadata Lookup"; }
        }

        public override int LookupWithConstrainedValues(IEnumerable<Medium2> mediums)
        {
            foreach (var medium in mediums)
            {
                var imageSource = ImageUriHelper.GetImageUri(ImageDirectory, medium);

                if (imageSource == null)
                {
                    var id = medium.GetImageIdentifier();
                    imageSource = GetImageFileName(id, "jpg", "LibraryId");

                    var bitmap = medium.GetBitmap();

                    SaveImageToDisk(bitmap, imageSource);
                }
                medium.ImageUrl = imageSource;
            }

            return mediums.Count();
        }

        #endregion
    }
}
