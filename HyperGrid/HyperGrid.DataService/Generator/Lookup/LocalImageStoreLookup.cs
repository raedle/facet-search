﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BlendedLibrary.HyperGrid.DataService.Domain;
using BlendedLibrary.HyperGrid.DataService.Properties;

namespace BlendedLibrary.HyperGrid.DataService.Generator.Lookup
{
    internal class LocalImageStoreLookup : AbstractMetadataLookup<Medium2>
    {
        #region private

        private readonly DirectoryInfo _localImageStoreDirectory;

        #endregion

        #region ctor

        protected LocalImageStoreLookup(DirectoryInfo imageDirectory, Func<IEnumerable<Medium2>, IEnumerable<Medium2>> constrainValues)
            : base(imageDirectory, constrainValues)
        {
            var s = Settings.Default;
            _localImageStoreDirectory = new DirectoryInfo(string.Format(@"{0}\{1}", imageDirectory.FullName, s.LocalImageStore));
        }

        #endregion

        public override int LookupWithConstrainedValues(IEnumerable<Medium2> values)
        {
            var found = 0;
            foreach (var m in values)
            {
                try
                {
                    var imageFile = _localImageStoreDirectory.GetFiles(string.Format("{0}", m.GetCleanISBN()), SearchOption.AllDirectories).Single();
                    m.ImageUrl = imageFile.FullName;
                    found++;
                }
                catch
                {
                    // ignore
                }
            }
            return found;
        }

        public override string Name
        {
            get { return "Local Image Store Lookup"; }
        }
    }
}
