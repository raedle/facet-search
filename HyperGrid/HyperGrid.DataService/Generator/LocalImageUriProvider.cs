﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FacetStreams.PivotDataService.Domain;

namespace FacetStreams.PivotDataService.Generator
{
    internal class LocalImageUriProvider
    {
        internal static string GetImageUri(DirectoryInfo imageDirectory, string id)
        {
            // Returns the filepath if image for isbn already exists
            var files = imageDirectory.GetFiles(string.Format("{0}.*", id), SearchOption.AllDirectories);
            return files.Length > 0 ? files[0].FullName : null;
        }

        internal static string GetImageUri(DirectoryInfo imageDirectory, Medium medium)
        {
            return GetImageUri(imageDirectory, medium.GetImageIdentifier());
        }
    }
}
