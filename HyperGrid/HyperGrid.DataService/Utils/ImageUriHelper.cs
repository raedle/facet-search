﻿using System;
using System.IO;
using System.Net;
using BlendedLibrary.Data.Domain;
using BlendedLibrary.HyperGrid.DataService.Domain;
using BlendedLibrary.HyperGrid.DataService.Properties;

namespace BlendedLibrary.HyperGrid.DataService.Utils
{
    public class ImageUriHelper
    {
        public static string GetImageUri(DirectoryInfo imageDirectory, string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return null;

            // Returns the filepath if image for id already exists
            foreach (var subDirectory in Settings.Default.ImageSubDirectories)
            {
                foreach (var i in new []{1, 5})
                {
                    string path;
                    try
                    {
                        path = Path.Combine(imageDirectory.FullName, subDirectory, id.Substring(0, i));
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    
                    var directory = new DirectoryInfo(path);
                    if (!directory.Exists)
                        continue;

                    var fileInfo = new FileInfo(Path.Combine(directory.FullName, string.Format("{0}.jpg", id)));
                    if (fileInfo.Exists)
                        return fileInfo.FullName;

                    fileInfo = new FileInfo(Path.Combine(directory.FullName, string.Format("{0}.png", id)));
                    if (fileInfo.Exists)
                        return fileInfo.FullName;

                    fileInfo = new FileInfo(Path.Combine(directory.FullName, string.Format("{0}.gif", id)));
                    if (fileInfo.Exists)
                        return fileInfo.FullName;

                    fileInfo = new FileInfo(Path.Combine(directory.FullName, string.Format("{0}.jpeg", id)));
                    if (fileInfo.Exists)
                        return fileInfo.FullName;

                    // fallback
                    //var files = directory.GetFiles(string.Format("{0}.*", id), SearchOption.TopDirectoryOnly);

                    //if (files.Length > 0)
                    //    return files[0].FullName;
                }
                //for (var i = 1; i < 5 && i < id.Length; i++)
                //{
                    //var path = Path.Combine(imageDirectory.FullName, subDirectory, id.Substring(0, i));
                    //var directory = new DirectoryInfo(path);
                    //if (!directory.Exists)
                    //    continue;

                    //var fileInfo = new FileInfo(Path.Combine(directory.FullName, string.Format("{0}.jpg", id)));

                    //if (fileInfo.Exists)
                    //    return fileInfo.FullName;

                    //fileInfo = new FileInfo(Path.Combine(directory.FullName, string.Format("{0}.png", id)));
                    //if (fileInfo.Exists)
                    //    return fileInfo.FullName;

                    //// fallback
                    //var files = directory.GetFiles(string.Format("{0}.*", id), SearchOption.TopDirectoryOnly);

                    //if (files.Length > 0)
                    //    return files[0].FullName;
                //}
            }
            return null;
        }

        public static string GetImageUri(DirectoryInfo imageDirectory, Medium medium)
        {
            return GetImageUri(imageDirectory, medium.GetImageIdentifier());
        }

        public static string GetDeepZoomImagePath(string id)
        {
            var s = Settings.Default;
            var basePath = Path.Combine(s.ImageDirectory, s.DeepZoomImagesSubPath);

            var subPath = id.Substring(0, id.Length > 5 ? 5 : 1);

            return Path.Combine(basePath, subPath, string.Format(@"{0}.dzi", id));
        }
        
        /// <summary>
        /// Function to download Image from website
        /// </summary>
        /// <param name="url">URL address to download image</param>
        /// <returns>Image</returns>
        public static System.Drawing.Image DownloadImage(string url)
        {
            System.Drawing.Image tmpImage = null;
            try
            {
                // Open a connection
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

                httpWebRequest.AllowWriteStreamBuffering = true;

                // You can also specify additional header values like the user agent or the referer: (Optional)
                //httpWebRequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)";
                //httpWebRequest.Referer = "http://www.google.com/";

                // set timeout for 20 seconds (Optional)
                httpWebRequest.Timeout = 20000;

                // Request response:
                var webResponse = httpWebRequest.GetResponse();

                // Open data stream:
                var webStream = webResponse.GetResponseStream();

                // convert webstream to image
                if (webStream != null) tmpImage = System.Drawing.Image.FromStream(webStream);

                // Cleanup
                webResponse.Close();
            }
            catch (Exception e)
            {
                // Error
                Console.WriteLine(@"Exception caught in process: {0}", e);
                return null;
            }

            return tmpImage;
        }
    }
}
