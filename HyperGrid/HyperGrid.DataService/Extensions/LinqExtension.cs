﻿using System.Collections.Generic;
using System.Linq;

namespace BlendedLibrary.HyperGrid.DataService.Extensions
{
    internal static class LinqExtension
    {
        /// <summary>
        /// Break a list of items into chunks of a specific size
        /// Todo: If a list count does not return % = 0 then the last values are ignored!!??
        /// </summary>
        public static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> source, int chunksize)
        {
            while (source.Any())
            {
                yield return source.Take(chunksize);
                source = source.Skip(chunksize);
            }
        }
    }
}
