﻿using System.Collections.Generic;

namespace BlendedLibrary.HyperGrid.DataService.Extensions
{
    public static class QueueExtensions
    {
        public static IEnumerable<T> Dequeue<T>(this Queue<T> queue, int take)
        {
            for (var i = 0; i < take; i++)
                yield return queue.Dequeue();
        }
    }
}
