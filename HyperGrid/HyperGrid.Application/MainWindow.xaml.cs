﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using BlendedLibrary.HyperGrid.Control;
using BlendedLibrary.HyperGrid.Controls;
using BlendedLibrary.HyperGrid.DataService.Domain;
using BlendedLibrary.HyperGrid.Properties;
using BlendedLibrary.HyperGrid.Service;
using BlendedLibrary.HyperGrid.Utils;
using BlendedLibrary.HyperGrid.ViewModel;
using GalaSoft.MvvmLight.Threading;
using DataGridCell = System.Windows.Controls.DataGridCell;
using DataObject = System.Windows.DataObject;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MouseEventArgs = System.Windows.Input.MouseEventArgs;

namespace BlendedLibrary.HyperGrid
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        #region dependency properties

        #region Basket

        public ObservableCollection<BasketViewModel> Basket
        {
            get { return (ObservableCollection<BasketViewModel>)GetValue(BasketProperty); }
            set { SetValue(BasketProperty, value); }
        }

        public static readonly DependencyProperty BasketProperty =
            DependencyProperty.Register("Basket", typeof(ObservableCollection<BasketViewModel>),
                typeof(MainWindow), new PropertyMetadata(new ObservableCollection<BasketViewModel>()));

        #endregion

        #region EnableSplitView

        public bool EnableSplitView
        {
            get { return (bool)GetValue(EnableSplitViewProperty); }
            set { SetValue(EnableSplitViewProperty, value); }
        }

        public static readonly DependencyProperty EnableSplitViewProperty =
            DependencyProperty.Register("EnableSplitView", typeof(bool), typeof(MainWindow), new PropertyMetadata(false));


        #endregion

        #endregion

        private readonly DispatcherTimer frontTimer;

        public MainWindow()
        {
            InitializeComponent();
            
            Loaded += RestoreLastWindowConfiguration;
            Closing += SaveWindowConfiguration;

            // Display Information about items during filtering
            DataGridView.OnFilterClear += hyperGrid_OnFilterClear;
            DataGridView.OnFiltering += hyperGrid_OnFiltering;
            DataGridView.OnFilterStart += hyperGrid_OnFilterStart;

            // Subscribe to drag and drop events
            Loaded += AddDragDropHandler;

            // Subscribe to MainViewModel events
            (DataContext as MainViewModel).ItemsChanged += OnItemsChanged;

            // Disable window moving when scrolling at list end / beginning
            Loaded += (sender, args) =>
                {
                    TreeHelper.TryFindChild<ScrollViewer>(DataGridView).ManipulationBoundaryFeedback +=
                        (o, eventArgs) => eventArgs.Handled = true;
                    TreeHelper.TryFindChild<ScrollViewer>(BasketView).ManipulationBoundaryFeedback +=
                        (o, eventArgs) => eventArgs.Handled = true;
                };

            // Swiping
            _swipeStopWatch = new Stopwatch();
            EnableSplitView = true;

            // Reset QR Image if basket is empty
            Basket.CollectionChanged += (obj, e) => BasketService.OnBasketChanged();

            // Display notifications from HyperGrid
            DataGridView.OnNotification += (sender, s) => DisplayNotification(s, Notification.Info);
        }

        private void OnItemsChanged(object sender, EventArgs eventArgs)
        {
            // Close DetailImageView if image is being displayed
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    OverlayDetailImage.CloseDetailImage(this);
                });
        }

        #region HyperGrid Events
        void hyperGrid_OnFilterStart(object sender, EventArgs e)
        {
            FilterTextBlock.Visibility = Visibility.Visible;
        }

        void hyperGrid_OnFiltering(object sender, FilterEventArgs e)
        {
            FilterTextBlock.DataContext = e.Items;
        }

        void hyperGrid_OnFilterClear(object sender, EventArgs e)
        {
            FilterTextBlock.Visibility = Visibility.Collapsed;
        }

        #endregion

        #region window location managment

        /// <summary>
        /// Saves the window configuration.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SaveWindowConfiguration(object sender, CancelEventArgs e)
        {
            var s = WindowSettings.Default;

            var screen = Screen.FromHandle(new WindowInteropHelper(this).Handle);

            s.ScreenName = screen.DeviceName;
            s.X = Left;
            s.Y = Top;
            s.Width = Width;
            s.Height = Height;
            s.WindowState = WindowState;

            s.Save();
        }

        /// <summary>
        /// Restores the window configuration as it was when the window
        /// was closed last time.
        /// </summary>
        private void RestoreLastWindowConfiguration(object sender, RoutedEventArgs e)
        {
            var s = WindowSettings.Default;

            var left = s.X;
            var top = s.Y;
            var width = s.Width;
            var height = s.Height;
            var windowState = s.WindowState;

            if (!string.IsNullOrWhiteSpace(s.ScreenName))
            {
                var screen = Screen.AllScreens.SingleOrDefault(scn => scn.DeviceName.Equals(s.ScreenName));
                if (windowState == WindowState.Maximized && screen != null)
                {
                    WindowState = WindowState.Normal;
                    Left = left;// screen.WorkingArea.Left + 10;
                    Top = top;//screen.WorkingArea.Top + 10;
                    ToFullScreen();
                }
                else
                {
                    Left = left;
                    Top = top;
                }

                Width = width;
                Height = height;
                WindowState = windowState;
            }
        }

        private void OnToggleFullScreen(object sender, KeyEventArgs e)
        {
#if DEBUG
            if (e.IsRepeat) return;

            switch (e.Key)
            {
                case Key.Escape:
                    ToggleFullScreen();
                    break;
            }
#endif
        }

        private void ToggleFullScreen()
        {
            if (WindowState == WindowState.Maximized)
            {
                ToNormalScreen();
            }
            else
            {
                ToFullScreen();
            }
        }

        private void ToFullScreen()
        {
            WindowState = WindowState.Maximized;
            WindowStyle = WindowStyle.None;
        }

        private void ToNormalScreen()
        {
            WindowState = WindowState.Normal;
            WindowStyle = WindowStyle.ThreeDBorderWindow;
        }

        #endregion

        #region Drag&Drop
        #region Helpers
        private DragDropHelper _dragDropHelper;

        private void AddDragDropHandler(object sender, RoutedEventArgs routedEventArgs)
        {
            _dragDropHelper = new DragDropHelper(this);

            // Drop events
            _dragDropHelper.AddDropHandler(InsertDropTarget, AddToBasket);
            _dragDropHelper.AddDropHandler(DeleteDropTarget, RemoveFromBasket);
            _dragDropHelper.AddDropHandler(BasketContainer, InsertInBasket);
            _dragDropHelper.AddDropHandler(this, CatchFromBasket);

            // Dragover events
            _dragDropHelper.AddDragOverHandler(this, MoveDragPreview);
            _dragDropHelper.AddDragOverHandler(BasketContainer, MoveDropIndicator);

            // DragEnter events
            _dragDropHelper.AddDragEnterHandler(InsertDropTarget, DropTarget_DragEnter);
            _dragDropHelper.AddDragEnterHandler(DeleteDropTarget, DropTarget_DragEnter);

            // DragLeave events
            _dragDropHelper.AddDragLeaveHandler(InsertDropTarget, DropTarget_DragLeave);
            _dragDropHelper.AddDragLeaveHandler(DeleteDropTarget, DropTarget_DragLeave);

            // Clean up
            _dragDropHelper.AddDragEndHandler((o, args) =>
                {
                    // Reenable buttons (they are disabled during drag)
                    // TODO: bad solution...
                    PrintButton.IsHitTestVisible = true;
                    ClearButton.IsHitTestVisible = true;
                    QrButton.IsHitTestVisible = true;

                    _isDragInProgress = false;

                    //Reset everything
                    InsertDropTarget.Visibility = Visibility.Collapsed;
                    InsertDropTarget.BorderThickness = new Thickness(0);

                    DeleteDropTarget.Visibility = Visibility.Collapsed;
                    DeleteDropTarget.BorderThickness = new Thickness(0);

                    DropIndexIndicator.Visibility = Visibility.Collapsed;
                    DragPreview.Visibility = Visibility.Collapsed;
                });
        }

        private int CalculateDropIndex(DragDropEventArgs e)
        {
            ScrollViewer basketScrollViewer = TreeHelper.TryFindChild<ScrollViewer>(BasketView);
            if (basketScrollViewer == null)
                return 0;

            double mousePosition = Math.Max(e.Position.Y, 0);
            double offset = basketScrollViewer.ContentVerticalOffset - BasketView.RowHeight / 2 + 5;

            int basketPos = (int)Math.Round((mousePosition + offset) / BasketView.RowHeight);
            return Math.Min(basketPos, Basket.Count);
        }

        private BasketViewModel GenerateBasketViewModel(MediumViewModel mvm, Availability availability)
        {
            Medium2 medium = new Medium2
                {
                    Title = mvm.Model2.Title,
                    Year = mvm.Model2.Year,
                    Description = mvm.Model2.Description,
                    BestStatus = availability.StatusMessage,
                    Signature = availability.CallNumber
                };

            BasketViewModel basketitem = new BasketViewModel
                {
                    Model2 = medium,
                    CoverUrl = mvm.CoverUrl,
                    AuthorsAsList = mvm.AuthorsAsList,
                    Location = availability.LocationString
                };

            return basketitem;
        }
        #endregion

        #region Dragging

        private void StartDragFromOverview(object sender, TouchEventArgs e)
        {
            FrameworkElement source = sender as FrameworkElement;
            DataGridRow row = TreeHelper.TryFindParent<DataGridRow>(source);

            if (row == null) return;

            MediumViewModel mvm = row.DataContext as MediumViewModel;

            // If still searching, return
            if (mvm != null && mvm.Model2.IsFetchingAvailabilities)
            {
                DisplayNotification(ResourcesLocator.GetString("AvailabilitySearchInProgress"), Notification.Info);
                return;
            }

            // Get best possible availability
            Availability bestAvailability = null;

            if (mvm != null)
                foreach (var availability in mvm.Model2.Availabilities.Where(availability => availability.StatusMessage != null))
                {
                    if (availability.StatusMessage.Equals("Verfügbar") || availability.StatusMessage.StartsWith("Verfügbar"))
                    {
                        bestAvailability = availability;
                        break;
                    }
                    if (availability.StatusMessage.Equals("Apparat") || availability.StatusMessage.StartsWith("laufende"))
                        bestAvailability = availability;
                    else if (bestAvailability == null)  //Only set this if no other can be found!
                        bestAvailability = availability;
                }

            // Don't initiate drag if there are no availabilites
            if (bestAvailability == null)
            {
                DisplayNotification(ResourcesLocator.GetString("NoCopiesAvailable"), Notification.Info);
                return;
            }

            // Standard drag&drop
            BasketViewModel bvm = GenerateBasketViewModel(mvm, bestAvailability);
            DoDragEvent(bvm, InsertDropTarget, "Overview", e.TouchDevice);

            e.Handled = true;
        }

        private void StartDragFromBasket(object sender, TouchEventArgs e)
        {
            FrameworkElement source = sender as FrameworkElement;
            DataGridRow row = TreeHelper.TryFindParent<DataGridRow>(source);

            if (row != null && row.DataContext is BasketViewModel)
            {
                // Position of Delete DropTarget depending on Splitview
                DeleteDropTarget.HorizontalAlignment = _inSplitView ? HorizontalAlignment.Left : HorizontalAlignment.Right;

                BasketViewModel basketitem = row.DataContext as BasketViewModel;

                // Delete original value from basket (for reordering)
                Basket.Remove(basketitem);

                DoDragEvent(basketitem, DeleteDropTarget, "Basket", e.TouchDevice);
            }

            e.Handled = true;
        }

        private void StartDragFromDetail(object sender, TouchEventArgs e)
        {
            e.Handled = true;
            FrameworkElement source = sender as FrameworkElement;
            DataGridRow row = TreeHelper.TryFindParent<DataGridRow>(source);
            if (row != null && row.DataContext is Availability)
            {
                //Generate BasketViewModel
                Debug.Assert(source != null, "source != null");
                Availability availability = (Availability)source.DataContext;

                DataGridRow parentRow = TreeHelper.TryFindParent<DataGridRow>(row);
                MediumViewModel mvm = parentRow.DataContext as MediumViewModel;

                BasketViewModel basketitem = GenerateBasketViewModel(mvm, availability);

                DoDragEvent(basketitem, InsertDropTarget, "Overview", e.TouchDevice);
            }

            e.Handled = true;
        }

        private bool _isDragInProgress;

        private void DoDragEvent(BasketViewModel basketitem, Border dropTarget, string message, TouchDevice device)
        {
            // While multi-drag is not yet implemented, only allow one drag at a time
            if (_isDragInProgress)
            {
                DisplayNotification(ResourcesLocator.GetString("NotificationMultiDrag"), Notification.Info);
                return;
            }

            if (Basket.Contains(basketitem))
                DisplayNotification(ResourcesLocator.GetString("NotificationAlreadyInBasket"), Notification.Info);

            _isDragInProgress = true;

            //Set DropTarget as visible
            dropTarget.Visibility = Visibility.Visible;

            //Enable DragPreview
            Point pos = device.GetTouchPoint(this).Position;
            Canvas.SetLeft(DragPreview, pos.X);
            Canvas.SetTop(DragPreview, pos.Y);
            DragPreview.Visibility = Visibility.Visible;
            DragPreview.DataContext = basketitem;

            // Disable buttons so they aren't activated if drag goes over a button
            // TODO: bad solution...
            PrintButton.IsHitTestVisible = false;
            ClearButton.IsHitTestVisible = false;
            QrButton.IsHitTestVisible = false;

            //Begin drag and drop 
            DataObject dragData = new DataObject(message, basketitem);
            _dragDropHelper.StartDrag(device, dragData);
        }

        private void MoveDragPreview(object sender, DragDropEventArgs e)
        {
            Point pos = e.Position;
            Canvas.SetLeft(DragPreview, pos.X);
            Canvas.SetTop(DragPreview, pos.Y);
        }

        private void MoveDropIndicator(object sender, DragDropEventArgs e)
        {
            ScrollViewer basketScrollviewer = TreeHelper.TryFindChild<ScrollViewer>(BasketView);
            if (basketScrollviewer == null)
                return;

            basketScrollviewer.UpdateLayout();
            int dropPos = CalculateDropIndex(e);
            double offset = ((dropPos + 1) * BasketView.RowHeight) - basketScrollviewer.ContentVerticalOffset;

            DropIndexIndicator.Visibility = Visibility.Visible;
            Canvas.SetTop(DropIndexIndicator, offset);

            // Also scroll BasketView if position is on the edges
            Point dragPos = e.Position;
            if (dragPos.X < 0 || dragPos.X > BasketView.ActualWidth)
                return;

            if (dragPos.Y < 200)
                basketScrollviewer.ScrollToVerticalOffset(basketScrollviewer.ContentVerticalOffset - 25);
            else if (dragPos.Y > BasketView.ActualHeight - 100)
                basketScrollviewer.ScrollToVerticalOffset(basketScrollviewer.ContentVerticalOffset + 25);

        }

        private void DropTarget_DragEnter(object sender, DragDropEventArgs e)
        {
            Border droptarget = sender as Border;
            if (droptarget != null)
                droptarget.BorderThickness = new Thickness(15);
        }

        private void DropTarget_DragLeave(object sender, DragDropEventArgs e)
        {
            Border droptarget = sender as Border;
            if (droptarget != null)
                droptarget.BorderThickness = new Thickness(0);
        }

        #endregion

        #region Dropping
        private void CatchFromBasket(object sender, DragDropEventArgs e)
        {
            if (e.Handled)
                return;

            BasketViewModel medium = e.Data.GetData("Basket") as BasketViewModel;
            if (medium != null)
                Basket.Add(medium);

            e.Handled = true;
        }

        private void AddToBasket(object sender, DragDropEventArgs e)
        {
            if (e.Handled)
                return;

            BasketViewModel medium = e.Data.GetData("Overview") as BasketViewModel;
            if (medium != null)
            {
                if (medium.Model2.Signature == null)
                    DisplayNotification(ResourcesLocator.GetString("NoSignature"), Notification.Alert);
                else if (Basket.Contains(medium))
                    DisplayNotification(ResourcesLocator.GetString("NotificationAlreadyInBasket"), Notification.Info);
                else
                {
                    Basket.Add(medium);
                    medium.InitiateLocation(Dispatcher.CurrentDispatcher);
                    DisplayNotification(ResourcesLocator.GetString("NotificationAddedToBasket"), Notification.Success);
                }
            }

            e.Handled = true;
        }


        private void InsertInBasket(object sender, DragDropEventArgs e)
        {
            if (e.Handled)
                return;

            // If splitview is active, data can come from either basket or overview
            BasketViewModel medium = e.Data.GetData("Basket") as BasketViewModel ??
                                     e.Data.GetData("Overview") as BasketViewModel;

            if (medium != null)
            {
                if (Basket.Contains(medium))
                    DisplayNotification(ResourcesLocator.GetString("NotificationAlreadyInBasket"), Notification.Info);
                else
                {
                    // Insert at calculated position
                    int basketPos = CalculateDropIndex(e);
                    Basket.Insert(basketPos, medium);
                    medium.InitiateLocation(Dispatcher.CurrentDispatcher);
                }
            }

            e.Handled = true;
        }

        private void RemoveFromBasket(object sender, DragDropEventArgs e)
        {
            if (e.Handled)
                return;

            e.Handled = true;
        }

        #endregion
        #endregion

        #region ClickEvents

        private void OpenQRImage_Click(object sender, RoutedEventArgs e)
        {
            OverlayDetailImage.OpenQrCode(Basket);
        }

        private void DeleteBasket_Click(object sender, RoutedEventArgs e)
        {
            bool result = ShowCustomDialog(ResourcesLocator.GetString("DeleteBasketConfirmation"));

            if (result)
                Basket.Clear();
        }

        private void PrintBasket_Click(object sender, RoutedEventArgs e)
        {
            DisplayNotification(ResourcesLocator.GetString("PrintingStartNotification"), Notification.Info);

            // Same hack as in hypergrid: PrintBasket blocks UI from redrawing, so wait 50ms to display notification
            // and then print in the dispatcherthread again.
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += (o, args) =>
                {
                    System.Threading.Thread.Sleep(50); // Wait for UI to draw

                    Dispatcher.Invoke(() =>
                    {
                        try
                        {
                            BasketService.PrintBasket(Basket);
                            DisplayNotification(ResourcesLocator.GetString("PrintingSuccessNotification"),
                                                Notification.Success);
                        }
                        catch (Win32Exception)
                        {
                            DisplayNotification(ResourcesLocator.GetString("GhostScriptNotFoundError"), Notification.Alert);
                        }
                        catch (Exception)
                        {
                            DisplayNotification(ResourcesLocator.GetString("ErrorMessage"), Notification.Alert);
                        }
                    });
                };

            bw.RunWorkerAsync();
        }

        private void OpenDetailImage(object sender, RoutedEventArgs e)
        {
            Image senderImage = sender as Image;
            string description = String.Empty;

            // try to get description
            if (senderImage.DataContext is MediumViewModel)
                description = (senderImage.DataContext as MediumViewModel).Model2.Title;

            // open detail image
            OverlayDetailImage.OpenDetailImage(sender as Image, this, description);

            // ignore any other events
            e.Handled = true;
        }

        private void CloseDetailImage(object sender, TouchEventArgs e)
        {
            if (e.Handled)
                return;

            OverlayDetailImage.CloseDetailImage(this);
            e.Handled = true;
        }

        private bool ShowCustomDialog(string text)
        {
            DarkOverlay.Visibility = Visibility.Visible;

            DialogWindow dw = new DialogWindow { DialogText = text , Owner = this };
            dw.ShowDialog();

            DarkOverlay.Visibility = Visibility.Collapsed;

            return dw.Result;
        }

        #endregion

        #region Swiping
        private bool _inSplitView;
        private bool _isSwiping;
        private int _currSwipeDir;
        private int _currSwipeId;
        private Point _swipeStartPoint;
        private readonly Stopwatch _swipeStopWatch;

        private enum SwipeDir
        {
            Split, Reset, Left, Right
        }

        private void SwipeStart(object sender, MouseEventArgs e)
        {
            SwipeStart(e.GetPosition(this));
        }

        private void SwipeStart(object sender, TouchEventArgs e)
        {
            if (_isSwiping)
                return;

            SwipeStart(e.GetTouchPoint(this).Position);
            e.TouchDevice.Deactivated += TouchDeviceOnDeactivated;
            _currSwipeId = e.TouchDevice.Id;
        }

        private void TouchDeviceOnDeactivated(object sender, EventArgs eventArgs)
        {
            TouchDevice device = sender as TouchDevice;
            if (device == null)
                return;

            SwipeEnd(device.GetTouchPoint(this).Position);
            device.Deactivated -= TouchDeviceOnDeactivated;
        }

        private void SwipeStart(Point p)
        {
            _isSwiping = true;
            _swipeStartPoint = p;
            _swipeStopWatch.Reset();
            _swipeStopWatch.Start();

            if (_inSplitView)
                DoSwipeGesture(SwipeDir.Reset, 100);
        }

        private void SwipeMove(object sender, TouchEventArgs e)
        {
            if (e.TouchDevice.Id != _currSwipeId)
                return;

            SwipeMove(e.GetTouchPoint(this).Position);
        }

        private void SwipeMove(Point p)
        {
            if (!_isSwiping)
                return;

            if (_currSwipeDir == 0)
                _currSwipeDir = Math.Sign(p.X - _swipeStartPoint.X);

            if (EnableSplitView)
            {
                double velocity = Math.Abs(p.X - _swipeStartPoint.X) / _swipeStopWatch.ElapsedMilliseconds;
                double percent = p.X / ActualWidth;

                if (_swipeStopWatch.ElapsedMilliseconds > 200 && velocity < 1.2
                    && percent > 0.4 && percent < 0.6)
                {
                    SplitViewIndicator.Visibility = Visibility.Visible;
                }
                else
                {
                    SplitViewIndicator.Visibility = Visibility.Collapsed;
                }
            }

            double offset = ActualWidth - p.X;
            TranslateTransform dataTranslate = DataContainer.RenderTransform as TranslateTransform;
            TranslateTransform basketTranslate = BasketContainer.RenderTransform as TranslateTransform;

            Debug.Assert(dataTranslate != null, "dataTranslate != null");
            Debug.Assert(basketTranslate != null, "basketTranslate != null");

            dataTranslate.X = -offset;
            basketTranslate.X = -offset;
        }

        private void SwipeEnd(object sender, MouseEventArgs e)
        {
            SwipeEnd(e.GetPosition(this));
        }

        private void SwipeEnd(object sender, TouchEventArgs e)
        {
            if (e.TouchDevice.Id != _currSwipeId)
                return;

            SwipeEnd(e.GetTouchPoint(this).Position);
        }

        private void SwipeEnd(Point p)
        {
            if (!_isSwiping)
                return;

            _isSwiping = false;
            _swipeStopWatch.Stop();

            long elapsedTime = _swipeStopWatch.ElapsedMilliseconds;
            double distance = Math.Abs(p.X - _swipeStartPoint.X);
            double velocity = distance / elapsedTime;

            SwipeDir dir = (_currSwipeDir < 0 ? SwipeDir.Left : SwipeDir.Right);

            double percent = p.X / ActualWidth;
            const double animationTime = 250;

            if (velocity > 1.2)
            {
                DoSwipeGesture(dir, Math.Min((1 / velocity * 5000), 400));
            }
            else if (elapsedTime > 200)
            {
                //Determine on Position which view should be active
                if (percent < 0.4)
                    DoSwipeGesture(SwipeDir.Left, animationTime);
                else if (percent > 0.6 || !(EnableSplitView))
                    DoSwipeGesture(SwipeDir.Right, animationTime);
                else if (EnableSplitView)
                    DoSwipeGesture(SwipeDir.Split, animationTime);
            }
            else //if elapsedtime <= 200, interpret it as a simple click
            {
                if (_inSplitView)
                {
                    DoSwipeGesture(percent < 0.5 ? SwipeDir.Left : SwipeDir.Right, animationTime);
                }
                else
                {
                    if (percent < 0.3)
                        DoSwipeGesture(SwipeDir.Right, animationTime);
                    else if (percent > 0.7)
                        DoSwipeGesture(SwipeDir.Left, animationTime);
                }
            }

            //Reset everything
            _currSwipeDir = 0;
            SplitViewIndicator.Visibility = Visibility.Collapsed;
        }

        private void DoSwipeGesture(SwipeDir direction, double animationTime)
        {
            //Create custom animations for each View
            Duration swipeDuration = new Duration(TimeSpan.FromMilliseconds(animationTime));
            TranslateTransform dataTranslate = DataContainer.RenderTransform as TranslateTransform;
            TranslateTransform basketTranslate = BasketContainer.RenderTransform as TranslateTransform;
            double arrowWidth = SwipeArrow.Width;

            Debug.Assert(dataTranslate != null, "dataTranslate != null");
            Debug.Assert(basketTranslate != null, "basketTranslate != null");

            DoubleAnimation gridAnimation = new DoubleAnimation
                {
                    From = dataTranslate.X,
                    Duration = swipeDuration,
                    FillBehavior = FillBehavior.Stop
                };

            switch (direction)
            {
                case SwipeDir.Split:
                    gridAnimation.From = 0;
                    gridAnimation.To = 0;
                    DataContainer.Margin = new Thickness(0, 0, ActualWidth / 2 - 20, 0);
                    BasketContainer.Margin = new Thickness(ActualWidth / 2, 0, 0, 0);
                    DataGridView.Margin = new Thickness(0, 0, -ActualWidth / 2, 0);
                    BasketView.Margin = new Thickness(0, 0, -ActualWidth / 2, 0);
                    //if (arrowWidth >= 70) //failsafe test, swiping sometimes ignores resetting the width?
                    //    arrowWidth = arrowWidth / 4;
                    _inSplitView = true;
                    break;
                case SwipeDir.Reset:
                    gridAnimation.From = -ActualWidth / 2;
                    gridAnimation.To = -ActualWidth / 2;
                    DataContainer.Margin = new Thickness(0);
                    BasketContainer.Margin = new Thickness(ActualWidth, 0, -ActualWidth, 0);
                    DataGridView.Margin = new Thickness(0, 0, 0, 0);
                    BasketView.Margin = new Thickness(0, 0, 0, 0);
                    //if (arrowWidth < 70) //failsafe test, swiping sometimes ignores resetting the width?
                    //    arrowWidth = arrowWidth * 4;
                    //else if (arrowWidth > 70)
                    //    arrowWidth = arrowWidth / 4;
                    _inSplitView = false;
                    break;
                case SwipeDir.Left:
                    gridAnimation.To = -ActualWidth;
                    //if (arrowWidth < 20) //failsafe test, swiping sometimes ignores resetting the width?
                    //    arrowWidth *= 4;
                    break;
                case SwipeDir.Right:
                    gridAnimation.To = 0;
                    //if (arrowWidth < 20) //failsafe test, swiping sometimes ignores resetting the width?
                    //    arrowWidth *= 4;
                    break;
                default:
                    throw new NotImplementedException();
                //break;
            }

            //DoubleAnimation arrowAnimation = new DoubleAnimation
            //{
            //    From = SwipeArrow.Width,
            //    To = arrowWidth,
            //    Duration = swipeDuration,
            //    FillBehavior = FillBehavior.Stop
            //};
            //Debug.Assert(arrowAnimation.To != null, "arrowAnimation.To != null");

            dataTranslate.BeginAnimation(TranslateTransform.XProperty, gridAnimation);
            basketTranslate.BeginAnimation(TranslateTransform.XProperty, gridAnimation);
            //SwipeArrow.BeginAnimation(WidthProperty, arrowAnimation);

            dataTranslate.X = gridAnimation.To.Value;
            basketTranslate.X = gridAnimation.To.Value;
            //SwipeArrow.Width = arrowAnimation.To.Value;
        }

        #endregion

        #region Notifications

        private void DisplayNotification(String text, Notification notification)
        {
            Brush color;

            switch (notification)
            {
                case Notification.Info:
                    color = Brushes.DeepSkyBlue;
                    break;
                case Notification.Success:
                    color = Brushes.LimeGreen;
                    break;
                case Notification.Alert:
                    color = Brushes.IndianRed;
                    break;
                default:
                    color = Brushes.White;
                    break;
            }

            UserNotificationTextBlock.Text = text;
            UserNotification.Background = color;

            DoubleAnimation da = new DoubleAnimation(5, 0, TimeSpan.FromSeconds(3.5))
                {
                    EasingFunction = new QuadraticEase()
                };

            UserNotification.Opacity = 1;
            da.Completed += (sender, args) => UserNotification.Opacity = 0;

            UserNotification.BeginAnimation(OpacityProperty, null);
            UserNotification.BeginAnimation(OpacityProperty, da);
        }

        private enum Notification
        {
            Info, Success, Alert
        }

        #endregion

        #region Eventhandling
        //Explanation:
        //Somehow, if BasketView (a VHyperGrid) is set to Collapsed during initialisation,
        //the filterboxes have only half the height they're supposed to have.
        //To fix this quickly, the view is Visible during initialisation and
        //just the margin is adjusted, which is also needed for dragging the views
        //TODO: Fix this in HyperGrid?
        private void AdjustMargin(object sender, SizeChangedEventArgs e)
        {
            if (Math.Abs(BasketContainer.Margin.Left - e.PreviousSize.Width) < 1) //DataGridView is active
                BasketContainer.Margin = new Thickness(ActualWidth, 0, -ActualWidth, 0);
            else if (_inSplitView) //splitview is activ
            {
                DataContainer.Margin = new Thickness(0, 0, ActualWidth / 2, 0);
                BasketContainer.Margin = new Thickness(ActualWidth / 2, 0, 0, 0);
            }
        }

        private void DataGridView_OnRowZoomInto_1(object sender, EventArgs e)
        {
            OnRowZoom(sender, 200, TimeSpan.FromMilliseconds(DataGridView.ZoomInDuration),
                textblock =>
                {
                    // TODO: set these directly to original layout? 
                    textblock.FontWeight = FontWeights.Bold;
                    textblock.FontSize = 32;
                    textblock.LineHeight = 42;

                    // Prevent loading event from decreasing fontsize
                    textblock.Tag = "loaded";
                });
        }

        private void DataGridView_OnRowZoomOut_1(object sender, EventArgs e)
        {
            OnRowZoom(sender, 50, TimeSpan.FromMilliseconds(DataGridView.ZoomOutDuration),
                textblock =>
                {
                    // "Reenable" loading event for textblock
                    textblock.Tag = null;
                    TitleTextBlock_Loaded(textblock, new RoutedEventArgs());
                });
        }

        private void OnRowZoom(object sender, double animateTo, TimeSpan duration, Action<TextBlock> setFont)
        {
            IEnumerable<DataGridCell> cellspresenter = sender as IEnumerable<DataGridCell>;
            Debug.Assert(cellspresenter != null, "cellspresenter != null");
            foreach (var item in cellspresenter)
            {
                StackPanel panel = TreeHelper.TryFindChild<StackPanel>(item);
                if (panel != null)
                {
                    // Animate Imagewidth of cover
                    Image image = panel.Children[1] as Image;

                    Debug.Assert(image != null, "image != null");
                    DoubleAnimation da = new DoubleAnimation(image.Width, animateTo, duration)
                        {
                            FillBehavior = FillBehavior.Stop
                        };

                    image.BeginAnimation(WidthProperty, null);
                    image.BeginAnimation(WidthProperty, da);
                    image.Width = animateTo;

                    // (Re)Set font
                    panel = panel.Children[2] as StackPanel;
                    if (panel != null)
                    {
                        TextBlock text = TreeHelper.TryFindChild<TextBlock>(panel.Children[0]);
                        setFont(text);
                    }
                    // Don't look in any other cell
                    return;
                }
            }
        }

        private void TitleTextBlock_Loaded(object sender, RoutedEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb.Tag == null)
                DecreaseFont(tb, 40);
        }

        private void BasketTextBlock_Loaded(object sender, RoutedEventArgs e)
        {
            DecreaseFont(sender as TextBlock, BasketView.RowHeight - 40);
        }

        private void DecreaseFont(TextBlock tb, double desiredMaxHeight)
        {

            while (tb.ActualHeight > desiredMaxHeight && tb.FontSize > 18)
            {
                if (tb.FontSize < 20)
                {
                    tb.VerticalAlignment = VerticalAlignment.Top;
                    tb.FontWeight = FontWeights.ExtraLight;
                }
                if (tb.FontSize < 22)
                {
                    tb.FontWeight = FontWeights.Light;
                }
                else if (tb.FontSize < 24)
                {
                    tb.FontWeight = FontWeights.Normal;
                }

                tb.LineHeight -= 2;
                tb.FontSize--;
                tb.UpdateLayout();
            }
        }

        #endregion
    }
}
