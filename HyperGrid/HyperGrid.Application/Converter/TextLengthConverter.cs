﻿using System;
using System.Windows.Data;

namespace BlendedLibrary.HyperGrid.Converter
{
    class TextLengthConverter : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length == 1)
                return (double)values[0] - 30;
            else if (values.Length == 0)
                return 0;

            double cellwidth = (double)values[0];
            double imagewidth = (double) values[1];

            return Math.Max(cellwidth - imagewidth - 175, 0);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
