﻿using System;
using System.Collections;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using BlendedLibrary.HyperGrid.DataService.Domain;
using BlendedLibrary.HyperGrid.ViewModel;

namespace BlendedLibrary.HyperGrid.Converter
{
    class DragImageConverter : IMultiValueConverter
    {
        private static BitmapImage dragIcon = new BitmapImage(new Uri("/HyperGrid;component/Resources/DragIcon.png", UriKind.Relative));
        private static BitmapImage basketIcon = new BitmapImage(new Uri("/HyperGrid;component/Resources/Basket.png", UriKind.Relative));
        private static BitmapImage noAvailabilityIcon = new BitmapImage(new Uri("/HyperGrid;component/Resources/NoAvailabilityIcon.png", UriKind.Relative));
        private static BitmapImage searchIcon = new BitmapImage(new Uri("/HyperGrid;component/Resources/SearchIcon.png", UriKind.Relative));

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            IList basket = values[1] as IList;

            // TODO hack
            var fetching = values[2];
            if (fetching == null) return noAvailabilityIcon;

            bool isFetchingAvailabilities = (bool)values[2];
            IList availabilities = values[3] as IList;

            if (isFetchingAvailabilities)
                return searchIcon;

            if (availabilities.Count == 0)
                return noAvailabilityIcon;

            foreach (var item in basket)
            {
                BasketViewModel basketitem = item as BasketViewModel;

                foreach(var availItem in availabilities)
                {
                    Availability availability = availItem as Availability;

                    if (basketitem.Model2.Signature.Equals(availability.CallNumber))
                        return basketIcon;
                }
            }

            return dragIcon;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
