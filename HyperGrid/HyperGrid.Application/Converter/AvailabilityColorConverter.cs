﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace BlendedLibrary.HyperGrid.Converter
{
    internal class AvailabilityColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string status = value as string;
            if (status == null)
                return Brushes.Black;

            //I.e. "Verfügbar (1 Woche)"
            if (status.StartsWith("Verfügbar"))
                return Brushes.Green;

            switch (status)
            {
                case "Verfügbar":
                    return Brushes.Green;
                case "Nicht Verfügbar":
                case "Ausgeliehen":
                    return Brushes.Red;
                case "in Bearbeitung":
                case "Apparat":
                case "laufende Magazinbestellung":
                    return Brushes.Orange;
                default:
                    return Brushes.Black;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
