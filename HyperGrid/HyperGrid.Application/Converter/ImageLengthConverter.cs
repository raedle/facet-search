﻿using System;
using System.Windows.Data;

namespace BlendedLibrary.HyperGrid.Converter
{
    class ImageLengthConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double rowHeight = (double)value;

            if (rowHeight > 80)
                return 200;
            else
                return 50;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
