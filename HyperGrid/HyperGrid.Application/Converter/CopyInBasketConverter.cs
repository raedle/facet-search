﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using BlendedLibrary.HyperGrid.DataService.Domain;
using BlendedLibrary.HyperGrid.ViewModel;

namespace BlendedLibrary.HyperGrid.Converter
{
    class IsCopyInBasketVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Count() < 3)
                return Visibility.Collapsed;

            ObservableCollection<BasketViewModel> basket = values[1] as ObservableCollection<BasketViewModel>;
            Availability availability = values[2] as Availability;

            if (basket == null || availability == null)
                return Visibility.Collapsed;

            foreach(var item in basket)
            {
                if (item.Model2 != null && item.Model2.Signature.Equals(availability.CallNumber))
                    return Visibility.Visible;
            }

            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class IsCopyInBasketColorConverter : IMultiValueConverter
    {
        private static IsCopyInBasketVisibilityConverter converter = new IsCopyInBasketVisibilityConverter();

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility vis = (Visibility)converter.Convert(values, targetType, parameter, culture);
            if(vis.Equals(Visibility.Visible))
                return new SolidColorBrush(Colors.LightSkyBlue);
            else
                return new SolidColorBrush(Colors.Transparent);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class IsCopyInBasketImageConverter : IMultiValueConverter
    {
        private static IsCopyInBasketVisibilityConverter converter = new IsCopyInBasketVisibilityConverter();
        private static BitmapImage dragIcon = new BitmapImage(new Uri("/HyperGrid;component/Resources/DragIcon.png", UriKind.Relative));
        private static BitmapImage basketIcon = new BitmapImage(new Uri("/HyperGrid;component/Resources/Basket.png", UriKind.Relative));

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility vis = (Visibility)converter.Convert(values, targetType, parameter, culture);
            if (vis.Equals(Visibility.Visible))
                return basketIcon;
            else
                return dragIcon;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
