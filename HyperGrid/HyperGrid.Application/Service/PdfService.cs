﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using BlendedLibrary.HyperGrid.Properties;
using BlendedLibrary.HyperGrid.Utils;
using BlendedLibrary.HyperGrid.ViewModel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Font = iTextSharp.text.Font;
using Image = iTextSharp.text.Image;

namespace BlendedLibrary.HyperGrid.Service
{
    public class PdfService : PdfPageEventHelper
    {
        private static readonly BaseFont _BaseFont = BaseFont.CreateFont(@"c:\windows\fonts\arial.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

        private static readonly Font TitleFont = new Font(_BaseFont, 15f);
        private static readonly Font DetailFont = new Font(_BaseFont, 11f);

        private int _pageCount = 1;
        private int _indexCount = 1;
        // to control OnEndPage event
        private bool _isIndexing;
        private bool _isOnlineDoc;

        private const float FooterHeight = 90f;
        private const float HeaderHeight = 100f;

        public void CreateOnlinePdf(IEnumerable<BasketViewModel> basket, string filename)
        {
            try
            {
                CreatePdf(basket, filename, true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void CreatePrintablePdf(IEnumerable<BasketViewModel> basket)
        {
            CreatePdf(basket, BasketServiceSettings.Default.PrintFilename, false);
        }

        private void CreatePdf(IEnumerable<BasketViewModel> basket, string filename, bool includeLocation)
        {
            var doc = new Document(PageSize.A4, 25, 25, HeaderHeight, FooterHeight);
            using (var fs = new FileStream(filename, FileMode.Create))
            {
                // Initialize writer
                var writer = PdfWriter.GetInstance(doc, fs);
                writer.PageEvent = this;
                // Enable full compression
                writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_5);
                writer.CompressionLevel = PdfStream.BEST_COMPRESSION;
                writer.SetFullCompression();

                doc.Open();

                _isIndexing = true;
                _isOnlineDoc = includeLocation;

                AddMetaData(doc);

                // Add content as list without location
                AddContent(doc, basket, false);
                // Add content as list with location image on new page
                if (includeLocation)
                {
                    doc.NewPage();
                    _indexCount = 1;
                    _pageCount = 1;
                    _isIndexing = false;
                    AddContent(doc, basket, true);
                }

                // End document
                doc.Close();
                writer.Close();
                fs.Close();
            }
        }


        private void AddContent(Document doc, IEnumerable<BasketViewModel> basket, bool includeLocation)
        {

            //Create and define table containing all entries
            PdfPTable table = new PdfPTable(3);
            table.DefaultCell.Border = 0;
            table.DefaultCell.PaddingBottom = 15;
            table.SetWidths(new [] { 1, 1, 12 });

            // Create top anchor for "back to index" anchors
            Anchor topAnchor = new Anchor {Name = "Index"};
            doc.Add(topAnchor);

            // Add each entry to table
            foreach (var item in basket)
            {
                Paragraph p = new Paragraph();
                p.Alignment = Element.ALIGN_JUSTIFIED | Element.ALIGN_TOP;

                // Add page number as index
                table.AddCell(new Phrase(_indexCount++.ToString(), TitleFont));

                // Add coverimage
                Image cover = Image.GetInstance(item.CoverUrl);
                table.AddCell(cover);

                // Title      
                p.Add(new Phrase(item.Model2.Title, TitleFont));
                p.Add("\n");

                // Authors
                p.Add(new Phrase(item.AuthorsAsList, DetailFont));
                p.Add("\n");
                // Location
                p.Add(new Phrase(ResourcesLocator.GetString("PdfLocation"), DetailFont));
                p.Add(new Phrase(": ", DetailFont));
                p.Add(new Phrase(item.Location, DetailFont));
                p.Add("\n");
                // Signature
                p.Add(new Phrase(ResourcesLocator.GetString("Signature"), DetailFont));
                p.Add(new Phrase(": ", DetailFont));
                p.Add(new Phrase(item.Model2.Signature, DetailFont));
                p.Add("\n");

                // Availability
                p.Add(new Phrase(ResourcesLocator.GetString("Status"), DetailFont));
                p.Add(new Phrase(": ", DetailFont));
                p.Add(new Phrase(item.Model2.BestStatus, DetailFont));


                

                // Setup link to / anchor of locationimage 
                Anchor link = new Anchor(p);
                if (!includeLocation)
                    link.Reference = "#" + item.Model2.Title.Replace(" ", "") + item.Model2.Signature.Replace(" ", "");
                else
                    link.Name = item.Model2.Title.Replace(" ", "") + item.Model2.Signature.Replace(" ", "");

                table.AddCell(link);
                table.CompleteRow();

                //Add location image
                if (includeLocation)
                {
                    PdfPCell cell;
                    // If the image hasn't been loaded yet, wait 2 secs (method is running in thread)
                    if (item.LocationImage == null)
                        Thread.Sleep(2000);
                    // If the image still hasn't been loaded, then either the server is down or there is no locationimage
                    if (item.LocationImage != null)
                    {
                        try
                        {
                            Image location = Image.GetInstance(item.LocationUrl);
                            location.ScalePercent(40.0f);
                            
                            cell = new PdfPCell(location) { Colspan = 3, BorderWidth = 0 };
                        }
                        catch (Exception)
                        {
                            p = new Paragraph { Alignment = Element.ALIGN_CENTER };
                            p.Add(new Phrase(ResourcesLocator.GetString("LocationImageNotFound")));

                            cell = new PdfPCell(p) { Colspan = 3 };
                        }
                    }
                    else // if there's no location image, inform the user via text
                    {
                        p = new Paragraph {Alignment = Element.ALIGN_CENTER};
                        p.Add(new Phrase(ResourcesLocator.GetString("LocationImageNotFound")));

                        cell = new PdfPCell(p) {Colspan = 3};
                    }

                    // Complete table
                    table.AddCell(cell);
                    table.CompleteRow();

                    // Force a new page
                    doc.Add(table);

                    table = new PdfPTable(3);
                    table.DefaultCell.Border = 0;
                    table.DefaultCell.PaddingBottom = 15;
                    table.SetWidths(new[] { 1, 1, 12 });

                    doc.NewPage();
                }
            }

            doc.Add(table);
        }

        private void WriteText(PdfContentByte cb, string text, int x, int y, BaseFont font, int size)
        {
            cb.SetFontAndSize(font, size);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, text, x, y, 0);
        }

        private void AddMetaData(Document doc)
        {
            doc.AddAuthor(ResourcesLocator.GetString("PdfAuthor"));
            doc.AddSubject(ResourcesLocator.GetString("PdfSubject"));
            doc.AddTitle(ResourcesLocator.GetString("PdfTitle"));

            doc.AddCreationDate();
        }

        //See: http://kuujinbo.info/cs/itext_img_hdr.aspx
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            PdfContentByte cb = writer.DirectContent;

            // Add Header
            // Headerimage
            Image png = Image.GetInstance(ResourcesLocator.GetString("PdfLogoLink"));
            png.ScaleAbsolute(160, 52);
            png.SetAbsolutePosition(400, 764);
            cb.AddImage(png);

            //Headertext
            cb.BeginText();
            WriteText(cb, ResourcesLocator.GetString("PdfAuthor"), 40, 776, _BaseFont, 10);
            WriteText(cb, DateTime.Now.ToString("yyyy-MM-dd"), 40, 764, _BaseFont, 10);
            cb.EndText();

            // Separate the header from the rows with a line
            cb.SetLineWidth(0f);
            cb.MoveTo(40, 760);
            cb.LineTo(560, 760);
            cb.Stroke();

            // Add Footer
            // Write out details
            cb.BeginText();
            cb.SetFontAndSize(_BaseFont, 8);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, ResourcesLocator.GetString("PdfLibraryWebsiteLink"), 50, 45, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, ResourcesLocator.GetString("PdfHciWebsiteLink"), 50, 29, 0);
            // Add pagenumber
            cb.SetFontAndSize(_BaseFont, 12);
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, _isIndexing ? ToRoman(_pageCount) : _pageCount.ToString(), 300, 29, 0);
            _pageCount++;
            cb.EndText();

            // Stamp a line above the page footer
            cb.SetLineWidth(0f);
            cb.MoveTo(30, FooterHeight);
            cb.LineTo(570, FooterHeight);
            cb.Stroke();

            // Add QR image if doc should be printed
            Image qr = Image.GetInstance(BasketServiceSettings.Default.QrCodeFilename);
            qr.ScaleAbsolute(FooterHeight, FooterHeight);
            qr.SetAbsolutePosition(495, 10);
            cb.AddImage(qr);
            
        }

        // See: http://stackoverflow.com/questions/7040289/converting-integers-to-roman-numerals
        public static string ToRoman(int number)
        {
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900); 
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            return " ";
        }
    }
}
