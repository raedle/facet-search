﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Controls;
using BlendedLibrary.HyperGrid.Properties;
using BlendedLibrary.HyperGrid.ViewModel;

namespace BlendedLibrary.HyperGrid.Service
{
    class BasketService
    {
        public static Object QrLock = new Object();

        private static Image _lastQrCode;

        public static Image CreateQrImage(IEnumerable<BasketViewModel> basket)
        {
            // only one thread allowed
            lock (QrLock)
            {

                // basket hasn't changed, reuse qr code
                if (_lastQrCode != null)
                    return _lastQrCode;

                // nothing to do
                if (!basket.Any())
                    return null;

                // create document
                string filename = GetFilename();
                var success = CreateAndUploadPdf(basket, filename);

                if (!success)
                    return null;

                // Create QR Image
                var s = BasketServiceSettings.Default;

                var qrCode = QRCodeService.GenerateQRCode(s.HttpHost + filename);
                _lastQrCode = qrCode;
                return qrCode;
            }
        }

        public static void OnBasketChanged()
        {
            _lastQrCode = null;
        }

        private static bool CreateAndUploadPdf(IEnumerable<BasketViewModel> basket, string filename)
        {
            try
            {
                // Create pdf
                var printer = new PdfService();
                printer.CreateOnlinePdf(basket, filename);

                // Try to shrink pdf - needs GhostScript installed (see printing)
                // If it fails still upload the (bigger) pdf
                try
                {
                    GSInterface.Compress(filename, filename + "s");

                    // Replace big file
                    File.Delete(filename);
                    File.Move(filename + "s", filename);
                }
                catch (Exception)
                {
                }

                //// Move pdf to server
                //var s = Settings.Default;
                //using (new NetworkConnection(s.ShareFolder, new NetworkCredential(s.ShareUser, s.SharePassword)))
                //{
                //    File.Move(filename, s.ShareFolder + filename);
                //}

                UploadFileToServer(filename);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        // Needs http://ghostscript.com/ 32bit version 9.07 installed!
        public static void PrintBasket(ObservableCollection<BasketViewModel> basket)
        {
            lock (QrLock)
            {
                // Ensure that online pdf (especially QR code) is up to date
                // Errors will be redirected to mainwindow - notification
                String filename = GetFilename();
                CreateAndUploadPdf(basket, filename);

                // Create pdf without location to print
                PdfService printer = new PdfService();
                printer.CreatePrintablePdf(basket);

                // Print
                var printerName = new System.Drawing.Printing.PrinterSettings().PrinterName;
                GSInterface.Print(BasketServiceSettings.Default.PrintFilename, printerName);
            }
        }

        private static string GetFilename()
        {
            return "Basket-" + DateTime.Now.ToString("yyyy-MM-dd-HH_mm_ss") + ".pdf"; //i.e. Basket-2013-02-14-16_23_22
        }

        /// <summary>
        /// Upload file to server using ftp protocol.
        /// </summary>
        /// <param name="filename"></param>
        private static void UploadFileToServer(string filename)
        {
            var s = BasketServiceSettings.Default;

            // Get the object used to communicate with the server.
            var request = (FtpWebRequest)WebRequest.Create(string.Format("{0}/{1}", s.FtpServer, filename));
            request.Method = WebRequestMethods.Ftp.UploadFile;

            // This example assumes the FTP site uses anonymous logon.
            request.Credentials = new NetworkCredential(s.FtpUsername, s.FtpPassword);

            using (var file = new FileStream(filename, FileMode.Open))
            {
                request.ContentLength = file.Length;

                using (var requestStream = request.GetRequestStream())
                {
                    var buf = new byte[1024];
                    int len;
                    while ((len = file.Read(buf, 0, buf.Length)) > 0)
                    {
                        requestStream.Write(buf, 0, len);
                    }
                }
            }

            var response = (FtpWebResponse)request.GetResponse();

            Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);

            response.Close();
        }
    }
}
