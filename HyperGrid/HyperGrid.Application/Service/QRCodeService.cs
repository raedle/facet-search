﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using BlendedLibrary.HyperGrid.Properties;
using MessagingToolkit.QRCode.Codec;

namespace BlendedLibrary.HyperGrid.Service
{
    public static class QRCodeService
    {
        private static QRCodeEncoder encoder;
   
        static QRCodeService()
        {
            encoder = new QRCodeEncoder();
            encoder.QRCodeVersion = 6;
        }

        public static Image GenerateQRCode(string path)
        {
            // needs to run in dispatcherthread
            Image qrCodeImage = null;
            
            App.Current.Dispatcher.Invoke(() =>
            {
                //Generate qrcode
                System.Drawing.Bitmap qrCode = encoder.Encode(path);
                qrCodeImage = BitmapToImage(qrCode);
                AddBorder(qrCodeImage);
                SaveToFile(qrCodeImage.Source as BitmapSource);
            });

            return qrCodeImage;
        }

        // See: http://stackoverflow.com/questions/5867657/copying-from-bitmapsource-to-writablebitmap
        private static void AddBorder(Image sourceImage) 
        {
            const int whiteBorder = 5;
            const int transparentBorder = 50;

            BitmapSource source = sourceImage.Source as BitmapSource;

            // Calculate stride of source
            int stride = source.PixelWidth * (source.Format.BitsPerPixel / 8);
            int whiteStride = (source.PixelWidth + 2 * whiteBorder + 2 * transparentBorder) * (source.Format.BitsPerPixel / 8);

            // Create data array to hold source pixel data
            byte[] data = new byte[stride * source.PixelHeight];
            byte[] white = new byte[whiteStride * (source.PixelHeight + 2 * whiteBorder + 2 * transparentBorder)];

            // Copy source image pixels to the data array
            source.CopyPixels(data, stride, 0);
            for (int i = 0; i < white.Length; i++)
                white[i] = byte.MaxValue;

            // Create WriteableBitmap to copy the pixel data to.      
            WriteableBitmap target = new WriteableBitmap(
              source.PixelWidth + 2 * whiteBorder + 2 * transparentBorder,
              source.PixelHeight + 2 * whiteBorder + 2 * transparentBorder,
              source.DpiX, source.DpiY,
              source.Format, null);

            // Paint bitmap white
            target.WritePixels(
                new Int32Rect(transparentBorder, transparentBorder, source.PixelWidth + 2 * whiteBorder, source.PixelHeight + 2 * whiteBorder),
                white, whiteStride, 0);

            // Write the image to the WriteableBitmap.
            target.WritePixels(
              new Int32Rect(whiteBorder + transparentBorder, whiteBorder + transparentBorder, source.PixelWidth, source.PixelHeight),
              data, stride, 0);

            // Re-set source 
            sourceImage.Source = target;
            
        }

        private static Image BitmapToImage(System.Drawing.Bitmap bitmap)
        {
            Image img = new Image();

            MemoryStream ms = new MemoryStream();
            BitmapImage bi = new BitmapImage();

            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            ms.Position = 0;

            bi.BeginInit();
            bi.StreamSource = ms;
            bi.EndInit();

            img.Source = bi;

            return img;
        }

        private static void SaveToFile(BitmapSource image)
        {
            try
            {
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(image));
                using (var stream = new FileStream(BasketServiceSettings.Default.QrCodeFilename, FileMode.Create))
                    encoder.Save(stream);
            }
            catch(Exception)
            {
                
            }
        }
    }
}
