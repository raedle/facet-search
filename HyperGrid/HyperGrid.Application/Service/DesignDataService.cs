﻿using System;

namespace BlendedLibrary.HyperGrid.Service
{
    public class DesignDataService : BaseDataService
    {
        #region events

        public override event EventHandler Connected;
        public override event EventHandler PerformSearch;
        public override event EventHandler SearchFinished;
        public override event EventHandler<ItemsChangedEventArgs> ItemsChanged;
        public override event EventHandler<EventArgs> ErrorOccured;

        #endregion

        #region public methods
        
        public override void Connect()
        {
            // ignore
        }

        #endregion
    }
}
