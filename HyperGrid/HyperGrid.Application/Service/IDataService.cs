﻿using System;

namespace BlendedLibrary.HyperGrid.Service
{
    public interface IDataService
    {
        #region events

        event EventHandler Connected;
        event EventHandler PerformSearch;
        event EventHandler SearchFinished;
        event EventHandler<ItemsChangedEventArgs> ItemsChanged;
        event EventHandler<EventArgs> ErrorOccured;

        #endregion

        #region properties

        bool IsConnected { get; set; }

        #endregion

        #region public methods

        void Connect();

        #endregion
    }
}
