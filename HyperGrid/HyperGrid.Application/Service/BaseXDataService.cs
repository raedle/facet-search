﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using BlendedLibrary.Common.Util;
using BlendedLibrary.Data;
using BlendedLibrary.HyperGrid.DataService.Domain;
using BlendedLibrary.HyperGrid.Properties;
using BlendedLibrary.HyperGrid.ViewModel;
using NLog;
using Tools.BaseX;
using DispatcherHelper = GalaSoft.MvvmLight.Threading.DispatcherHelper;

namespace BlendedLibrary.HyperGrid.Service
{
    public class BaseXDataService : BaseDataService
    {
        #region private fields

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private bool _isConnecting;
        private readonly TaskScheduler _uiTaskScheduler;

        #endregion

        #region events

        public override event EventHandler Connected;
        public override event EventHandler PerformSearch;
        public override event EventHandler SearchFinished;
        public override event EventHandler<ItemsChangedEventArgs> ItemsChanged;
        public override event EventHandler<EventArgs> ErrorOccured;

        #endregion

        #region private fields

        private BaseXClient _client;

        private CancellationTokenSource _queryTaskCancellationTokenSource;

        #endregion

        #region ctor

        public BaseXDataService()
        {
            _uiTaskScheduler = TaskScheduler.FromCurrentSynchronizationContext();
        }

        #endregion

        #region public methods

        public override void Connect()
        {
            ConnectToService();
        }

        #endregion

        /// <summary>
        /// Connect to data service.
        /// </summary>
        private void ConnectToService()
        {
            if (Logger.IsInfoEnabled)
                Logger.Info("Connecting to data service.");

            _isConnecting = true;

            var task = Task.Factory.StartNew(DoConnect, TaskCreationOptions.LongRunning);

            task.ContinueWith(t =>
                              {
                                  IsConnected = true;

                                  if (Connected != null)
                                      Connected(this, new EventArgs());

                              }, _uiTaskScheduler);
        }

        private void DoConnect()
        {
            var s = DataServiceSettings.Default;

            var success = false;
            while (!success)
            {
                try
                {
                    _client = InitializeClient();

                    _client.ErrorOccored += (sender, args) =>
                    {
                        // do not connect if already trying to connect.
                        if (_isConnecting) return;

                        if (ErrorOccured != null)
                            ErrorOccured(this, args);

                        // do reconnect to service on error/failure -> this might be caused by restart of Facet Search application
                        ConnectToService();
                    };

                    _isConnecting = false;
                    success = true;
                    break;
                }
                catch (BaseXException e)
                {
                    if (Logger.IsWarnEnabled)
                        Logger.Warn("Could not connect to server. Trying to connect again in {0} ms.", s.ReconnectTimeout);

                    Thread.Sleep(s.ReconnectTimeout);
                }
            }

            if (Logger.IsInfoEnabled)
                Logger.Info("Add watches for incoming data service events [{0},{1},{2}]", EventHelper.Event.ShowResults, EventHelper.Event.RemoveResults, EventHelper.Event.ControlMessage);

            // Observe triggered events with s.DatabaseEventName only
            var observableAdd = Observable.FromEventPattern<BaseXEventArgs>(_client, "EventTriggered");
            observableAdd.Where(evt => evt.EventArgs.EventName.Equals(EventHelper.Event.ShowResults))
                      .Select(evt => evt.EventArgs)
                      .Subscribe(OnShowResults);

            var observableRemove = Observable.FromEventPattern<BaseXEventArgs>(_client, "EventTriggered");
            observableRemove.Where(evt => evt.EventArgs.EventName.Equals(EventHelper.Event.RemoveResults))
                      .Select(evt => evt.EventArgs)
                      .Subscribe(OnRemoveResults);

            var observableControlMessage = Observable.FromEventPattern<BaseXEventArgs>(_client, "EventTriggered");
            observableControlMessage.Where(evt => evt.EventArgs.EventName.Equals(EventHelper.Event.ControlMessage))
                      .Select(evt => evt.EventArgs)
                      .Subscribe(OnControlMessage);

            _client.TriggerEvent(EventHelper.Event.ControlMessage, string.Format("\"{0}\"", EventHelper.Message.HyperGridConnected), EventHelper.DummyQuery);
        }

        /// <summary>
        /// Initializes data service client.
        /// </summary>
        /// <returns></returns>
        private BaseXClient InitializeClient()
        {
            var s = DataServiceSettings.Default;

            var host = s.Host;
            var port = s.Port;
            var username = s.Username;
            var password = s.Password;
            var name = s.Name;

            // Initialize connection to BaseX server
            var client = new BaseXClient(host, port, username, password, name)
                .UseForAdminLogin(username, password);
            client.SerializerError += (sender, e) =>
                                          {
                                              if (ErrorOccured != null)
                                                  ErrorOccured(this, e);
                                          };

            CreateIfEventNotExistAndWatchEvent(client, name, EventHelper.Event.ShowResults);
            CreateIfEventNotExistAndWatchEvent(client, name, EventHelper.Event.RemoveResults);
            CreateIfEventNotExistAndWatchEvent(client, name, EventHelper.Event.ControlMessage);

            return client;
        }

        /// <summary>
        /// Create data service events if not already existing.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="databaseName"></param>
        /// <param name="eventName"></param>
        private static void CreateIfEventNotExistAndWatchEvent(BaseXClient client, string databaseName, string eventName)
        {
            var result = client.ShowEvents();

            if (!result.Contains(eventName))
            {
                client.CreateEvent(eventName);
            }
            client.WatchEvent(eventName, databaseName);
        }

        /// <summary>
        /// Shows incoming results in hyper grid.
        /// </summary>
        /// <param name="evt"></param>
        private void OnShowResults(BaseXEventArgs evt)
        {
            // Invoke events on UI thread.
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
                                                  {
                                                      if (PerformSearch != null)
                                                          PerformSearch(this, new EventArgs());
                                                  });

            //Data and Result have been interchanged in basex, but not in the connector -> Data is in Result field
            var payload = evt.Data;
            var count = int.Parse(evt.Results.Split(':')[1]);

            Debug.WriteLine("XmlResult: " + payload);

            if (_queryTaskCancellationTokenSource != null)
                _queryTaskCancellationTokenSource.Cancel();

            _queryTaskCancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = _queryTaskCancellationTokenSource.Token;

            var collection = new ObservableCollection<MediumViewModel>();

            // make collection modifyable for UI thread.
            var collectionLock = new object();
            BindingOperations.EnableCollectionSynchronization(collection, collectionLock);

            // do not query when result count is 0
            if (count == 0)
            {
                // Invoke events on UI thread.
                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    if (ItemsChanged != null)
                        ItemsChanged(this, new ItemsChangedEventArgs(collection));
                });
                return;
            }

            var s = DataServiceSettings.Default;

            // Calculate max page size. Remove page if module result is zero.
            var pages = (s.MaxResults/s.ResultChunkSize);
            if (s.MaxResults%s.ResultChunkSize == 0)
                --pages;

            FetchResults(payload, cancellationToken, collection, s.ResultChunkSize, 0, pages);
        }

        private void FetchResults(string query, CancellationToken cancellationToken, ObservableCollection<MediumViewModel> collection, int pageSize = 50, int fromPage = 0, int toPage = 0)
        {
            var queryTask = Task.Factory.StartNew(() => _client.QueryIndex(query, pageSize, fromPage), cancellationToken);

            queryTask.ContinueWith(t =>
                {
                    var xmlResult = t.Result;
                    var resultList = CreateMediumsFromXML(xmlResult);
                    if (!cancellationToken.IsCancellationRequested)
                    {
                        //since this updates the UI it must be cancallable if a new query arrives
                        AddResultsToItems(resultList, cancellationToken, collection);
                    }

                    // fetch results of next page if pages are left
                    if (fromPage < toPage)
                    {
                        FetchResults(query, cancellationToken, collection, pageSize, ++fromPage, toPage);
                    }
                    else
                    {
                        if (SearchFinished != null)
                            SearchFinished(this, EventArgs.Empty);
                    }
                },
                cancellationToken,
                TaskContinuationOptions.NotOnCanceled,
                TaskScheduler.Current);
        }

        private List<Medium2> CreateMediumsFromXML(string xmlResult)
        {

            var serializer = CreateXmlSerializer(typeof(Medium2));
            var settings = new XmlReaderSettings
            {
                ValidationFlags = XmlSchemaValidationFlags.None,
                ValidationType = ValidationType.None,
                ConformanceLevel = ConformanceLevel.Auto,
                IgnoreProcessingInstructions = true
            };

            var lengthStartTag = "<results>".Length;
            var lengthEndTag = "</results>".Length;

            xmlResult = xmlResult.Substring(lengthStartTag, xmlResult.Length - (lengthStartTag + lengthEndTag));

            var tokenizer = xmlResult.Split(new[] { "</medium>" }, StringSplitOptions.RemoveEmptyEntries);

            var i = 0;
            var resultList = new List<Medium2>();
            foreach (var token in tokenizer)
            {
                i++;

                var mediumRaw = token.Trim();
                if (string.IsNullOrWhiteSpace(mediumRaw)) continue;

                var res = string.Format("{0}</medium>", token.Trim());

                var reader = XmlReader.Create(new StringReader(res), settings);

                try
                {
                    var objectResult = serializer.Deserialize(reader) as Medium2;
                    Debug.WriteLine(res);
                    if (objectResult != null)
                    {
                        objectResult.IsFetchingAvailabilities = false;
                        resultList.Add(objectResult);
                    }
                }
                catch (Exception e)
                {
                    // ignore
                    Console.WriteLine("{0} [{1}]: {2}", i, e.Message, res);
                }
            }
            return resultList;
        }

        private void AddResultsToItems(IEnumerable<Medium2> results, CancellationToken cancellationToken, ObservableCollection<MediumViewModel> collection)
        {
            var first = true;
            foreach (var medium in results)
            {
                //stop the execution, if the task was cancelled
                if (cancellationToken.IsCancellationRequested)
                    break;

                collection.Add(new MediumViewModel { Model2 = medium });

                // only send out notification once - but add item first,
                // because else the application will display "no results found"
                if (first && ItemsChanged != null)
                {
                    first = false;

                    // Invoke events on UI thread.
                    DispatcherHelper.CheckBeginInvokeOnUI(() =>
                                                          {
                                                              if (ItemsChanged != null)
                                                                  ItemsChanged(this, new ItemsChangedEventArgs(collection));
                                                          });
                }
            }

            // no items in results - application can show "no results found"
            if (first && ItemsChanged != null && !cancellationToken.IsCancellationRequested)
            {
                // Invoke events on UI thread.
                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    if (ItemsChanged != null)
                        ItemsChanged(this, new ItemsChangedEventArgs(collection));
                });
            }
        }

        private void OnRemoveResults(BaseXEventArgs evt)
        {
            // Invoke events on UI thread.
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                if (ItemsChanged != null)
                    ItemsChanged(this, new ItemsChangedEventArgs(null));
            });

            // if hyper grid is started when facet search already has a serach query formulated, then the cancellation source is null
            if (_queryTaskCancellationTokenSource != null)
                _queryTaskCancellationTokenSource.Cancel();
        }

        private static void OnControlMessage(BaseXEventArgs e)
        {
            switch (e.EventName)
            {
                case EventHelper.Event.ControlMessage:
                    switch (e.Results)
                    {
                        case EventHelper.Message.KeepAlive:
                            if (Logger.IsDebugEnabled)
                                Logger.Debug("Received alive message.");
                            break;
                    }
                    break;
            }
        }

        protected virtual XmlSerializer CreateXmlSerializer(Type targetType)
        {
            return new XmlSerializer(targetType);
        }
    }
}
