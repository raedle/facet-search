﻿using System;
using System.Collections.ObjectModel;
using BlendedLibrary.HyperGrid.ViewModel;

namespace BlendedLibrary.HyperGrid.Service
{
    public class ItemsChangedEventArgs : EventArgs
    {
        #region Items

        public ObservableCollection<MediumViewModel> Items { get; private set; }

        #endregion

        public ItemsChangedEventArgs(ObservableCollection<MediumViewModel> items)
        {
            Items = items;
        }
    }
}
