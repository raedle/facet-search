﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlendedLibrary.HyperGrid.Properties;

namespace BlendedLibrary.HyperGrid.Service
{
    public abstract class BaseDataService : IDataService, INotifyPropertyChanged
    {
        private bool _isConnected;

        #region events

        public abstract event EventHandler Connected;
        public abstract event EventHandler PerformSearch;
        public abstract event EventHandler SearchFinished;
        public abstract event EventHandler<ItemsChangedEventArgs> ItemsChanged;
        public abstract event EventHandler<EventArgs> ErrorOccured;

        #endregion

        #region properties

        #region IsConnected

        public bool IsConnected
        {
            get { return _isConnected; }
            set
            {
                if (_isConnected == value)
                {
                    return;
                }

                _isConnected = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #endregion

        #region public methods

        public abstract void Connect();

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
