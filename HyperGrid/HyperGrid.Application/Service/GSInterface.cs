﻿using System.Diagnostics;
using BlendedLibrary.HyperGrid.Properties;

namespace BlendedLibrary.HyperGrid.Service
{
    // Needs http://ghostscript.com/ 32bit version installed!
    // See: http://stackoverflow.com/questions/5508763/c-sharp-prevent-adobe-reader-window-from-coming-up-when-trying-to-print-a-docume
    public static class GSInterface
    {
        private static readonly string GhostScriptExePath;

        static GSInterface()
        {
            GhostScriptExePath = BasketServiceSettings.Default.PathToGhostScriptExe;
        }

        public static void CallGhostScript(string[] args)
        {
            var p = new Process
                {
                    StartInfo =
                        {
                            FileName = GhostScriptExePath,
                            Arguments = string.Join(" ", args),
                            WindowStyle = ProcessWindowStyle.Hidden
                        }
                };

            p.Start();
            // Wait 20 seconds
            p.WaitForExit(20 * 1000);
        }


        public static void Print(string filename, string printerName)
        {
            CallGhostScript(new[] {
            "-q",
            "-sDEVICE=mswinpr2",
            "-sPAPERSIZE=a4",
            "-dNOPAUSE",
            "-dNoCancel",
            "-dBATCH",
            "-dDuplex",
            string.Format(@"-sOutputFile=""\\spool\{0}""", printerName),
            string.Format(@"""{0}""", filename)
        });
        }


        public static void Compress(string input, string output)
        {
            const string threshold = "2.0";
            const string sampleType = "Bicubic";
            const string dpi = "300";

            CallGhostScript(new[]
                {
                    "-sDEVICE=pdfwrite",
                    string.Format("-dColorImageDownsampleThreshold={0}", threshold),
                    string.Format("-dGrayImageDownsampleThreshold={0}", threshold),
                    string.Format("-dMonoImageDownsampleThreshold={0}", threshold),
                    string.Format("-dColorImageDownsampleType=/{0}", sampleType),
                    string.Format("-dGrayImageDownsampleType=/{0}", sampleType),
                    string.Format("-dMonoImageDownsampleType=/{0}", sampleType),
                    "-dDownsampleColorImages=true",
                    "-dDownsampleGrayImages=false",
                    "-dDownsampleMonoImages=false",
                    string.Format("-dColorImageResolution={0}", dpi),
                    string.Format("-dGrayImageResolution={0}", dpi),
                    string.Format("-dMonoImageResolution={0}", dpi),
                    "-dAutoFilterColorImages=false",
                    "-dAutoFilterGrayImages=false",
                    "-dEncodeColorImages=true",
                    "-dEncodeGrayImages=true",
                    "-dEncodeMonoImages=true",
                    "-dColorImageFilter=/DCTEncode",
                    "-dGrayImageFilter=/DCTEncode",
                    "-dMonoImageFilter=/CCITTFaxEncode",
                    "-dColorConversionStrategy=/LeaveColorUnchanged",
                    string.Format(@"-o ""{0}""", output),
                    string.Format(@"""{0}""", input)
                });
        }
    }
}
