﻿using System.Windows;

namespace BlendedLibrary.HyperGrid.Controls
{
    /// <summary>
    /// Interaction logic for DialogWindow.xaml
    /// </summary>
    public partial class DialogWindow
    {
        #region Dependency Properties
        public string DialogText
        {
            get { return (string)GetValue(DialogTextProperty); }
            set { SetValue(DialogTextProperty, value); }
        }

        public static readonly DependencyProperty DialogTextProperty =
            DependencyProperty.Register("DialogText", typeof(string), typeof(DialogWindow), 
            new PropertyMetadata("Ein Fehler ist aufgetreten."));



        public string CheckBoxText
        {
            get { return (string)GetValue(CheckBoxTextProperty); }
            set { SetValue(CheckBoxTextProperty, value); }
        }

        public static readonly DependencyProperty CheckBoxTextProperty =
            DependencyProperty.Register("CheckBoxText", typeof(string), typeof(DialogWindow));



        public bool IsCheckBoxVisible
        {
            get { return (bool)GetValue(IsCheckBoxVisibleProperty); }
            set { SetValue(IsCheckBoxVisibleProperty, value); }
        }

        public static readonly DependencyProperty IsCheckBoxVisibleProperty =
            DependencyProperty.Register("IsCheckBoxVisible", typeof(bool), typeof(DialogWindow), new PropertyMetadata(false));




        public bool IsCheckBoxToggled
        {
            get { return (bool)GetValue(CheckBoxToggledProperty); }
            set { SetValue(CheckBoxToggledProperty, value); }
        }

        public static readonly DependencyProperty CheckBoxToggledProperty =
            DependencyProperty.Register("CheckBoxToggled", typeof(bool), typeof(DialogWindow), new PropertyMetadata(false));

        #endregion

        public bool Result;

        public DialogWindow()
        {
            InitializeComponent();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            Result = false;
            this.Close();
        }

        private void ConfirmButtonClick(object sender, RoutedEventArgs e)
        {
            Result = true;
            this.Close();
        }
    }
}
