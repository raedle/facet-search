﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using BlendedLibrary.HyperGrid.Service;
using BlendedLibrary.HyperGrid.Utils;
using BlendedLibrary.HyperGrid.ViewModel;

namespace BlendedLibrary.HyperGrid.Controls
{
    /// <summary>
    /// Interaction logic for DetailImage.xaml
    /// </summary>
    public partial class DetailImage 
    {
        public static readonly DependencyProperty ImageSourceProperty = DependencyProperty.Register(
            "ImageSource", typeof (ImageSource), typeof (DetailImage), new PropertyMetadata(null));

        public ImageSource ImageSource
        {
            get { return (ImageSource) GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }


        public static readonly DependencyProperty DescriptionProperty = DependencyProperty.Register(
            "Description", typeof (string), typeof (DetailImage), new PropertyMetadata(default(string)));

        public string Description
        {
            get { return (string) GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        private Image _clickedImage;
        // indicates if the control is in detailview or hidden, to prevent multiple animations
        private bool isHidden = true;

        public DetailImage()
        {
            InitializeComponent();

            // hack to get the whole control registered in the visual tree
            Loaded += (sender, args) =>
            {
                Visibility = Visibility.Visible;
                Dispatcher.InvokeAsync(() => Visibility = Visibility.Collapsed, DispatcherPriority.Background);
            };
        }

        public void CloseDetailImage(Window parentWindow)
        {
            if (isHidden)
                return;
            isHidden = true;

            // stop all previous animations
            SizeTransform.BeginAnimation(ScaleTransform.ScaleYProperty, null);
            SizeTransform.BeginAnimation(ScaleTransform.ScaleXProperty, null);
            SizeTransform.BeginAnimation(ScaleTransform.CenterXProperty, null);
            SizeTransform.BeginAnimation(ScaleTransform.CenterYProperty, null);

            PositionTransform.BeginAnimation(TranslateTransform.XProperty, null);
            PositionTransform.BeginAnimation(TranslateTransform.YProperty, null);
            BackgroundBrush.BeginAnimation(Brush.OpacityProperty, null);

            UpdateLayout();

            if (_clickedImage != null)
            {
                // calculate positions of elements on screen
                Point absolutePosSource = _clickedImage.TransformToAncestor(parentWindow).Transform(new Point(0, 0));
                Point absolutePosDetail = MainImage.TransformToAncestor(parentWindow).Transform(new Point(0, 0));

                // start animations
                DoubleAnimation heightAnimation = CreateDoubleAnimation(1, _clickedImage.ActualHeight/MainImage.ActualHeight);
                DoubleAnimation widthAnimation = CreateDoubleAnimation(1, _clickedImage.ActualWidth/MainImage.ActualWidth);

                DoubleAnimation translateXAnimation = CreateDoubleAnimation(0, absolutePosSource.X - absolutePosDetail.X);
                DoubleAnimation translateYAnimation = CreateDoubleAnimation(0, absolutePosSource.Y - absolutePosDetail.Y);
                SizeTransform.BeginAnimation(ScaleTransform.ScaleYProperty, heightAnimation);
                SizeTransform.BeginAnimation(ScaleTransform.ScaleXProperty, widthAnimation);
                SizeTransform.BeginAnimation(ScaleTransform.CenterXProperty, translateXAnimation);
                SizeTransform.BeginAnimation(ScaleTransform.CenterYProperty, translateYAnimation);

                PositionTransform.BeginAnimation(TranslateTransform.XProperty, translateXAnimation);
                PositionTransform.BeginAnimation(TranslateTransform.YProperty, translateYAnimation);
            }


            // fade out background
            DoubleAnimation opacityAnimation = CreateDoubleAnimation(BackgroundBrush.Opacity, 0);

            opacityAnimation.Completed += (sender, args) =>
            {
                if (_clickedImage != null)
                    _clickedImage.Visibility = Visibility.Visible;
                Reset();
            };

            BackgroundBrush.BeginAnimation(Brush.OpacityProperty, opacityAnimation);
        }

        private void Reset()
        {
            Visibility = Visibility.Collapsed;

            ImageSource = null;
            Description = String.Empty;

            LoadingTextBlock.Visibility = Visibility.Collapsed;

            _clickedImage = null;
        }

        public void OpenDetailImage(Image img, Window parentWindow, string description)
        {
            if (!isHidden)
                return;
            isHidden = false;

            // save clicked image for closing
            _clickedImage = img;

            // set dependencyproperties
            ImageSource = img.Source;
            Description = description;

            // only one image should be visible
            Visibility = Visibility.Visible;
            img.Visibility = Visibility.Hidden;

            // get new actualheight
            UpdateLayout();

            // calculate positions of elements on screen
            Point absolutePosSource = img.TransformToAncestor(parentWindow).Transform(new Point(0, 0));
            Point absolutePosDetail = MainImage.TransformToAncestor(parentWindow).Transform(new Point(0, 0));

            // start animations
            DoubleAnimation heightAnimation = CreateDoubleAnimation(img.ActualHeight / MainImage.ActualHeight, 1);
            DoubleAnimation widthAnimation = CreateDoubleAnimation(img.ActualWidth/MainImage.ActualWidth, 1);

            DoubleAnimation translateXAnimation = CreateDoubleAnimation(absolutePosSource.X - absolutePosDetail.X, 0);
            DoubleAnimation translateYAnimation = CreateDoubleAnimation(absolutePosSource.Y - absolutePosDetail.Y, 0);
            DoubleAnimation opacityAnimation = CreateDoubleAnimation(0, BackgroundBrush.Opacity);

            SizeTransform.BeginAnimation(ScaleTransform.ScaleYProperty, heightAnimation);
            SizeTransform.BeginAnimation(ScaleTransform.ScaleXProperty, widthAnimation);
            SizeTransform.BeginAnimation(ScaleTransform.CenterXProperty, translateXAnimation);
            SizeTransform.BeginAnimation(ScaleTransform.CenterYProperty, translateYAnimation);

            PositionTransform.BeginAnimation(TranslateTransform.XProperty, translateXAnimation);
            PositionTransform.BeginAnimation(TranslateTransform.YProperty, translateYAnimation);
            BackgroundBrush.BeginAnimation(Brush.OpacityProperty, opacityAnimation);
        }

        private DoubleAnimation CreateDoubleAnimation(double from, double to)
        {
            return new DoubleAnimation
            {
                From = from,
                To = to,
                Duration = TimeSpan.FromMilliseconds(250),
                FillBehavior = FillBehavior.Stop
            }; 
        }

        public void OpenQrCode(IEnumerable<BasketViewModel> basket)
        {
            if (!isHidden)
                return;
            isHidden = false;

            // show loading message
            Visibility = Visibility.Visible;
            LoadingTextBlock.Visibility = Visibility.Visible;

            // create threadsafe basket
            List<BasketViewModel> threadItems = new List<BasketViewModel>();
            threadItems.AddRange(basket);

            // start creating document in a thread
            Task.Run(() =>
            {
                var qrCodeImage = BasketService.CreateQrImage(basket);

                Dispatcher.Invoke(() =>
                {
                    // check if another image was clicked in the meantime
                    if (LoadingTextBlock.Visibility != Visibility.Visible)
                        return;

                    if (qrCodeImage != null)
                    {
                        ImageSource = qrCodeImage.Source;
                        Description = ResourcesLocator.GetString("QRCodeDescription");
                    }
                    else // error happened
                    {
                        Description = ResourcesLocator.GetString("ErrorOccured");
                    }

                    LoadingTextBlock.Visibility = Visibility.Collapsed;
                });
            });


            // dim background
            DoubleAnimation opacityAnimation = CreateDoubleAnimation(0, BackgroundBrush.Opacity);
            BackgroundBrush.BeginAnimation(Brush.OpacityProperty, opacityAnimation);
        }
    }
}
