﻿using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using BlendedLibrary.HyperGrid.DataService.Domain;
using GalaSoft.MvvmLight;

namespace BlendedLibrary.HyperGrid.ViewModel
{
    public class BasketViewModel : ViewModelBase
    {
        static BasketViewModel()
        {
            new Object();
        }

        public Medium2 Model2
        {
            get;
            set;
        }

        public string CoverUrl
        {
            get;
            set;
        }

        public int Room
        {
            get;
            set;
        }

        public string AuthorsAsList
        {
            get;
            set;
        }


        public BitmapImage LocationImage
        {
            get;
            set;
        }

        public string LocationUrl
        {
            get;
            set;
        }

        public string Location
        {
            get;
            set;
        }

        public void InitiateLocation(Dispatcher dispatcher)
        {
            Thread t = new Thread(() =>
                {
                    if (Model2 == null || LocationImage != null)
                        return;

                    string signatur = Model2.Signature.Replace(" ", "%20");
                    string group = signatur.Substring(0, 3); //TODO: verify that group is always only 3 character long
                    string room = GetRoomFromHtml("http://libero.ub.uni-konstanz.de:8080/bibmap/BIBMAP_Server?signatur=" + signatur + "&GRUPPE=" + @group + "&LANG=de");
                
                    try
                    {
                        Room = Convert.ToInt32(room);
                    }
                    catch
                    {
                        Room = 0;
                        Console.WriteLine(@"Error while converting roomnumber");
                    }

                    dispatcher.BeginInvoke(new Action(() =>
                        {
                            LocationUrl = @"http://libero.ub.uni-konstanz.de:8080/bibmap/BIBMAP_Server?room=" + room + ".png&signatur=" + signatur + "&GRUPPE=" + @group + "&LANG=de";
                            LocationImage = new BitmapImage(new Uri(LocationUrl));
                            RaisePropertyChanged("LocationImage");
                        }));
                });

            t.Start();

        }

        //TODO: Get roomdata from other source than digging through html content
        private static string GetRoomFromHtml(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Timeout = 2000;
            string room = ""; 

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK ||
                    response.StatusCode == HttpStatusCode.Moved ||
                    response.StatusCode == HttpStatusCode.Redirect)
                {
                    using (Stream inputStream = response.GetResponseStream())
                    {
                        if (inputStream != null)
                        {
                            StreamReader reader = new StreamReader(inputStream);
                            string htmlsource = reader.ReadToEnd();
                            string occurence = Regex.Match(htmlsource, @"<img usemap=" + "\"" + @"#\d+").Value;
                            room = Regex.Match(occurence, @"\d+").Value;
                        }
                    }
                }
            }
            catch (WebException e) //Something is offline?
            { }

            return room;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            BasketViewModel vm = obj as BasketViewModel;
            if (vm == null)
                return false;
            if (vm.Model2 == null && Model2 == null)
                return true;
            if (vm.Model2 == null || Model2 == null || vm.Model2.Signature == null || Model2.Signature == null)
                return false;
            
            return (vm.Model2.Title.Equals(Model2.Title) && vm.Model2.Signature.Equals(Model2.Signature));
        }
    }
}
