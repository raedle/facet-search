using System;
using System.Collections.ObjectModel;
using System.Deployment.Application;
using System.Windows.Input;
using System.Windows.Threading;
using BlendedLibrary.Common.Control;
using BlendedLibrary.HyperGrid.Properties;
using BlendedLibrary.HyperGrid.Service;
using BlendedLibrary.HyperGrid.Utils;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using NLog;
using Tools.BaseX;

namespace BlendedLibrary.HyperGrid.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        #region private fields

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region commands

        public RelayCommand<MediumViewModel> ItemSelectedCommand { get; private set; }

        public RelayCommand IgnoreItemSelectedCommand { get; private set; }

        public RelayCommand CloseDetailItemInformationCommand { get; private set; }

        public RelayCommand<KeyEventArgs> ShortcutCommand { get; private set; }

        #endregion

        #region private fields

        private readonly IDataService _dataService;

        private bool _ignoreItemSelected;

        #endregion

        #region properties

        #region ApplicationVersion

        /// <summary>
        /// The <see cref="ApplicationVersion" /> property's name.
        /// </summary>
        public const string ApplicationVersionPropertyName = "ApplicationVersion";

        private string _applicationVersion = string.Empty;

        /// <summary>
        /// Sets and gets the ApplicationVersion property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ApplicationVersion
        {
            get
            {
                return _applicationVersion;
            }

            set
            {
                if (_applicationVersion == value)
                {
                    return;
                }

                _applicationVersion = value;
                RaisePropertyChanged(ApplicationVersionPropertyName);
            }
        }

        #endregion

        #region Items

        /// <summary>
        /// The <see cref="Items" /> property's name.
        /// </summary>
        public const string ItemsPropertyName = "Items";

        private ObservableCollection<MediumViewModel> _items;

        /// <summary>
        /// Sets and gets the Items property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<MediumViewModel> Items
        {
            get
            {
                return _items;
            }

            set
            {
                if (_items == value)
                {
                    return;
                }

                _items = value;
                RaisePropertyChanged(ItemsPropertyName);
            }
        }

        #endregion

        #region IsFetchingResults

        /// <summary>
        /// The <see cref="IsFetchingResults" /> property's name.
        /// </summary>
        public const string IsFetchingResultsPropertyName = "IsFetchingResults";

        private bool _isFetchingResults = false;

        /// <summary>
        /// Sets and gets the IsFetchingResults property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsFetchingResults
        {
            get
            {
                return _isFetchingResults;
            }

            set
            {
                if (_isFetchingResults == value)
                {
                    return;
                }

                _isFetchingResults = value;
                RaisePropertyChanged(IsFetchingResultsPropertyName);
            }
        }

        #endregion

        #region SelectedItem

        /// <summary>
        /// The <see cref="SelectedItem" /> property's name.
        /// </summary>
        public const string SelectedItemPropertyName = "SelectedItem";

        private MediumViewModel _selectedItem;

        /// <summary>
        /// Sets and gets the SelectedItem property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public MediumViewModel SelectedItem
        {
            get
            {
                return _selectedItem;
            }

            set
            {
                if (_selectedItem == value)
                {
                    return;
                }

                _selectedItem = value;
                RaisePropertyChanged(SelectedItemPropertyName);
            }
        }

        #endregion

        #region IsDetailItemInformationVisible

        /// <summary>
        /// The <see cref="IsDetailItemInformationVisible" /> property's name.
        /// </summary>
        public const string IsDetailItemInformationVisiblePropertyName = "IsDetailItemInformationVisible";

        private bool _isDetailItemInformationVisible;

        /// <summary>
        /// Sets and gets the IsDetailItemInformationVisible property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsDetailItemInformationVisible
        {
            get
            {
                return _isDetailItemInformationVisible;
            }

            set
            {
                if (_isDetailItemInformationVisible == value)
                {
                    return;
                }

                _isDetailItemInformationVisible = value;
                RaisePropertyChanged(IsDetailItemInformationVisiblePropertyName);
            }
        }

        #endregion

        #region UserFeedback

        /// <summary>
        /// The <see cref="UserFeedback" /> property's name.
        /// </summary>
        public const string UserFeedbackPropertyName = "UserFeedback";

        private string _userFeedback;

        /// <summary>
        /// Sets and gets the UserFeedback property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string UserFeedback
        {
            get
            {
                return _userFeedback;
            }

            set
            {
                if (_userFeedback == value)
                {
                    return;
                }

                _userFeedback = value;
                RaisePropertyChanged(UserFeedbackPropertyName);
            }
        }

        #endregion
        
        #endregion

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService)
        {
            // initialize commands
            ItemSelectedCommand = new RelayCommand<MediumViewModel>(OnItemSelected);
            IgnoreItemSelectedCommand = new RelayCommand(OnIgnoreItemSelected);
            CloseDetailItemInformationCommand = new RelayCommand(OnCloseDetailItemInformation);
            ShortcutCommand = new RelayCommand<KeyEventArgs>(OnShortcut);

            _dataService = dataService;

            _dataService.Connected += OnDataServiceConnected;
            _dataService.PerformSearch += OnDataServicePerformSearch;
            _dataService.SearchFinished += OnDataServiceSearchFinished;
            _dataService.ItemsChanged += OnDataServiceItemsChanged;
            _dataService.ErrorOccured += OnDataServiceErrorOccured;

            UserFeedback = string.Format("{0}", ResourcesLocator.GetString("Connecting"));

            _dataService.Connect();

            if (ApplicationDeployment.IsNetworkDeployed)
                ApplicationVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
        }

        private void OnShortcut(KeyEventArgs e)
        {
            if (e.IsRepeat) return;

            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                switch (e.Key)
                {
                    case Key.S:
                        ShowSettingsDialog();
                        break;
                }
            }
        }

        private void OnIgnoreItemSelected()
        {
            _ignoreItemSelected = true;
        }

        private void OnItemSelected(MediumViewModel item)
        {
            if (_ignoreItemSelected)
            {
                _ignoreItemSelected = false;
                return;
            }

            SelectedItem = item;

            if (item != null)
                IsDetailItemInformationVisible = true;
        }

        private void OnCloseDetailItemInformation()
        {
            IsDetailItemInformationVisible = false;
        }

        private void OnDataServiceConnected(object sender, EventArgs e)
        {
            UserFeedback = null;
        }

        void OnDataServicePerformSearch(object sender, EventArgs e)
        {
            IsFetchingResults = true;
            UserFeedback = ResourcesLocator.GetString("SearchInProgress");
        }

        void OnDataServiceSearchFinished(object sender, EventArgs e)
        {
            IsFetchingResults = false;
        }

        public event EventHandler ItemsChanged;

        void OnDataServiceItemsChanged(object sender, ItemsChangedEventArgs e)
        {
            // fire event
            if (ItemsChanged != null)
                ItemsChanged(this, new EventArgs());

            Items = e.Items;

            UserFeedback = Items != null && Items.Count == 0 ? ResourcesLocator.GetString("NoItemsFound") : null;
        }

        private void OnDataServiceErrorOccured(object sender, EventArgs e)
        {
            var args = e as BaseXSerializerEventArgs;
            if (args != null)
            {
                if (Logger.IsErrorEnabled)
                    Logger.Error(args.Exception);

                UserFeedback = string.Format("{0}", ResourcesLocator.GetString("ErrorOccured"));
            }

            var baseXEventArgs = e as BaseXErrorEventArgs;
            if (baseXEventArgs != null)
            {
                if (Logger.IsErrorEnabled)
                    Logger.Error(baseXEventArgs.Exception);

                UserFeedback = string.Format("{0}", ResourcesLocator.GetString("ErrorOccuredReconnect"));
            }

            IsFetchingResults = false;
        }

        #region dialogs

        private void ShowSettingsDialog()
        {
            // Present configuration dialog to user
            var dialog = new SettingsDialog
            {
                Objects = new ObservableCollection<SettingsDialogItem>
                          {
                              new SettingsDialogItem("Settings", Settings.Default),
                              new SettingsDialogItem("Data Service Settings", DataServiceSettings.Default),
                              new SettingsDialogItem("Basket Service Settings", BasketServiceSettings.Default),
                              new SettingsDialogItem("Window Settings", WindowSettings.Default)
                          }
            };
            dialog.ShowDialog();
        }

        #endregion
    }
}