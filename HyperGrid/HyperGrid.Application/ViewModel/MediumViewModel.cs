﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using BlendedLibrary.HyperGrid.DataService.Domain;
using BlendedLibrary.HyperGrid.DataService.Service;
using GalaSoft.MvvmLight;

namespace BlendedLibrary.HyperGrid.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MediumViewModel : ViewModelBase
    {
        #region properties

        #region Model2

        /// <summary>
        /// The <see cref="Model2" /> property's name.
        /// </summary>
        public const string Model2PropertyName = "Model2";

        private Medium2 _model2;

        /// <summary>
        /// Sets and gets the Model2 property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Medium2 Model2
        {
            get
            {
                return _model2;
            }

            set
            {
                if (_model2 != null)
                    _model2.PropertyChanged += Model2PropertyChanged;

                if (_model2 == value)
                {
                    return;
                }

                _model2 = value;
                RaisePropertyChanged(Model2PropertyName);

                if (_model2 != null)
                    _model2.PropertyChanged += Model2PropertyChanged;
            }
        }

        void Model2PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if ("ImageUrl".Equals(e.PropertyName))
                RaisePropertyChanged("CoverUrl");
        }

        #endregion

        public string AuthorsAsList
        {
            get
            {
                return ListToString(Model2.Authors);
            }
        }

        public string KeywordsAsList
        {
            get
            {
                return ListToString(Model2.Keywords);
            }
        }

        private string ListToString(List<string> list)
        {
            if (list == null || list.Count < 1)
                return "";

            var sb = new StringBuilder();
            foreach (var entry in list)
            {
                sb.AppendFormat("{0}; ", entry);
            }
            return sb.ToString();
        }

        public DateTime Date
        {
            get
            {
                try
                {
                    return new DateTime(Model2.Year, 1, 1);
                }
                catch (Exception)
                {
                    return new DateTime(1948, 1, 1);
                }
            }
        }

        #region IsResolvingCover

        /// <summary>
        /// The <see cref="IsResolvingCover" /> property's name.
        /// </summary>
        public const string IsResolvingCoverPropertyName = "IsResolvingCover";

        private bool _isResolvingCover;

        /// <summary>
        /// Sets and gets the IsResolvingCover property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsResolvingCover
        {
            get
            {
                return _isResolvingCover;
            }

            set
            {
                if (_isResolvingCover == value)
                {
                    return;
                }

                _isResolvingCover = value;
                RaisePropertyChanged(IsResolvingCoverPropertyName);
            }
        }

        #endregion

        #region CoverUrl

        public string CoverUrl
        {
            get
            {
                if (Model2.ImageUrl == null && !IsResolvingCover)
                {
                    IsResolvingCover = true;
                    CoverService.Instance.Resolve(Model2);
                }

                return Model2.ImageUrl;
            }

            set
            {
                Model2.ImageUrl = value;
                RaisePropertyChanged("CoverUrl");
            }
        }

        #endregion

        #endregion
    }
}