﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Threading;
using BlendedLibrary.HyperGrid.Properties;
using GalaSoft.MvvmLight.Threading;
using NLog;

namespace BlendedLibrary.HyperGrid
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region private fields

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        public App()
        {
            DispatcherHelper.Initialize();

            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.GetCultureInfo(Settings.Default.Language);
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.GetCultureInfo(Settings.Default.Language);

            Startup += OnStartup;
            DispatcherUnhandledException += OnDispatcherUnhandledException;

            AppDomain.CurrentDomain.FirstChanceException += (sender, args) => { Console.WriteLine(args.Exception); };
            AppDomain.CurrentDomain.UnhandledException += (sender, args) => { Console.WriteLine(args.ExceptionObject); };
        }

        static void OnStartup(object sender, StartupEventArgs e)
        {
            DispatcherHelper.Initialize();
        }

        void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            if (Logger.IsFatalEnabled)
                Logger.Fatal(e.Exception.Message, e.Exception);

            if (Settings.Default.ShowUnhandledDispatcherExceptionMessageBox)
                MessageBox.Show(e.Exception.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);

            e.Handled = true;
        }
    }
}
