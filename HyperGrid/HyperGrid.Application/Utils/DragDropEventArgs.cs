﻿using System.Windows;

namespace BlendedLibrary.HyperGrid.Utils
{
    class DragDropEventArgs : RoutedEventArgs
    {
        private DragDropEventArgs(){}

        public IDataObject Data;
        public Point Position;

        public DragDropEventArgs(IDataObject data, Point point, RoutedEvent routedEvent)
        {
            Data = data;
            Position = point;
            RoutedEvent = routedEvent;
        }
    }
}
