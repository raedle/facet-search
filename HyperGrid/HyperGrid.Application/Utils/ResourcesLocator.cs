﻿using System;
using System.Resources;

namespace BlendedLibrary.HyperGrid.Utils
{
    internal class ResourcesLocator
    {
        private static readonly ResourceManager ResourceManager = new ResourceManager("BlendedLibrary.HyperGrid.Properties.Resources", typeof(ResourcesLocator).Assembly);

        public static string GetString(string name)
        {
            var message = ResourceManager.GetString(name);

            if (message != null)
                message = message.Replace("\\n", Environment.NewLine);

            return message;
        }
    }
}
