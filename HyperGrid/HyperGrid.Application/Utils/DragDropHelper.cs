﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace BlendedLibrary.HyperGrid.Utils
{
    class DragDropHelper
    {
        #region Attributes
        internal delegate void EventFunction(Object o, DragDropEventArgs e);

        private readonly UIElement _parent;
        private readonly Dictionary<TouchDevice, DataObject> _currentDragActions = new Dictionary<TouchDevice, DataObject>();
        private readonly Dictionary<TouchDevice, List<DependencyObject>> _previousHitResults = new Dictionary<TouchDevice, List<DependencyObject>>(); 

        private readonly Dictionary<DependencyObject, EventFunction> _dropHandler = new Dictionary<DependencyObject, EventFunction>();
        private readonly Dictionary<DependencyObject, EventFunction> _dragEnterHandler = new Dictionary<DependencyObject, EventFunction>();
        private readonly Dictionary<DependencyObject, EventFunction> _dragLeaveHandler = new Dictionary<DependencyObject, EventFunction>();
        private readonly Dictionary<DependencyObject, EventFunction> _dragOverHandler = new Dictionary<DependencyObject, EventFunction>();
        private readonly List<EventFunction> _dragEndHandler = new List<EventFunction>();
        #endregion

        #region Constructors

        private DragDropHelper() {}

        public DragDropHelper(UIElement parent)
        {
            _parent = parent;
            _parent.PreviewTouchMove += ParentOnPreviewTouchMove;
            _parent.PreviewTouchUp += ParentOnPreviewTouchUp;
        }

        #endregion

        #region Toucheventhandling / Event firing

        private List<DependencyObject> DoHitTest(Point at)
        {
            var hitResultsList = new List<DependencyObject>();

            VisualTreeHelper.HitTest(_parent, null,
                                     result =>
                                         {
                                            if(result.VisualHit is UIElement && ((UIElement)result.VisualHit).Visibility != Visibility.Collapsed)
                                                hitResultsList.Add(result.VisualHit);
                                            return HitTestResultBehavior.Continue;
                                         },
                new PointHitTestParameters(at));

            hitResultsList.Add(_parent);

            return hitResultsList;
        }

        private void ParentOnPreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (!_currentDragActions.ContainsKey(e.TouchDevice))
                return;

            var hitResults = DoHitTest(e.GetTouchPoint(_parent).Position);
            DragDropEventArgs eventArgs = new DragDropEventArgs(_currentDragActions[e.TouchDevice],
                                                                e.GetTouchPoint(_parent).Position, e.RoutedEvent);

            foreach (var result in hitResults.Where(result => _dropHandler.ContainsKey(result)))
            {
                _dropHandler[result].Invoke(result, eventArgs);
                e.Handled = true;
            }

            foreach (var function in _dragEndHandler)
            {
                function.Invoke(_parent, eventArgs);
                e.Handled = true;
            }

            _currentDragActions.Remove(e.TouchDevice);
        }

        private void ParentOnPreviewTouchMove(object sender, TouchEventArgs e)
        {
            if (!_currentDragActions.ContainsKey(e.TouchDevice))
                return;

            // Fetch hitresults, build eventargs
            var hitResults = DoHitTest(e.GetTouchPoint(_parent).Position);
            DragDropEventArgs eventArgs = new DragDropEventArgs(_currentDragActions[e.TouchDevice],
                                                                e.GetTouchPoint(_parent).Position, e.RoutedEvent);
            

            // Invoke dragover methods
            foreach (var result in hitResults.Where(result => _dragOverHandler.ContainsKey(result)))
            {
                _dragOverHandler[result].Invoke(result, eventArgs);
                e.Handled = true;
            }

            // Invoke dragenter / dragleave
            if (_previousHitResults.ContainsKey(e.TouchDevice))
            {
                var previousResults = _previousHitResults[e.TouchDevice];

                // If a result is not in hitResults anymore (but was previously) drag has left => DragLeave
                foreach (var prevResult in previousResults
                    .Where(o => !(hitResults.Contains(o)))
                    .Where(o => _dragLeaveHandler.ContainsKey(o)))
                {
                    _dragLeaveHandler[prevResult].Invoke(prevResult, eventArgs);
                }

                // If a result is not in prevHitresults, it must be new therefore DragEnter
                foreach (var result in hitResults.Where(result => !(previousResults.Contains(result)) && _dragEnterHandler.ContainsKey(result)))
                {
                    _dragEnterHandler[result].Invoke(result, eventArgs);
                }

                _previousHitResults.Remove(e.TouchDevice);
            }

            _previousHitResults.Add(e.TouchDevice, hitResults);
        }

        #endregion

        #region Handler Handling

        public void StartDrag(TouchDevice touchdevice, DataObject data)
        {
            _currentDragActions.Add(touchdevice, data);
        }

        public void AddDropHandler(UIElement element, EventFunction f)
        {
            _dropHandler.Add(element, f);
        }

        public void AddDragEnterHandler(UIElement element, EventFunction f)
        {
            _dragEnterHandler.Add(element, f);
        }

        public void AddDragLeaveHandler(UIElement element, EventFunction f)
        {
            _dragLeaveHandler.Add(element, f);
        }

        public void AddDragOverHandler(UIElement element, EventFunction f)
        {
            _dragOverHandler.Add(element, f);
        }

        public void AddDragEndHandler(EventFunction f)
        {
            _dragEndHandler.Add(f);
        }
        #endregion
    }
}
