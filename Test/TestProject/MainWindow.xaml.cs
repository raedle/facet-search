﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using BlendedLibrary.Common.Annotations;

namespace TestProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region properties

        #region Position

        /// <summary>
        /// The <see cref="AssociatedObject" /> property's name.
        /// </summary>
        public const string AssociatedObjectPropertyName = "AssociatedObject";

        private AssociatedObject _associatedObject = new AssociatedObject();

        /// <summary>
        /// Sets and gets the AssociatedObject property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public AssociatedObject AssociatedObject
        {
            get
            {
                return _associatedObject;
            }

            set
            {
                if (_associatedObject == value)
                {
                    return;
                }

                _associatedObject = value;
                RaisePropertyChanged(AssociatedObjectPropertyName);
            }
        }

        #endregion

        #endregion

        public MainWindow()
        {
            InitializeComponent();
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
