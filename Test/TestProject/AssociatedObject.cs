﻿using BlendedLibrary.Common.Model;

namespace TestProject
{
    public class AssociatedObject : IPosition
    {
        public Position TopLeft
        {
            get { return new Position(0, 0); }
        }

        public Position Center
        {
            get { return new Position(300, 300); }
        }

        public void UpdateTopLeftPosition(double x, double y)
        {
            
        }

        public void UpdateCenterPosition(double x, double y)
        {
            
        }
    }
}
